# README #
[TOC]

# EBI-MP #

Utilities for estimating distribution of taxon abundance distribution 

## Generating jar file ##

 To get jar file "emp_dev_scala_fat-assembly-${version}.jar", e.g. "emp_dev_scala_fat-assembly-1.0.jar", with all libraries

  * __sbt fat:assembly__

 To get jar file "emp_dev_scala-assembly-1.0.jar" that does not contain other libraries

  * __sbt slim:assembly__
  
Generate jar file with other libraries "emp_dev_scala-assembly-1.0-deps.jar" only

  * __sbt assemblyPackageDependency  __

### Typical configuration to  run  "fat" jar  file "emp_dev_scala_fat-assembly-1.0.jar" where TAD input file is "Data/Brazil.sample", number of iterations is 100,000, and coverage of species is 80%. 
```java  -cp   emp_dev_scala_fat-assembly-1.0.jar     ExperimentScalaRunner -in "Data/Brazil.sample"   -s 100000 -c 0.8 ```

### Typical configuration to  run  slim jar file "emp_dev_scala-assembly-1.0.jar" and dependency jar file "emp_dev_scala-assembly-1.0-deps.jar"
```java  -cp   emp_dev_scala-assembly-1.0.jar:emp_dev_scala-assembly-1.0-deps.jar     ExperimentScalaRunner -in "Data/Brazil.sample"   -s 100000 -c 0.8 ```



|Option|Required  |Default             |Description                                         |Other values            |
|:--------|:--------:|--------------------|:---------------------------------------------------|------------------------|
| --in    |    Y     |                    |input file name                                     |                        |
| --out   |          |input file directory|output directory name                               |                        |
|  -c or --coverage    |          |   0.9              |is decimal number, required coverage of species = D/S, D - number of species in the sample, S - total number of species|   |
|  -s     |          | 800,000            |integer generate  MCMC samples|                     |                        |
|  -d     |          |  lognormal         |type of taxon abundance distribution (TAD)  used to fit data to|             |
| --m0    |          |                    |initial value for variance parameter of TAD for MCMC|                        |
| --v0    |          |                    |initial value for variance parameter of TAD for MCMC|                        |  
| --s0    |          |                    |initial value for number of all species for MCMC    |                        |  



### Input file format ###

+ Taxon abundance distribution data - format recognized based on filename suffix "csv" or "sample"
    + CSV file of abundance with header on the first line, where abundance is first column and number of taxa is second column OR

    + or "sample" file of abundance without header, where on the first line is number of data points/rows followd by TAD data, where abundance is first column and number of taxa is second column





# Command line parameters #

``` 
         Usage: parser [-v] [-f file] [-s sopt] ...

						Where:

						Required parameters:

						-in input file name

						-out path for output file

						Optional:

						-s integer generate  MCMC samples

            -mod string with values: all, mu, sigma, S

            -version string with  values q for Quince's version ((storing ln of mean of un-logged value of  rv~LogNormal)) , s - for my version (storing mean of log value of rv~LogNormal)

            -c is decimal number, required coverage of species = D/S, D - number of species in the sample, S - total number of species

            -d distribution as string "lognormal", "inverseG", "sichel"

	```