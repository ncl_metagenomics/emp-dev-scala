# EMP SW development

[TOC]

## New input parameter
- -g, -\-generated -- marks whether this is real data or generated data values: yes - if real data, no - if synthetic data
	- optional  parameter
	- default value: synthetic







#  Syntetics sample preparation

## Generating synthetic community data from poilog TAD (Poisson-lognormal distribution)

### Running sofware
### Generated file names
- output data file
	- abundance distribution file  *__```44xSRS711891_Abundance_Community_Synthetic_100.0_pc_lognormal_161024-2234_41.csv```__*
		- components of name construction:
			- ```${datasetName}_${fileFormat}_${typeOfData}_${generated}_${Coverage of speciest}_pc_${distribution}_${DateTime}.csv```
		- parts:
			- ```${datasetName}``` -- possible value: 44xSRS711891
			- ```${fileFormat}``` -- Abundance or TAD
			- ```${typeOfData}``` -- Community or Sample
			+ `${generated}` -- with values Real or Synthetic
			- ```${Coverage of species}```-- fractional coverage of species, double values
			- ```${distribution}``` --  type of TAD distribution
			- ```${DateTime}``` --  date and time stamp


## Sampling from community data

* SamplingPoilog
	* ```resampleWithoutReplacement(abundanceIM: List[Long], coverage: Double): List[Long] ```

	## Reading command line parameters

* utils.**ReadingExperimentParameters**

	* ***Work in progress***  ``` getInputParams4SubsamplingCommunity(args: Array[String]): InputParameters4SubsamplingCommmunity```

       - ```getInputParams4MCMCAndSamplingEffort```

```java
import collection.JavaConverters._
import scala.collection.breakOut
val l: List[Double] = j.asScala.map(_.doubleValue)(breakOut)
```

#Diary
##161026

1. synthetic generation of community data with required parameters for lognormal
	2. json info file with parameters with name construction
		+ `info_${datasetName}_${fileFormat}_${typeOfData}_${generated}_${Coverage of speciest}_pc_${distribution}_${DateTime}.json`
	3. save genereated synghetic data into:
		+  ```${datasetName}_${fileFormat}_${typeOfData}_${generated}_${Coverage of speciest}_pc_${distribution}_${DateTime}.csv```


##161128

