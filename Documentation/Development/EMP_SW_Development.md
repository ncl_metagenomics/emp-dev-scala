

# EMP SW development

[TOC]



# Parameters for inverse Gauss distribution fitting.

## General Setings

### Old set

```sh
dmtcp_launch  java -Xmx1000M -XX:ParallelGCThreads=1 -XX:-UseParallelGC -cp  "${extJAR}:${mcJAR}" ExperimentScalaRunner --in "${tadFile}" -s ${nIter} -c 0.9 --paramASigma 0.1 --paramBSigma 0.1  --SSigma 400 --initMu ${initMu} --initialS ${initialS} --initVariance ${initVariance} --varCovarFile "${varCovarFile}" --varianceScalingFactor ${varianceScalingFactor}
```



### New set

"-n", "--nIterations" - number of iterations changed to "-n" (previously "-s")

```
--in "src/main/resources/Data/data_sampleSize_TAD_90_pc_D_8925.3_syntheticSampleSizes_lognormal_170209_0213-39_17-02-09_0219-42.sample" --thin 2 --nIterations 1000 -c 0.9 --paramASigma 0.1 --paramBSigma 0.1  --SSigma 400 --initMu ${initMu} --initialS ${initialS} --initVariance ${initVariance} --varCovarFile "${varCovarFile}" --varianceScalingFactor ${varianceScalingFactor}
```



# Parameters for sample size estimation

### Old 



    -c 0.9
"-c"  - coverage, used for species coverage, but not clear how it was used

New

```sh
-c 0.9
```

--cS -species coverage

--cI - individuals coverage

# SearchMonkey

## Deleting documents from topsy

### Find all files tjo be deleted

```sh
rt.jar_106010
ckpt_java_5bb340d424f98180-40000-58aa56c8.dmtcp
emp_dev_scala-assembly-1.0-deps.jar_102066

(rt.jar_\d{1,}$||ckpt_java_.*\.dmtcp$ || emp_dev_scala-assembly-.*.jar_\d{1,}$ )
```

# R

```R
.libPaths()
.libPaths(c('~/MyRlibs',.libPaths()))
.libPaths(c('/media/sf_Dropbox/Newcastle-Project/src/Projects/Quince-C/R/mypackages',.libPaths()))
```

# Scala

## Collecting Results 

### ResultsCollector

basics.ResultsCollector

# SBT

## Scala compiling

### SBT Jar file managing

```scala
sbt reload update compile slim:assembly assemblyPackageDependency
```

```scala
sbt ~ ;reload ;compile ; slim:assembly ; fat:assembly 
sbt compile
sbt assembly
sbt assemblyPackageDependency

[warn] Multiple main classes detected. Run 'show discoveredMainClasses' to see the list
java -cp "jar1.jar:jar2.jar" Main
```

### SBT autobuild

```sh
sbt  ~ ;reload;slim:assembly;assemblyPackageDependency;fat:assembly
assemblyPackageDependency
```

### Scala read parameters

```shell
sbt fat:assembly
sbt slim:assembly
// ******************************************************************************
Splitting your project and deps JARs

To make a JAR file containing only the external dependencies, type

normal> assemblyPackageDependency
normal

This is intended to be used with a JAR that only contains your project

assemblyOption in assembly := (assemblyOption in assembly).value.copy(includeScala = false, includeDependency = false)

NOTE: If you use  -jar  option for  java , it will ignore  -cp , so if you have multiple JAR files you have to use  -cp  and pass the main class:  java -cp "jar1.jar:jar2.jar" Main 
```

# EMP SW development

## New input parameter

- -g, -\-generated -- marks whether this is real data or generated data values: yes - if real data, no - if synthetic data
  - optional  parameter
  - default value: synthetic


# Synthetic sample preparation R

## community_syntetic_generator_raw_otu_4community_calledFromInit.r

/media/sf_data/nps409/Other/Dropbox/Newcastle-Project/src/Projects/Quince-C/R/EBI/R/community_syntetic_generator_raw_otu_4community_calledFromInit.r

## resampleCommunity4Sample.r

## Resampling from community using hashmap

### resampleCommunity4SampleHM.r

resampling sampling effort from community data /media/sf_Dropbox/Newcastle-
Project/src/Projects/Quince-C/R/EBI/R/ resampleCommu-
nity4SampleHM.r

## Sampling community from lognormal

### r_sample_raw_otu_4community.r

### r_resampleCommunity.r

/media/sf_Dropbox/Newcastle-Project/src/Projects/ Quince-
C/R/EBI/R/r_resampleCommunity.r

#  Syntetics sample preparation Scala

## utils.ReadingExperimentParameters#getInputParams4SamplingCommunity

```scala
def getInputParams4SamplingCommunity(args: Array[String]): Parameters4Sampling = {

    var (dsName, dataFormat, scopeOfData, generatedOrReal, coverageOfSpecies, tadDistribution, fileFormat, outputDir, mean, variance, numerOfSpecies) = getInputParams4SamplingCommunity4Test(args)

    val parameters4Distribution: Parameters4Distribution = tadDistribution match {
      case TADDistributionInfo.LOGNORMAL.name => Parameters4ForLognormalDistribution(mean, variance)
    }

    val parameters4FileName: Parameters4FileName = Parameters4FileName(dsName, Option(dataFormat), scopeOfData, generatedOrReal, coverageOfSpecies, tadDistribution, MCMC.DATE_STR, fileFormat, outputDir)

    val parameters4Sampling = Parameters4Sampling(parameters4FileName, parameters4Distribution, numerOfSpecies)
    println("")

    /**
      * Parameters4ForLognormalDistribution(meanOfLogValue:Double,varianceOfLogValue:Double)      *
      */

    /**
      * Parameters4FileName(dataSetName: String = "44xSRS711891", dataFormat: Option[String] = Some("Abundance"), scopeOfData: String = "Community", isGeneratedOrSynth: String = "Synthetic", coverage: Double = 1d, dateTimeStamp: String, fileNameSuffix: String = "csv", outDir: String = ".")
      */

    parameters4Sampling
  }
```

## utils.sampling.SamplingPoilog#main

##### Test values 

```sh
--dataSetName "Brazil" --dataFormat "TAD" --numberOfSpecies 100 -m -4.43313575825946 -v 7.13590051646696 --realOrSynth "Synthetic"  --scopeOfData "Sample" --outDirectory "/media/sf_data/nps409/Work2Sync/Projects/emp_dev_scala_runs/TAD_DATA_Synthetic_Out/TESTS_OF_SAMPLING"
```

##### Real values 

```shell
--dataSetName "Brazil" --dataFormat "TAD" --numberOfSpecies 27642 -m -4.43313575825946 -v  7.13590051646696 --realOrSynth "Synthetic" --scopeOfData "Sample"  --outDirectory "/media/sf_data/nps409/Work2Sync/Projects/emp_dev_scala_runs/TAD_DATA_Synthetic_Out/TESTS_OF_SAMPLING"
```

##### Real values topsy

```shell
--dataSetName "Brazil" --dataFormat "TAD" --numberOfSpecies 27642 -m -4.43313575825946 -v  7.13590051646696 --realOrSynth "Synthetic" --scopeOfData "Sample"  --outDirectory "../../../TAD_DATA_Synthetic_Out/GENERATED_SYNTHETIC"
```

```sh
"../../../TAD_DATA_Synthetic_Out/GENERATED_SYNTHETIC"
```

#### Hron

##### Brazil

```sh
java -cp  "${extJAR}:${mcJAR}:${rScalaJAR}" "utils.sampling.SamplingPoilog" --dataSetName "Brazil" --dataFormat "TAD" --numberOfSpecies 27642 -m -4.43313575825946 -v  7.13590051646696 --realOrSynth "Synthetic" --scopeOfData "Sample"  --outDirectory "../../../TAD_DATA_Synthetic_Out/GENERATED_SYNTHETIC"
```

##### Canada

```shell
java -cp  "${extJAR}:${mcJAR}:${rScalaJAR}" "utils.sampling.SamplingPoilog" --dataSetName "Canada" --dataFormat "TAD" --numberOfSpecies 89306 -m -5.17641847520714 -v  6.46411567758581 --realOrSynth "Synthetic" --scopeOfData "Sample"  --outDirectory "../../../TAD_DATA_Synthetic_Out/GENERATED_SYNTHETIC"
```

##### Florida

```sh
java -cp  "${extJAR}:${mcJAR}:${rScalaJAR}" "utils.sampling.SamplingPoilog" --dataSetName "Florida" --dataFormat "TAD" --numberOfSpecies 53557 -m -5.09002922198777 -v 6.43361507000434
  --realOrSynth "Synthetic" --scopeOfData "Sample"  --outDirectory "../../../TAD_DATA_Synthetic_Out/GENERATED_SYNTHETIC"
```

###### End

-----------------------------

```sh
dmtcp_launch java -Xmx1G -XX:ParallelGCThreads=1 -XX:-UseParallelGC -cp  "${extJAR}:${mcJAR}" "utils.sampling.SamplingPoilog" --dataSetName "Brazil" --dataFormat "TAD" --numberOfSpecies 27642 -m -4.43313575825946 -v  7.13590051646696 --realOrSynth "Synthetic" --scopeOfData "Sample"  --outDirectory "../../../TAD_DATA_Synthetic_Out/GENERATED_SYNTHETIC"
```

##### Example path

```sh
/media/sf_data/nps409/Work2Sync/Projects/emp_dev_scala_runs/TAD_DATA_Synthetic_Out/TESTS_OF_SAMPLING/SRS711891_Synthetic_Out/Brazil_synthetic_170131-1945_15_GENERATED
/media/sf_data/nps409/Work2Sync/Projects/emp_dev_scala_runs/TAD_DATA_Synthetic_Out/TESTS_OF_SAMPLING/Brazil_Synthetic_Out/Brazil_Synthetic_170131-2011_15_GENERATED
/media/sf_data/nps409/Work2Sync/Projects/emp_dev_scala_runs/TAD_DATA_Synthetic_Out/TESTS_OF_SAMPLING/Brazil_Synthetic_OutSRS711891_Synthetic_Out/Brazil_Synthetic_170131-2008_28_GENERATED
```

```sh
"/media/sf_data/nps409/Work2Sync/Projects/emp_dev_scala_runs/TAD_DATA_Synthetic_Out/TESTS_OF_SAMPLING" + File.separator + "SRS711891_Synthetic_Out" + File.separator + s"${parameters.dsName}_synthetic_${DATE_STR}_GENERATED"
```

## utils.sampling.SamplingPoilog#resampleWithoutReplacement

## SamplingCommunityFromPoissonLognormal

#### Parameters:

```scala

  object ParametersNames extends Enum4EMP2[ParametersNames] {
    val DATASET_NAME = new ParametersNames(List("dataSetName", "ds"), List(""), List()) {}
    val DATA_FORMAT = new ParametersNames(List("dataFormat", "df"), List("TAD", "Abundance"), List()) {
      override val acceptedEnum: List[TADDistributionInfo] = TADDistributionInfo.values
    }
    val SCOPE_OF_DATA = new ParametersNames(List("scopeOfData", "sc"), List("Community", "Sample"), List()) {}
    val REAL_OR_SYNTHETIC = new ParametersNames(List("realOrSynth", "ros"), List("Real", "Synthetic"), List()) {}
    val COVERAGE_OF_SPECIES = new ParametersNames(List("coverageOfSpecies", "cs"), List(11000), List()) {}
    val TAD_DISTRIBUTION = new ParametersNames(List("distribution", "dist"), List("Lognormal"), List()) {}
    val FILE_FORMAT = new ParametersNames(List("fileFormat", "ff"), List("csv", "sample"), List()) {}
    val OUTPUT_DIRECTORY = new ParametersNames(List("outDirectory", "od"), List("."), List()) {}
    val TAD_MEAN = new ParametersNames(List("mean", "m"), List(1.1), List()) {}
    val TAD_VARIANCE = new ParametersNames(List("variance", "v"), List(), List()) {}
    val NUMBER_OF_SPECIES = new ParametersNames(List("numberOfSpecies", "ns", "S"), List(11000), List()) {}
    //
  }
```

Input parameters

- ```sh
  --dataSetName --ds "Brazil" 
  ```


- ```sh
  --dataFormat --df "TAD"  -->  Values: "Abundance", "TAD". 
  ```

- ```shell
  --scopeOfData  --sc "Community", "Sample" --> Values: "Community", "Sample".
  ```

```sh
--dataSetName "Brazil" --dataFormat "TAD"  --scopeOfData
```

Parameters for running <u>**SamplingCommunityFromPoissonLognormal#runner01**</u>

```sh
--dataSetName "Brazil" --dataFormat "TAD"  --scopeOfData Sample
```

Data parameters from <u>**utils.ReadingExperimentParameters#getInputParams4SamplingCommunity**</u>

```sh
ReadingExperimentParameters#getInputParams4SamplingCommun --> getInputParams4SamplingCommunity4Test
```

```sh
SamplingCommunityFromPoissonLognormal -> main-> runner01 -> utils.sampling.SamplingPoilog$#getAbundaceForPoilog
```

```scala
def getAbundaceForPoilog(parameters: ParametersSamplingPoilog, fw: CSVWriter, header: List[String]): List[Long] = {
    fw.writeRow(header)
    val coverage = 1d
    val nS: Int = (parameters.S * coverage).round.toInt
    println(s"""nS = $nS""")
    val sigmaLog = Math.sqrt(parameters.varLog)
    def rPoilog = PoilogRScala.rPoilog(parameters.S, parameters.meanLog, sigmaLog)
    var iterPerPart: ListBuffer[Int] = new ListBuffer[Int]()
    var parts = 10
    var nIterPerPart: Int = (nS / parts)
    var ml: List[Int] = List.fill[Int](parts - 1)(nIterPerPart)
    iterPerPart ++= ml
    iterPerPart += nS - (parts - 1) * nIterPerPart
    var abundance = (1 to parts).toList.tap(x => println("Part " + x)).par.map(i => (1 to iterPerPart(i - 1)).map(subIdx => {
      val n = rPoilog;
      if (subIdx % 20 == 0) println(s"Part = $i - subIdx = $subIdx/${iterPerPart(i - 1)}, n = $n");
      fw.writeRow(List(n));
      n
    })).flatten.toList
    //    abundanceToOTUs
    fw.close()
    println("-------------------------")
    println("")
    abundance
  }s
```

## utils.sampling.SamplingPoilog#main

### Command line parameters

- ${coverage} -- ff
- $S -- ff
- ${meanLog} -- dd

```
val clOptions: InputParametersMCMCAndSamplingEffort = ReadingExperimentParameters.getInputParams4DataSampling(args)
```

```
-S -coverage -meanLog 
```

## Generating synthetic community data from poilog TAD (Poisson-lognormal distribution)

### Running sofware
### Generated file names
- output data file
  - abundance distribution file  *__```44xSRS711891_Abundance_Community_Synthetic_100.0_pc_lognormal_161024-2234_41.csv```__*
    - components of name construction: 
      - ```${datasetName}_${dataFormat}_${scopeOfData}_${generated}_${Coverage of speciest}_pc_${distribution}_${DateTime}.csv```
    - parts:
      - ```${datasetName}``` -- possible value: 44xSRS711891
      - ```${dataFormat}``` -- Abundance or TAD
      - ```${scopeOfData}``` -- Community or Sample
      + `${generated}` -- with values Real or Synthetic
      - ```${Coverage of species}```-- fractional coverage of species, double values
      - ```${distribution}``` --  type of TAD distribution
      - ```${DateTime}``` --  date and time stamp


## Sampling community data

## Sampling from community data

* SamplingPoilog
  * ```resampleWithoutReplacement(abundanceIM: List[Long], coverage: Double): List[Long] ```

  ## Reading command line parameters

* utils.**ReadingExperimentParameters**

  * ***Work in progress***  ``` getInputParams4SubsamplingCommunity(args: Array[String]): InputParameters4SubsamplingCommmunity```

       - ```getInputParams4MCMCAndSamplingEffort```		

```scala
import collection.JavaConverters._;
import scala.collection.breakOut;
val l: List[Double] = j.asScala.map(_.doubleValue)(breakOut)  
```



***
# R package for SAD estimation

***

## Amy Willis* and John Bunge: Estimating Diversity via Frequency Ratios
+ package: *__breakaway__*

function for species richness estimation

**Description**

This function implements the species richness estimation procedure outlined in Willis & Bunge (2015). The diversity estimate, standard error, estimated model coefficients, model details and plot of the fitted model are returned.

**Usage**

**breakaway*(*data*, *print* = TRUE, *plot* = TRUE, *answers* = FALSE, *force* = FALSE)**

**Arguments**

**data	**
The sample frequency count table for the population of interest. The first row must correspond to the singletons. Acceptable formats include a matrix, data frame, or file path (csv or txt). The standard frequency count table format is used: two columns, the first of which contains the frequency of interest (eg. 1 for singletons, species observed once, 2 for doubletons, species observed twice, etc.) and the second of which contains the number of species observed this many times. Frequencies (first column) should be ordered least to greatest. At least 6 contiguous frequencies are necessary. Do not concatenate large frequencies. See dataset apples for sample formatting.
**print**	
**Logical**: whether the results should be printed to screen. If FALSE, answers should be set to TRUE so that results will be returned.
**plot**	
Logical: whether the data and model fit should be plotted.
**answers**	
Logical: whether the function should return an argument. If FALSE, print should be set to TRUE.
**force**	
Logical: force breakaway to run in the presence of frequency count concatenation. breakaway checks that the user has not concatenated multiple upper frequencies. force=TRUE will force breakaway to fit models in the presence of this. breakaway's diversity estimates cannot be considered reliable in this case.

***

# API-R

#API-Scala

## ExperimentScalaRunner

### Input parameters - reading data:

* **varCovarFile**
* s
* dff

#### --varianceScalingFactor 1.0

#### --varCovarFile ""

## MCMCIO - input/output class

utils.MCMCIO#readDataPosteriorEstimates

## ExperimentScalaRunner

Parameters:

* paramAStDev - variance of mu for generating candidate value
* paramBStDev - variance of sigma for generating candidate value
* SStDev - number of species in category
* paramA0 - initial value for parameter A (mu for lognormal)
* paramB0 - initial value for parameter A (mu for lognormal)
* paramS0 - initial value for number of species in community



## SamplingPoilog

- utils.sampling.**SamplingPoilog**
  - utils.sampling.SamplingPoilog#**getSample**

  `case class Parameters4FileName(dataSetName: String = "44xSRS711891", dataFormat: Option[String] = Some("Abundance"), scopeOfData: String = "Community", isGeneratedOrSynth: String = "Synthetic", coverage: Double = 1d, dateTimeStamp: String, fileNameSuffix: String = "csv", outDir: String = ".")`

  Parameters:

| Option                | Required | Default | Description | Other values |
| :-------------------- | :------: | ------- | :---------- | ------------ |
| -\-dataSetName, -\-ds |    Y     |         |             |              |
|-\-dataFormat, -\-df		| N
|-\-outDirectory, -\-od|N	|
|-\-numOfSpecies, -S|N	|

****


##Parameters4FileName

* Class  utils.sampling.Parameters4FileName

case class Parameters4FileName(dataSetName: String = "44xSRS711891", dataFormat: Option[String] = Some("Abundance"), scopeOfData: String = "Community", isGeneratedOrSynth: String = "Synthetic", coverage: Double = 1d, tadDistribution:String,dateTimeStamp: String, fileFormat: String = "csv", outDir: String = ".")

| Option | Required | Default | Description | Other values |
| ------ | -------- | ------- | ----------- | ------------ |
|        |          |         |             |              |

| :-------------------------------------- | :------------: | ------------------   -- | :-------------------------------------------------------- | ------------------------------- |
| --------------------------------------- | -------------- | ----------------------- | ---------------------------------------- | ------------------------------- |
|                                         |                |                         |                                          |                                 |

case class Parameters4Sampling(paramsFN: Parameters4FileName, parameters4Distribution: Parameters4Distribution, numberOfSpeciest: Int)

  case class Parameters4ForLognormalDistribution(meanOfLogValue: Double, varianceOfLogValue: Double) extends Parameters4Distribution

  |Option                             |Required |Default          |Description                                         |Other values            |
|:--------------------------------------|:------------:|------------------   --|:--------------------------------------------------------|-------------------------------|

-----

##ParametersNames

* utils.ReadingExperimentParameters.ParametersNames

 object ParametersNames extends Enum4EMP2[ParametersNames] {
```scala
val DATASET_NAME = new ParametersNames(List("dataSetName", "ds"),List("")) {}
val DATA_FORMAT = new ParametersNames(List("dataFormat", "df"),List("TAD","Abundance")) {}
val SCOPE_OF_DATA = new ParametersNames(List("scopeOfData", "sc"),List("Community","Sample")) {}
  val REAL_OR_SYNTHETIC = new ParametersNames(List("realOrSynth","ros"),List("Real","Synthetic")) {}
val COVERAGE_OF_SPECIES = new ParametersNames(List("coverageOfSpecies","cs"),List(11000)) {}
  val TAD_DISTRIBUTION = new ParametersNames(List("distribution","dist"), List("Lognormal"), List()) {}
val FILE_FORMAT = new ParametersNames(List("fileFormat", "ff"),List("csv","sample")) {}
val OUTPUT_DIRECTORY = new ParametersNames(List("outDirectory", "od"),List(".")) {}
```
 val TAD_MEAN = new ParametersNames(List("mean","m"), List(1.1), List()) {
```scala
  1.1
}
val TAD_VARIANCE = new ParametersNames(List("variance","v"), List(), List()) {
  1.1
}
val NUMBER_OF_SPECIES = new ParametersNames(List("numberOfSpecies", "ns", "S"), List(11000), List()) {}
```
  }

    |Option                             |Required |Default          |Description                                         |Other values            |
|:--------------------------------------|:------------:|------------------   --|:--------------------------------------------------------|-------------------------------|

  

  `--ds "44xSRR15897XX"  --df "TAD" --sc "Community"   --ros "Synthetic" --cs 1.0 --dist "Lognormal"`
  `--ff "csv" --od "" -m 8.869098764747136 -v 8.869098764747136 -S 15862`


 `variance: 8.869098764747136 `
`nAccepted: 202659`
`nLL: 1870.9725567347195`
`time_sec: 132232.379682638`
`nIter: 800000`
`mu: -0.9335555974214564`
`S: 15862`
` 
****
## SubsamplingFromCommunity

```sh
--in
"/media/sf_data/nps409/Work2Sync/Projects/emp_dev_scala_runs/TAD_DATA_Synthetic_Out/TESTS_OF_SAMPLING/44xSRR15897XX_Abundance_Community_Synthetic_100.0pc_Lognormal_161116-1520_28/44xSRR15897XX_TAD_Community_Synthetic_100.0pc_Lognormal_161116-1520_28.csv"
-c
0.9
--out
"/media/sf_data/nps409/Work2Sync/Projects/emp_dev_scala_runs/TAD_DATA_Synthetic_Out/TESTS_OF_SAMPLING/44xSRR15897XX_Abundance_Community_Synthetic_100.0pc_Lognormal_161116-1520_28/"
```

-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

s

```sh
#!/bin/bash
# all

date_formatted=$(date +%y%m%d-%H%M-%S)

# External JAR
extJAR="emp_dev_scala-assembly-1.0-deps.jar"
# MyCode JAR
mcJAR="emp_dev_scala-assembly-1.0.jar"
jarDir="../../../jars"
rScalaJAR="/home/peter/R/x86_64-pc-linux-gnu-library/3.3/rscala/java/rscala_2.11-1.0.9.jar"

# Combining
extJAR="${jarDir}/${extJAR}"
mcJAR="${jarDir}/${mcJAR}"
java -cp  "${extJAR}:${mcJAR}:${rScalaJAR}" ExperimentScalaRunner --in "/media/sf_data/nps409/Work2Sync/Projects/emp_dev_scala_runs/TAD_DATA_Real_Out/GOS/GOS.sample" -s 600000 -c 0.9
```

##ResultsCollector

* main.ResultsCollector - no comman line arguments at a moment.

## Class



* /media/sf_data/nps409/Work2Sync/Projects/emp_dev_scala_runs/TAD_DATA_Real_Out/AUTO_RUN_TEST

# Output Summary File



# Questions 

Am I using mulitiplicative update of parameters?



# Diary

## 161026

- [ ] synthetic generation of community data with required parameters for lognormal
      - [ ] json info file with parameters with name construction
            - [ ]`info_${datasetName}_${fileFormat}_${typeOfData}_${generated}_${Coverage of speciest}_pc_${distribution}_${DateTime}.json`
- [ ] save genereated synghetic data into:
      - ```${datasetName}_${dataFormat}_${typeOfData}_${generated}_${Coverage of speciest}_pc_${distribution}_${DateTime}.csv```

## 161026 -- Thursday, 27. October 2016 03:54pm



## 170121 -- Tuesday, 





## 170207

* resampleCommunity4SampleHM_ExtParams.r

* community_syntetic_generator_raw_otu_4community_calledFromInit.r

* community_data_generator_Brazil.R

* Brazil_with_dir.R

* community_data_generator.R

* community_data_generator.R

* ​

  ​
  ​			

  Instal libpng

  * sudo apt-get install libpng16-dev
  * sudo ldconfig


## Use [`xargs`](http://manpages.ubuntu.com/xargs.1):

## Use [`xargs`](http://manpages.ubuntu.com/xargs.1):

```sh
    xargs rm < file  # or
    xargs -a file rm
```

But that will not work if the file names/paths contain characters that should be escaped.

If your filenames don't have newlines, you can do:

```sh
tr '\n' '\0' < file | xargs -0 rm # or
xargs -a file -I{} rm {}
```


`Alternatively, you can create the following script:`

```sh
#!/bin/bash
if [ -z "$1" ]; then

    echo -e "Usage: (basename 0) FILE\n"

    exit 1

fi

if [ ! -e "$1" ]; then

    echo -e "$1: File doesn't exist.\n"

    exit 1

fish

while read -r line; do

    [ -n "line" ] && rm -- "line"

done < "$1"
```

Save it as  `/usr/local/bin/delete-from`, grant it execution permission:

```sh
sudo chmod +x /usr/local/bin/delete-from
```

#### Then run it with:

```sh
delete-from /path/to/file/with/list/of/files
```

```sh
sbt  slim:assembly assemblyPackageDependency
```

#### Compile jars:

```sh
sbt  slim:assembly assemblyPackageDependency
```

#### Run R to analyse *MCMC trace*

```
mcmc_chain_analysis.rmd
```

#### R constructing data frame from lists

```R
> lll <- c(2,3)
> ll2 <- c(4,-5)
> ll3 <- list(aa=lll,bb=ll2)
> ll4 <- as.data.frame(ll3)
> ll4
  aa bb
1  2  4
2  3 -5
```

# 170223

#### R code for analyzing MCMC chain 

- mcmc_chain_analysis_call.R

- mcmc_chain_analysis_called.rmd

  ​

#### R code for analyzing MCMC chain  CORRELATION

- mcmc_chain_analysis_cross_correlation.rmd

```
mcmc_chain_analysis_call.R
```

#### Real QC paper datasets results 

```sh
/media/sf_data/nps409/Work2Sync/Projects/emp_dev_scala_runs/TAD_DATA_Real_Out/RESULTS_TOPSY_170123/AUTO_RUN_REAL_170116/Illinois
```

## 170301

```scala
 val outDirOpt = parser.acceptsAll(asList("out", "o"), "directory - output directory").withRequiredArg().ofType(classFile2)
    val inputFileOpt = parser.acceptsAll(asList("in", "i"), "CSV file name with TAD data").withRequiredArg().ofType(classFile2)
    val nMCMCiterationsOpt = parser.acceptsAll(asList("nIterations", "s"), "Number of MCMC iterations").withOptionalArg().ofType(classOf[java.lang.Integer]).defaultsTo(600000)
    val thinningIntervalOpt = parser.acceptsAll(asList("thinning", "t"), "Thinning interval for MCMC").withOptionalArg().ofType(classOf[java.lang.Integer]).defaultsTo(10)
    val speciesCoverageOpt = parser.acceptsAll(asList("coverage", "c"), "Expected fraction of species we like to observe in sample").withOptionalArg().ofType(classOf[java.lang.Double]).defaultsTo(0.9)
    val distributionOpt = parser.acceptsAll(asList("distribution", "d"), "Form of TAD distribution").withOptionalArg().ofType(classOf[java.lang.String]).defaultsTo("lognormal")
    val versionOpt = parser.acceptsAll(asList("version"), "version of calculating distribtution value").withOptionalArg().ofType(classString2).defaultsTo("s")
    // ****   MCMC parameters    *****
    val paramASigmaOpt = parser.acceptsAll(asList("paramASigma"), "Standard deviation of first parameter").withOptionalArg().ofType(classOf[java.lang.Double]).defaultsTo(0.1)
    val paramBSigmaOpt = parser.acceptsAll(asList("paramBSigma"), "Standard deviation of second parameter").withOptionalArg().ofType(classOf[java.lang.Double]).defaultsTo(0.2)
    val paramSStDevOpt = parser.acceptsAll(asList("SSigma"), "Standard deviation of second parameter").withOptionalArg().ofType(classOf[java.lang.Integer]).defaultsTo(400)
    //*****************
    val correlationFileOpt = parser.acceptsAll(asList("correleationFile"), "CSV file name with TAD data").withOptionalArg().ofType(classFile2)
    //-------------------
    // mcmcParamASigma
    // -------------------------------------
    val helpOpt = parser.acceptsAll(asList("h", "?"), "show help").forHelp()
```

```scala
val workingDir: String = System.getProperty("user.dir")
    println(s"Working dir $workingDir")
    val inputFile = new File(inputFile0.getAbsolutePath)
    println(s"Absolute path to input file  $inputFile")

    val outDir: File = (if (options.has(outDirOpt)) {
      Success(outDirOpt.value(options))
    } else {
      if (inputFile != null) {
        Success(inputFile.getParentFile)
      }
      else {
        println("Help on options")
        parser.printHelpOn(System.out)
        Failure(new Exception("Input file  is null is not specified or does not exist!"))
      }
    }
      ).get

    val nMCMCIterations: Int = nMCMCiterationsOpt.value(options)
    val speciesCoverage: Double = speciesCoverageOpt.value(options)
    val distribution: String = distributionOpt.value(options)
    val version: String = versionOpt.value(options)
    //----------------------------------
    val paramASigma: Double = paramASigmaOpt.value(options)
    val paramBSigma: Double = paramBSigmaOpt.value(options)
    val paramSStDev: Int = paramSStDevOpt.value(options)
    //-----------------------------------
    val thinningInterval = thinningIntervalOpt.value(options)
    //------------------------------------------------------
    val correlationFile:Option[File] = if (options.hasArgument(correlationFileOpt)) {
      Some(correlationFileOpt.value(options))
    } else None
```

```scala
case class InputParametersMCMCAndSamplingEffort(tadFileName: File, outDir: File, nMCMCIterations: Int = 800000, coverage: Double = 0.9, distribution: String = "lognormal", relTollerance: Double = 1.05, relTolleranceStrict: Double = 1.001, rootFindingMethod: String = "Pegasus", mu0: Double = 1d, variance0: Double = 1d, s0: Double = 10000, version: String = "s", mode: String = "all", thinningInterval: Int = 10, paramASigma: Double = 0.1, paramBSigma: Double = 0.2, paramSStDev: Int = 400,correlationFile:Option[File])

```

## 170306

### QC paper synthetic Correlated parameters

```sh
file:///media/sf_data/nps409/Other/Dropbox/Newcastle-Project/src/Projects/Quince-C/R/Sample Generator/file:/media/sf_data/nps409/Work2Sync/Projects/emp_dev_scala_runs/TAD_DATA_Synthetic_Out/Topsy/QC_Paper_170306_correlation
```



# Notes



## .bashrc - settings for paths

```sh
# User specific aliases and functions

export JAVA_HOME="$HOME/applications/sunjdk/1.8/jre1.8.0_112"

export SCALA_HOME="$HOME/applications/scala/scala-2.12.0"

export PATH="$HOME/bin:$JAVA_HOME/bin:$SCALA_HOME/bin:$PATH"

export PATH="$HOME/applications/Rpackages:$PATH"
# sftp://topsy-login/home/nps409/applications/Rpackages/poilog/R
```

## Collector - Pattern matching for output files

Rewrite: **getInfoFromFileNameOfPosteriors01**.

pattern for file: 

```scala
(?i)^sample_size_posterior-(\w)-(\w)-(\w{1,})-((\w{1,})\.(\w{3,}))_(\d{6})-(\d{4})_(\d{2})_correct_Coverage_(\d*)_%o_([a-z]*)_(\d{6})-(\d{4})_(\d{2})\.csv$
```

directory:

```scala
a	
```

## Mount topsy  directory

is my msmcSigmaVariance or mcmcSigmaStandardDev ?mount topsy  directory



```sh
sudo sshfs -o allow_other nps409@topsy-login:/ /mnt/topsy
sudo umount /mnt/topsy
#***********************************
sudo sshfs -o allow_other,defer_permissions,IdentityFile=~/.ssh/id_rsa root@xxx.xxx.xxx.xxx:/ /mnt/droplet
#*************************************************
sudo sshfs -o allow_other IdentityFile=/home/peter/.ssh/id_rsa  nps409@topsy-login:/  /mnt/topsy
sudo umount /mnt/topsy
```

## Topsy reporting 



```sh
 qstat -r | grep B* -B1
```

------

|      | Brazil       | Canada  | Florida | FS312a | FS312b | FS396a | FS396b | Illionois |  GOS  |
| ---- | ------------ | ------- | ------- | ------ | ------ | :----: | ------ | --------- | :---: |
| 90%  | f600K, r1.2M | f1200K, | r       | r-90%  | r      |   ff   | r1.2M  | r1.2M     |  ff   |
| 100% | r            | r       | r       | r      | r      | r1.2M  | r1.2M  | r1.2M     | r1.2M |
|      |              |         |         |        |        |        |        |           |       |





------

|      | Brazil | Canada | Florida | FS312a | FS312b | FS396a | FS396b | Illionois | GOS                     |
| ---: | ------ | ------ | ------- | ------ | ------ | :----: | ------ | --------- | ----------------------- |
|  90% |        |        |         |        |        |        | r1.2M  |           |                         |
| 100% |        |        |         |        |        |        |        |           | GOS_100_0220_1.2M.dmtcp |
|      |        |        |         |        |        |        |        |           |                         |





**************************************************************

```sh
job-ID  prior   name       user         state submit/start at     queue                          slots ja-task-ID 
-----------------------------------------------------------------------------------------------------------------
  96890 0.71942 Braz_0211_ nps409       r     02/18/2017 16:47:23 all.q@compute-0-2.local            1        
       Full jobname:     Braz_0211_1.2M.dmtcp
       Master Queue:     all.q@compute-0-2.local
       Requested PE:     threaded 1
       Granted PE:       threaded 1
       Hard Resources:   h_vmem=18G (0.000000)
       Soft Resources:   
  96931 0.71942 Braz_0211_ nps409       r     02/19/2017 03:51:38 all.q@compute-0-6.local            1        
       Full jobname:     Braz_0211_0.6M.dmtcp
       Master Queue:     all.q@compute-0-6.local
       Requested PE:     threaded 1
       Granted PE:       threaded 1
       Hard Resources:   h_vmem=18G (0.000000)
       Soft Resources:   
  96932 0.71942 Br_100_020 nps409       r     02/19/2017 03:56:23 all.q@compute-0-6.local            1        
       Full jobname:     Br_100_0207_1.2M.dmtcp
       Master Queue:     all.q@compute-0-6.local
       Requested PE:     threaded 1
       Granted PE:       threaded 1
       Hard Resources:   h_vmem=18G (0.000000)
       Soft Resources:   
  96934 0.71942 Br_100_021 nps409       r     02/19/2017 04:03:38 all.q@compute-0-6.local            1        
       Full jobname:     Br_100_0210_1.2M.dmtcp
       Master Queue:     all.q@compute-0-6.local
       Requested PE:     threaded 1
       Granted PE:       threaded 1
       Hard Resources:   h_vmem=18G (0.000000)
       Soft Resources:   
  96940 0.71942 Ca_90_0219 nps409       r     02/19/2017 04:37:38 all.q@compute-0-9.local            1        
       Full jobname:     Ca_90_0219_1.2M.dmtcp
       Master Queue:     all.q@compute-0-9.local
       Requested PE:     threaded 1
       Granted PE:       threaded 1
       Hard Resources:   h_vmem=18G (0.000000)
       Soft Resources:   
  96967 0.71942 FS2a_90_02 nps409       r     02/19/2017 09:54:23 all.q@compute-0-14.local           1        
       Full jobname:     FS2a_90_0208_1.2M.dmtcp
       Master Queue:     all.q@compute-0-14.local
       Requested PE:     threaded 1
       Granted PE:       threaded 1
       Hard Resources:   h_vmem=18G (0.000000)
       Soft Resources:   
  96968 0.71942 FS2a_100_0 nps409       r     02/19/2017 09:57:38 all.q@compute-0-14.local           1        
       Full jobname:     FS2a_100_0213_1.2M.dmtcp
       Master Queue:     all.q@compute-0-14.local
       Requested PE:     threaded 1
       Granted PE:       threaded 1
       Hard Resources:   h_vmem=18G (0.000000)
       Soft Resources:   
  96970 0.71942 FS2b_90_02 nps409       r     02/19/2017 10:02:53 all.q@compute-0-14.local           1        
       Full jobname:     FS2b_90_0213_1.2M.dmtcp
       Master Queue:     all.q@compute-0-14.local
       Requested PE:     threaded 1
       Granted PE:       threaded 1
       Hard Resources:   h_vmem=18G (0.000000)
       Soft Resources:   
  96972 0.71942 FS2b_100_0 nps409       r     02/19/2017 10:07:08 all.q@compute-0-14.local           1        
       Full jobname:     FS2b_100_0213_1.2M.dmtcp
       Master Queue:     all.q@compute-0-14.local
       Requested PE:     threaded 1
       Granted PE:       threaded 1
       Hard Resources:   h_vmem=18G (0.000000)
       Soft Resources:   
  96973 0.71942 FS6b_90_02 nps409       r     02/19/2017 10:11:23 all.q@compute-0-15.local           1        
       Full jobname:     FS6b_90_0208_1.2M.dmtcp
       Master Queue:     all.q@compute-0-15.local
       Requested PE:     threaded 1
       Granted PE:       threaded 1
       Hard Resources:   h_vmem=18G (0.000000)
       Soft Resources:   
[nps409@topsy-login-1-0 s_dmtcp_1200K_90_pc_170219_0414]$ qstat -r | grep B* -B1qstat
grep: 1qstat: invalid context length argument

[nps409@topsy-login-1-0 s_dmtcp_1200K_90_pc_170219_0414]$ qstat
job-ID  prior   name       user         state submit/start at     queue                          slots ja-task-ID 
-----------------------------------------------------------------------------------------------------------------
  96890 0.71978 Braz_0211_ nps409       r     02/18/2017 16:47:23 all.q@compute-0-2.local            1        
  96931 0.71978 Braz_0211_ nps409       r     02/19/2017 03:51:38 all.q@compute-0-6.local            1        
  96932 0.71978 Br_100_020 nps409       r     02/19/2017 03:56:23 all.q@compute-0-6.local            1        
  96934 0.71978 Br_100_021 nps409       r     02/19/2017 04:03:38 all.q@compute-0-6.local            1        
  96940 0.71978 Ca_90_0219 nps409       r     02/19/2017 04:37:38 all.q@compute-0-9.local            1        
  96967 0.71978 FS2a_90_02 nps409       r     02/19/2017 09:54:23 all.q@compute-0-14.local           1        
  96968 0.71978 FS2a_100_0 nps409       r     02/19/2017 09:57:38 all.q@compute-0-14.local           1        
  96970 0.71978 FS2b_90_02 nps409       r     02/19/2017 10:02:53 all.q@compute-0-14.local           1        
  96972 0.71978 FS2b_100_0 nps409       r     02/19/2017 10:07:08 all.q@compute-0-14.local           1        
  96973 0.71978 FS6b_90_02 nps409       r     02/19/2017 10:11:23 all.q@compute-0-15.local           1   
```


## Redirection output to files and console

```sh
#!/bin/bash
echo "Testing redirection!"   > >(tee stdout.log) 2> >(tee stderr.log >&2)
```

```sh
#!/bin/bash
echo "Testing redirection!"   >  >(tee -a stdout.log) 2> >(tee -a stderr.log >&2)
```

### VM option

```sh
-Xprofsh
```

----------------------------------



## Delete Trash can from command



```sh
 rm -rf ~/.local/share/Trash/*
```


## Manual  installation of packages

--------------------------------

### 

### Multivariate normal

#### DJW

OK. This would be worth confirming on another dataset, but it looks as though this is probably the cause of the mixing issues. 

There are various ways to get around it, but probably the simplest is to switch from your current update, which is actually a multivariate normal proposal with a diagonal covariance matrix (although you don't think of it that way), to a correlated multivariate normal. 

To start off with, you could use a random walk proposal, as you are now, but with a variance matrix which is (a scaled version of) the posterior covariance matrix from a preliminary run. 

Don't use a library function for **MVN** simulation - they are always inefficient. Just construct it yourself using **3 iid** **N(0,1)'s** and the **Cholesky** **triangle** for the **variance matrix**. 

Don't re-compute the Cholesky triangle at each iteration - just store it (hard-coded?) in your initial config. 

So long as your proposal is a random walk, it should still be symmetric, so you shouldn't need its likelihood in the acceptance ratio. 

You can apply a scalar scale factor to the variance (or the traingle) to tune the overall acceptance rate. 

Once you have it implemented, you should check your implementation works by comparing against long runs on data sets where your original algorithm worked OK. 

Switching to this update should help mixing, but shouldn't affect the posterior distribution at all.

------------

##### Details

### Monte Carlo simulation[[edit](https://en.wikipedia.org/w/index.php?title=Cholesky_decomposition&action=edit&section=7)]

The Cholesky decomposition is commonly used in the [Monte Carlo method](https://en.wikipedia.org/wiki/Monte_Carlo_method) for simulating systems with multiple correlated variables: The [correlation matrix](https://en.wikipedia.org/wiki/Correlation_matrix) is decomposed, to give the lower-triangular **L**. Applying this to a vector of uncorrelated samples, **u**, produces a sample vector **Lu** with the covariance properties of the system being modeled.[[7\]](https://en.wikipedia.org/wiki/Cholesky_decomposition#cite_note-Matlab_documentation-7)

For a simplified example that shows the economy one gets from Cholesky's decomposition, say one needs to generate two correlated normal variables ${ \displaystyle x_{1}}$ and **${\displaystyle x_{2}}$** with given correlation coefficient ${\displaystyle \rho }$ . All one needs to do is to gener![img](https://wikimedia.org/api/rest_v1/media/math/render/svg/d7af1b928f06e4c7e3e8ebfd60704656719bd766)ate two uncorrelated Gaussian random variables {\displaystyle z_{1}}![z_{1}](https://wikimedia.org/api/rest_v1/media/math/render/svg/c3621e468231ab352b7caa30bcf0ce9b452241a6) and {\displaystyle z_{2}}![z_{2}](https://wikimedia.org/api/rest_v1/media/math/render/svg/5abf655fa14f7ea44ad0ca781b59ff59c5f49117). We set {\displaystyle x_{1}=z_{1}}![x_{1}=z_{1}](https://wikimedia.org/api/rest_v1/media/math/render/svg/0d90bf16fbdd905ba7a1e9b51a1d2764e3021078) and $$

tps://wikimedia.org/api/rest_v1/media/math/render/svg/7245f6c4ea61f935419a0dd979e792f5adf9e533).

## Results

```R
​```{r AutoCorrelationOut,echo=FALSE, fig.width=5, fig.height=7}
  myCov <- cov(x.2)
  myCov
​```
   dataSetType: "synthetic"
   dataSetName: "Brazil_0_9_170207"
   posteriorFilePath: "/media/sf_data/nps409/Work2Sync/Projects/emp_dev_scala_runs/TAD_DATA_Synthetic_Out/Topsy/QC_Paper_170219/Brazil_0_9_170207/s-1200K-lognormal-Pegasus-1.05-1.001-170212-1814_54/posterior-s-s-all-data_sampleSize_TAD_90_pc_D_24877.8_syntheticSampleSizes_lognormal_170207_1942-44_17-02-07_2010-22.csv_170212-1814_54.csv"
```