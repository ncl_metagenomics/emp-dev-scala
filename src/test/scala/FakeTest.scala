package basics

object FakeTest extends App {
  val mcmcMetro = new MetropolisHMCMC()
  val fileName = "/media/sf_data/nps409/Other/Dropbox/Newcastle-Project/src/Projects/Quince-C/Data//test01.sample"
  val data = MetropolisHMCMC.readData(fileName)
  val mu = -2.9892483422814546
  val sigma = Math.sqrt(7.7132180011860028)
  val s = 14146
  val nL = MetropolisHMCMC.getD(data.drop(0))
  val res = mcmcMetro.nLogLikelihood(mu, sigma, s, data, nL)
  println("nLogLikelihood = " + res)
}  