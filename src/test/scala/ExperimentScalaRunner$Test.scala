import breeze.linalg.{DenseMatrix, cholesky}
import org.scalatest.{BeforeAndAfterEach, FunSuite}

/**
  * Created by peter on 28/02/17.
  */
class ExperimentScalaRunner$Test extends FunSuite with BeforeAndAfterEach {

  override def beforeEach() {

  }

  override def afterEach() {

  }

  test("testFinalize") {

  }

  test("testSep") {

  }

  test("testCholesky") {

  }

  test("testMain") {

  }

  test("testGetLowerCorrelationMatrix") {
    val correlation = DenseMatrix((1d, -0.8725896, -0.9481653), (-0.8725896, 1d, 0.7611276), (-0.9481653, 0.7611276, 1d))
    val choleskyLowerTriangularExpected = DenseMatrix((1.0000000, 0.0000000, 0.0000000), (-0.8725896, 0.4884540817434532, 0.0000000), (-0.9481653, -0.1355942806834117, 0.2873965116730858))
    println(s"Cholesky Lower expected:")
    println(s"$choleskyLowerTriangularExpected")
    val choleskyLowerTriangularCalculated:DenseMatrix[Double] = utils.Statistics4MCMC.choleskyUpperTriangular(correlation)
    println(s"Cholesky Lower Triangular Calculated:")
    println(s"$choleskyLowerTriangularCalculated")
    val relTolerance = 0.0001
    val absTolerance = choleskyLowerTriangularExpected * relTolerance
    assert(choleskyLowerTriangularCalculated.equals(choleskyLowerTriangularCalculated))
  }

  test("testGetUpperCorrelationMatrix") {
    val correlation = DenseMatrix((1d, -0.8725896, -0.9481653), (-0.8725896, 1d, 0.7611276), (-0.9481653, 0.7611276, 1d))
    val choleskyUpperTriangularExpected = DenseMatrix((1d, -0.8725896, -0.9481653), (0d, 0.4884540817434532, -0.1355942806834117), (0d, 0.0000000, 0.2873965116730858))
    println(s"Cholesky Upper expected:")
    println(s"$choleskyUpperTriangularExpected")
    val choleskyLowerTriangularCalculated = utils.Statistics4MCMC.choleskyUpperTriangular(correlation)
    val choleskyUpperTriangularCalculated = utils.Statistics4MCMC.choleskyUpperTriangular(correlation)
    println(s"Cholesky Upper Triangular Calculated:")
    println(s"$choleskyUpperTriangularCalculated")
    val relTolerance = 0.0001
    val absTolerance = choleskyUpperTriangularExpected * relTolerance

  }

}
