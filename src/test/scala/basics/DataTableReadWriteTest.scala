package basics

import java.io.FileReader

import org.scalatest.FunSuite

import scala.collection.immutable.ListMap


/**
  * Created by peter on 06/06/17.
  */
class DataTableReadWriteTest extends FunSuite {

  test("testReadCsvWOHeader") {

    val colTypes: ListMap[String, _root_.scala.Double.type] = ListMap("nIter" -> Double, "mu" -> Double, "variance" -> Double, "S" -> Double, "nLL" -> Double)
    //    val colTypes: ListMap[String, _root_.scala.Double.type] = ListMap("nIter" -> Double, "mu" -> Double, "variance" -> Double, "S" -> Double, "nLL" -> Double, "n_accepted" -> Double)
    val name: String = "Posteriors"
    val file = new FileReader("/media/sf_data/nps409/Work2Sync/Projects/emp_dev_scala_runs/TAD_DATA_Synthetic_Out/QC_paper/QC_170213_test4CCode/Brazil_0_9_170207/c_Brazil_170502_1316/posteriorParams_c.csv_0.sample")
    val dt = DataTableReadWrite.readCsvWOHeaderByRow(name, file, colTypes)
    assert(false)
  }

  test("testReadCsvWOHeaderTmpBackup"){
    val colTypes: ListMap[String, _root_.scala.Double.type] = ListMap("nIter" -> Double, "mu" -> Double, "variance" -> Double, "S" -> Double, "nLL" -> Double)
    //    val colTypes: ListMap[String, _root_.scala.Double.type] = ListMap("nIter" -> Double, "mu" -> Double, "variance" -> Double, "S" -> Double, "nLL" -> Double, "n_accepted" -> Double)
    val name: String = "Posteriors"
    val file = new FileReader("/media/sf_data/nps409/Work2Sync/Projects/emp_dev_scala_runs/TAD_DATA_Synthetic_Out/QC_paper/QC_170213_test4CCode/Brazil_0_9_170207/c_Brazil_170502_1316/posteriorParams_c.csv_0.sample")
    val dt = DataTableReadWrite.readCsvWOHeaderByColumns(name, file, colTypes)
    assert(false)

  }

  test("testWriteCsv") {

  }

  test("testReadCsv") {

  }

}