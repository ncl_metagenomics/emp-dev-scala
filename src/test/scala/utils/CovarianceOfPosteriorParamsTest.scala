package utils

import java.io.{File, FileWriter}

import breeze.linalg.DenseMatrix
import org.scalatest.{BeforeAndAfterEach, FunSuite}
import shapeless.Succ
import utils.CSVWriter4Breeze.writeWithHeader
import utils.CovarianceOfPosteriorParams.getFileNamefCovarianceMatrix
import utils.MCMCIO.correlationAndCovarianceForCSVData

import scala.util.{Failure, Success, Try}

/**
  * Created by peter on 13/04/17.
  */
class CovarianceOfPosteriorParamsTest extends FunSuite with BeforeAndAfterEach {


  override def beforeEach() {

  }

  override def afterEach() {

  }

  test("testGetFileNamefCovarianceMatrix") {

    val postParamFileN = "/media/sf_data/nps409/Work2Sync/Projects/emp_dev_scala_runs/TAD_DATA_Synthetic_Out/Topsy/QC_Paper_170319_covar_hetero/GOS_90_170209/s-800K-lognormal-Pegasus-1.05-1.001-170327-1020_45/posterior-s-s-all-data_sampleSize_TAD_90_pc_D_8925.3_syntheticSampleSizes_lognormal_170209_0213-39_17-02-09_0219-42.csv_170327-1020_45.csv"
    //    val correlationAndCovariance = utils.MCMCIO.correlationAndCovarianceForCSVData(postParamFileN, colNames, numRows2Skip)
    //*****************************************
    val dataStr = basics.MCMC.DATE_STR2
    println(s"basics.MCMC.DATE_STRdate str = ${basics.MCMC.DATE_STR}")
    val outFN: String = utils.CovarianceOfPosteriorParams.getFileNamefCovarianceMatrix(postParamFileN, dataStr, nameSpecifier = "adjusted_partial")
    /*
    val dm = // ...
    breeze.linalg.csvwrite(new File("text.txt"), dm, separator = ' ')
     */

    println(s"---------------------------------------------------------------------")
    //------------------------------------------------------------------------
    val colNames = List[String]("mu", "variance", "S")
    val numRows2Skip = 10000
    //------------------------------------------------------
    //    val adjustedCovariance: DenseMatrix[Double] = utils.MCMCIO.getPartialAdjusteCovarinceFromPosteriors4Lognormal(postParamFileN, colNames, numRows2Skip)
    val adjCovTmp: Try[DenseMatrix[Double]] = utils.MCMCIO.getPartialAdjusteCovarinceFromPosteriors4Lognormal(postParamFileN, colNames, numRows2Skip)
    val adjustedCovariance = adjCovTmp match {
      case Success(adjustedCovariance: DenseMatrix[Double]) =>
        println(s"Number of data points used ")
        writeWithHeader(new java.io.FileWriter(outFN), adjustedCovariance, colNames, separator = ',')
        println(s"Writing output to  file : ${outFN}")
        Success(outFN)

      case Failure(e) => Failure(e)
    }

    println("==============================================================")
  }
}
