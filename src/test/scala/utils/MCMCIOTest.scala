package utils

import org.scalatest.FunSuite

/**
  * Created by peter on 21/04/17.
  */
class MCMCIOTest extends FunSuite {
test("absoluteToRelativePath"){
  val targetPath = "/var/data/stuff/xyz.dat"
  val basePath: String = "/var/data/alpha"
  val relDir: Option[String] = MCMCIO.absoluteToRelativePath(basePath,targetPath)
  println(s"Relative dir : ${relDir}")
}

}
