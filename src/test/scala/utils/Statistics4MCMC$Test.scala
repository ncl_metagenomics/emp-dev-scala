package utils

import breeze.linalg.{DenseMatrix, DenseVector, trace}
import org.scalatest.FunSuite
import utils._

/**
  * Created by peter on 10/03/17.
  */
class Statistics4MCMC$Test extends FunSuite {
  val fileName = "data/DemoData/correlation.csv"

  test("testReadMatrix3By3") {
    val mExpected = DenseMatrix((1d, 2d, 3d), (4d, 5d, 6d), (7d, 8d, 9d))
    val colTypes = Map("varA" -> Double, "varB" -> Double, "varS" -> Double)
    val res = utils.Statistics4MCMC.readMatrix3By3V2(fileName, colTypes)
    assert(res.equals(mExpected))
  }

  test("covarianceToCorrelationAndVariance") {
    val covarExpected = DenseMatrix((4d, 18d, 16d), (18d, 9d, 12d), (16d, 12d, 16d))
    val corrExpected = DenseMatrix((1d, 3d, 2d), (3d, 1d, 1d), (2d, 1d, 1d))
    val variance = DenseVector((2d,3d,4d))
    val (varV,corr) = utils.Statistics4MCMC.covarianceToCorrelationAndVariance(covarExpected)

    println("*************************")

    assert(corr.equals(corrExpected))
  }

  test("getAdustedCovariance"){
    val covarExpected = DenseMatrix((4d, 18d, 16d), (18d, 9d, 12d), (16d, 12d, 16d))
    val covarAdjustedExpected = DenseMatrix((24.249999999999996 , 44.319860108082466 , 39.395431207184416), (44.319860108082466 , 9d, 12d), (39.395431207184416, 12d, 16d))
    val (newVariance ,covarAdjustedCalculated) = utils.Statistics4MCMC.getAdustedAndScaledCovarianceForLognormal(covarExpected, 1d)
    assert(covarAdjustedCalculated.equals(covarAdjustedExpected))
  }

}
