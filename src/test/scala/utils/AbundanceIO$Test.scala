package utils

import java.io.File

import org.scalatest.{BeforeAndAfter, FeatureSpec, FlatSpec, GivenWhenThen}
import utils.sampling._

/**
  * Created by peter on 31/10/16.
  */
class AbundanceIO$Test extends FeatureSpec with GivenWhenThen {
  //"44xSRS711891_Abundance_Community_Synthetic_100.0pc_lognormal_161024-2234_41.csv"
  //New: 44xSRS711891_Abundance_Community_Synthetic_092.5pc_lognormal_161024-2234_41.csv"

  val (iVals, correctResult): (InitVals, String) = initVals()
  val correctResultFilePath: String = "." + File.separator + "44xSRS711891_Abundance_Community_Synthetic_092.5pc_lognormal_161024-2234_41.csv"

  case class InitVals(dataSetName: String = "44xSRS711891", dataFormat: String = "Abundance", scopeOfData: String = "Community", isGeneratedOrSynth: String = "Synthetic", coverage: Double = 1d, dateTimeStamp: String, fileNameSuffix: String = "csv", outDir: String = ".")

  def initVals(): (InitVals, String) = {
    val dataSetName: String = "44xSRS711891"
    val dataFormat: String = DataFormatInfo.ABUNDANCE.name //"Abundance"
    val scopeOfData: String = ScopeOfDataInfo.COMMUNITY.name //"Community"
    val isGeneratedOrSynth: String = RealOrSyntheticInfo.SYNTHETIC.name //"Synthetic"
    val coverage: Double = 0.925
    val fileNameSuffix = "csv"
    val correctResult: String = "44xSRS711891_Abundance_Community_Synthetic_092.5pc_lognormal_161024-2234_41.csv"
    (InitVals(dataSetName, dataFormat, scopeOfData, isGeneratedOrSynth, coverage, "161024-2234_41", fileNameSuffix), correctResult)
  }

  //  s"""AbundanceIO.outputFileName()""" should s"""return correct filename """ in {
  //    val (iVals,results)=initVals()
  //    val obtainedVal = AbundanceIO.outputFileName(iVals.dataSetName, iVals.dataFormat, iVals.scopeOfData,iVals.isGeneratedOrSynth,iVals.coverage,TADDistribution.LOGNORMAL.name)
  //    assertResult(results, s"whops!") {obtainedVal}
  //  }

  feature("Construct abundance file name") {
    scenario("name for community, lognormal") {
      Given(" AbundanceIO.outputFileName")
      When(s"inputs ${initVals()}")
      Then(s"get correct file name $correctResult ")
      val obtainedResult = AbundanceIO.outputFileName4CommunityData(iVals.dataSetName, iVals.dataFormat, iVals.scopeOfData, iVals.isGeneratedOrSynth, iVals.coverage, TADDistributionInfo.LOGNORMAL.name, iVals.dateTimeStamp)
      assertResult(correctResult, s"whops!") {
        obtainedResult
      }
    }
  }

  feature("Construct FileName with path") {
    scenario("") {
      val obtainedResult = AbundanceIO.getOutputFilePath(iVals.outDir, iVals.dataSetName, iVals.dataFormat, iVals.scopeOfData, iVals.isGeneratedOrSynth, iVals.coverage, TADDistributionInfo.LOGNORMAL.name, iVals.dateTimeStamp)
      assertResult(correctResultFilePath, s"whops!") {
        obtainedResult
      }
    }
  }


}
