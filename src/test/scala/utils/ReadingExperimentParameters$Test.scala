package utils

import org.scalatest.FunSuite
import utils.sampling.{FileFormatInfo, RealOrSyntheticInfo, ScopeOfDataInfo}
import utils.ReadingExperimentParameters.{ParametersNames => PN}

//utils.ReadingExperimentParameters.ParametersNames

/**
  * Created by peter on 02/11/16.
  */
class ReadingExperimentParameters$Test extends FunSuite {
  val dsNameR = "44xSRS711891"
  val correctResult = ""
  val testNamePrefix = "Testing input option "
  val dataFormat = "Abundance"
  val scopeOfData = ScopeOfDataInfo.COMMUNITY.name
  val generatedOrReal = RealOrSyntheticInfo.SYNTHETIC.name
  val coverageOfSpecies = 0.925
  val fileFormat = FileFormatInfo.CSV.name
  val distributionTAD = "lognormal"
  val meanTAD = 0.02
  val varianceTAD = 0.001
  val outputDir = "temp"
  val numberOfSpecies:Int = 11000
  val args = Array("--dataSetName", "44xSRS711891", "--dataFormat", dataFormat, "--scopeOfData", scopeOfData, "--realOrSynth", generatedOrReal, "--coverage", coverageOfSpecies.toString, "--fileFormat", fileFormat, "--distribution", distributionTAD, "--mean", meanTAD.toString, "--variance", varianceTAD.toString, s"--${PN.OUTPUT_DIRECTORY.synonyms(0)}", outputDir, s"--${PN.NUMBER_OF_SPECIES.synonyms(0)}",numberOfSpecies.toString )
  /**
    * ase class Parameters4FileName(dataSetName: String = "44xSRS711891", dataFormat: Option[String] = Some("Abundance"), scopeOfData: String = "Community", isGeneratedOrSynth: String = "Synthetic", coverage: Double = 1d, dateTimeStamp: String, fileNameSuffix: String = "csv", outDir: String = ".")
    */

  val dfR = ReadingExperimentParameters.getInputParams4SamplingCommunity4Test(args)

  val argsIG = Array[String]("","",
    "--in","data/DemoData/Brazil_0_9_170207/data_sampleSize_TAD_90_pc_D_24877.8_syntheticSampleSizes_lognormal_170207_1942-44_17-02-07_2010-22.sample",
    "-n","100000",
    "-d", "IG",
    "--varCovarFile","../../../Results_TAD_5_pc_v01/Canada_c_0.05_synthetic_S_170704_1818-16/varCovarMatrix_adjusted_partial_Canada_90_170207_170423_0943-23_R.csv"
  )

  val generalParameters = ReadingExperimentParameters.getInputGeneralParams4MCMCA(argsIG)

  print(args)

  var tName: String = s"""testNamePrefix  dataSetName =  "$dsNameR" """
  test(tName) {
    val nResult /*:(String,String)*/ = ReadingExperimentParameters.getInputParams4SamplingCommunity4Test(args)
    assertResult("44xSRS711891", "") {
      nResult._1
    }
  }

  test(testNamePrefix + "dataFormat = " + dataFormat) {
    //    val dfR = ReadingExperimentParameters.getInputParams4SamplingCommunity4Test(args)
    assertResult(dataFormat, "") {
      dfR._2
    }

  }

  test(testNamePrefix + "scopeOfData = " + scopeOfData) {
    val dfR = ReadingExperimentParameters.getInputParams4SamplingCommunity4Test(args)
    assertResult(scopeOfData, "") {
      dfR._3
    }

  }

  test(testNamePrefix + "generated  or real?  = " + generatedOrReal) {
    //    val dfR = ReadingExperimentParameters.getInputParams4SamplingCommunity4Test(args)
    assertResult(generatedOrReal, "") {
      dfR._4
    }

  }


  test(testNamePrefix + "coverage of species = " + coverageOfSpecies) {
    //    val dfR = ReadingExperimentParameters.getInputParams4SamplingCommunity4Test(args)
    assertResult(coverageOfSpecies, "") {
      dfR._5
    }
  }


  test(testNamePrefix + "TAD distribution = " + distributionTAD) {
    //    val dfR = ReadingExperimentParameters.getInputParams4SamplingCommunity4Test(args)
    assertResult(distributionTAD, "") {
      dfR._6
    }
  }

  test(testNamePrefix + "file format= " + fileFormat) {
    //    val dfR = ReadingExperimentParameters.getInputParams4SamplingCommunity4Test(args)
    assertResult(fileFormat, "") {
      dfR._7
    }
  }

  tName = s"""$testNamePrefix  output directory =  "$outputDir" """
  test(tName) {
    //    val dfR = ReadingExperimentParameters.getInputParams4SamplingCommunity4Test(args)
    assertResult(outputDir, "") {
      dfR._8
    }
  }

  test(testNamePrefix + "mean of TAD = " + meanTAD) {
    //    val dfR = ReadingExperimentParameters.getInputParams4SamplingCommunity4Test(args)
    assertResult(meanTAD, "") {
      dfR._9
    }
  }

  tName = s"""$testNamePrefix  variance of TAD =  "$varianceTAD" """
  test(tName) {
    //    val dfR = ReadingExperimentParameters.getInputParams4SamplingCommunity4Test(args)
    assertResult(varianceTAD, "") {
      dfR._10
    }
  }

  tName = s"""$testNamePrefix  variance of TAD =  "$numberOfSpecies" """
  test(tName) {
    //    val dfR = ReadingExperimentParameters.getInputParams4SamplingCommunity4Test(args)
    assertResult(varianceTAD, "") {
      dfR._10
    }
  }

  test("utils.ReadingExperimentParameters.getInputGeneralParams4MCMCA"){
    assertResult(generalParameters.tadFileName.getAbsolutePath,""){
      println(s"${generalParameters.toString}")
      println(s"+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
      println(s"${generalParameters}")
      println(s"+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
      println(s"+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
      println(s"+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
      println(s"tadFileName = ${generalParameters.tadFileName}")
      "/media/sf_data/nps409/Work2Sync/Projects/emp-dev-scala/data/DemoData/Brazil_0_9_170207/data_sampleSize_TAD_90_pc_D_24877.8_syntheticSampleSizes_lognormal_170207_1942-44_17-02-07_2010-22.sample"
    }

  }
}



