import org.joda.time.DateTime
import org.scalatest.FunSuite
import utils.DateTimeUtils

/**
  * import org.joda.time.DateTime
  * Created by peter on 12/12/16.
  */
class DateUtils$Test2 extends FunSuite {

  test("testSecondsToDHM") {
    val s = DateTime.now
    Thread.sleep(3600)
    val end = DateTime.now.plusHours(1).plusHours(2).plusMinutes(3)
    val res = DateTimeUtils.secondsToDHM(start = s)(end)
    val res2 = DateTimeUtils.durationToString(s)(end)
    println(s"ress = $res")
    println(s"res2 = $res2")
    println(s"***************************")

  }

  test("testDurationToString"){
    val s = DateTime.now
    Thread.sleep(2500)
    // 1000*60*10+1000*60*60*5)
    val end0 = DateTime.now
    val end2 = end0.plusDays(2).plusHours(3)
    val end = end2
    val getIntervalString =  DateTimeUtils.secondsToDHM(start = s)(_)
    val res = getIntervalString(end)//DateUtils.secondsToDHM(start = s)(end)
    val res2 = DateTimeUtils.durationToString(s)(end)
    println("testDurationToString")
    println(s"res = $res")
    println(s"res2 = $res2")
    println(s"***************************")

  }

}
