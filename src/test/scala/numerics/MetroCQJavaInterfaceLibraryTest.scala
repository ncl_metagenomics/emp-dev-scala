package numerics

import java.io.File

import basics.MetropolisHMCMC
import com.sun.jna.Pointer
import com.sun.jna.ptr.PointerByReference
import numerics.IGMetropolisMCMC.ParametersIG
import org.scalatest.{BeforeAndAfterEach, FunSuite}

import scala.util.{Failure, Success, Try}

class MetroCQJavaInterfaceLibraryTest extends FunSuite with BeforeAndAfterEach {

  override def beforeEach() {

  }

  override def afterEach() {

  }

  test("testGetCommandLineParams") {

  }

  test("testReadAbundanceData") {

  }


  test("testChao") {

  }

  test("testCompare_doubles") {

  }

  test("testF1") {

  }

  test("testNLogLikelihood") {

  }


  /**
    * Testing
    * MetroCQJavaInterfaceLibrary#negLogLikelihood(double, double, int, com.sun.jna.Pointer)
    */
  test("testLogLikelihood") {
    val fileNameOnlyTAD = "test.sample"
    val fileNameTAD = "src/main/resources/Data/test.sample"

    val data: Array[(Int, Int)] = (if (fileNameOnlyTAD.matches(""".*\.csv$""")) Success(MetropolisHMCMC.readDataASCSVTAD(fileNameTAD))

    else if (fileNameOnlyTAD.matches(""".*\.sample""")) Success(MetropolisHMCMC.readData(fileNameTAD))
    else Failure(new Exception("Input file must be *.csv or *.sample file"))).get

    println(s""" Input filename = $fileNameTAD""")

    val nD_Scala = MetropolisHMCMC.getD(data)
    val nL_Scala = MetropolisHMCMC.getL(data)

    //****************************************************************
    //**** Scala Code                                             ****
    //****************************************************************

    println(s"""nD_Scala = $nD_Scala """)
    println(s"""nL_Scala = $nL_Scala """)
    println(s""" Scala number of entries/rows  ${data.size} """)

    val params = ParametersIG(MetropolisHMCMC.getD(data) * 2, 2.2, 3.5)
    val negLogLScalaJNA: Try[Double] = numerics.IGMetropolisMCMC.negLogLikelihood(params)(data)
    println(s"""negLogLScalaJNA = ${negLogLScalaJNA}""")

    println(s""" ------------------------------- """)

    // ****************************************************************
    //**** C Code                                             ****
    //****************************************************************

    val cq: MetroCQJavaInterfaceLibrary = MetroCQJavaInterfaceLibrary.INSTANCE
    //**********************
    val f = new File(fileNameTAD)
    if (f.exists && !f.isDirectory) System.out.println("Hura file exists!")
    else System.out.println("File does not exist!")
    val dataC = new s_Data()
    cq.readAbundanceData(fileNameTAD, dataC)
    val nD_CQC = dataC.nL
    println(s"Data read in C code: nD_CQC = ${nD_CQC}")
    val nL_CQC = dataC.nJ
    println(s"Data read in C code (CQ paper names): nL_CQC = ${nL_CQC}")
    val numLines_DataFile = dataC.nNA
    println(s"numLines_DataFile = $numLines_DataFile")

    val dataF: PointerByReference = dataC.aanAbund;
    val dataF0: Pointer = dataF.getValue()
    var x0 = dataF0.getIntArray(0, 2)
    val negLogLikelihoodC = cq.negLogLikelihood(params.alpha, params.beta, params.nS, dataC.getPointer)
    println(s"""negLogLikelihoodC = ${negLogLikelihoodC}""")
    println(s""" ------------------------------- """)

  }


  test("testLogLikelihood01") {
    val fileNameOnlyTAD = "test01.sample"
    val fileNameTAD = "src/main/resources/Data/test01.sample"

    val data: Array[(Int, Int)] = (if (fileNameOnlyTAD.matches(""".*\.csv$""")) Success(MetropolisHMCMC.readDataASCSVTAD(fileNameTAD))

    else if (fileNameOnlyTAD.matches(""".*\.sample""")) Success(MetropolisHMCMC.readData(fileNameTAD))
    else Failure(new Exception("Input file must be *.csv or *.sample file"))).get

    println(s""" Input filename = $fileNameTAD""")

    val nD_Scala = MetropolisHMCMC.getD(data)
    val nL_Scala = MetropolisHMCMC.getL(data)

    //****************************************************************
    //**** Scala Code                                             ****
    //****************************************************************

    println(s"""nD_Scala = $nD_Scala """)
    println(s"""nL_Scala = $nL_Scala """)
    println(s""" Scala number of entries/rows  ${data.size} """)

    val params = ParametersIG(MetropolisHMCMC.getD(data) * 2, 2.2, 3.5)
    val negLogLScalaJNA: Try[Double] = numerics.IGMetropolisMCMC.negLogLikelihood(params)(data)
    println(s"""negLogLScalaJNA = ${negLogLScalaJNA}""")

    println(s""" ------------------------------- """)

    // ****************************************************************
    //**** C Code                                             ****
    //****************************************************************

    val cq: MetroCQJavaInterfaceLibrary = MetroCQJavaInterfaceLibrary.INSTANCE
    //**********************
    val f = new File(fileNameTAD)
    if (f.exists && !f.isDirectory) System.out.println("Hura file exists!")
    else System.out.println("File does not exist!")

    val dataC = new s_Data()
    cq.readAbundanceData(fileNameTAD, dataC)
    val nD_CQC = dataC.nL
    println(s"Data read in C code: nD_CQC = ${nD_CQC}")
    val nL_CQC = dataC.nJ
    println(s"Data read in C code (CQ paper names): nL_CQC = ${nL_CQC}")
    val numLines_DataFile = dataC.nNA
    println(s"numLines_DataFile = $numLines_DataFile")

    val dataF: PointerByReference = dataC.aanAbund;
    val dataF0: Pointer = dataF.getValue()
    var x0 = dataF0.getIntArray(0, 2)
    val negLogLikelihoodC = cq.negLogLikelihood(params.alpha, params.beta, params.nS, dataC.getPointer)
    println(s"""negLogLikelihoodC = ${negLogLikelihoodC}""")
    println(s""" ------------------------------- """)

  }


  test("testLogLikelihood02") {
    val fileNameOnlyTAD = "test02.sample"
    val fileNameTAD = "src/main/resources/Data/test02.sample"

    val data: Array[(Int, Int)] = (if (fileNameOnlyTAD.matches(""".*\.csv$""")) Success(MetropolisHMCMC.readDataASCSVTAD(fileNameTAD))
    else if (fileNameOnlyTAD.matches(""".*\.sample""")) Success(MetropolisHMCMC.readData(fileNameTAD))
    else Failure(new Exception("Input file must be *.csv or *.sample file"))).get

    println(s""" Input filename = $fileNameTAD""")

    val nD_Scala = MetropolisHMCMC.getD(data)
    val nL_Scala = MetropolisHMCMC.getL(data)

    //****************************************************************
    //**** Scala Code                                             ****
    //****************************************************************

    println(s"""nD_Scala = $nD_Scala """)
    println(s"""nL_Scala = $nL_Scala """)
    println(s""" Scala number of entries/rows  ${data.size} """)

    val params = ParametersIG(MetropolisHMCMC.getD(data) * 2, 2.2, 3.5)
    val negLogLScalaJNA: Try[Double] = numerics.IGMetropolisMCMC.negLogLikelihood(params)(data)
    println(s"""negLogLScalaJNA = ${negLogLScalaJNA}""")

    println(s""" ------------------------------- """)

    // ****************************************************************
    //**** C Code                                             ****
    //****************************************************************

    val cq: MetroCQJavaInterfaceLibrary = MetroCQJavaInterfaceLibrary.INSTANCE
    //**********************
    val f = new File(fileNameTAD)
    if (f.exists && !f.isDirectory) System.out.println("Hura file exists!")
    else System.out.println("File does not exist!")

    println(s""" Input filename = $fileNameTAD""")
    val dataC = new s_Data()
    cq.readAbundanceData(fileNameTAD, dataC)
    val nD_CQC = dataC.nL
    println(s"Data read in C code: nD_CQC = ${nD_CQC}")
    val nL_CQC = dataC.nJ
    println(s"Data read in C code (CQ paper names): nL_CQC = ${nL_CQC}")
    val numLines_DataFile = dataC.nNA
    println(s"numLines_DataFile = $numLines_DataFile")

    val dataF: PointerByReference = dataC.aanAbund;
    val dataF0: Pointer = dataF.getValue()
    var x0 = dataF0.getIntArray(0, 2)
    val negLogLikelihoodC = cq.negLogLikelihood(params.alpha, params.beta, params.nS, dataC.getPointer)
    println(s"""negLogLikelihoodC = ${negLogLikelihoodC}""")
    println(s""" ------------------------------- """)

  }

  test("testMinimiseSimplex") {

  }

  test("testFreeAbundanceData") {

  }

  test("testNegLogLikelihood") {

  }

  test("testOutputResults") {

  }

  test("testGetProposal") {

  }

  test("testMetropolis") {

  }

  test("testSimpsons") {

  }

  test("testMcmc") {

  }

  test("testBesselExt") {

  }

}
