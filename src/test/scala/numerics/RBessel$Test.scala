package numerics


import org.scalatest._

/**
  * Created by peter on 17/02/17.
  */
class RBessel$Test extends FunSuite with BeforeAndAfterEach with Matchers with
OptionValues with Inside with Inspectors{

  val relTollerance: Double = 0.000001

  override def beforeEach() {

  }

  override def afterEach() {

  }


  test("RBesselk0 for value  1") {
    val expected = 0.4210244
    var x = 1
    val tolerance = Bessel.besselk0(x)*relTollerance
    RBessel.k0(x) should equal (expected +- tolerance)
  }



}
