package numerics

import breeze.stats.distributions.Uniform
import org.scalacheck.{Gen, Prop}
import org.scalatest
import org.scalatest._
import org.scalatest.prop.{GeneratorDrivenPropertyChecks, PropertyChecks}
import org.scalatest.matchers._
import org.scalatest.prop.TableDrivenPropertyChecks._


/**
  * Created by peter on 17/02/17.
  */
class Bessel$k0$Test extends FunSuite with BeforeAndAfterEach with org.scalatest.Matchers with
  OptionValues with Inside with Inspectors with GeneratorDrivenPropertyChecks {


  override def beforeEach() {

  }

  override def afterEach() {

  }


  test("k0 for value  1") {
    val rk0_x1 = 0.4210244
    var x = 1
    val tolerance = Bessel.besselk0(x) * 0.000001
    Bessel.besselk0(x) should equal(rk0_x1 +- tolerance)
  }


  test("k0 for value  1.5") {
    val rk0 = 0.2138056
    var x = 1.5
    val sk0 = Bessel.besselk0(x)
    val tolerance = sk0 * 0.000001
    println(s"x =  $x, BesselK0 = $sk0, RBesselk0 = $rk0")
    sk0 should equal(rk0 +- tolerance)
  }

  test("k0 for random values compare with Bessel used by R package 'Bessel. Testing Using genrator of values from ScalaCheck.'") {
    val relativetolerance = 0.000001

    val arguments1: Gen[Double] = Gen.choose(Double.MinPositiveValue, 750)
    //    val posDouble: Gen[Double] = Gen.choose(0.00000000000000000000000000000000000001, 750)
    //val p = Prop.forAll(posDouble)(x => {RBessel.k0(x)*(1+relativetolerance)>Bessel.k0(x)} )
    //    p.check
    val args = arguments1
    println(s"Min: ${Double.MinPositiveValue}")
    println(s"Max: ${Double.MaxValue}")
    implicit val generatorDrivenConfig = PropertyCheckConfig(minSuccessful=50)
    val minNumTestVals = generatorDrivenConfig.minSuccessful
    println(s"Testing for ${minNumTestVals} values")

    println(s"sssss")
    GeneratorDrivenPropertyChecks.forAll(arguments1) { (x: Double) =>
      //if  (x >= 0) {
      whenever(x > Double.MinPositiveValue) {
        x > 0
        val rk0 = RBessel.k0(x)
        val k0 = Bessel.besselk0(x)
        val tolerance0 = rk0 * relativetolerance
        val tolerance = if (tolerance0 > 0) tolerance0 else Double.MinPositiveValue
        println(s"x = $x, Besselk0=$k0, RBesselk0=$rk0, tollerence = $tolerance")
        k0 should equal(rk0 +- tolerance)
      }

    }

//    GeneratorDrivenPropertyChecks.forAll(posDouble) { (x: Double) =>
//      //if  (x >= 0) {
//      whenever(x > Double.MinPositiveValue) {
//        x > 0
//        val rk0 = RBessel.k0(x)
//        val k0 = Bessel.k0(x)
//        val tolerance0 = rk0 * relativetolerance
//        val tolerance = if (tolerance0 > 0) tolerance0 else Double.MinPositiveValue
//        println(s"x = $x, Besselk0=$k0, RBesselk0=$rk0, tollerence = $tolerance")
//        k0 should equal(rk0 +- tolerance)
//      }
//
//    }

  }

  test("k0 for random values compare with Bessel used by R package 'Bessel. Randomly sampled arguments.'") {
    val relativetolerance = 0.000001

    val unif = new Uniform(Double.MinPositiveValue + 10, 700)

    val uniformVals = unif.sample(100)

    val uniformValsList: List[Double] = uniformVals.toList

    val t = new org.scalatest.prop.TableFor1("x", uniformValsList: _*)

    println(s"Min: ${Double.MinPositiveValue}")
    println(s"Max: ${Double.MaxValue}")
    println(s"Testing for ${t.size} values")
    org.scalatest.prop.TableDrivenPropertyChecks.forAll(t) { (x: Double) =>
      //if  (x >= 0) {
      whenever(x > Double.MinPositiveValue) {
        x > 0
        val rk0 = RBessel.k0(x)
        val k0 = Bessel.besselk0(x) //Bessel.k0(x)
        val tolerance0 = rk0 * relativetolerance
        val tolerance = if (tolerance0 > 0) tolerance0 else Double.MinPositiveValue
        println(s"x = $x, Besselk0=$k0, RBesselk0=$rk0, tollerence = $tolerance")
        k0 should equal(rk0 +- tolerance)
      }

    }

  }


  test("k0 for random values compare with Bessel used by R package 'Bessel. Hand picked values.'") {
    val relativetolerance = 0.000001

    val uniformValsList: List[Double] = List(Double.MinPositiveValue * 10, 0.00000001, 0.1, 1.5, 110, 560, 700, 800, 895, 15954, 4546446)

    val t = new org.scalatest.prop.TableFor1("x", uniformValsList: _*)

    println(s"Min: ${Double.MinPositiveValue}")
    println(s"Max: ${Double.MaxValue}")
    println(s"Testing for ${t.size} values")
    println(s"sssss")
    org.scalatest.prop.TableDrivenPropertyChecks.forAll(t) { (x: Double) =>
      //if  (x >= 0) {
      whenever(x > Double.MinPositiveValue) {
        x > 0
        val rk0 = RBessel.k0(x)
        val k0 = Bessel.besselk0(x)
        val tolerance0 = rk0 * relativetolerance
        val tolerance = if (tolerance0 > 0) tolerance0 else Double.MinPositiveValue
        println(s"x = $x, Besselk0=$k0, RBesselk0=$rk0, tollerence = $tolerance")
        k0 should equal(rk0 +- tolerance)
      }

    }

  }


}


// *******************************************************************************************************************
// *******************************************************************************************************************
// *******************************************************************************************************************

