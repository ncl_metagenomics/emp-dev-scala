package numerics

import org.scalacheck.Gen
import org.scalatest.{BeforeAndAfterEach, FunSuite}

class GSLLibCTest extends FunSuite with BeforeAndAfterEach {

  override def beforeEach() {

  }

  override def afterEach() {

  }

  test("testGsl_sf_bessel_Knu") {

  }

  test("testGsl_sf_bessel_lnKnu") {

  }

  test("testGsl_sf_lnfact") {
    import org.scalatest.prop.GeneratorDrivenPropertyChecks._
    import org.scalactic.Explicitly._
    import org.scalatest.Matchers._

    val nGen = Gen.choose[Int](1, 100) // for (alpha <- Gen.choose[Double](0, 10000)) yield  alpha

    var n0: Int = 10
    var log10FactGSL = GSLLibC.Instance.gsl_sf_lnfact(n0)
    var log10FactApache = org.apache.commons.math3.util.CombinatoricsUtils.factorialLog(n0)

//    assert(log10FactGSL === log10FactApache)
    log10FactGSL should be (log10FactApache +- 1E-13)

    forAll((nGen, "n")) { (n: Int) =>
      whenever(n > 0) {
        val log10FactGSL = GSLLibC.Instance.gsl_sf_lnfact(n)
        val log10FactApache = org.apache.commons.math3.util.CombinatoricsUtils.factorialLog(n)
        log10FactGSL should be (log10FactApache +- 1E-13)
        //        assert(log10FactGSL === log10FactApache)
        // log10FactApache
      }


    }

  }

  test("testGsl_sf_fact") {

  }

  test("testPuts") {

  }

  test("testGsl_sf_bessel_Knu_scaled") {

  }

}
