package numerics

import org.apache.commons.math3.fraction.Fraction
import org.scalatest._
import org.scalacheck.Prop._
import org.scalacheck._
import org.scalatest.prop.PropertyChecks

import scala.util.{Success, Failure, Try}

class MetroIGCQLibCTest extends FunSuite with Matchers with OptionValues with Inside with Inspectors with PropertyChecks {

  test("besselFncForMetroIG") {
    var resCQ: Double = MetroIGCQLibC.Instance.besselExt(1, 2.2, 3.1)
    var resScalaWithGSLJNATry: Try[Double] = BesselCQ.calculate(1, 2.2, 3.1)
    var resScalaWithGSLJNA = resScalaWithGSLJNATry.get
    println(s"resCQ = $resCQ")
    println(s"resScalaWithJNA = $resScalaWithGSLJNA")

    assert(resCQ === resScalaWithGSLJNA)
  }


  test("gg") {
    import org.scalacheck._
    import org.scalacheck.Gen
    import org.scalatest.prop.Whenever
    import org.scalatest.enablers.WheneverAsserting._

    implicit val generatorDrivenConfig = PropertyCheckConfig(minSize = 1, maxSize = 5)
    val freqVal = Gen.choose[Int](1, 100000) // for (n <- Gen.choose[Int](0, 10000)) yield  n
    val alphaVal = Gen.choose[Double](0.12, 100000) // for (alpha <- Gen.choose[Double](0, 10000)) yield  alpha
    val betaVal = Gen.choose[Double](0.1, 100000) //  for (beta <- Gen.choose[Double](0, 10000)) yield  beta

    forAll((freqVal, "n"), (alphaVal, "alpha"), (betaVal, "beta"), minSuccessful(100), maxDiscardedFactor(0.1)) { (n: Int, alpha: Double, beta: Double) => {
      println("----------------------------------------------------------------------------------------")
      println(s"n = $n , alpha = $alpha, beta = $beta ")
      println("----------------------------------------------------------------------------------------")
      val besselCQ_calculateWrapped: Try[Double] = BesselCQ.calculate(n, alpha, beta)
      val besselCQ_calculate: Double = besselCQ_calculateWrapped match {
        case Success(x: Double) => x
        case Failure(e) => println(e.getMessage); Double.NaN
      }
      whenever(n>0 && !besselCQ_calculate.isNaN && !besselCQ_calculate.isInfinite) {
        val metroIGCQLibC_besselExt: Double = MetroIGCQLibC.Instance.besselExt(n, alpha, beta)
        println("=====================================================================")
        println(s"metroIGCQLibC_besselExt = $metroIGCQLibC_besselExt")
        println(s"besselCQ_calculate =      $besselCQ_calculate")
        println("=====================================================================")
        assert(metroIGCQLibC_besselExt === besselCQ_calculate)
      }
    }
    }
  }
}

//
//  class Fraction(n: Int, d: Int) {
//
//    require(d != 0)
//    require(d != Integer.MIN_VALUE)
//    require(n != Integer.MIN_VALUE)
//
//    val numer = if (d < 0) -1 * n else n
//    val denom = d.abs
//
//    override def toString = numer + " / " + denom
//  }