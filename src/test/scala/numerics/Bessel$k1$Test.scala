package numerics

import breeze.stats.distributions.Uniform
import org.scalacheck.{Gen, Prop}
import org.scalatest
import org.scalatest._
import org.scalatest.prop.{GeneratorDrivenPropertyChecks, PropertyChecks}
import org.scalatest.matchers._
import org.scalatest.prop.TableDrivenPropertyChecks._


/**
  * Created by peter on 17/02/17.
  */
class Bessel$k1$Test extends FunSuite with BeforeAndAfterEach with org.scalatest.Matchers with
  OptionValues with Inside with Inspectors with GeneratorDrivenPropertyChecks {

  override def beforeEach() {

  }

  override def afterEach() {

  }

  test("k1 for random values compare with Bessel used by R package Bessel. Randomly sampled arguments.") {
    val relativetolerance = 0.000001
    val unif = new Uniform(Double.MinPositiveValue + 10, 700)
    val uniformVals = unif.sample(100)
    val t = new org.scalatest.prop.TableFor1("x", uniformVals: _*)

    println(s"Min: ${Double.MinPositiveValue}")
    println(s"Max: ${Double.MaxValue}")
    println(s"Testing for ${t.size} values")
    org.scalatest.prop.TableDrivenPropertyChecks.forAll(t) { (x: Double) =>
      //if  (x >= 0) {
      whenever(x > Double.MinPositiveValue) {
        x > 0
        val rk0 = RBessel.k1(x)
        val k0 = Bessel.besselk1(x)//Bessel.k1(x)
        val tolerance0 = rk0 * relativetolerance
        val tolerance = if (tolerance0 > 0) tolerance0 else Double.MinPositiveValue
        println(s"x = $x, Besselk0=$k0, RBesselk0=$rk0, tollerence = $tolerance")
        k0 should equal(rk0 +- tolerance)
      }
    }

    val unif2 = new Uniform(Double.MinPositiveValue , 0.01)
    val uniformVals2 = unif2.sample(100)
    val t2 = new org.scalatest.prop.TableFor1("x", uniformVals2: _*)

    println(s"Testing for ${t.size} values")
    org.scalatest.prop.TableDrivenPropertyChecks.forAll(t2) { (x: Double) =>
      //if  (x >= 0) {
      whenever(x >= Double.MinPositiveValue) {
        x > 0
        val rk0 = RBessel.k1(x)
        val k0 = Bessel.besselk1(x)
        val tolerance0 = rk0 * relativetolerance
        val tolerance = if (tolerance0 > 0) tolerance0 else Double.MinPositiveValue
        println(s"x = $x, Besselk0=$k0, RBesselk0=$rk0, tollerence = $tolerance")
        k0 should equal(rk0 +- tolerance)
      }
    }

  }
}