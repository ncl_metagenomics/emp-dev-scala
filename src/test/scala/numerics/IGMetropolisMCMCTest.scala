package numerics

import numerics.IGMetropolisMCMC.ParametersIG
import org.scalacheck.Gen
import org.scalatest.{BeforeAndAfterEach, FunSuite, Matchers}

import scala.util.Try


class IGMetropolisMCMCTest extends FunSuite with BeforeAndAfterEach with Matchers {

  override def beforeEach() {

  }

  override def afterEach() {

  }

  test("testMain") {

  }

  test("testLogMeassure") {
    import org.scalatest.prop.GeneratorDrivenPropertyChecks._
    var n = 1
    var alpha = 2.5
    var beta = 3.1
    var data: Array[(Int, Int)] = Array((1,10 ),(2,5))

    val param: ParametersIG = new ParametersIG(n,alpha,beta)
    //todo:171021 work here
    val logLScalaJNA:Try[Double] = numerics.IGMetropolisMCMC.nLogLikelihood(param)(data)
    val logLCQJNA = numerics.MetroIgCQJna.logLikelihood(n, alpha, beta)

  }

  test("logLikelihood") {
    import org.scalatest.prop.GeneratorDrivenPropertyChecks._
    var n = 1
    var alpha = 2.5
    var beta = 3.1
    var params = ParametersIG(n, alpha, beta)
    val logLikelihood_Scala = numerics.IGMetropolisMCMC.logLikelihood(n,params)
    val metroIgCQJna_logLikelihood = numerics.MetroIgCQJna.logLikelihood(n, alpha, beta)

    logLikelihood_Scala.getOrElse(Double.NaN) should be(metroIgCQJna_logLikelihood  +- 1E-13)

    val nVals = for (n <- Gen.choose[Int](1, 100000)) yield n
    val alphaVals = for (alpha <- Gen.choose[Double](0.001, 100000)) yield alpha
    val betaVals = for (beta <- Gen.choose[Double](0.001, 1000000)) yield beta

    forAll(nVals, alphaVals, betaVals) { (n: Int, alpha: Double, beta: Double) =>
      params = ParametersIG(n, alpha, beta)
      val logLikelihood_Scala: Try[Double] = numerics.IGMetropolisMCMC.logLikelihood(n,params)
      //      println("=====================================================================")
      //      println(s"n = $n, alpha = $alpha, beta = $beta")
      //      println(s"logLikelihood_Scala = $logLikelihood_Scala")
      //      println("=====================================================================")
      whenever(n>0 && !logLikelihood_Scala.getOrElse(Double.NaN).isInfinite && !logLikelihood_Scala.getOrElse(Double.NaN).isNaN && alpha>0 && beta>0){
        println(s"n = $n, alpha = $alpha, beta = $beta")
        val metroIgCQJna_logLikelihood = numerics.MetroIgCQJna.logLikelihood(n, params.alpha, params.beta)
        println("=====================================================================")
        println(s"logLikelihood_Scala = $logLikelihood_Scala")
        println(s"metroIgCQJna_logLikelihood =      $metroIgCQJna_logLikelihood")
        println("=====================================================================")
        logLikelihood_Scala.getOrElse(Double.NaN) should be(metroIgCQJna_logLikelihood  +- 1E-9)
      }

    }

  }

}
