package basics
import org.scalatest.FunSpec
import org.scalatest.matchers._
import org.scalatest.Matchers

import org.apache.commons.math3._
import org.apache.commons.math3.util._
import org.apache.commons.math3.special.Gamma
import org.apache.commons.math3.analysis.solvers._
import org.apache.commons.math3.analysis.differentiation._
import org.apache.commons.math3.util.CombinatoricsUtils._
import Math.log
import Math.PI
import Math.pow

import com.typesafe.scalalogging.{LazyLogging, Logger}


import scala.annotation.tailrec
import org.slf4j.LoggerFactory

/**
 * @author peter
 */
class LogLikelihoodLogNormalTest extends FunSpec with Matchers {
  describe("LogLikelihoodLogNormal") {    
    it("Check log likelihood value with Quince's code for specific n = 2") {
      var n = 2
      var mu = 1
      var sigma = 1
      val llLogN = new LogLikelihoodPoissonLogNormal()
      val res2 = llLogN.calculateWithRLikeScala(mu, sigma, n)
      //println("Result ", res2)
      res2 should be(-1.9179227050603647 +- 0.0000001)
    }

    
    
    it("Check log likelihood value with Quince's code for specific n = 1" ) {
      var n = 1
      var mu = 1
      var sigma = 1
      val llLogN = new LogLikelihoodPoissonLogNormal()
      val res1 = llLogN.calculateWithRLikeScala(mu, sigma, n)
      //println("Result ", res1)
      res1 should be(-1.7387870759784998 +- 0.0000001)
    }

    
    it("Check log likelihood value with Quince's code for specific n = 3") {
      var n = 3
      var mu = 1
      var sigma = 1
      val llLogN = new LogLikelihoodPoissonLogNormal()
      val res3 = llLogN.calculateWithRLikeScala(mu, sigma, n)
      //println("Result ", res3)
      res3 should be(-2.1781044772581319 +- 0.0000001)
    }
 
    it("Check log likelihood value got from (Rampal) for n>100 version in  Quince's code for specific n = 3") {
      var n = 110
      var mu = -1.78
      var varianceLnX = 25.0
      val llLogN = new LogLikelihoodPoissonLogNormal()
      val res3 = llLogN.calculateWithApproximation(mu, varianceLnX, n)
      //println("Result ", res3)
      res3 should be(-8.0674958791310569 +- 0.0000001)
    } 
  }

  //  it("Check log likelihood value") {
  //    val llLogN = new LogLikelihoodLogNormal()
  //    val res1 = llLogN.calculate(-1.78, 6, 10)
  //    //println("Result ", res1)
  //    res1 should be(5)
  //  }

  describe("Newton-Raphson solver") {
    var n = 1
    var mu = 1
    var sigma = 1
    var U = 0.5
    val solver = new NewtonRaphsonSolver(1E-10)
    it("Newto-Raphson Solver") {
      val derivExp = new DerivExponent(n, mu, sigma)
      val argDistributionMax = solver.solve(1000000, derivExp, 0, U) // maximum of distribution
      argDistributionMax should be(0.44285440100219348 +- 0.0000001)
    }
  }  
}



case class DerivExponent(n: Int, mu: Double, sigma: Double) extends UnivariateDifferentiableFunction {
  def value(y: Double): Double = {
    n * y - Math.exp(y) + (y - mu) / (sigma * sigma)
  }
  /**
   *  f(y)=n-exp(y)-(y-mi)/(sigma*sigma)
   *  d/dy(f(y) = -exp(y)-1/(sigma*sigma)
   *
   */
  def value(t: DerivativeStructure): DerivativeStructure = {
    t.exp().multiply(-1).add(n).subtract(t.subtract(mu).divide(sigma * sigma))
  }
}