package basics
/**
 * @author peter
 */

import org.scalatest.FunSpec
import org.scalatest.matchers._
import org.scalatest.Matchers
import org.apache.commons.math3.special.Gamma
import org.apache.commons.math3.analysis.solvers._
import org.apache.commons.math3.analysis.differentiation._

// -in "../Data/Illinois.sample" -out "../Output/Illinois.out" -s 1

class NegLogLikelihoodTest extends FunSpec with Matchers {
  describe("NegLogLikelihoodTest") {
    it("Negative log likelihood value for 4 data records") {
      val mcmcMetro = new MetropolisHMCMC()
      //val fileName = "/media/sf_data/nps409/Other/Dropbox/Newcastle-Project/src/Projects/Quince-C/Data//test01.sample"
      //var data = mcmcMetro.readData(fileName)
      val data = Array((4, 4), (1, 1466), (2, 450), (3, 268), (4, 171))
      val mu = 1
      val sigma = Math.sqrt(1)
      val nL = MetropolisHMCMC.getD(data.drop(1))
      val s = 4710 //nL*2
      val res = mcmcMetro.nLogLikelihood(mu, sigma, s, data, nL)
//      println("nLogLikelihood = " + res)
      res should be(3056.714655927346 +- 0.001) //70.0
    }

    it("Negative log likelihood value for 3 data records") {
      val mcmcMetro = new MetropolisHMCMC()
      //val fileName = "/media/sf_data/nps409/Other/Dropbox/Newcastle-Project/src/Projects/Quince-C/Data//test01.sample"
      //var data = mcmcMetro.readData(fileName)
      val data = Array((3, 3), (1, 1466), (2, 450), (3, 268))
      val mu = 1.2
      val varianceLnX =(1.2)
      val nL = MetropolisHMCMC.getD(data.drop(1))
      val s = 4568 //nL*2
      val res = mcmcMetro.nLogLikelihood(mu, varianceLnX, s, data, nL)
      //println("nLogLikelihood = " + res)
      res should be(4044.8637565478639 +- 0.001) //70.0
    }

    it("Negative log likelihood value for 2 data records") {
      val mcmcMetro = new MetropolisHMCMC()
      //val fileName = "/media/sf_data/nps409/Other/Dropbox/Newcastle-Project/src/Projects/Quince-C/Data//test01.sample"
      //var data = mcmcMetro.readData(fileName)
      val data = Array((2, 2), (1, 1466), (2, 450))
      val mu = 1
      val varianceLnX = 1 //Math.sqrt(7.777415993673233)
      val nL = MetropolisHMCMC.getD(data.drop(1))
      val s = 3832 //nL*2
      val res = mcmcMetro.nLogLikelihood(mu, varianceLnX, s, data, nL)
      //println("nLogLikelihood = " + res)
      res should be(3267.23205177618 +- 0.001) //70.0
    }

    it("Negative log likelihood value for single data record") {
      val mcmcMetro = new MetropolisHMCMC()
      //val fileName = "/media/sf_data/nps409/Other/Dropbox/Newcastle-Project/src/Projects/Quince-C/Data//test01.sample"
      //var data = mcmcMetro.readData(fileName)
      val data = Array((1, 1), (1, 1466))
      val mu = 1
      val sigma = 1 //Math.sqrt(7.777415993673233)
      val nL = MetropolisHMCMC.getD(data.drop(1))
      val s = 2932 //nL*2
      val res = mcmcMetro.nLogLikelihood(mu, sigma, s, data, nL)
      //println("nLogLikelihood = " + res)
      res should be(3235.245821842298 +- 0.001) //70.0
    }

    it("Negative log likelihood value for Brazil dataset 2") {
      val mcmcMetro = new MetropolisHMCMC()
      //val fileName = "/media/sf_Newcastle-Project/src/Projects/Quince-C/Data/Brazil.sample"
      val fileName = "data/Brazil.sample"
      var data = MetropolisHMCMC.readData(fileName)
      //val data = Array((1,1),(1,1466))
      val mu = -1.9828338834052317
      val varianceLnX = (5.9900819821872426)
      val nL = MetropolisHMCMC.getD(data.drop(1))
      val s = 8603 //nL*2
      val res = mcmcMetro.nLogLikelihood(mu, varianceLnX, s, data, nL)
      //println("nLogLikelihood = " + res)
      res should be(363.558 +- 0.001) //70.0
    }

    it("Negative log likelihood value for Brazil dataset 01") {
      val mcmcMetro = new MetropolisHMCMC()
      //val fileName = "/media/sf_Newcastle-Project/src/Projects/Quince-C/Data/Brazil.sample"
      val fileName = "data/Brazil.sample"
      var data = MetropolisHMCMC.readData(fileName)
      //val data = Array((1,1),(1,1466))
      val mu = -1.7828338834052317
      val varianceLnX = (6.1900819821872428)
      val nL = MetropolisHMCMC.getD(data.drop(1))
      val s = 8803 //nL*2
      val res = mcmcMetro.nLogLikelihood(mu, varianceLnX, s, data, nL)
      //println("nLogLikelihood = " + res)
      res should be(330.96397501573665 +- 0.000000001) //70.0
    }

    it("Negative log likelihood value for Brazil dataset 02") {
      val mcmcMetro = new MetropolisHMCMC()
      //val fileName = "/media/sf_Newcastle-Project/src/Projects/Quince-C/Data/Brazil.sample"
      val fileName = "data/Brazil.sample"
      var data = MetropolisHMCMC.readData(fileName)
      //val data = Array((1,1),(1,1466))
      val mu = -1.792677583405232
      val varianceLnX = (6.2056788821872431)
      val nL = MetropolisHMCMC.getD(data.drop(1))
      val s = 8800 //nL*2
      val res = mcmcMetro.nLogLikelihood(mu, varianceLnX, s, data, nL)
      //println("nLogLikelihood = " + res)
      res should be(330.99368778413918 +- 0.000000001)
    }

    it("Negative log likelihood value for Brazil dataset 03") {
      val mcmcMetro = new MetropolisHMCMC()
      //val fileName = "/media/sf_Newcastle-Project/src/Projects/Quince-C/Data/Brazil.sample"
      val fileName = "data/Brazil.sample"
      var data = MetropolisHMCMC.readData(fileName)
      //val data = Array((1,1),(1,1466))
      val mu = -1.8025212834052322
      val varianceLnX = (6.2212757821872433)
      val nL = MetropolisHMCMC.getD(data.drop(1))
      val s = 8797 //nL*2
      val res = mcmcMetro.nLogLikelihood(mu, varianceLnX, s, data, nL)
      //println("nLogLikelihood = " + res)
      res should be(331.08250632768613 +- 0.000000001)
    }

    it("Negative log likelihood value for Brazil dataset 04") {
      val mcmcMetro = new MetropolisHMCMC()
      //val fileName = "/media/sf_Newcastle-Project/src/Projects/Quince-C/Data/Brazil.sample"
      val fileName = "data/Brazil.sample"
      var data = MetropolisHMCMC.readData(fileName)
      //val data = Array((1,1),(1,1466))
      val mu = -1.792677583405232
      val varianceLnX = (6.2056788821872431)
      val nL = MetropolisHMCMC.getD(data.drop(1))
      val s = 8800 //nL*2
      val res = mcmcMetro.nLogLikelihood(mu, varianceLnX, s, data, nL)
      //println("nLogLikelihood = " + res)
      res should be(330.99368778413918 +- 0.000000001)
    }

    it("Negative log likelihood value for Brazil dataset 05") {
      val mcmcMetro = new MetropolisHMCMC()
      val fileName = "data/Brazil.sample"
      var data = MetropolisHMCMC.readData(fileName)
      val mu = -1.792677583405232
      val varianceLnX = (6.2056788821872431)
      val nL = MetropolisHMCMC.getD(data.drop(1))
      val s = 8800
      val res = mcmcMetro.nLogLikelihood(mu, varianceLnX, s, data, nL)
      //println("nLogLikelihood = " + res)
      res should be(330.99368778413918 +- 0.000000001)
    }

    it("Negative log likelihood value for Brazil dataset 06") {
      val mcmcMetro = new MetropolisHMCMC()
      val fileName = "data/Brazil.sample"
      var data = MetropolisHMCMC.readData(fileName)
      val mu = -1.8025212834052322
      val varianceLnX = (6.2212757821872433)
      val nL = MetropolisHMCMC.getD(data.drop(1))
      val s = 8797
      val res = mcmcMetro.nLogLikelihood(mu, varianceLnX, s, data, nL)
      //println("nLogLikelihood = " + res)
      res should be(331.08250632768613 +- 0.000000001)
    }

    it("Negative log likelihood value for Brazil dataset 07") {
      val mcmcMetro = new MetropolisHMCMC()
      val fileName = "data/Brazil.sample"
      var data = MetropolisHMCMC.readData(fileName)
      val mu = -1.8123649834052324
      val varianceLnX = (6.2368726821872436)
      val nL = MetropolisHMCMC.getD(data.drop(1))
      val s = 8794
      val res = mcmcMetro.nLogLikelihood(mu, varianceLnX, s, data, nL)
      //println("nLogLikelihood = " + res)
      res should be(331.23004579899134 +- 0.000000001)
    }

    it("Negative log likelihood value for Brazil dataset 08") {
      val mcmcMetro = new MetropolisHMCMC()
      val fileName = "data/Brazil.sample"
      var data = MetropolisHMCMC.readData(fileName)
      val mu = -1.8222086834052327
      val varianceLnX = 6.2524695821872438
      val nL = MetropolisHMCMC.getD(data.drop(1))
      val s = 8791
      val res = mcmcMetro.nLogLikelihood(mu, varianceLnX, s, data, nL)
      //println("nLogLikelihood = " + res)
      res should be(331.43592456972692 +- 0.000000001)
    }

    it("Negative log likelihood value for Brazil dataset 09") {
      val mcmcMetro = new MetropolisHMCMC()
      val fileName = "data/Brazil.sample"
      var data = MetropolisHMCMC.readData(fileName)
      val mu = -1.8320523834052329
      val varianceLnX = (6.268066482187244)
      val nL = MetropolisHMCMC.getD(data.drop(1))
      val s = 8788
      val res = mcmcMetro.nLogLikelihood(mu, varianceLnX, s, data, nL)
      //println("nLogLikelihood = " + res)
      res should be(331.69976419652812 +- 0.000000001)
    }

    it("Negative log likelihood value for Brazil dataset 10") {
      val mcmcMetro = new MetropolisHMCMC()
      val fileName = "data/Brazil.sample"
      var data = MetropolisHMCMC.readData(fileName)
      val mu = -1.8418960834052331
      val varianceLnX = (6.2836633821872443)
      val nL = MetropolisHMCMC.getD(data.drop(1))
      val s = 8785
      val res = mcmcMetro.nLogLikelihood(mu, varianceLnX, s, data, nL)
      //println("nLogLikelihood = " + res)
      res should be(332.0211893873493 +- 0.000000001)
    }

    it("Negative log likelihood value for Brazil dataset 11") {
      val mcmcMetro = new MetropolisHMCMC()
      val fileName = "data/Brazil.sample"
      var data = MetropolisHMCMC.readData(fileName)
      val mu = -1.8517397834052334
      val varianceLnX = (6.2992602821872445)
      val nL = MetropolisHMCMC.getD(data.drop(1))
      val s = 8782
      val res = mcmcMetro.nLogLikelihood(mu, varianceLnX, s, data, nL)
      //println("nLogLikelihood = " + res)
      res should be(332.39982796795084 +- 0.000000001)
    }

    it("Negative log likelihood value for Brazil dataset 12") {
      val mcmcMetro = new MetropolisHMCMC()
      val fileName = "data/Brazil.sample"
      var data = MetropolisHMCMC.readData(fileName)
      val mu = -1.8615834834052336
      val varianceLnX = (6.3148571821872448)
      val nL = MetropolisHMCMC.getD(data.drop(1))
      val s = 8779
      val res = mcmcMetro.nLogLikelihood(mu, varianceLnX, s, data, nL)
      //println("nLogLikelihood = " + res)
      res should be(332.83531084953574 +- 0.00000001)
    }

    it("Negative log likelihood value for Brazil dataset 13") {
      val mcmcMetro = new MetropolisHMCMC()
      val fileName = "data/Brazil.sample"
      var data = MetropolisHMCMC.readData(fileName)
      val mu = -1.8714271834052338
      val varianceLnX = (6.330454082187245)
      val nL = MetropolisHMCMC.getD(data.drop(1))
      val s = 8776
      val res = mcmcMetro.nLogLikelihood(mu, varianceLnX, s, data, nL)
      //println("nLogLikelihood = " + res)
      res should be(333.3272719958768 +- 0.00000001)
    }

    it("Negative log likelihood value for Brazil dataset 14") {
      val mcmcMetro = new MetropolisHMCMC()
      val fileName = "data/Brazil.sample"
      var data = MetropolisHMCMC.readData(fileName)
      val mu = -1.8812708834052341
      val varianceLnX = (6.3460509821872453)
      val nL = MetropolisHMCMC.getD(data.drop(1))
      val s = 8773
      val res = mcmcMetro.nLogLikelihood(mu, varianceLnX, s, data, nL)
      //println("nLogLikelihood = " + res)
      res should be(333.87534839163709 +- 0.00000001)
    }

    it("Negative log likelihood value for Brazil dataset 15") {
      val mcmcMetro = new MetropolisHMCMC()
      val fileName = "data/Brazil.sample"
      var data = MetropolisHMCMC.readData(fileName)
      val mu = -1.8911145834052343
      val varianceLnX = (6.3616478821872455)
      val nL = MetropolisHMCMC.getD(data.drop(1))
      val s = 8770
      val res = mcmcMetro.nLogLikelihood(mu, varianceLnX, s, data, nL)
      //println("nLogLikelihood = " + res)
      res should be(334.47918001086509 +- 0.00000001)
    }

    it("Negative log likelihood value for Brazil dataset 16") {
      val mcmcMetro = new MetropolisHMCMC()
      val fileName = "data/Brazil.sample"
      var data = MetropolisHMCMC.readData(fileName)
      val mu = -1.9009582834052345
      val varianceLnX = (6.3772447821872458)
      val nL = MetropolisHMCMC.getD(data.drop(1))
      val s = 8767
      val res = mcmcMetro.nLogLikelihood(mu, varianceLnX, s, data, nL)
      //println("nLogLikelihood = " + res)
      res should be(335.13840978554799 +- 0.00000001)
    }

    it("Negative log likelihood value for Brazil dataset 17") {
      val mcmcMetro = new MetropolisHMCMC()
      val fileName = "data/Brazil.sample"
      var data = MetropolisHMCMC.readData(fileName)
      val mu = -1.9108019834052348
      val varianceLnX = (6.392841682187246)
      val nL = MetropolisHMCMC.getD(data.drop(1))
      val s = 8764
      val res = mcmcMetro.nLogLikelihood(mu, varianceLnX, s, data, nL)
      //println("nLogLikelihood = " + res)
      res should be(335.85268357505265 +- 0.00000001)
    }

    it("Negative log likelihood value for Brazil dataset 18") {
      val mcmcMetro = new MetropolisHMCMC()
      val fileName = "data/Brazil.sample"
      var data = MetropolisHMCMC.readData(fileName)
      val mu = -1.920645683405235
      val varianceLnX = (6.4084385821872463)
      val nL = MetropolisHMCMC.getD(data.drop(1))
      val s = 8761
      val res = mcmcMetro.nLogLikelihood(mu, varianceLnX, s, data, nL)
      //println("nLogLikelihood = " + res)
      res should be(336.62165013582853 +- 0.00000001)
    }

    it("Negative log likelihood value for Brazil dataset pars 01") {
      val mcmcMetro = new MetropolisHMCMC()
      val fileName = "data/Brazil.sample"
      var data = MetropolisHMCMC.readData(fileName)
      val mu = -1.78
      val varianceLnX = 2.487971060924946*2.487971060924946
      val nL = MetropolisHMCMC.getD(data.drop(1))
      val s = 5760
      val res = mcmcMetro.nLogLikelihood(mu, varianceLnX, s, data, nL)
      //println("nLogLikelihood = " + res)
      res should be(695.7727461260365 +- 0.00000001)
    }

    it("Negative log likelihood value for Brazil dataset pars 02") {
      val mcmcMetro = new MetropolisHMCMC()
      val fileName = "data/Brazil.sample"
      var data = MetropolisHMCMC.readData(fileName)
      val mu = -1.7898437000000005
      val varianceLnX = 2.4911035506377495*2.4911035506377495
      val nL = MetropolisHMCMC.getD(data.drop(1))
      val s = 5757
      val res = mcmcMetro.nLogLikelihood(mu, varianceLnX, s, data, nL)
      //println("nLogLikelihood = " + res)
      res should be(701.5992313904717 +- 0.00000001)
    }
    //707.46211055848107

    //nIter,dMDash,dVDash,nSDash,nA,dLogP
    //0,-1.780000,6.190000,5760,1,-2.053457
    //0,-1.780000,6.190000,5760,2,-2.935243
    //0,-1.780000,6.190000,5760,3,-3.517845

    
    it("Log likelihood value for Brazil dataset, n = 2") {
      val ll = new LogLikelihoodPoissonLogNormal()
      val mu = -1.78
      val varianceLnX = (6.19000000000000)
      val n = 2
      val log0 = ll.calculateWithRLikeScala(mu, varianceLnX, n)
      log0 should be(-2.9352432 +- 0.00001)
    }
    
     
    
    it("Log likelihood value for Brazil dataset, n = 0") {
      val ll = new LogLikelihoodPoissonLogNormal()
      val mu = -1.78
      val varianceLnX = (6.1900000000000004)
      val log0 = ll.calculateWithRLikeScala(mu, varianceLnX, 0)
      log0 should be(-0.39675800373513792 +- 0.00000001)
    }
    // dLog0
    // *********************************
    // *
    // ********************************
    //def calculateWithIntegral(mu: Double, sigma: Double, n: Int)
    it("calculateWithIntegral result ") {

      // res should be(362.85226632656122 +- 0.001) //70.0
    }
  }

}
//nLogLikelihood