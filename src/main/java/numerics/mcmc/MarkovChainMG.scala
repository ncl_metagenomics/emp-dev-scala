package numerics.mcmc

import breeze.numerics.log
import breeze.stats.distributions.{MarkovChain, Process, Rand, RandBasis}
import breeze.stats.distributions.RandBasis._
import breeze.stats.distributions.Rand._

import scala.util.Try

/**
  * Markov chains for Metagnomics
  */
object MarkovChainMG {


  /**
    * Given an initial state and an arbitrary Markov transition, return a sampler
    * for doing mcmc
    */
  def apply[T](init: T)(resample: T => Rand[T]): Process[T] = new Process[T] {
    val inner = resample(init);

    def draw() = {
      val next = inner.draw();
      next
    }

    override def observe(x: T) = {
      MarkovChain(x)(resample)
    }
  }

  object Kernels {
    /**
      * Note this is not Metropolis-Hastings
      *
      * @param logMeasure the distribution we want to sample from
      * @param proposal   the <b>symmetric</b> proposal distribution generator
      *
      */
    def metropolis[T](proposal: T => Rand[T])(logMeasure: T => Try[Double])(isProposalValid: T => Boolean)(implicit rand: RandBasis = Rand) = { t: T =>
      for {next <- proposal(t);
           isValid = isProposalValid(next);
           x = if (isValid) {
             val newLL: Double = logMeasure(next).getOrElse(Double.NegativeInfinity);
             val oldLL: Double = logMeasure(t).getOrElse(0);
             if (!newLL.isNegInfinity) {
               val a = newLL - oldLL;
               val u = rand.uniform.draw();
               if (scala.math.log(u) < a) next else t
             } else t

           } else t

      } yield {
        //        println(s"Accepted values $x")
        x
      }

    }

  }


  /**
    * Performs Metropolis distributions on a random variable.
    * Note this is not Metropolis-Hastings
    *
    * @param init       The initial parameter
    * @param logMeasure the distribution we want to sample from
    * @param proposal   the <b>symmetric</b> proposal distribution generator
    *
    */
  def metropolis[T](init: T, proposal: T => Rand[T])(logMeasure: T => Try[Double])(isProposalValid: T => Boolean): Process[T] = {
    val r: Process[T] = MarkovChainMG(init)(Kernels.metropolis(proposal)(logMeasure)(isProposalValid))
    println("")
    r
  }


}