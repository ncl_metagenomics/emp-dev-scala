package numerics.mcmc.thinning

import scala.language.higherKinds

trait Thinnable[F[_]]  {
  def thin[T](f: F[T], th: Int): F[T]
}





