package numerics.mcmc.thinning

object DefaultThinnable {
  implicit val streamThinnable: Thinnable[Stream] =
  new Thinnable[Stream] {
    def thin[T](s: Stream[T],th: Int): Stream[T] = {
      val ss = s.drop(th-1)
      if (ss.isEmpty) Stream.empty else
      ss.head #:: thin(ss.tail, th)
    }
  }
}
