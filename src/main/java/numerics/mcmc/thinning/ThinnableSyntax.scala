package numerics.mcmc.thinning

import scala.language.higherKinds

object ThinnableSyntax {
  implicit class ThinnableSyntax[T,F[T]](value: F[T]) {
    def thin(th: Int)( implicit inst: Thinnable[F]): F[T] =
      inst.thin(value,th)
  }

}
