getStatistics <- function(activeDF, percetiles2Calc) {
  res.median <- median(activeDF)
  res.mean <- mean(activeDF)
  res.10And90PERCENTILE = quantile(activeDF, percetiles2Calc)
  r <- list(
    median = res.median,
    mean = res.mean,
    percentiles01 = unname(res.10And90PERCENTILE[1]),
    percentiles02 = unname(res.10And90PERCENTILE[2])
  )
  names(r)[3] <- paste0("percentile_",percetiles2Calc[1]*100)
  names(r)[4] <- paste0("percentile_",percetiles2Calc[2]*100)
  r
}