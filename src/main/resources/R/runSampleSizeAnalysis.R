rm(list=ls())
cat("\014")  
options(digits=10)

# library(optigrab)
source("LibSampleSizeAnalysis.R")

varName <- "L"
fN <- "/media/sf_data/nps409/Work2Sync/Projects/emp_dev_scala_runs/TAD_DATA_Synthetic_Out/SRS711891_Synthetic_Out/AutoEval_160629_1555-27_160816/44xSRS711891_TAD__50_pc_D_7747.5_lognormal_160629_1555-27_16-06-30_1235-09/s-800K-160630-1511-35/sample_size_posterior-s-s-all-44xSRS711891_TAD__50_pc_D_7747.5_lognormal_160629_1555-27_16-06-30_1235-09.sample_160630-1511_36_900_%o_160630-1511_36.csv
"
print(paste0("fN = ",fN))
# d <- read.csv(file=fN,header=TRUE)
# d <- d[order(d$coverage),]
# ***********************************
# graphS(fN) 
# graphCIVariance(fN) 

debug(getHistogram)
getHistogram(fN,varName)