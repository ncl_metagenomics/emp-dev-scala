rm(list=ls())
source('libs/utilities.R')
source("libs/utilities_tad_02.R")
source("libs/utilities_tad_03.R")
source("libs/utilities_tad.R")

fN <- "/media/sf_data/nps409/Work2Sync/Projects/emp_dev_scala_runs/TAD_DATA_Real_Out/Illinois/c-400K-160510-1102-21/posterior-c_Illinois_160510-1102_27_0.sample"
# fN <- "/media/sf_data/nps409/Work2Sync/Projects/emp_dev_scala_runs/TAD_DATA_Real_Out/Florida/c-400K-160510-1110-08/posterior-c_Florida_160510-1110_13_0.sample"
fN <- "/media/sf_data/nps409/Work2Sync/Projects/emp_dev_scala_runs/TAD_DATA_Real_Out/SRR1589726/s-400K-160528-0344-49/posterior-s-s-all-SRR1589726_MERGED_FASTQ_abundance.sample_160528-0344_49.csv"

fN <- "/media/sf_data/nps409/Work2Sync/Projects/emp_dev_scala_runs/TAD_DATA_Real_Out/SRR1589726/s-400K-160528-0344-49_c-400K-160604-0250-23/sample_size_posterior-s-s-all-SRR1589726_MERGED_FASTQ_abundance.sample_160528-0344_49_329_%o_160613-2218_44.csv"

fN <- "/media/sf_data/nps409/Work2Sync/Projects/emp_dev_scala_runs/TAD_DATA_Real_Out/SRR1589726/s-400K-160528-0344-49_c-400K-160604-0250-23/sample_size_posterior-s-s-all-SRR1589726_MERGED_FASTQ_abundance.sample_160528-0344_49_324_%o_160613-2219_45.csv"

fN <- "/media/sf_data/nps409/Work2Sync/Projects/emp_dev_scala_runs/TAD_DATA_Real_Out/SRR1589726/s-400K-160528-0344-49_c-400K-160604-0250-23/sample_size_posterior-s-s-all-SRR1589726_MERGED_FASTQ_abundance.sample_160528-0344_49_24_%_160613-2153_30.csv"

fN <- "/media/sf_data/nps409/Work2Sync/Projects/emp_dev_scala_runs/TAD_DATA_Real_Out/SRR1589726/s-400K-160528-0344-49_c-400K-160604-0250-23/sample_size_posterior-s-s-all-SRR1589726_MERGED_FASTQ_abundance.sample_160528-0344_49_dynamic_S_%o_160615-1942_12.csv"

fN <- "/media/sf_data/nps409/Work2Sync/Projects/emp_dev_scala_runs/TAD_DATA_Real_Out/EP-06-02/s-400K-160617-1217-17/sample_size_posterior-s-s-all-EP-06-02_otu.tsv_tad.sample_160617-1217_18_900_%o_160617-1217_18.csv"


fN <- "/media/sf_data/nps409/Work2Sync/Projects/emp_dev_scala_runs/TAD_DATA_Real_Out/SRR1589726_SRR1589727_SRR1589728_SRR1589729_SRR1589730_SRR1589731/s-400K-160610-2230-00_s-400K-160610-2230-00/sample_size_posterior-s-s-all-SRR1589726_SRR1589727_SRR1589728_SRR1589729_SRR1589730_SRR1589731_tad.sample_160610-2230_00_dynamic_S_%o_160617-2052_08.csv"

fN <- "/media/sf_data/nps409/Work2Sync/Projects/emp_dev_scala_runs/TAD_DATA_Real_Out/SRR1589726_SRR1589727_SRR1589728_SRR1589729_SRR1589730_SRR1589731/s-400K-160610-2230-00_s-400K-160610-2230-00/sample_size_posterior-s-s-all-SRR1589726_SRR1589727_SRR1589728_SRR1589729_SRR1589730_SRR1589731_tad.sample_160610-2230_00_900_%o_160614-1359_01.csv"

fN <- "/media/sf_data/nps409/Work2Sync/Projects/emp_dev_scala_runs/TAD_DATA_Real_Out/44xSRR15897XX/s-400K-160617-1217-14_c-400K-160616-1839-32/sample_size_posterior-s-s-all-SRR15897XX_x44_merged_otu.tsv_tad.sample_160617-1217_14_900_%o_160617-1217_14.csv"

fN <- "/media/sf_data/nps409/Work2Sync/Projects/emp_dev_scala_runs/TAD_DATA_Real_Out/SRR1589726/s-400K-160528-0344-49_c-400K-160604-0250-23/sample_size_posterior-s-s-all-SRR1589726_MERGED_FASTQ_abundance.sample_160528-0344_49_900_%o_160620-1418_36.csv"

fN <- "/media/sf_data/nps409/Work2Sync/Projects/emp_dev_scala_runs/TAD_DATA_Real_Out/30xSRR15897XX/s-800K-160620-1828-41_c-800K-160620-1830-39/sample_size_posterior-s-s-all-30xSRR15897XX_merged_otu.tsv_tad.sample_160620-1828_41_900_%o_160620-1828_41.csv"

fN <- "/media/sf_data/nps409/Work2Sync/Projects/emp_dev_scala_runs/TAD_DATA_Real_Out/20xSRR15897XX/s-800K-160620-1544-34_c-800K-160620-1623-40/sample_size_posterior-s-s-all-20xSRR15897XX_merged_otu.tsv_tad.sample_160620-1544_35_900_%o_160620-1544_35.csv"

fN <- "/media/sf_data/nps409/Work2Sync/Projects/emp_dev_scala_runs/TAD_DATA_Real_Out/EP-06-02/s-1000K-160622-1947-35_c-1000K-160622-1948-18/sample_size_posterior-s-s-all-EP-06-02_tad.sample_160622-1947_36_900_%o_160622-1947_36.csv"

fN <- "/media/sf_data/nps409/Work2Sync/Projects/emp_dev_scala_runs/TAD_DATA_Real_Out/SRR1589726/s-600K-160622-1512-04_c-400K-160622-1515-27/sample_size_posterior-s-s-all-SRR1589726_tad.sample_160622-1512_05_900_%o_160622-1512_05.csv"

fN <- "/media/sf_data/nps409/Work2Sync/Projects/emp_dev_scala_runs/TAD_DATA_Real_Out/6xSRR15897XX/s-400K-160622-1526-54_c-400K-160622-1529-30/sample_size_posterior-s-s-all-6xSRR15897XX_tad.sample_160622-1526_54_900_%o_160622-1526_54.csv"

fN <- "/media/sf_data/nps409/Work2Sync/Projects/emp_dev_scala_runs/TAD_DATA_Real_Out/6xSRR15897XX/s-600K-160623-1746-58_c-400K-160622-1529-30/sample_size_posterior-s-s-all-6xSRR15897XX_tad.sample_160623-1746_59_900_%o_160623-1746_59.csv"

fN <- "/media/sf_data/nps409/Work2Sync/Projects/emp_dev_scala_runs/TAD_DATA_Real_Out/6xSRR15897XX/s-600K-160623-1746-58_c-400K-160622-1529-30/sample_size_posterior-s-s-all-6xSRR15897XX_tad.sample_160623-1746_59_900_%o_160624-1748_54.csv"

fN <- "/media/sf_data/nps409/Work2Sync/Projects/emp_dev_scala_runs/TAD_DATA_Real_Out/SRR1589726/s-600K-160622-1512-04_c-400K-160622-1515-27/sample_size_posterior-s-s-all-SRR1589726_tad.sample_160622-1512_05_900_%o_160624-1728_21.csv"

fN <- "/media/sf_data/nps409/Work2Sync/Projects/emp_dev_scala_runs/TAD_DATA_Real_Out/10xSRR15897XX/s-600K-160623-1750-16_c-600K-160623-1752-33/sample_size_posterior-s-s-all-10xSRR15897XX_tad.sample_160623-1750_16_900_%o_160623-1750_16.csv"

fN <- "/media/sf_data/nps409/Work2Sync/Projects/emp_dev_scala_runs/TAD_DATA_Real_Out/15xSRR15897XX/s-600K-160623-1801-46_c-400K-160623-1806-09/sample_size_posterior-s-s-all-15xSRR15897XX_tad.sample_160623-1801_47_900_%o_160624-1741_11.csv"

fN <- "/media/sf_data/nps409/Work2Sync/Projects/emp_dev_scala_runs/TAD_DATA_Real_Out/20xSRR15897XX/s-600K-160624-1807-07_c-600K-160624-1808-00/sample_size_posterior-s-s-all-20xSRR15897XX_tad.sample_160624-1807_08_900_%o_160624-1807_08.csv"

fN <- "/media/sf_data/nps409/Work2Sync/Projects/emp_dev_scala_runs/TAD_DATA_Real_Out/25xSRR15897XX/s-600K-160624-1814-13_c-400K-160624-1815-04/sample_size_posterior-s-s-all-25xSRR15897XX_tad.sample_160624-1814_14_900_%o_160624-1814_14.csv"

fN <- "/media/sf_data/nps409/Work2Sync/Projects/emp_dev_scala_runs/TAD_DATA_Real_Out/30xSRR15897XX/s-600K-160624-1827-20_c-600K-160624-1830-12/sample_size_posterior-s-s-all-30xSRR15897XX_tad.sample_160624-1827_21_900_%o_160624-1827_21.csv"

fN <- "/media/sf_data/nps409/Work2Sync/Projects/emp_dev_scala_runs/TAD_DATA_Real_Out/35xSRR15897XX/s-600K-160624-2052-27_c-600K-160624-2053-38/sample_size_posterior-s-s-all-35xSRR15897XX_tad.sample_160624-2052_29_900_%o_160624-2052_29.csv"

fN <- "/media/sf_data/nps409/Work2Sync/Projects/emp_dev_scala_runs/TAD_DATA_Real_Out/40xSRR15897XX/s-600K-160624-2104-31-c-600K-160624-2108-29/sample_size_posterior-s-s-all-40xSRR15897XX_tad.sample_160624-2104_33_900_%o_160624-2104_33.csv"

fN <- "/media/sf_data/nps409/Work2Sync/Projects/emp_dev_scala_runs/TAD_DATA_Real_Out/44xSRR15897XX/s-800K-160627-1833-21_c-600K-160627-1148-33/sample_size_posterior-s-s-all-44xSRR15897XX_tad.sample_160627-1833_21_900_%o_160627-1833_21.csv"

fN <- "/media/sf_data/nps409/Work2Sync/Projects/emp_dev_scala_runs/TAD_DATA_Synthetic_Out/SRS711891_Synthetic_Out/44xSRS711891_TAD__40_pc_D_6198_lognormal_160629_1555-27_16-06-29_1623-28/s-400K-160629-2119-45_c/sample_size_posterior-s-s-all-44xSRS711891_TAD__40_pc_D_6198_lognormal_160629_1555-27_16-06-29_1623-28.sample_160629-2119_46_900_%o_160629-2119_46.csv"

fN <- "/media/sf_data/nps409/Work2Sync/Projects/emp_dev_scala_runs/TAD_DATA_Synthetic_Out/SRS711891_Synthetic_Out/44xSRS711891_TAD__20_pc_D_3099_lognormal_160629_1555-27_16-06-30_1242-52/s-800K-160630-1513-10_c/sample_size_posterior-s-s-all-44xSRS711891_TAD__20_pc_D_3099_lognormal_160629_1555-27_16-06-30_1242-52.sample_160630-1513_10_900_%o_160630-1513_10.csv"

fN <- "/media/sf_data/nps409/Work2Sync/Projects/emp_dev_scala_runs/TAD_DATA_Synthetic_Out/SRS711891_Synthetic_Out/44xSRS711891_TAD__30_pc_D_4648.5_lognormal_160629_1555-27_16-06-30_1240-46/s-800K-160630-1502-38_c/sample_size_posterior-s-s-all-44xSRS711891_TAD__30_pc_D_4648.5_lognormal_160629_1555-27_16-06-30_1240-46.sample_160630-1502_39_900_%o_160630-1502_39.csv"

fN <- "/media/sf_data/nps409/Work2Sync/Projects/emp_dev_scala_runs/TAD_DATA_Synthetic_Out/SRS711891_Synthetic_Out/44xSRS711891_TAD__70_pc_D_10846.5_lognormal_160629_1555-27_16-06-30_1229-41/s-800K-160701-1045-01/sample_size_posterior-s-s-all-44xSRS711891_TAD__70_pc_D_10846.5_lognormal_160629_1555-27_16-06-30_1229-41.sample_160701-1045_01_900_%o_160701-1045_01.csv"

fN <- "/media/sf_data/nps409/Work2Sync/Projects/emp_dev_scala_runs/TAD_DATA_Synthetic_Out/SRS711891_Synthetic_Out/44xSRS711891_TAD__30_pc_D_4648.5_lognormal_160629_1555-27_16-06-30_1240-46/s-800K-160630-1502-38/sample_size_posterior-s-s-all-44xSRS711891_TAD__30_pc_D_4648.5_lognormal_160629_1555-27_16-06-30_1240-46.sample_160630-1502_39_900_%o_160630-1502_39.csv"

fN <- "/media/sf_data/nps409/Work2Sync/Projects/emp_dev_scala_runs/TAD_DATA_Synthetic_Out/SRS711891_Synthetic_Out/44xSRS711891_TAD__30_pc_D_4648.5_lognormal_160629_1555-27_16-06-30_1240-46/s-800K-160630-1502-38/sample_size_posterior-s-s-all-44xSRS711891_TAD__30_pc_D_4648.5_lognormal_160629_1555-27_16-06-30_1240-46.sample_160630-1502_39_900_%o_160630-1502_39.csv"

fN <- "/media/sf_data/nps409/Work2Sync/Projects/emp_dev_scala_runs/TAD_DATA_Synthetic_Out/SRS711891_Synthetic_Out/44xSRS711891_TAD__50_pc_D_7747.5_lognormal_160629_1555-27_16-06-30_1308-36/s-800K-160701-1441-37/sample_size_posterior-s-s-all-44xSRS711891_TAD__50_pc_D_7747.5_lognormal_160629_1555-27_16-06-30_1308-36.sample_160701-1441_38_900_%o_160701-1441_38.csv"

fN <- "/media/sf_data/nps409/Work2Sync/Projects/emp_dev_scala_runs/TAD_DATA_Synthetic_Out/SRS711891_Synthetic_Out/44xSRS711891_TAD__65_pc_D_10071.75_lognormal_160629_1555-27_16-06-30_1359-33/s-800K-160701-1041-14/sample_size_posterior-s-s-all-44xSRS711891_TAD__65_pc_D_10071.75_lognormal_160629_1555-27_16-06-30_1359-33.sample_160701-1041_15_900_%o_160701-1041_15.csv"

fN <- "/media/sf_data/nps409/Work2Sync/Projects/emp_dev_scala_runs/TAD_DATA_Synthetic_Out/SRS711891_Synthetic_Out/44xSRS711891_TAD__40_pc_D_6198_lognormal_160629_1555-27_16-06-29_1623-28/s-800K-160630-1457-27/sample_size_posterior-s-s-all-44xSRS711891_TAD__40_pc_D_6198_lognormal_160629_1555-27_16-06-29_1623-28.sample_160630-1457_27_900_%o_160630-1457_27.csv"

fN <- "/media/sf_data/nps409/Work2Sync/Projects/emp_dev_scala_runs/TAD_DATA_Synthetic_Out/SRS711891_Synthetic_Out/44xSRS711891_TAD__20_pc_D_3099_lognormal_160629_1555-27_16-06-30_1242-52/s-800K-160630-1513-10/sample_size_posterior-s-s-all-44xSRS711891_TAD__20_pc_D_3099_lognormal_160629_1555-27_16-06-30_1242-52.sample_160630-1513_10_900_%o_160630-1513_10.csv"

fN <- "/media/sf_data/nps409/Work2Sync/Projects/emp_dev_scala_runs/TAD_DATA_Synthetic_Out/SRS711891_Synthetic_Out/44xSRS711891_TAD__85_pc_D_13170.75_lognormal_160630_1444-36_16-07-01_1812-36/s-800K-160804-0102-48/sample_size_posterior-s-s-all-44xSRS711891_TAD__85_pc_D_13170.75_lognormal_160630_1444-36_16-07-01_1812-36.sample_160804-0102_50_850_%o_160804-0102_50.csv"


fN <- "/media/sf_data/nps409/Work2Sync/Projects/emp_dev_scala_runs/TAD_DATA_Synthetic_Out/SRS711891_Synthetic_Out/44xSRS711891_TAD__90_pc_D_13945.5_lognormal_160630_1444-36_16-07-01_1812-57/s-800K-160804-0042-50/sample_size_posterior-s-s-all-44xSRS711891_TAD__90_pc_D_13945.5_lognormal_160630_1444-36_16-07-01_1812-57.sample_160804-0042_52_900_%o_160804-0042_52.csv"

fN <- "/media/sf_data/nps409/Work2Sync/Projects/emp_dev_scala_runs/TAD_DATA_Synthetic_Out/SRS711891_Synthetic_Out/44xSRS711891_TAD__45_pc_D_6972.75_lognormal_160629_1555-27_16-07-01_1517-02/s-800K-160804-0143-06/sample_size_posterior-s-s-all-44xSRS711891_TAD__45_pc_D_6972.75_lognormal_160629_1555-27_16-07-01_1517-02.sample_160804-0143_07_450_%o_160804-0143_07.csv"

fN <- "/media/sf_data/nps409/Work2Sync/Projects/emp_dev_scala_runs/TAD_DATA_Synthetic_Out/SRS711891_Synthetic_Out/44xSRS711891_TAD__55_pc_D_8522.25_lognormal_160630_1444-36_16-07-01_1803-39/s-800K-160804-0132-53/sample_size_posterior-s-s-all-44xSRS711891_TAD__55_pc_D_8522.25_lognormal_160630_1444-36_16-07-01_1803-39.sample_160804-0132_54_550_%o_160804-0132_54.csv"

fN <- "/media/sf_data/nps409/Work2Sync/Projects/emp_dev_scala_runs/TAD_DATA_Synthetic_Out/SRS711891_Synthetic_Out/44xSRS711891_TAD__45_pc_D_6972.75_lognormal_160629_1555-27_16-07-01_1517-02/s-800K-160804-0143-06/sample_size_posterior-s-s-all-44xSRS711891_TAD__45_pc_D_6972.75_lognormal_160629_1555-27_16-07-01_1517-02.sample_160804-0143_07_900_%o_160811-1603_32.csv"

fN <- "/media/sf_data/nps409/Work2Sync/Projects/emp_dev_scala_runs/TAD_DATA_Synthetic_Out/SRS711891_Synthetic_Out/44xSRS711891_TAD__55_pc_D_8522.25_lognormal_160630_1444-36_16-07-01_1803-39/s-800K-160804-0132-53/sample_size_posterior-s-s-all-44xSRS711891_TAD__55_pc_D_8522.25_lognormal_160630_1444-36_16-07-01_1803-39.sample_160804-0132_54_900_%o_160811-1541_28.csv"

fN <- "/media/sf_data/nps409/Work2Sync/Projects/emp_dev_scala_runs/TAD_DATA_Synthetic_Out/SRS711891_Synthetic_Out/44xSRS711891_TAD__50_pc_D_7747.5_lognormal_160629_1555-27_16-06-30_1308-36/s-800K-160701-1441-37/sample_size_posterior-s-s-all-44xSRS711891_TAD__50_pc_D_7747.5_lognormal_160629_1555-27_16-06-30_1308-36.sample_160701-1441_38_900_%o_160701-1441_38.csv"

fN <- "/media/sf_data/nps409/Work2Sync/Projects/emp_dev_scala_runs/TAD_DATA_Synthetic_Out/SRS711891_Synthetic_Out/44xSRS711891_synthetic_lognormal_160630_1444-36_RESULTS/44xSRS711891_TAD__90_pc_D_13945.5_lognormal_160630_1444-36_16-07-01_1812-57/s-800K-160804-0042-50/sample_size_posterior-s-s-all-44xSRS711891_TAD__90_pc_D_13945.5_lognormal_160630_1444-36_16-07-01_1812-57.sample_160804-0042_52_900_%o_160804-0042_52.csv"

fN <- "/media/sf_data/nps409/Work2Sync/Projects/emp_dev_scala_runs/TAD_DATA_Synthetic_Out/SRS711891_Synthetic_Out/44xSRS711891_synthetic_lognormal_160630_1444-36_RESULTS/44xSRS711891_TAD__90_pc_D_13945.5_lognormal_160630_1444-36_16-07-01_1812-57_more_precise/s-800K-160804-0042-50/sample_size_posterior-s-s-all-44xSRS711891_TAD__90_pc_D_13945.5_lognormal_160630_1444-36_16-07-01_1812-57.sample_160804-0042_52_900_%o_160831-2132_58.csv"


fN <- "/media/sf_data/nps409/Work2Sync/Projects/emp_dev_scala_runs/TAD_DATA_Synthetic_Out/SRS711891_Synthetic_Out/44xSRS711891_synthetic_lognormal_160630_1444-36_RESULTS/44xSRS711891_TAD__90_pc_D_13945.5_lognormal_160630_1444-36_16-07-01_1812-57_more_precise_1_02_1_01/s-800K-160804-0042-50/sample_size_posterior-s-s-all-44xSRS711891_TAD__90_pc_D_13945.5_lognormal_160630_1444-36_16-07-01_1812-57.sample_160804-0042_52_900_%o_160901-1049_55.csv"

fN <- "/media/sf_data/nps409/Work2Sync/Projects/emp_dev_scala_runs/TAD_DATA_Synthetic_Out/SRS711891_Synthetic_Out/44xSRS711891_synthetic_lognormal_160630_1444-36_RESULTS/44xSRS711891_TAD__90_pc_D_13945.5_lognormal_160630_1444-36_16-07-01_1812-57_more_precise_1_01_1_005/s-800K-160804-0042-50/sample_size_posterior-s-s-all-44xSRS711891_TAD__90_pc_D_13945.5_lognormal_160630_1444-36_16-07-01_1812-57.sample_160804-0042_52_900_%o_160901-1055_09.csv"

fN <- "/media/sf_data/nps409/Work2Sync/Projects/emp_dev_scala_runs/TAD_DATA_Synthetic_Out/SRS711891_Synthetic_Out/44xSRS711891_synthetic_lognormal_160630_1444-36_RESULTS/44xSRS711891_TAD__90_pc_D_13945.5_lognormal_160630_1444-36_16-07-01_1812-57_more_precise_1_005_1_001/s-800K-160804-0042-50/sample_size_posterior-s-s-all-44xSRS711891_TAD__90_pc_D_13945.5_lognormal_160630_1444-36_16-07-01_1812-57.sample_160804-0042_52_900_%o_160901-1803_42.csv"

fN <- "/media/sf_data/nps409/Work2Sync/Projects/emp_dev_scala_runs/TAD_DATA_Synthetic_Out/SRS711891_Synthetic_Out/AutoEval160816_160630_1444-36/44xSRS711891_TAD__65_pc_D_10071.75_lognormal_160630_1444-36_16-07-01_1805-47/s-800K-160826-2006-04/sample_size_posterior-s-s-all-44xSRS711891_TAD__65_pc_D_10071.75_lognormal_160630_1444-36_16-07-01_1805-47.sample_160826-2006_05_correct_Coverage_900_%o_160909-1616_16.csv"

fN <-  "/media/sf_data/nps409/Work2Sync/Projects/emp_dev_scala_runs/TAD_DATA_Synthetic_Out/SRS711891_Synthetic_Out/AutoEval160816_160630_1444-36/44xSRS711891_TAD__80_pc_D_12396_lognormal_160630_1444-36_16-07-01_1811-08/s-800K-160804-0109-21/sample_size_posterior-s-s-all-44xSRS711891_TAD__80_pc_D_12396_lognormal_160630_1444-36_16-07-01_1811-08.sample_160804-0109_22_correct_Coverage_900_%o_160909-1616_16.csv"

outDir <- dirname(fN)

nS2PNGOut <- file.path(outDir,"histogram_S.png")
nL2PNGOut <- file.path(outDir,"histogram_L.png")

nS2PDFOut <- file.path(outDir,"histogram_S.pdf")
nL2PDFOut <- file.path(outDir,"histogram_L.pdf")

nL2EPSOut <- file.path(outDir,"histogram_L.eps")



# dev.copy(png,'myplot.png')
# dev.off()




sample.size.in.target <- 38783 #6634
lines2Skip.BurnIn <- 10000 

d0 <- read.csv(fN)
l <- length(d0[[1]])
ntake <- l-lines2Skip.BurnIn

d <- tail(d0,ntake)

# skip.num <- 2
# skip.denum <-5 
# l <- length(d[[1]])
# idx.start <- (l+1)*skip.num/skip.denum
# ntake <- (l+1)*(skip.denum-skip.num)/skip.denum
# d <- tail(d,ntake)
# names(d) <- c("iter","mu","var","S","NLL")
str(d)
options(digits=10)
summary(d)
mu.median <- median(d$mu)
percentile <- c(0.025,0.975)
stats <- getStatistics(d$nL_Recommended,percentile)
stats


# =======================================================================================
# +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# ***************************************************************************************
# Transform path!!!


# =======================================================================================
# +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# ***************************************************************************************

trueValue <- sample.size.in.target

varName <- "nL_Recommended"

currentVar <- d[,varName]


nL.density <- density(currentVar)
mymodes_indices <- find_modes(density(currentVar)$y) #you need to try it on the y axis
hightOfMode <- density(currentVar)$y[mymodes_indices]  #just to confirm that those are the correct
modes.var <- density(currentVar)$x[mymodes_indices] #the actual modes


nBreaks <-
  seq(min(currentVar),
      max(currentVar),
      length.out = 100)

allS.min <-
  min(c(min(currentVar), min(currentVar)))
allS.max <-
  max(c(max(currentVar), max(currentVar)))
xlim = c(allS.min, allS.max)




legend.text <-  paste(
  "Histogram and density ",
  varName,
  "\n true value of ",
  varName,
  " = ",
  trueValue
)

if (!is.null(trueValue && !is.nan(trueValue))) {
  legend.text <-  paste(
    "Histogram and density of ",
    varName
  )
}


p1 <-
  hist(currentVar,   breaks = nBreaks, plot = F)
plot(
  p1,
  col = rgb(1, 0, 0, 0.4),
  xlim = xlim,
  freq = F,
  main =legend.text,
  xlab =
    varName
)

boundariesOfModes <- function(boundaries, modes.var) {
  f1 <- function(i, boundaries, modes) {
    selectedMode <- modes[i]
    return(1)
  }
  a <- boundaries[1]
  b <- modes.var[1]
  print(paste(boundaries[1]))
  res <- lapply(seq_along(modes.var), f1, boundaries, modes.var)
  res
}

# boundariesOfModes(p1$breaks,modes.var)

if (!is.null(trueValue && !is.nan(trueValue))) {
  abline(v = trueValue, col = "blue")
}


dens <- density(currentVar)
lines(dens, col = "green4", lwd=3)
lapply(seq_along(modes.var),function(i, modes.var){abline(v=modes.var[i], col = 'blue', lty=4,lwd=3)},modes.var)
# ********************************
# red

legend(
  'topright',
  c('Scala Code histogram','PDF', 'Modes of pdf'),
  fill = c(rgb(1, 0, 0, 0.4), "green4","darkblue"),
  bty = 'n',
  border = NA
)
dev.copy(png, nL2PNGOut)
dev.off()
dev.copy(pdf, nL2PDFOut)
dev.off()

#' --------------------
# currentVar 


medianOut <- file.path(outDir,paste0("medians_",varName,".csv"))
write.csv(modes.var,medianOut, row.names = FALSE)


series.coverage <- d$coverage

titleGraph2 <- paste("Sample size vs respective coverage")
plot(currentVar,series.coverage, xlab = varName, ylab="Species coverage",col="green1",main=titleGraph2)
abline(h=0.9, col='red', lty=2,lwd=2)
lapply(seq_along(modes.var),function(i, modes.var){abline(v=modes.var[i], col = 'darkblue', lty=4,lwd=3)},modes.var)

legend(
  'bottomleft',
  c('Estimates', 'Targeted coverage','Modes of pdf'),
  fill = c("green1","red","darkblue"),
  bty = 'n',
  border = NA
)


graph.nL.coverageOut.pdf <- file.path(outDir,paste0("graph_nL_vs_coverage.pdf"))
graph.nL.coverageOut.png <- file.path(outDir,paste0("graph_nL_vs_coverage.png"))
graph.nL.coverageOut.jpg <- file.path(outDir,paste0("graph_nL_vs_coverage.jpeg"))

dev.copy(pdf,graph.nL.coverageOut.pdf)
dev.off()

dev.copy(png,graph.nL.coverageOut.png)
dev.off()

dev.copy(jpeg,graph.nL.coverageOut.jpg)
dev.off()

print(paste0("Sample size vs Coverage = ",graph.nL.coverageOut.png))
print(paste( "modes for ",varName," are ",paste(modes.var,"",collapse = ", ",sep = ""),collapse = "\n"))
print(paste0("Output to ",medianOut))


#  plot(c(100, 200), c(300, 450), type= "n", xlab = "", ylab = "")
#  rect(110, 300, 175, 350, density = 5, border = "red")
