package numerics

import basics.MetropolisHMCMC
import breeze.linalg.DenseVector
import breeze.numerics.exp
import org.apache.commons.math3.util.CombinatoricsUtils
import spire.std.double

import scala.util.{Failure, Success, Try}

object IGMetropolisMCMC {
  /**
    *
    * @param args
    */
  def main(args: Array[String]): Unit = {

  }

  //===================================
  /**
    *
    * @param nS
    * @param alpha
    * @param beta
    */
  case class ParametersIG(nS: Int, alpha: Double, beta: Double)

  // double logLikelihood(int n, double dAlpha, double dBeta)
  /**
    *
    * @param parametersIG
    * @return
    */
  def logLikelihood(n: Int, parametersIG: ParametersIG): Try[Double] = {
    //logMeasure
    import org.apache.commons.math3.special.Gamma._
    import org.apache.commons.math3.util.CombinatoricsUtils._
    //************************************************************

    val alpha = parametersIG.alpha
    val beta = parametersIG.beta

    val result = if (n >= 0) {
      val dLogFacN = if (n < 50) factorialLog(n)
      else logGamma(n + 1.0) // gsl_sf_lngamma(((double) n) + 1.0)

      val besselCQ_calculateWrapped: Try[Double] = BesselCQ.calculate(n, alpha, beta)
      val ret = besselCQ_calculateWrapped match {
        case Success(r) => r
        case Failure(e) => println; 0.0 //sd(n,alpha,beta)
      }
      Success(ret - dLogFacN)
    } else Failure(new Exception(s"n shoud >0, n = $n"))
    result
  }

  val PENALTY = 1.0e20

  def negLogLikelihood(params: ParametersIG)(data: Array[(Int, Int)]): Try[Double] = {
    import org.apache.commons.math3.util.CombinatoricsUtils.{factorialLog => acmFactorialLog}
    import params._
    val nD = MetropolisHMCMC.getD(data)

    val cq2 = MetroIGCQLibC.Instance

    val cgsl = GSLLibC.Instance

    def sumPartLL(x: (Int, Int)) = {
      val nI = x._1 //number of individuals
      val logP: Try[Double] = logLikelihood(nI, params)
      val logL = logP.get * x._2 - acmFactorialLog(x._2) // cgsl.gsl_sf_lnfact(x._2)// CombinatoricsUtils.factorialLog(x._2)
      //x._2 number of species with x._1 or nI individuals observed
      //todo 171015 work here
      Success(logL)
    }

    val alpha = params.alpha
    val beta = params.beta
    val nS = params.nS

    val r = if (alpha <= 0.0 || beta <= 0.0) PENALTY else {
      val logL00 = data.map(x => sumPartLL(x).get).sum
      val logP0 = logLikelihood(0, params)
      //      val cq:MetroCQJavaInterfaceLibrary = MetroCQJavaInterfaceLibrary.INSTANCE

      val logP1 = (nS - nD) * logP0.get
      //      val logP2 = -cgsl.gsl_sf_lnfact(nS-nD)
      val logP2 = -acmFactorialLog(nS - nD)
      val logP3 = acmFactorialLog(nS)
      //      val logP3 = cgsl.gsl_sf_lnfact(nS)
      val logL = logL00 + logP1 + logP2 + logP3
      -logL
    }
    Success(r)
  }

  /**
    *
    * @param paramsDV
    * @param data
    * @return
    */
  def negLogLikelihood(paramsDV: DenseVector[Double])(data: Array[(Int, Int)]): Try[Double] = {
    import org.apache.commons.math3.util.CombinatoricsUtils.{factorialLog => acmFactorialLog}
    //    import params._
    //    val nD = MetropolisHMCMC.getD(data)
    val cq2 = MetroIGCQLibC.Instance
    val cgsl = GSLLibC.Instance
    val params: ParametersIG = ParametersIG(paramsDV(0).toInt, paramsDV(1), paramsDV(2))

    /**
      *
      * @param x
      * @return
      */
    def sumPartLL(x: (Int, Int)) = {
      val nI = x._1 //number of individuals
      val logP: Try[Double] = logLikelihood(nI, params)
      val logL = logP.get * x._2 - acmFactorialLog(x._2) // cgsl.gsl_sf_lnfact(x._2)// CombinatoricsUtils.factorialLog(x._2)
      //x._2 number of species with x._1 or nI individuals observed
      //todo 171015 work here
      Success(logL)
    }

    //
    val alpha = params.alpha
    val beta = params.beta
    val nS = params.nS
    val nD = MetropolisHMCMC.getD(data)
    //
    Try(if (alpha <= 0.0 || beta <= 0.0) PENALTY else {
      val logL00 = data.map(x => sumPartLL(x).get).sum
      val logP0 = logLikelihood(0, params)

      val logP1 = (nS - nD) * logP0.get
      val logP2 = -acmFactorialLog(nS - nD)
      val logP3 = acmFactorialLog(nS)
      val logL = logL00 + logP1 + logP2 + logP3
      -logL
    }
    )

  }

  /**
    *
    * @param paramsDV
    * @param data
    * @return
    */
  def posLogLikelihood(paramsDV: DenseVector[Double])(data: Array[(Int, Int)]): Try[Double] = {
    import org.apache.commons.math3.util.CombinatoricsUtils.{factorialLog => acmFactorialLog}
    //    import params._
    //    val nD = MetropolisHMCMC.getD(data)
    val cq2 = MetroIGCQLibC.Instance
    val cgsl = GSLLibC.Instance
    val params: ParametersIG = ParametersIG(paramsDV(0).toInt, paramsDV(1), paramsDV(2))

    /**
      *
      * @param x
      * @return
      */
    def sumPartLL(x: (Int, Int)) = {
      val nI = x._1 //number of individuals
      val logP: Try[Double] = logLikelihood(nI, params)
      val logL = logP.get * x._2 - acmFactorialLog(x._2) // cgsl.gsl_sf_lnfact(x._2)// CombinatoricsUtils.factorialLog(x._2)
      //x._2 number of species with x._1 or nI individuals observed
      //todo 171015 work here
      Success(logL)
    }

    //
    val alpha = params.alpha
    val beta = params.beta
    val nS = params.nS
    val nD = MetropolisHMCMC.getD(data)
    //
    val r = if (alpha <= 0.0 || beta <= 0.0) PENALTY else {
      val logL00 = data.map(x => sumPartLL(x).get).sum
      val logP0 = logLikelihood(0, params)

      val logP1 = (nS - nD) * logP0.get
      val logP2 = -acmFactorialLog(nS - nD)
      val logP3 = acmFactorialLog(nS)
      val logL = logL00 + logP1 + logP2 + logP3
      logL
    }
    Success(r)
  }

  /**
    *
    * @param paramsDV
    * @param data
    * @return
    */
  def posLogLikelihoodWithLogParams(paramsDV: DenseVector[Double])(data: Array[(Int, Int)])(nD:Int): Try[Double] = {
    import org.apache.commons.math3.util.CombinatoricsUtils.{factorialLog => acmFactorialLog}
    //    import params._
    //    val nD = MetropolisHMCMC.getD(data)
    val cq2 = MetroIGCQLibC.Instance
    val cgsl = GSLLibC.Instance
    // *** Transform parametern from log_e/ln scale, ln(x) to x

//    val params: ParametersIG = ParametersIG(paramsDV(0).toInt, paramsDV(1), paramsDV(2))
    val params: ParametersIG = ParametersIG(scala.math.exp(paramsDV(0)).toInt, scala.math.exp(paramsDV(1)), scala.math.exp(paramsDV(2)))

    /**
      *
      * @param x
      * @return
      */
    def sumPartLL(x: (Int, Int)) = {
      val nI = x._1 //number of individuals
      val logP: Try[Double] = logLikelihood(nI, params)
      val logL = logP.get * x._2 - acmFactorialLog(x._2) // cgsl.gsl_sf_lnfact(x._2)// CombinatoricsUtils.factorialLog(x._2)
      //x._2 number of species with x._1 or nI individuals observed
      //todo 171015 work here
      Success(logL)
    }

    //
    val alpha = params.alpha
    val beta = params.beta
    val nS = params.nS
//    val nD = MetropolisHMCMC.getD(data)
    //
    val r = if (alpha <= 0.0 || beta <= 0.0) Success(PENALTY ) else {
      val r: Try[Double] =if(nS - nD>=0){
        val logL00 = data.map(x => sumPartLL(x).get).sum
        val logP0 = logLikelihood(0, params)
        val logP1 = (nS - nD) * logP0.get
        val logP2 = -acmFactorialLog(nS - nD)
        val logP3 = acmFactorialLog(nS)
        val logL = logL00 + logP1 + logP2 + logP3
        Success(logL)
      }else{
        val message =s"nS-nD=${nS-nD}<0, ND=$nD, nS=$nS "
        println(message)
        val e = new Exception(message)
        throw e
        Failure(e)
      }
      r
    }
    r
  }

  def nLogLikelihood(params: ParametersIG)(data: Array[(Int, Int)]): Try[Double] = {
    import org.apache.commons.math3.util.CombinatoricsUtils.factorialLog
    import params._
    val nD = MetropolisHMCMC.getD(data)

    def sumPartLL(x: (Int, Int)) = {
      // println("(" + x._1 + ", " + x._2 + ")")
      val nI = x._1 //number of individuals
      val logP: Try[Double] = logLikelihood(nI, params)
      val logL = logP.get * x._2 - CombinatoricsUtils.factorialLog(x._2)
      //x._2 number of species with x._1 or nI individuals observed
      //todo 171015 work here
      Success(logL)
    }

    val result = if (alpha <= 0.0 || beta <= 0.0) return Failure(new Exception(s"Invalid values of  parameters. alpha = ${alpha}, beta = =${beta}."))

    val logL00 = data.drop(1).map(x => sumPartLL(x).get).sum

    val logP0 = logLikelihood(0, params)
    val logP1 = (nS - nD) * logP0.get
    val logP2 = -factorialLog(nS - nD)
    val logP3 = factorialLog(nS)
    val logL = logL00 + logP1 + logP2 + logP3
    Success(-logL)
  }


  def evaluate = {

  }


}
