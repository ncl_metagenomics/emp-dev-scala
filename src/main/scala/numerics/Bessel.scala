package numerics

import breeze.numerics.{abs, Bessel => BBessel}

import scala.annotation.tailrec
import scala.math._
import scala.util.{Failure, Success, Try}

/**
  * Created by peter on 15/02/17.
  */
object Bessel {

  val k0_poly = List(
    1.1593151565841244842077226e-01,
    2.7898287891460317300886539e-01,
    2.5248929932161220559969776e-02,
    8.4603509072136578707676406e-04,
    1.4914719243067801775856150e-05,
    1.6271068931224552553548933e-07,
    1.2082660336282566759313543e-09,
    6.6117104672254184399933971e-12
  )

  val i0_poly = List(
    1.0000000000000000044974165e+00,
    2.4999999999999822316775454e-01,
    2.7777777777892149148858521e-02,
    1.7361111083544590676709592e-03,
    6.9444476047072424198677755e-05,
    1.9288265756466775034067979e-06,
    3.9908220583262192851839992e-08
  )

  val ak02_data = List[Double](
    -.1201869826307592240E-1,
    -.9174852691025695311E-2,
    +.1444550931775005821E-3,
    -.4013614175435709729E-5,
    +.1567831810852310673E-6,
    -.7770110438521737710E-8,
    +.4611182576179717883E-9,
    -.3158592997860565771E-10,
    +.2435018039365041128E-11,
    -.2074331387398347898E-12,
    +.1925787280589917085E-13,
    -.1927554805838956104E-14,
    +.2062198029197818278E-15,
    -.2341685117579242403E-16
  )

  val  GSL_DBL_EPSILON = 2.2204460492503131e-16

  val GSL_LOG_DBL_MAX = 7.0978271289338397e+02

  val M_LN2 = 0.69314718055994530942

  /* log_e 2 */


  case class ChebSeriesStruct(c: List[Double], expansionOrder: Int, a: Int, b: Int, singlePrecisionOrder: Int)

  type ChebSeries = ChebSeriesStruct

  case class BkResult(value: Double, error: Double)

  case class Results(e: Double, d: Double, dd: Double)


  case class Returns(value: Double, error: Double)

  val ak0_data: List[Double] = List[Double](
    -3.28737867094650101e-02, -4.49369057710236880e-02, +2.98149992004308095e-03, -3.03693649396187920e-04, +3.91085569307646836e-05, -5.86872422399215952e-06, +9.82873709937322009e-07, -1.78978645055651171e-07, +3.48332306845240957e-08, -7.15909210462546599e-09, +1.54019930048919494e-09, -3.44555485579194210e-10, +7.97356101783753023e-11, 1.90090968913069735e-11, 4.65295609304114621e-12, 1.16614287433470780e-12, 2.98554375218596891e-13, -7.79276979512292169e-14, +2.07027467168948402e-14, -5.58987860393825313e-15, +1.53202965950646914e-15, -4.25737536712188186e-16, +1.19840238501357389e-16, -3.41407346762502397e-17
  )

  val ak0_cs: ChebSeriesStruct = new ChebSeriesStruct(c = ak0_data, expansionOrder = 23, a = -1, b = 1, singlePrecisionOrder = 10) //ak0_data ::: List(23, -1, 1, 10)


  val ak02_cs: ChebSeriesStruct = new ChebSeriesStruct(c = ak02_data, expansionOrder = 13, a = -1, b = 1, singlePrecisionOrder = 8)


  val g1_data = List[Double](
    -0.3040578798253495954,
    -0.0566890984597120588,
    0.0039046158173275644,
    -0.0003746075959202261,
    0.0000435431556559844,
    -0.0000057417294453025,
    0.0000008282552104503,
    -0.0000001278245892595,
    0.0000000207978352949,
    -0.0000000035313205922,
    0.0000000006210824236,
    -0.0000000001125215474,
    0.0000000000209088918,
    -0.0000000000039715832,
    0.0000000000007690431,
    -0.0000000000001514697,
    0.0000000000000302892,
    -0.0000000000000061400,
    0.0000000000000012601,
    -0.0000000000000002615,
    0.0000000000000000548
  )


  /**
    *
    * @param cs
    * @param x
    * @return
    */
  def cheb_eval_e(cs: ChebSeries, x: Double): Returns = {

    /**
      *
      *
      */


    /**
      *
      * @param inputs
      * @param y
      * @return
      */
    def updateResult(inputs: Results, cs: ChebSeries, y: Double): Returns = {

      //todo: 170917 19:49 work here
      val coeficients = cs.c

      val temp = inputs.d

      val d = y * inputs.d - inputs.dd + 0.5 * coeficients(0)

      val e = inputs.e + abs(y * temp) + abs(inputs.dd) + 0.5 * abs(coeficients(0))
      val error = GSL_DBL_EPSILON * e + abs(coeficients(cs.expansionOrder)) // GSL_DBL_EPSILON * e + fabs(cs->c[cs->order]);

      Returns(value = d, error = error)
    }

    val d = 0.0
    val dd = 0.0
    val y = (2.0 * x - cs.a - cs.b) / (cs.b - cs.a)
    val y2 = 2.0 * y
    val e = 0.0
    val coeficients: List[Double] = cs.c.toList
    val coefShort = coeficients.slice(1, cs.expansionOrder + 1)
    val results1: Results = coefShort.reverse.foldLeft[Results](Results(0.0, 0.0, 0.0)) {
      (acc, coef) => {
        val temp = acc.d
        val d = y2 * acc.d - acc.dd + coef
        val e = acc.e + abs(y2 * temp) + abs(acc.dd) + abs(coef)
        val dd = temp
        Results(e, d, dd)
      }
    }
    //todo: 170917 19:48 work here
    updateResult(results1, cs, y)
  }

  /**
    *
    */
  def gsl_poly_eval(coeficients: List[Double], x: Double) = {
    coeficients.reverse.foldLeft[Double](0.0) {
      (z, c) => z * x + c
    }

  }


  def besselk0(x: Double): Double = {
    val ans: Double = if (x <= 2.0) {
      val y = x * x / 4d
      val ans: Double = (-Math.log(x / 2d) * BBessel.i0(x)) + (-0.57721566 + y * (0.42278420 + y * (0.23069756 + y * (0.3488590e-1 + y * (0.262698e-2 + y * (0.10750e-3 + y * 0.74e-5))))))
      ans
    } else {
      val y = 2.0 / x
      val ans: Double = (exp(-x) / sqrt(x)) * (1.25331414 + y * (-0.7832358e-1
        + y * (0.2189568e-1 + y * (-0.1062446e-1 + y * (0.587872e-2
        + y * (-0.251540e-2 + y * 0.53208e-3))))))
      ans
    }
    ans
  }


  def besselk1(x: Double): Double = {
    val ans: Double = if (x <= 2.0) {
      val y = x * x / 4.0
      val ans: Double = (Math.log(x / 2.0) * BBessel.i1(x)) + (1.0 / x) * (1.0 + y * (0.15443144
        + y * (-0.67278579 + y * (-0.18156897 + y * (-0.1919402e-1
        + y * (-0.110404e-2 + y * (-0.4686e-4)))))))
      ans
    } else {
      val y = 2.0 / x
      val ans: Double = (Math.exp(-x) / sqrt(x)) * (1.25331414 + y * (0.23498619
        + y * (-0.3655620e-1 + y * (0.1504268e-1 + y * (-0.780353e-2
        + y * (0.325614e-2 + y * (-0.68245e-3)))))))
      ans
    }
    ans
  }


  def besselk(n: Int, x: Double): Double = {
    require(n >= 2, "n must be >=2")
    val res: Double = if (n >= 2) {
      val tox = 2.0 / x
      val bkm: Double = besselk0(x)
      val bk = Bessel.besselk1(x) //k1(x)
      var res: (Double, Double) = (1 to (n - 1)).foldLeft[(Double, Double)](bkm, bk)((bks, i) => {
        val bkp = bks._1 + i * tox * bks._2
        //bkp=bkm+j*tox*bk;
        val bkm = bks._2
        // bkm=bk
        val bk = bkp; //bk=bkp;
        println(s"a=$i");
        (bkm, bk)
      })
      res._2 // Some(0.0)
    }
    else
      throw new Exception("n must be >=2")
    res
  }

  val M_PI = 3.14159265358979323846

  /* pi */

  case class ResultsTemmeGamma(g_1pnu: Double, g_1mnu: Double, g1: Double, g2: Double)

  def gsl_sf_temme_gamma(nu: Double) = {
    val anu = abs(nu)
    /* functions are even */
    val x = 4.0 * anu - 1.0
    var r_g1 = cheb_eval_e(g1_cs, x)
    var r_g2 = cheb_eval_e(g2_cs, x)
    val g1 = r_g1.value
    val g2 = r_g2.value
    val g_1mnu = 1.0 / (r_g2.value + nu * r_g1.value)
    val g_1pnu = 1.0 / (r_g2.value - nu * r_g1.value)
    Success(ResultsTemmeGamma(g_1pnu, g_1mnu, g1, g2))
  }

  def gsl_sf_bessel_K_scaled_temme(nu: Double, x: Double): Try[BesselTemmeResults] = {
    def besselTemmeCalc(max_iter: Int, fk: Double, ck: Double = 1, pk: Double, qk: Double, half_x: Double, sum0: Double, sum1: Double): (Double, Double) = {
      def besselTemmeCalcAccumulator(k: Int = 0, fk: Double, ck: Double = 1, pk: Double, qk: Double, sum0: Double, sum1: Double): (Double, Double) = {
        if (k >= max_iter) {
          (sum0, sum1)
        } else 0
        val fkN = (k * fk + pk + qk) / (k * k - nu * nu)
        val ckN = ck * half_x * half_x / k
        val pkN = pk / (k - nu)
        val qkN = qk / (k + nu)
        val hk = -k * fkN + pkN
        val del0 = ck * fk
        val del1 = ck * hk
        val sum0N = sum0 + del0
        val sum1N = sum1 + del1
        if (abs(del0) < 0.5 * abs(sum0N) * GSL_DBL_EPSILON)
          (sum0N, sum1N)
        else {
          besselTemmeCalcAccumulator(k + 1, fkN, ckN, pkN, qkN, sum0N, sum1N)
        }
      }

      besselTemmeCalcAccumulator(k = 0, fk, ck, pk, qk, sum0, sum1)
    }

    val max_iter = 15000
    val half_x = 0.5 * x
    val ln_half_x = log(half_x)
    val half_x_nu = exp(nu * ln_half_x)
    val pi_nu = M_PI * nu
    val sigma = -nu * ln_half_x
    val sinrat = if (abs(pi_nu) < GSL_DBL_EPSILON) 1.0 else pi_nu / sin(pi_nu)
    val sinhrat = if (abs(sigma) < GSL_DBL_EPSILON) 1.0 else sinh(sigma) / sigma
    val ex = exp(x)
    val resultsTemmeGamma0 = gsl_sf_temme_gamma(nu) //return: &g_1pnu, &g_1mnu, &g1, &g2
    val resultsTemmeGamma = resultsTemmeGamma0.get
    val fk = sinrat * (cosh(sigma) * resultsTemmeGamma.g1 - sinhrat * ln_half_x * resultsTemmeGamma.g2)
    val pk = 0.5 / half_x_nu * resultsTemmeGamma.g_1pnu
    val qk = 0.5 * half_x_nu * resultsTemmeGamma.g_1mnu
    val hk = pk
    //    val ck = 1.0
    /**
      * sum0 = fk;
      * sum1 = hk;
      */

    val besselTR = besselTemmeCalc(max_iter, ck = 1, fk = fk, pk = pk, qk = qk, half_x = half_x, sum0 = fk, sum1 = pk)
    val K_nu = besselTR._1 * ex
    val K_nup1 = besselTR._2 * 2.0 / x * ex
    val Kp_nu = -K_nup1 + nu / x * K_nu
    Success(BesselTemmeResults(K_nu, K_nup1, Kp_nu))
  }

  /**
    *
    * @param k_nu
    * @param k_nup1
    * @param kp_nu
    */
  case class BesselTemmeResults(k_nu: Double, k_nup1: Double, kp_nu: Double)


  /**
    *
    * @param nu
    * @param x
    */
  def gsl_sf_bessel_K_scaled_steed_temme_CF2(nu: Double, x: Double): Try[BesselTemmeResults] = {

    case class ResultsREccur(hi: Double, s: Double, ai: Double)

    case class ResultsAccum(hi: Double, s: Double, ai: Double, i: Double = 0)
    /**
      *
      * @param ai
      * @param Qi
      * @param maxIter
      * @param ci
      * @param qi
      * @param bi
      * @param qip1
      */
    def besselKSteedTemmeReccur(ai: Double, Qi: Double, maxIter: Int, ci: Double, qi: Double, bi: Double, qip1: Double, hi: Double, s: Double): ResultsAccum /*ResultsREccur*/ = {


      /**
        *
        * @param i
        * @param ai
        * @param Qi
        * @param ci
        * @param qi
        * @param bi
        * @param qip1
        * @param delhi
        * @param hi
        * @param s
        * @return
        */
      @tailrec
      def besselKSteedTemmeAccum(i: Int, ai: Double, Qi: Double, ci: Double, qi: Double, bi: Double, qip1: Double, delhi: Double, hi: Double, s: Double, di: Double): ResultsAccum = {
        if (i > maxIter) {
          di == 1.0 / bi;
          ResultsAccum(hi, s, ai, i)
          //  K_nu, double * K_nup1, double * Kp_nu
        } else {
          val aiN = ai - 2.0 * (i - 1)
          val ciN = ci - aiN * ci / i
          val tmp = (qi - bi * qip1) / aiN
          val qiN = qip1
          val qip1N = tmp
          val QiN = Qi + ciN * qip1N
          val biN = bi + 2.0
          val diN = 1.0 / (bi + ai * di)
          val delhiN = (bi * di - 1.0) * delhi
          val hiN = hi + delhi
          val dels = Qi * delhi
          val sN = s + dels
          if ((abs(dels / s) < GSL_DBL_EPSILON)) {
            ResultsAccum(hiN, sN, aiN, i)
          } else {
            besselKSteedTemmeAccum(i + 1, aiN, QiN, ciN, qiN, biN, qip1N, delhiN, hiN, sN, diN)
          }
        }
      }

      val di = 1.0 / bi

      val r: ResultsAccum = besselKSteedTemmeAccum(0, ai, Qi, ci, qi, bi, qip1, delhi = 1, hi, s, di)
      //todo: 190921 10:57
      //      val hiN = r.hi*(1)
      //      ResultsREccur(0.0, 0.0)
      r
    }

    val maxiter = 10000
    val i = 1
    val bi = 2.0 * (1.0 + x);
    val di = 1.0 / bi;
    val delhi = di
    val hi = di
    val qi = 0.0
    val qip1 = 1.0
    val ai = -(0.25 - nu * nu)
    val a1 = ai
    val ci = -ai
    val Qi = -ai
    // ===========
    val s = 1.0 + Qi * delhi
    // ===========
    val r: ResultsAccum = besselKSteedTemmeReccur(ai, Qi, maxiter, ci, qi, bi, qip1, hi, s)
    // hi *= -a1
    val hiN = r.hi * (-r.ai)
    // K_nu   = sqrt(M_PI/(2.0*x)) / s
    val K_nu = sqrt(M_PI / (2.0 * x)) / r.s
    // K_nup1 = *K_nu * (nu + x + 0.5 - hi)/x
    val K_nup1N = K_nu * (nu + x + 0.5 - r.hi) / x
    val Kp_nuN = -K_nup1N + nu / x * K_nu

    if (r.i == maxiter) {
      Failure(new Exception(s"Reached maximum of iterations"))
    }
    else {
      Success(BesselTemmeResults(K_nu, K_nup1N, Kp_nuN))
    }
    //todo: 190919 14:44 work here
    // return: &K_mu, &K_mup1, &Kp_mu
  }

  // CQ version of bessel fucntion
  /**
    *
    * loglikelihood: status = bessel(&dRet,n, dAlpha,dBeta);
    *
    * @param n
    * @param dAlpha
    * @param dBeta
    */
  def cqBessel(n: Int, dAlpha: Double, dBeta: Double): Unit = {
    //dG = dGamma
    val dG = -0.5
    // dResults
    val dR = 0.0
    // dO = dOmega
    val dO = 0.0
    // todo: 170929 work here

  }

  def gsl_sf_bessel_Knu_scaled_e10_e(nu: Double, x: Double): Try[BkResult] = {
    if (x <= 0.0 || nu < 0.0) Failure(new Exception("")) else {
      val N: Int = (nu + 0.5).toInt

      val mu: Double = nu - N; /* -1/2 <= mu <= 1/2 */

      val n = 0
      val e10 = 0
      if (x < 2.0) {
        val r = gsl_sf_bessel_K_scaled_temme(mu, x) // return: , &K_mu, &K_mup1, &Kp_mu
      } else {
        gsl_sf_bessel_K_scaled_steed_temme_CF2(mu, x); // return: &K_mu, &K_mup1, &Kp_mu
      }

      Failure(new Exception(""))
    }


    Failure(new Exception(""))
  }

  /**
    * Computes the logarithm of the irregular modified Bessel function of fractional order \nu, \ln(K_\nu(x)) for x>0, \nu>0.
    * I  mplementation of gsl function in C
    *
    */
  def gsl_sf_bessel_lnKnu_e(nu: Double, x: Double) = {
    val r: Try[BkResult] =
      if (x <= 0.0 || nu < 0.0)
        Failure(new Exception("Wrong values of input parameters. Required x>0 and nu>=0!"))
      else if (nu == 0) {
        val K_scaled: Try[Bessel.BkResult] = gsl_sf_bessel_K0_scaled_e(x)
        val value = -x + log(abs(K_scaled.get.value))
        val err1 = GSL_DBL_EPSILON * abs(x) + abs(K_scaled.get.error / K_scaled.get.value);
        val err = err1 + GSL_DBL_EPSILON * abs(value);
        Success(BkResult(value, err))
      } else {
        //if (x < 2.0 && nu > 1.0)


        /* Make use of the inequality
                      * Knu(x) <= 1/2 (2/x)^nu Gamma(nu),
                      * which follows from the integral representation
                      * [Abramowitz+Stegun, 9.6.23 (2)]. With this
                      * we decide whether or not there is an overflow
                      * problem because x is small.
                      */
        val lg_nu = breeze.numerics.lgamma(nu)
        val ln_bound = -M_LN2 - nu * log(0.5 * x) + lg_nu
        val r: Try[BkResult] =
          if (ln_bound > GSL_LOG_DBL_MAX - 20.0 && x < 2.0 && nu > 1.0) {
            /* x must be very small or nu very large (or both). */
            val xi = 0.25 * x * x
            val sum0 = 1.0 - xi / (nu - 1.0)
            val sum = if (nu > 2.0) sum0 + (xi / (nu - 1.0)) * (xi / (nu - 2.0)) else sum0
            val value = ln_bound + log(sum);
            val err1 = 0 // error of calculating lgamma
            val err = err1 + 2.0 * GSL_DBL_EPSILON * abs(value)
            Success(BkResult(value, err))
          } else {
            /* can drop-through here? No, but is should */
            /* We passed the above tests, so no problem.
            * Evaluate as usual. Note the possible drop-through
            * in the above code!
            */
            gsl_sf_bessel_Knu_scaled_e10_e(nu, x)
            Failure(new Exception(""))
          }
        r
      }

    r
  }


  /**
    * compute the scaled irregular modified cylindrical Bessel function of zeroth order \exp(x) K_0(x) for x>0.
    * According gsl implementaion used by  QC code
    *
    * @param x
    */
  def gsl_sf_bessel_K0_scaled_e(x: Double): Try[BkResult] = {
    val r: Try[BkResult] = if (x <= 0.0)
      Failure(new Exception("Wrong values of input parameters. Required x>0!"))
    else if (x < 1.0) {
      val lx = log(x)
      val ex = exp(x)
      val x2 = x * x
      val value: Double = ex * gsl_poly_eval(k0_poly, x2) - lx * (1.0 + 0.25 * x2 * gsl_poly_eval(i0_poly, 0.25 * x2))
      val err1 = ex * (1.6 + abs(lx) * 0.6) * GSL_DBL_EPSILON
      val err2 = 2.0 * GSL_DBL_EPSILON * abs(value)
      val err: Double = err1 + err2
      Success(BkResult(value, err))
    } else if (x <= 8) {

      val sx = sqrt(x)
      val c = cheb_eval_e(ak0_cs, (16.0 / x - 9.0) / 7.0)
      val value = (1.203125 + c.value) / sx;
      /* 1.203125 = 77/64 */
      val err1 = c.error / sx
      val err = err1 + 2.0 * GSL_DBL_EPSILON * abs(value);
      Success(BkResult(value, err))
      //todo: 170916 work here

    } else {
      val sx = sqrt(x)
      // cheb_eval_e(&ak02_cs, 16.0/x-1.0, &c);
      val c = cheb_eval_e(ak02_cs, 16.0 / x - 1.0)
      val value = (1.25 + c.value) / sx;
      val err1 = (c.error + GSL_DBL_EPSILON) / sx;
      val err = err1 + 2.0 * GSL_DBL_EPSILON * abs(value);
      Success(BkResult(value, err))
    }
    r
  }


  // order = expansionOrder, order_sp=singlePrecisionOrder


  /*

    struct cheb_series_struct {
      double * c;   /* coefficients                */
      int order;    /* order of expansion          */
      double a;     /* lower interval point        */
      double b;     /* upper interval point        */
      int order_sp; /* effective single precision order */
    };
    typedef struct cheb_series_struct cheb_series;

     */


  /**
    *
    */


  /**
    *
    */


  /**
    * According "Numerical recipes" 2017 - Webnote 8 p.3
    *
    * @param n = nu, order of Bessel function
    * @param x = agument
    * @return value of Bessel function for order n and value x
    */
  def besselik(n: Int, x: Double): Try[Double] = {
    val MAXIT: Int = 10000
    val EPS = scala.Double.MinPositiveValue //The smallest difference between two values of scala.Double.
    val FPMIN: Double = scala.Double.MinPositiveValue / EPS //Machines SMALLEST FLOATING-POINT NUMBER
    val XMIN = 20.5
    val PI = scala.math.Pi

    case class Results(b: Double = 0.0, c: Double = 0.0, d: Double = 0.0, h: Double = 0.0) {}

    def calc1(maxit: Int, b: Double, c: Double, d: Double, eps: Double, xi2: Double, h: Double): Try[Results] = {
      @tailrec
      def calc1Acumulator(counts: Int, values: Results): Try[Results] = {
        val del = values.c * values.d
        if (counts == 0 || abs(del - 1.0) <= EPS)
          if (abs(del - 1.0) > EPS)
            Failure(new Exception(s"Convergence was not achieved after max nmber of iteration maxit = ${
              maxit
            }"))
          else
            Try(values)
        else {
          val b = values.b + xi2
          val d = 1.0 / (b + values.d)
          val c = b + 1.0 / values.c
          val del = c * d
          val h = del * values.h
          val rn = Results(b, c, d, h)
          calc1Acumulator(counts - 1, rn)
        }
      }

      val r: Try[Results] = calc1Acumulator(MAXIT, Results(b, c, d, h))
      r
    }

    /*
        Output from Besselik
         */
    case class Results2(params: Try[Results], xi: Double, nl: Double, xmu: Double)

    val r: Try[Results2] = if (x <= 0 || n <= 0) Failure(new Exception(s"Bad arguments in besselik n = $n and x = $x")) else {
      val nl: Double = scala.math.rint(n + 0.5)
      val xmu = n - nl
      val xmu2 = xmu * xmu
      val xi = 1.0 / x
      val xi2 = 2.0 * xi
      val h0 = n * xi
      val h = if (h0 < FPMIN) FPMIN else h0
      val b: Double = xi2 * n
      val params0: Try[Results] = calc1(MAXIT, b, c = h, d = 0.0, EPS, xi2, h)
      Success(Results2(params0, xi, nl, xmu))
    }

    r match {
      case Failure(e) => throw e
    }
    val p = r.toOption.get.params
    //---------****-----1
    val finalR = p match {
      case Failure(e) => {
        throw e
      }
      case Success(v) => {
        val ril = FPMIN
        val ripl = v.h * ril
        val ril1 = ril
        val rip1 = ripl
        val fact = n * r.get.xi

        //todo: 170911  work here to ouput bessel value
        val r2 = loop2Besselik(r.get.xi, ril, ripl, r.toOption.get.nl, fact)
        Success(r2)
      }
    }
    val f = finalR.get.ripl / finalR.get.ril
    loop3Besselik(x, XMIN, r.get.xmu, finalR.get.fact, EPS)
    //loop3Besselik()
    //todo: 170908  continue implementing Besselik here


    //    val a: Results = r.params.toOption.get
    //todo: 170908  work here to ouput bessel value
    Success(0.0) // Complete this
  }

  /**
    *
    */
  def loop3Besselik(x: Double, XMIN: Double, xmu: Double, fact: Double, EPS: Double): Any = {

    def loop3BesselikAcumulator(x: Double, xmu: Double, fact: Double): Any = {
      val x2 = 0.5 * x
      val pimu = scala.math.Pi * xmu
      val factN = if (scala.math.abs(pimu) < EPS) 1.0 else pimu / sin(pimu)
      val d = -log(x2) // natural logarithm
      val e = xmu * d
      //todo: 170914 work here now
      val fact2 = if (abs(e) < EPS) 1.0 else sinh(e) / e
      val xx = 8.0 * sqrt(xmu) - 1.0
      //      val gam1 = chebev()
      //  cccc

    }

    loop3BesselikAcumulator(x, xmu, fact)
  }


  /**
    *
    * @param x1
    * @param ril
    * @param ripl
    * @param nl
    * @param fact
    * @return
    */
  def loop2Besselik(x1: Double, ril: Double, ripl: Double, nl: Double, fact: Double): loop2BesselikAccumulatorResult = {
    @tailrec
    def loop2BesselikAccumulator(iteration: Int, fact0: Double, ripl0: Double, ril0: Double): loop2BesselikAccumulatorResult = {
      if (iteration <= 0)
        loop2BesselikAccumulatorResult(ril0, fact0, ripl0)
      else {
        val ritemp = fact0 * ril0 * ripl0
        val fact = fact0 - x1
        val ripl = fact * ritemp * ril0
        val ril = ritemp
        loop2BesselikAccumulator(iteration - 1, fact, ripl, ril)
      }
    }

    loop2BesselikAccumulator(nl.toInt, fact, ripl, ril)
  }

  case class loop2BesselikAccumulatorResult(ril: Double, fact: Double, ripl: Double)

  //
  //  object k extends UFunc {
  //
  //    implicit object ImplKInt extends Impl2[Int, Int, Double] {
  //      def apply(n: Int, x: Int): Double = ImplKDouble(n, x.toDouble)
  //    }
  //
  //    implicit object ImplKDouble extends Impl2[Int, Double, Double] {
  //      import k0.ImplDouble
  //
  //
  //      def apply(n: Int, x: Double): Double = {
  //        require(n >= 2, "n must be >=2")
  //        val res: Double = if (n >= 2) {
  //          val tox = 2.0 / x
  //          val bkm:Double =  Besselk0(x)
  //          val bk = 1.2
  //          // k1(x)
  //          var res: (Double, Double) = (1 to n).foldLeft[(Double, Double)](bkm, bk)((bks, i) => {
  //            val bkp = bks._1 + i * tox * bks._2
  //            //bkp=bkm+j*tox*bk;
  //            val bkm = bks._2
  //            // bkm=bk
  //            val bk = bkp; //bk=bkp;
  //            println(s"a=$i");
  //            (bkm, bk)
  //          })
  //          res._2 // Some(0.0)
  //        }
  //        else
  //          throw new Exception("n must be >=2")
  //        res
  //      }
  //    }
  //
  //  }


  //  object k0 extends UFunc {
  //
  //    implicit object ImplInt extends Impl[Int, Double] {
  //      def apply(x: Int): Double = ImplDouble(x.toDouble)
  //    }
  //
  //    implicit object ImplDouble extends Impl[Double, Double] {
  //
  //      def apply(x: Double): Double = {
  //        val ans: Double = if (x <= 2.0) {
  //          val y = x * x / 4d
  //          val ans: Double = (-Math.log(x / 2d) * BBessel.i0(x)) + (-0.57721566 + y * (0.42278420 + y * (0.23069756 + y * (0.3488590e-1 + y * (0.262698e-2 + y * (0.10750e-3 + y * 0.74e-5))))))
  //          ans
  //        } else {
  //          val y = 2.0 / x
  //          val ans: Double = (exp(-x) / sqrt(x)) * (1.25331414 + y * (-0.7832358e-1
  //            + y * (0.2189568e-1 + y * (-0.1062446e-1 + y * (0.587872e-2
  //            + y * (-0.251540e-2 + y * 0.53208e-3))))))
  //          ans
  //        }
  //        ans
  //      }
  //    }
  //
  //  }

  //  object k1 extends UFunc {
  //
  //    def apply(x: Double): Double = {
  //      val ans: Double = if (x <= 2.0) {
  //        val y = x * x / 4.0
  //        val ans: Double = (Math.log(x / 2.0) * BBessel.i1(x)) + (1.0 / x) * (1.0 + y * (0.15443144
  //          + y * (-0.67278579 + y * (-0.18156897 + y * (-0.1919402e-1
  //          + y * (-0.110404e-2 + y * (-0.4686e-4)))))))
  //        ans
  //      } else {
  //        val y = 2.0 / x
  //        val ans: Double = (Math.exp(-x) / sqrt(x)) * (1.25331414 + y * (0.23498619
  //          + y * (-0.3655620e-1 + y * (0.1504268e-1 + y * (-0.780353e-2
  //          + y * (0.325614e-2 + y * (-0.68245e-3)))))))
  //        ans
  //      }
  //      ans
  //    }
  //
  //  }

  val g2_data = List[Double](
    -0.0967329367532432218,
    -0.0452077907957459871,
    0.0028190005352706523,
    -0.0002899167740759160,
    0.0000407444664601121,
    -0.0000071056382192354,
    0.0000014534723163019,
    -0.0000003364116512503,
    0.0000000859774367886,
    -0.0000000238437656302,
    0.0000000070831906340,
    -0.0000000022318068154,
    0.0000000007401087359,
    -0.0000000002567171162,
    0.0000000000926707021,
    -0.0000000000346693311,
    0.0000000000133950573,
    -0.0000000000053290754,
    0.0000000000021775312,
    -0.0000000000009118621,
    0.0000000000003905864,
    -0.0000000000001708459,
    0.0000000000000762015,
    -0.0000000000000346151,
    0.0000000000000159996,
    -0.0000000000000075213,
    0.0000000000000035970,
    -0.0000000000000017530,
    0.0000000000000008738,
    -0.0000000000000004487,
    0.0000000000000002397,
    -0.0000000000000001347,
    0.0000000000000000801,
    -0.0000000000000000501
  )


  val g1_cs: ChebSeriesStruct = new ChebSeriesStruct(c = g1_data, expansionOrder = 20, a = -1, b = 1, singlePrecisionOrder = 13)

  val g2_cs = new ChebSeriesStruct(c = g2_data, expansionOrder = 13, a = -1, b = 1, singlePrecisionOrder = 8)

}
