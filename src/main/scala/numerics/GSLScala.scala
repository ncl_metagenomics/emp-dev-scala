package numerics

import com.sun.jna.{Library, Native, Platform}

trait GSLLibC extends Library {
  def puts(s: String)

  /**
    *
    * @param nu
    * @param x
    * @return
    */
  def gsl_sf_bessel_Knu_scaled(nu:Double, x:Double):Double
//  double gsl_sf_bessel_lnKnu(const double nu, const double x)
  /**
    *
    * @param nu
    * @param x
    * @return
    */
  def gsl_sf_bessel_lnKnu(nu:Double,  x:Double):Double

//  double gsl_sf_bessel_Knu(const double nu, const double x)

  /**
    *
    * @param nu
    * @param x
    * @return
    */
  def gsl_sf_bessel_Knu(nu:Double,  x:Double):Double

  // gsl_sf_fact(n)
  /**
    *
    * @param n
    * @return
    */
  def gsl_sf_fact(n:Int):Double

  /**
    *
    * @param n
    * @return
    */
  def gsl_sf_lnfact(n:Int):Double
}

object GSLLibC {
  def Instance = Native.loadLibrary("gsl",classOf[GSLLibC]).asInstanceOf[GSLLibC]
//  def

}

object BesselJNASpecial {
  def main(args: Array[String]) {

    var res = GSLLibC.Instance.gsl_sf_bessel_Knu_scaled(1.5,2.5)
    println(s"gsl_sf_bessel_Knu_scaled = $res")

    res = GSLLibC.Instance.gsl_sf_bessel_Knu(1.5,2.5)
    println(s"gsl_sf_bessel_Knu = $res")

    res = GSLLibC.Instance.gsl_sf_bessel_lnKnu(1.5,2.5)
    println(s"gsl_sf_bessel_lnKnu = $res")


  }
}