package numerics

import scala.util.Try

object RunGSL_sf_bessel_Knu_scaled_e10_e extends App{

  val x = 2.0
  val nu = 0.5
  val r: Try[Bessel.BkResult] = Bessel.gsl_sf_bessel_Knu_scaled_e10_e(nu, x)
  println(s"Scala Bessel.gsl_sf_bessel_Knu_scaled_e10_e: value = ${r.get.value}, error = ${r.get.error}")
  val r2: Try[Bessel.BesselTemmeResults] = Bessel.gsl_sf_bessel_K_scaled_temme(nu, x)
  println(s"Bessel.gsl_sf_bessel_K_scaled_temme: k_nu = ${r2.get.k_nu}, k_nup1 = ${r2.get.k_nup1}, kp_nu = ${r2.get.kp_nu}")


}
