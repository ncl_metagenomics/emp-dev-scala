package numerics

import com.sun.jna.{Library, Native,Platform}

trait MetroIGCQLibC extends Library {
  //    def puts(s: String)
  //    def gsl_sf_bessel_Knu_scaled(nu:Double, x:Double):Double
  //    //  double gsl_sf_bessel_lnKnu(const double nu, const double x)
  //    def gsl_sf_bessel_lnKnu(nu:Double,  x:Double):Double
  //
  //    //  double gsl_sf_bessel_Knu(const double nu, const double x)
  //
  //    def gsl_sf_bessel_Knu(nu:Double,  x:Double):Double

  def besselExt(n:Int,alpha:Double,beta:Double):Double

  def logLikelihood(n:Int, alpha:Double, beta:Double):Double
  //libmetroig
}

object MetroIGCQLibC{
//  def Instance = Native.loadLibrary( "metroig", classOf[MetroIGCQLibC]).asInstanceOf[MetroIGCQLibC]
def Instance = Native.loadLibrary(
  if (Platform.isWindows) "msvcrt" else "c",
  classOf[MetroIGCQLibC]).asInstanceOf[MetroIGCQLibC]
}

object MetroIgCQJna {
  def main(args: Array[String]): Unit = {
    var res = MetroIGCQLibC.Instance.besselExt(1,2.2,3.1)
    println(s"gsl_sf_bessel_Knu_scaled = $res")
  }

  /**
    *
    * @param n
    * @param alpha
    * @param beta
    * @return
    */
  def logLikelihood(n:Int, alpha:Double, beta:Double):Double = numerics.MetroIGCQLibC.Instance.logLikelihood(n,alpha,beta)

  /**
    *
    * @param n
    * @param alpha
    * @param beta
    * @return
    */
  def besselExt(n:Int,alpha:Double,beta:Double):Double = numerics.MetroIGCQLibC.Instance.besselExt(n,alpha,beta)

}
