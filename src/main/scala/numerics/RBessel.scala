package numerics

import breeze.generic.UFunc
import org.ddahl.rscala._

//import numerics.Bessel.k.Impl2
//import numerics.Bessel.k0.Impl
//import numerics.Bessel.k1.Impl2

/**
  * Created by peter on 17/02/17.
  */
object RBessel {
  val R = RClient()

  def k1(x: Double): Double = {
    R.evalR (s"library(Bessel);res<-besselK(x=$x, nu=1, expon.scaled = FALSE)")
    R.evalD0("res")
  }

  def k0( x: Double): Double = {
    R.evalR (s"library(Bessel);res<-besselK(x=$x, nu=0, expon.scaled = FALSE)")
    R.evalD0("res")
  }

  def k(n: Int, x: Double): Double = {
    R.evalR (s"library(Bessel);res<-besselK(x=$x, nu=$n, expon.scaled = FALSE)")
    R.evalD0("res")
  }

//  object k extends UFunc {
//    implicit object ImplInt extends Impl2[Int, Int, Double] {
//      def apply(n: Int, x: Int): Double = ImplDouble(n, x.toDouble)
//    }
//
//    implicit object ImplDouble extends Impl2[Int, Double, Double] {
//
//      def apply(n: Int, x: Double): Double = {
//        R.evalR (s"library(Bessel);res<-besselK(x=$x, nu=$n, expon.scaled = FALSE)")
//        R.evalD0("res")
//      }
//    }
//  }

//  object k0 extends UFunc {
//
//    implicit object ImplInt extends Impl[Int,  Double] {
//      def apply(x: Int): Double = ImplDouble(x.toDouble)
//    }
//
//    implicit object ImplDouble extends Impl[Double, Double] {
//
//      def apply( x: Double): Double = {
//        R.evalR (s"library(Bessel);res<-besselK(x=$x, nu=0, expon.scaled = FALSE)")
//        R.evalD0("res")
//      }
//    }
//
//  }

//  object k1 extends UFunc {
//
//    implicit object ImplInt extends Impl[ Int, Double] {
//      def apply( x: Int): Double = ImplDouble(x.toDouble)
//    }
//
//    implicit object ImplDouble extends Impl[Double, Double] {
//
//      def apply(x: Double): Double = {
//        R.evalR (s"library(Bessel);res<-besselK(x=$x, nu=1, expon.scaled = FALSE)")
//        R.evalD0("res")
//      }
//
//    }
//
//  }

}


