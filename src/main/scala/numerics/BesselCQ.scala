package numerics

import GSLLibC._
import breeze.numerics.log
import org.apache.commons.math3.special.Gamma._

import scala.util.{Failure, Success, Try}


/**
  * calculate value of Bessel function for loglikelihood of poisson-inversrse Gaussian distribution
  *
  */
object BesselCQ {
  // todo: 171009 work here

  def gsl_sf_bessel_lnKnu(nu: Double, x: Double) = Instance.gsl_sf_bessel_lnKnu(nu, x)

  def gsl_sf_bessel_lnKnu_safer(nu: Double, x: Double): Try[Double] = {
    if (x <= 0.0 || nu < 0.0)
      Failure(new Exception(s"Incorrect input values x<=0.0 and nu<0.0 x = $x, nu = $nu"))
    else Success(BesselCQ.gsl_sf_bessel_lnKnu(nu, x))

  }


  def main(args: Array[String]): Unit = {


  }

  def currentMethodName(): String = Thread.currentThread.getStackTrace()(2).getMethodName

  /**
    *
    * @param n
    * @param alpha
    * @param beta
    * @return
    */
  def calculate(n: Int, alpha: Double, beta: Double) = {
    val gamma = -0.5
    val nc: Double = n
    //    val res = 0
    val nu: Double = Math.abs(gamma + nc)
    val gamma2: Double = Math.abs(gamma)

    val omega: Double = Math.sqrt(alpha * alpha + beta * beta) - beta


    val logK2_Old_W: Try[Double] = gsl_sf_bessel_lnKnu_safer(nu, alpha)
    val logK2 = logK2_Old_W match {
      case Success(logK2_Old) =>
        val logK2: Try[Double] =
          if (logK2_Old.isInfinity) {
          val logK2_New: Try[Double] =
            if (alpha < 0.1 * Math.sqrt(nu + 1.0)) {
              val tmp1: Double = logGamma(nu) + (nu - 1.0) * Math.log(2.0) - nu * Math.log(alpha)
              Success(tmp1)
            } else {
              Failure(new Exception("alpha >= 0.0 * Math.sqrt(nu + 1.0)"))
            }
          logK2_New
        } else {
          Success(logK2_Old)
        }
        logK2

      case Failure(e)=> Failure(e)

    }

    val tmp0_gsl_sf_bessel_lnKnu_wrapped = gsl_sf_bessel_lnKnu_safer(gamma2, omega)
    val tmp0_gsl_sf_bessel_lnKnu =  tmp0_gsl_sf_bessel_lnKnu_wrapped match {
      case Success(tmp1_bessel) => tmp1_bessel
      case Failure(e) => println(e.getMessage); Double.NaN
    }
    val dLogK1 = gamma * log(omega / alpha) - tmp0_gsl_sf_bessel_lnKnu
    val temp1 = Math.log((beta * omega) / alpha)

    logK2 match {
      case Success(a) => Try(n * temp1 + a + dLogK1)
      case Failure(e) => Failure(new Exception(s"Failure in ${currentMethodName()} ${e.getMessage}"))
    }
  }


}
