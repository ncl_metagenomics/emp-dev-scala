package basics

import com.github.tototoshi.csv.CSVWriter
import com.github.tototoshi.csv.defaultCSVFormat
import com.github.tototoshi.csv._

object CompareLogPbyRecord {
  val mcmcMetro = new MetropolisHMCMC()

  def main(args: Array[String]) = {
    //val quinceLogPByRecord = "data/nLL_debug_by_record_short_160128_2147.csv"
    val quinceLogPByRecord = "data/T2_160503/nLL_debug_by_record_s.csv"//"data/nLL_debug_by_record_160128_2303.csv"
    val relError = 1 / 10000000
    val absError = 0.00001
    val res: List[(Boolean, Double, CompareLogPbyRecord.Parameters, Double, Double)] = getFailedValues(quinceLogPByRecord, relError, absError)
    val res2: List[((Boolean, Double, CompareLogPbyRecord.Parameters, Double, Double), (Double, Double))] = addDifferences(res)
    //******
    //writeIncorectNLL("output/incorret_logPs.csv", res)
    writeIncorectNLL02("output/T2_160503/incorret_logPs_s.csv", res2)
    println(s"${res.length - 1} incorrect logPs found!!!")
    println("test")
  }

  def addDifferences(res: List[(Boolean, Double, CompareLogPbyRecord.Parameters, Double, Double)]) = {
    // first is NLL from scala code and second is Quince's NLL
    var diffScala0 = res map { e => (e._2, e._3.dLogP) } sliding (2) map { e => (e(0)._1 - e(1)._1, e(0)._2 - e(1)._2) }
    var diffScala = List((0.0, 0.0)) ::: (diffScala0.toList)
    var combRes = (res, diffScala).zipped.toList
    println(s"Orig array length ${res.length}")
    println(s"Diff array length ${diffScala0.length}")
    println(s"New (ammended) Diff array length ${diffScala0.length}")
    combRes
  }

  def writeIncorectNLL(filename: String, data: List[((Boolean, Double, Parameters, Double, Double))]) {
    val writer = CSVWriter.open(filename)
    //nIter,dMDash,dVDash,nSDash,nA,dLogP
    val header = List("nIter", "dMDash", "dSigma", "dVDash", "nA", "logPQuince", "logPScala", "difLogP", "relDifLotP")
    writer.writeRow(header)
    data.map(e => writer.writeRow(List(e._3.nIter, e._3.dMDash, Math.sqrt(e._3.dVDash), e._3.dVDash, e._3.nA, e._3.dLogP, e._2, e._4, e._5)))
    writer.close()
    println(s"Writing to filename $filename")
  }

  def writeIncorectNLL02(filename: String, data: List[((Boolean, Double, CompareLogPbyRecord.Parameters, Double, Double), (Double, Double))]) {
    val writer = CSVWriter.open(filename)
    //nIter,dMDash,dVDash,nSDash,nA,dLogP
    val header = List("nIter", "dMDash", "dSigma", "dVDash", "nA", "logPQuince", "logPScala", "difLogP", "relDifLotP", "tnLLDiffQuince", "tnLLDiffScala")
    writer.writeRow(header)
    data.map(e => writer.writeRow(List(e._1._3.nIter, e._1._3.dMDash, Math.sqrt(e._1._3.dVDash), e._1._3.dVDash, e._1._3.nA,
      e._1._3.dLogP, e._1._2, e._1._4, e._1._5, e._2._1, e._2._2)))
    writer.close()
    println(s"Writing to filename $filename")
  }

  case class Parameters(nIter: Int, dMDash: Double, dVDash: Double, nA: Int, dLogP: Double)

  def getFailedValues(qOutFileN: String, relError: Double, absError: Double): List[(Boolean, Double, Parameters, Double, Double)] = {
    val tuples = io.Source.fromFile(qOutFileN).getLines.drop(1).map(l => getFlaggedValue(l, relError, absError)) //.filter(_._1)
    tuples.toList
  }

  /**
   * @param line
   *
   */
  def getFlaggedValue(line: String, relError: Double, absError: Double): (Boolean, Double, Parameters, Double, Double) = {
    val pars: Parameters = parseLine(line)
    val ll = new LogLikelihoodPoissonLogNormal()
    val mu = pars.dMDash
    val sigma = Math.sqrt(pars.dVDash)
    val n = pars.nA
    val logPQuince = pars.dLogP
    val logPScala = ll.calculateWithRLikeScala(mu, sigma, n)
    val difLogP = logPQuince.abs - logPScala.abs
    val relDifLotP = difLogP / logPQuince.abs
    // nIter,dMDash,dVDash,nSDash,nA,dLogP

    val isInCorrect = !(((logPQuince.abs - logPScala.abs) < logPQuince.abs * relError) && (logPQuince.abs - logPScala.abs) < absError)
    val res = (isInCorrect, logPScala, pars, difLogP, relDifLotP)
    if (pars.nIter % 200 == 0) {
      println(s"processing record ${pars.nIter} - logP value is ${if (isInCorrect) "incorrect" else "correct"}")
    }
    if (isInCorrect && false) { println(s"processing record ${pars.nIter} - logP value is incorrect, diff = ${logPQuince.abs - logPScala.abs}, relDiff = ${(logPQuince.abs - logPScala.abs) / logPQuince}") }
    res
  }

  def parseLine(line: String): Parameters = {
    // nIter,dMDash,dVDash,nSDash,nA,dLogP
    val data = line.split(",")
    val nIter = try {
      data(0).toInt
    } catch {
      case e: NumberFormatException => { println(s"nIter = ${data(0)} "); 0 }
    }
    //nIter,dMDash,dVDash,nSDash,nA,dLogP
    val dMDash = data(1).toDouble
    val dVDash = data(2).toDouble
    val nSDash = data(3).toInt
    val nA = data(4).toInt
    val logP = data(5).toDouble
    new Parameters(nIter, dMDash, dVDash, nA, logP)
  }

  def readData(fileName: String): Array[(Int, Int)] = {
    val nums = """(\d+) (\d+)""".r
    val heads = """(\d+)""".r
    val tuples = io.Source.fromFile(fileName).getLines collect {
      case nums(label, num) => (label.toInt -> num.toInt)
      case heads(num) => (num.toInt -> num.toInt)
    }
    tuples.toArray
  }

}
