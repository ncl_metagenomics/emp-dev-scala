package basics

import java.io.FileWriter

import com.github.tototoshi.csv.{CSVWriter, defaultCSVFormat}
import basics.MetropolisHMCMCObj.ParamsPS
import basics.ResultsCollector.StatisticsInformation
import net.jcazevedo.moultingyaml._
import org.joda.time.DateTime

import scala.util.{Failure, Success, Try}
import utils.MyYamlParamsProtocolForRunner._
import utils._



object ExperimentScalaRunnerOld {
  def main(args: Array[String]): Unit = {
    val options = MCMC.getProgramOptions(args)
    val version = options('version).toString()
    val estimateWhat = options('mod).toString()
    val fileNameTAD: String = options('in).toString()
    val outDirectoryName0: String = options('out).toString()
    val distibution: String = options('distibution).toString()
    val outFileNamePosteriorParams = MCMC.modifyFileName(fileNameTAD, outDirectoryName0, version, estimateWhat)
    val outputYamlFileN = MCMC.constructYamlFileName(fileNameTAD, outDirectoryName0, version, estimateWhat)
    val mu0: Double = options('mu0).toString().toDouble //1
    //var sigma0: Double = Math.sqrt(options('variance0).toString().toDouble) //1
    val variance0 = options('variance0).toString().toDouble
    //********************
    val mcmcMetro = new MetropolisHMCMC()
    val data = MetropolisHMCMC.readData(fileNameTAD)
    val nIter: Int = options('s).toString().toInt //cast2Int(options('s))
    //****************
    val nD = MetropolisHMCMC.getD(data)
    val s01: Int = options('s0).toString().toInt //match { case Some(x: Int) => x } // nL * 2
    val s0 = estimateWhat match {
      case "all" => if (s01 < (nD * 1.5)) nD * 1.5 else s01
      case "S" => if (s01 < (nD * 1.5)) nD * 1.5 else s01
      case _ => s01
    }
    val tCoverage: Double = (options('coverage)) match {
      case s: String => s.toDouble
      case d: Double => d
      case i: Int => i.toDouble
      case _ => throw new Error("coverage must be Double!"); Double.NaN
    }

    println(s"Input Parameters: file = ${fileNameTAD}, mu0 = ${mu0},  variancer0 = ${variance0}, s0 = ${s0}, nIter = ${nIter}")
    // Running MCMC computation
    val (res: (ParamsPS, Double), time) = MCMC.profile(mcmcMetro.calculate(mu0, variance0, s0, data, _nL = Some(nD), nIter = nIter, estimateWhat, version, outFileNamePosteriorParams, distibution, None))
    println(s"Estimate: mu = ${res._1.mu}, v = ${res._1.variance}, S = ${res._1.S}, NLL = ${res._2}")
    println(s"File written into $outFileNamePosteriorParams")
    println(s"Time of calculation = ${time * 1E-9} sec")
    val outParams = utils.Params(res._1.mu, res._1.variance, res._1.S, res._1.nAccepted, time * 1e-9, res._2, nIter,  PosteriorParamsStat(StatisticsInformation(0.0,0.0,0.0,0.0,0.0),StatisticsInformation(0.0,0.0,0.0,0.0,0.0),StatisticsInformation(0.0,0.0,0.0,0.0,0.0)))
    //constructYamlFileName
    val yamlP = outParams.toYaml
    val fw = new FileWriter(outputYamlFileN)
    fw.write(yamlP.prettyPrint)
    fw.close();

    //********** sample size calculation ***********
    //    val optionsSampSize = SamplingEffort.getProgramParameters(args) //List('arg1, 'arg2)
    //    println(optionsSampSize)
    //    println(s"mcmcf = ${optionsSampSize('mcmcf).toString()}")
    //
    //    val postParamFileN = optionsSampSize('mcmcf).toString()
    //    val abundDataFileN = optionsSampSize('tadf).toString()
    //    val outputFile = SamplingEffort.constructOutputFileName(postParamFileN)
    // -------------------------------------------------------------------------------------------
    val abundDataFileN = fileNameTAD
    val postParamFileN = outFileNamePosteriorParams
    val outputFileSamplingEffort = SamplingEffort.constructOutputFileNameAllOutputs(postParamFileN, tCoverage, "Bisection")
    val outputIncorrectFile: String = SamplingEffort.constructIncorrectOutputFileName(postParamFileN, tCoverage, "Bisection")
    //    val abuData = SamplingEffort.readData(abundDataFileN)
    val abuData = data
    val nL = abuData.map(x => x._2).par.sum
    val dropNumerator = 2
    val dropDenominator = 5
    val (dataPosterior, head) = SamplingEffort.readDataPosteriorEstimates(postParamFileN, dropNumerator, dropDenominator)
    println(s"${dataPosterior(1).mu}, ${dataPosterior(1).variance}, ${dataPosterior(1).nCommunityTaxa}")

    val tollerance = 1.1 //relative error for difference of algorihms
    val tighterTollerance = 1.05
    val mu = -1.6
    val variance = 6.0
    //targeted coverage
    //    val tCoverage = 0.9 //targeted coverage
    val targetFractionUnseen = 1 - tCoverage
    val lnTargetP = Math.log(targetFractionUnseen)
    val lnPInit = SamplingEffort.logPLogNormal.calculateWithRLikeScala(mu, variance, 0)
    //val nL = 19018
    //val nD = 2424

    println(s"*** Estimate Sampling Effort *******")

    val nSampleEstW = SamplingEffort.getSampleSizeValueWithBrentPegasus(mu, variance, nL, tCoverage, tollerance, tighterTollerance,DateTime.now)
    println(s"Initial: nL = $nL, lnPInit = $lnPInit, c = ${1 - Math.exp(lnPInit)}")
    println(s"Target: lnTargetP = $lnTargetP, c = ${1 - Math.exp(lnTargetP)}")

    nSampleEstW match {
      case Success(nSampleEst) => {
        println(s"Solution: required sampl size = ${nSampleEst.nL}, muS=${nSampleEst.nL}, c = ${nSampleEst.nL}")
        println(s"Solution required sample size as multiple of actual sample size = ${nSampleEst.nL}")
      }
      case Failure(e)=> println(e.getMessage)
    }

    val outputFileSamplingEffort2 = SamplingEffort.constructOutputFileName4Correct(postParamFileN, tCoverage, "")

    implicit val fwCSV = SamplingEffort.OutputCSVWriters(CSVWriter.open(outputFileSamplingEffort2), CSVWriter.open(outputIncorrectFile))

    val (res2: Array[Try[SamplingEffort.SampleSizeEstimateDS]], time2: Long) = SamplingEffort.profile(SamplingEffort.getSampleSizeDistribution(dataPosterior, mu, variance, nL, tCoverage, tollerance, tighterTollerance))
    //val outSampleFN: String = outPutFN(postParamFileN)
    // val (res2: (ParamsPS, Double), time2) = profile(mcmcMetro.calculate(mu0, variance0, s0, data, _nL = Some(nL), nIter = nIter, estimateWhat, version, outfileName))
    SamplingEffort.writeOutput(res2, outputFileSamplingEffort)

    val outParamsWithCoverage = utils.ParamsWithCoverage(outParams.muLastVal, outParams.varianceLastVal, outParams.nSLastVal, outParams.nAccepted, time2, outParams.nIter, tCoverage, s"sampling effort for ${tCoverage * 100}% of species")
    val yamlPWCoverage = outParamsWithCoverage.toYaml
    println(yamlPWCoverage.prettyPrint)
    val outputYamlWCovrgFileN = MCMC.constructAnyFileName(fileNameTAD, outDirectoryName0, s"sample_size-s_", version, estimateWhat, "yaml")
    val fwYamlWCoverage = new FileWriter(outputYamlWCovrgFileN)
    fwYamlWCoverage.write(yamlPWCoverage.prettyPrint)
    fwYamlWCoverage.close()


    println(s"Output written to $outputFileSamplingEffort")
    println(s"Time of calculation = ${time2 * 1E-9} seconds")
    println("Finished estimating of sampling effort")
  }
}
