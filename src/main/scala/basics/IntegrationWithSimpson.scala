/**
  * @author peter
  */

package basics
import breeze.integrate


case class IntegrationWithSimpson() extends Integral {
  def calculate(f:Double=>Double, a:Double,b:Double, nSegments: Integer):Double = {
    integrate.simpson(f, a, b, nSegments)
  }
  
}