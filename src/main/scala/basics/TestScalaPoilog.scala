package basics


import com.github.tototoshi.csv.CSVWriter
import com.github.tototoshi.csv.defaultCSVFormat
import utils.sampling._


object TestScalaPoilog {
  def main(args: Array[String]) = {
    val quinceLogPByRecord = "data/T2_160503/nLL_debug_by_record_s.csv" //"data/nLL_debug_by_record_160128_2303.csv"
    val relError = 1 / 10000000
    val absError = 0.00001
    val outFileName = "output/T2_160528_0134/debug_poilog_vals_added_160528_0133a.csv"
    val outFileName2 = "output/T2_160528_0134/debug_poilog_vals_added_160528_0133b.csv"
    val fw2 = TestDetailsOfPoilog.getWriter(outFileName2)
    val header = List("nIter","isInCorrect","n","mu","sigma","v","logPWithR","logPWithPoilogScala","logPSPoilog")
    fw2.writeRow(header)
    val res: List[(Boolean, Double, Double, Double, Parameters, Double, Double)] = addPoilogRAndScalaValues4All(quinceLogPByRecord, relError, absError, fw2)
    writeAll("output/T2_160528_0030/all_compare_Rpoilog_ScalaPoilog.csv", res)
    fw2.close()
  }

  // *******************************************************************************************************************
  case class Parameters(nIter: Int, dMDash: Double, dVDash: Double, nA: Int, dLogP: Double)

  def addPoilogRAndScalaValues4All(qOutFileN: String, relError: Double, absError: Double, fw: CSVWriter): List[(Boolean, Double, Double, Double, Parameters, Double, Double)] = {
    val tuples = io.Source.fromFile(qOutFileN).getLines.drop(1).map(l => addPoilogRAndScalaValues(l, relError, absError, fw)) //.filter(_._1)
    tuples.toList
  }

  def writeAll(filename: String, data: List[(Boolean, Double, Double, Double, Parameters, Double, Double)]) {
    val f = new java.io.File(filename)
    val fDir = f.getParent
    (new java.io.File(fDir)).mkdirs()
    val writer = CSVWriter.open(filename)
    //nIter,dMDash,dVDash,nSDash,nA,dLogP
    val header = List("nIter", "similar_values", "logPWithR", "logPWithPoilogScala", "logPSPoilog", "dMDash", "dSigma", "dVDash", "nA", "logPQuince", "difLogP", "relDifLotP")
    writer.writeRow(header)
    data.map(e => writer.writeRow(List(e._5.nIter, e._1, e._2, e._3, e._4, e._5.dMDash, Math.sqrt(e._5.dVDash), e._5.dVDash, e._5.nA,
      e._5.dLogP, e._6, e._7)))
    writer.close()
    println(s"Writing to filename $filename")
  }

  def addPoilogRAndScalaValues(line: String, relError: Double, absError: Double, fw: CSVWriter): (Boolean, Double, Double, Double, Parameters, Double, Double) = {
    val pars: Parameters = parseLine(line)
    val ll = new LogLikelihoodPoissonLogNormal()
    val nIter = pars.nIter
    val mu = pars.dMDash
    val v = pars.dVDash
    val sigma = Math.sqrt(v)
    val n = pars.nA
    val logPQuince = pars.dLogP

    val logPWithR = ll.calculateWithRLikeScala(mu, v, n)
    val logPWithPoilogScala = Math.log(Poilog.dpoilog(n, mu, sigma))
    val logPSPoilog = Math.log(PoilogRScala.getPSPoilog(mu, v, n))

    val difLogP = logPWithPoilogScala - logPWithR
    val relDifLotP = difLogP / logPWithR
    // nIter,dMDash,dVDash,nSDash,nA,dLogP

    val isInCorrect = !((relDifLotP.abs < relError) && difLogP.abs < absError)
    val res = (isInCorrect, logPWithR, logPWithPoilogScala, logPSPoilog, pars, difLogP, relDifLotP)
    val header = List("nIter","isInCorrect","n","mu","sigma","v","logPWithR","logPWithPoilogScala","logPSPoilog")
    fw.writeRow(List(nIter, isInCorrect,n,mu,sigma,v,logPWithR,logPWithPoilogScala,logPSPoilog))
    if (nIter % 200 == 0) {
      println(s"processing record ${pars.nIter} - logP value is ${if (isInCorrect) "incorrect" else "correct"}")
    }
    if (isInCorrect && false) { println(s"processing record ${pars.nIter} - logP value is incorrect, diff = $difLogP, relDiff = $relDifLotP") }
    res

  }

  //  def writeIncorectNLL02(filename: String, data: List[((Boolean, Double, Parameters, Double, Double), (Double, Double))]) {
  //    val writer = CSVWriter.open(filename)
  //    //nIter,dMDash,dVDash,nSDash,nA,dLogP
  //    val header = List("nIter", "logPWithR", "logPWithPoilogScala", "dMDash", "dSigma", "dVDash", "nA", "logPQuince", "logPScala", "difLogP", "relDifLotP", "tnLLDiffQuince", "tnLLDiffScala")
  //    writer.writeRow(header)
  //    data.map(e => writer.writeRow(List(e.nIter, e._1._3.dMDash, Math.sqrt(e._1._3.dVDash), e._1._3.dVDash, e._1._3.nA,
  //      e._1._3.dLogP, e._1._2, e._1._4, e._1._5, e._2._1, e._2._2)))
  //    writer.close()
  //    println(s"Writing to filename $filename")
  //  }

  def parseLine(line: String): Parameters = {
    // nIter,dMDash,dVDash,nSDash,nA,dLogP
    val data = line.split(",")
    val nIter = try {
      data(0).toInt
    } catch {
      case e: NumberFormatException => { println(s"nIter = ${data(0)} "); 0 }
    }
    //nIter,dMDash,dVDash,nSDash,nA,dLogP
    val dMDash = data(1).toDouble
    val dVDash = data(2).toDouble
    val nSDash = data(3).toInt
    val nA = data(4).toInt
    val logP = data(5).toDouble
    new Parameters(nIter, dMDash, dVDash, nA, logP)
  }
}
