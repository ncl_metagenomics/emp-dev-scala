package basics

import java.io.File


/**
  * Created by peter on 25/09/16.
  */
object RunGraphOfCollectedResults {
  def main(args: Array[String]): Unit = {
    val fileName = "/media/sf_data/nps409/Work2Sync/Projects/emp_dev_scala_runs/TAD_DATA_Synthetic_Out/SRS711891_Synthetic_Out/AutoEval_160629_1555-27_160816/aggregatedResults.csv"
    val file = new File(fileName)
    ResultsCollector.graphCollectedResults(file,"R","LibGraphsForSummary.R")
  }

}
