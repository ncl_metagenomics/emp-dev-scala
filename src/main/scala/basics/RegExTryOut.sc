import basics.ResultsCollector.FileDirRegExPattern

import scala.util.matching.Regex

val pattern = FileDirRegExPattern.REAL_DATA_S.posteriorDir.r
val pattern2 ="""(.*)""".r
val dirName = "s-s-400K-lognormal-Pegasus-1.05-1.001-161202-1509_20"
val dataSetDate = dirName match{
  case pattern(a0,a1,_*) => a1
//  case pattern2(a) => a
  case _ => "datasetDateUnknown"
}

val p =
  """s-\d{1,8}K-(\w*)-(\w*).*""".r
dirName match {
  case p(field1, field2, field3, _*) => field2
}
