package basics

import MetropolisHMCMCObj.ParamsPS
import net.jcazevedo.moultingyaml._
import utils.MyYamlParamsProtocolForRunner._
import utils._
import java.io.FileWriter

import basics.ResultsCollector.StatisticsInformation
import org.apache.commons.io.{FilenameUtils => FilenameUtilsApache}

object MCMC {
  final val DATE_STR = FileUtils.getDateAsString(FileUtils.fString)
  final val DATE_STR2 = FileUtils.getDateAsString(FileUtils.fString2)

  def cast2Int(number: Any): Int = number match {
    case n: Number => n.intValue()
    case x => throw new IllegalArgumentException(s"$x is not a number.")
  }

  def cast2Double(number: Any): Double = number match {
    case n: Number => n.doubleValue()
    case x => throw new IllegalArgumentException(s"$x is not a number.")
  }

  def parseOptions(args: List[String], required: List[Symbol], optional: Map[String, Symbol], definedParameters: Map[Symbol, Any], iterations: Int): Map[Symbol, Any] = {
    val usage =
      """
						Usage: parser [-v] [-f file] [-s sopt] ...
						Where:
						Required parameters:\n"
						-in input file name
						-out path for output file
						Optional:
						-s integer generate  MCMC samples
            -mod string with values: all, mu, sigma, S
            -version string with  values q for Quince's version ((storing ln of mean of un-logged value of  rv~LogNormal)) , s - for my version (storing mean of log value of rv~LogNormal)
            -c is decimal number, required coverage of species = D/S, D - number of species in the sample, S - total number of species
            -d distribution as string "lognormal", "inverseG", "sichel"
						"""

    if (args.length == 0 && iterations < 1) println(usage)
    args match {
      // Empty list
      case Nil => definedParameters

      // Keyword arguments
      case "-s" :: value :: tail =>
        parseOptions(tail, required, optional, definedParameters ++ Map(optional("-s") -> value.toInt), iterations + 1)
      case "-s0" :: value :: tail =>
        parseOptions(tail, required, optional, definedParameters ++ Map(optional("-s0") -> value.toInt), iterations + 1)
      case key :: value :: tail if optional.get(key) != None =>
        parseOptions(tail, required, optional, definedParameters ++ Map(optional(key) -> value), iterations + 1)

      // Positional arguments
      case value :: tail if required != Nil =>
        parseOptions(tail, required.tail, optional, definedParameters ++ Map(required.head -> value), iterations + 1)

      // Exit if an unknown argument is received
      case _ =>
        printf("unknown argument(s): %s\n", args.mkString(", "))
        sys.exit(1)
    }
  }

  def main(args: Array[String]) = {
    // Required positional arguments by key in options
    val options = getProgramOptions(args)
    val version = options('version).toString()
    val estimateWhat = options('mod).toString()
    val fileName: String = options('in).toString()
    val outDirectoryName0: String = options('out).toString()
    val distribution = options('distribution).toString()
    val outfileName = modifyFileName(fileName, outDirectoryName0, version, estimateWhat)
    val outputYamlFileN = constructYamlFileName(fileName, outDirectoryName0, version, estimateWhat)
    val mu0: Double = options('mu0).toString().toDouble
    //1
    //var sigma0: Double = Math.sqrt(options('variance0).toString().toDouble) //1
    val variance0 = options('variance0).toString().toDouble
    //********************
    val mcmcMetro = new MetropolisHMCMC()
    val data = MetropolisHMCMC.readData(fileName)
    val nIter: Int = options('s).toString().toInt
    //cast2Int(options('s))
    //****************
    val nD = MetropolisHMCMC.getD(data)
    val s01: Int = options('s0).toString().toInt
    //match { case Some(x: Int) => x } // nL * 2
    val s0 = estimateWhat match {
      case "all" => if (s01 < (nD * 1.5)) nD * 1.5 else s01
      case "S" => if (s01 < (nD * 1.5)) nD * 1.5 else s01
      case _ => s01
    }

    println(s"Input Parameters: file = ${fileName}, mu0 = ${mu0},  variancer0 = ${variance0}, s0 = ${s0}, nIter = ${nIter}")
    // Running MCMC computation
    val ((res: ParamsPS, nLL: Double), time) = profile(mcmcMetro.calculate(mu0, variance0, s0, data, _nL = Some(nD), nIter = nIter, estimateWhat, version, outfileName, distribution, None))
    println(s"Estimate: mu = ${res.mu}, v = ${res.variance}, S = ${res.S}, NLL = ${nLL}")
    println(s"File written into $outfileName")
    println(s"Time of Scala dist params calculation = ${time * 1E-9} sec")
    val outParams = utils.Params(res.mu, res.variance, res.S, res.nAccepted, nLL, time * 1e-9, nIter, PosteriorParamsStat(StatisticsInformation(0.0,0.0,0.0,0.0,0.0),StatisticsInformation(0.0,0.0,0.0,0.0,0.0),StatisticsInformation(0.0,0.0,0.0,0.0,0.0)))
    //constructYamlFileName
    val yamlP = outParams.toYaml
    val fw = new FileWriter(outputYamlFileN)
    fw.write(yamlP.prettyPrint)
    fw.close();
  }

  def getProgramOptions(args: Array[String]) = {
    // Required positional arguments by key in options
    val required = List('arg1, 'arg2)
    // Optional arguments by flag which map to a key in options
    val optional = Map("-in" -> 'in, "-out" -> 'out, "-s" -> 's, "-mod" -> 'mod, "-mu0" -> 'mu0, "-variance0" -> 'variance0, "-s0" -> 's0, "-version" -> 'version, "-c" -> 'coverage, "-d" -> 'distribution)
    // Default options that are passed in
    val definedParameters: Map[Symbol, Any] = Map()
    // Parse options based on the command line args
    val options = parseOptions(args.toList, required, optional, definedParameters, 0)
    println(options)
    println(s"s = ${options('s)}")
    options
  }

  def modifyCCodeFileName(inFileName: String, outPath: String, version: String, estimateWhat: String): String = {
    val inFileNameOnly0 = new java.io.File(inFileName).getName
    val basename = FilenameUtilsApache.getBaseName(inFileNameOnly0)
    val parDir = FilenameUtilsApache.getPathNoEndSeparator((FilenameUtilsApache.getPathNoEndSeparator(outPath)))

    val outFileNameOnly = "posterior-c_" + basename + "_" + MCMC.DATE_STR // + version + "-" + estimateWhat + "-" + basename + "_" + MCMC.DATE_STR + ".csv"

    new java.io.File(outPath).mkdirs
    val outFileName = outPath + java.io.File.separatorChar + outFileNameOnly
    // val newFileName:String = FileUtils.appendString2FileName(outFileName, MCMC.STR_FORMAT)
    outFileName
  }

  val dirSeparator = java.io.File.separatorChar


  def modifyFileName(inFileName: String, outPath: String, version: String, estimateWhat: String): String = {
    val inFileNameOnly0 = new java.io.File(inFileName).getName
    val outFileNameOnly = "posterior-s-" + version + "-" + estimateWhat + "-" + inFileNameOnly0 + "_" + MCMC.DATE_STR + ".csv"
    //    val subDirName = ""
    new java.io.File(outPath).mkdirs
    val outFileName = outPath + dirSeparator /*+subDirName + dirSeparator */ + outFileNameOnly
    // val newFileName:String = FileUtils.appendString2FileName(outFileName, MCMC.STR_FORMAT)
    outFileName
  }

  /**
    *
    * @param names
    *                  //    * @param inFileName
    * @param outPath
    * @param version
    * @param estimateWhat
    * @param extension file name extension without "."
    * @return
    */
  def modifyFileNameInGeneral(names: String, outPath: String, version: String, estimateWhat: String, extension: String): String = {
    //TODO: 170301 TUNE NAME FOR OUTPUT FILE
    //    val inFileNameOnly0 = new java.io.File(inFileName).getName
    val outFileNameOnly = names + "_" + version + "-" + estimateWhat + "-" /*+ inFileNameOnly0*/ + "_" + MCMC.DATE_STR + "." + extension
    //    val subDirName = ""
    new java.io.File(outPath).mkdirs
    val outFileName = outPath + dirSeparator /*+subDirName + dirSeparator */ + outFileNameOnly
    // val newFileName:String = FileUtils.appendString2FileName(outFileName, MCMC.STR_FORMAT)
    outFileName
  }


  def constructYamlFileName(inFileName: String, outPath: String, version: String, estimateWhat: String): String = {
    val inFileNameOnly0 = new java.io.File(inFileName).getName
    val outFileNameOnly = "posterior-s-" + version + "-" + estimateWhat + "-" + inFileNameOnly0 + "_" + MCMC.DATE_STR + ".yaml"
    new java.io.File(outPath).mkdirs
    val outFileName = outPath + java.io.File.separatorChar + outFileNameOnly
    // val newFileName:String = FileUtils.appendString2FileName(outFileName, MCMC.STR_FORMAT)
    outFileName.replaceAll("//", "/")
  }

  def constructYamlFileName4CCode(inFileName: String, outPath: String, version: String, estimateWhat: String): String = {
    val inFileNameOnly0 = new java.io.File(inFileName).getName
    val outFileNameOnly = "posterior-c-" + version + "-" + estimateWhat + "-" + inFileNameOnly0 + "_" + MCMC.DATE_STR + ".yaml"

    new java.io.File(outPath).mkdirs
    val outFileName = outPath + java.io.File.separatorChar + outFileNameOnly
    // val newFileName:String = FileUtils.appendString2FileName(outFileName, MCMC.STR_FORMAT)
    outFileName.replaceAll("//", "/")
  }

  def constructAnyFileName(inFileName: String, outPath: String, prefix: String, version: String, estimateWhat: String, extentsion: String): String = {
    val inFileNameOnly0 = new java.io.File(inFileName).getName
    val outFileNameOnly = prefix + version + "-" + estimateWhat + "-" + inFileNameOnly0 + "_" + MCMC.DATE_STR + "." + extentsion

    new java.io.File(outPath).mkdirs
    val outFileName = outPath + java.io.File.separatorChar + outFileNameOnly
    // val newFileName:String = FileUtils.appendString2FileName(outFileName, MCMC.STR_FORMAT)
    outFileName.replaceAll("//", "/")
  }


  def constructOutputFileName(fileName: String): String = {
    val parent = new java.io.File(fileName).getParent
    val newParent = parent + java.io.File.separator + ".." + java.io.File.separator + "output-sampleSize"
    val shortName = new java.io.File(fileName).getName
    val newShortName = "sample_size_" + shortName.replaceAll("\\.[^.]*$", "") + "_" + MCMC.DATE_STR + ".csv" //sCode_sVersion_
    new java.io.File(newParent).mkdirs
    val newFullFN = newParent + java.io.File.separator + newShortName
    newFullFN
  }

  def toInt(in: String): Option[Int] = {
    try {
      Some(Integer.parseInt(in.trim))
    } catch {
      case e: NumberFormatException => None
    }
  }

  def safeStringToInt(str: String): Option[Int] = {
    import scala.util.control.Exception._
    catching(classOf[NumberFormatException]) opt str.toInt
  }

  def toDouble(in: String): Option[Double] = {
    try {
      Some(in.trim.toDouble)
    } catch {
      case e: NumberFormatException => None
    }
  }

  import System.nanoTime

  def profile[R](code: => R, t: Long = nanoTime) = (code, nanoTime - t)

  // usage:
  val (result, time) = profile {
    /* block of code to be profiled*/
    println("")
  }
}
