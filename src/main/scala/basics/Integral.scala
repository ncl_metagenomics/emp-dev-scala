package basics
trait Integral
{
  def calculate(f:Double=>Double, a:Double,b:Double, nSegments: Integer):Double
}
