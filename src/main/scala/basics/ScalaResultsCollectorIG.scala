package basics


import breeze.stats.DescriptiveStats
import com.github.martincooper.datatable.DataColumn
import utils.{AbundanceIO, Enum4EMPColectResults, ParamsSampleSize}
//import net.jcazevedo.moultingyaml.DefaultYamlProtocol
import java.io.{File, FileReader}

import basics.ResultsCollector.{FileDirRegExPattern, OtuInfo, StatisticsInformation, TadDataInfoFromJSON, getFileNameWithPosteriors, getInfoFromFileNameOfPosteriors, getStatisticsFromPosteriorParamsIG, _}

import com.github.martincooper.datatable.{DataTable, DataValue}

import scala.io.Source
import scala.util.{Failure, Success, Try}
import spray.json._
import spray.json.{DefaultJsonProtocol, JsonFormat}

import com.typesafe.scalalogging.Logger
import scala.collection.immutable.ListMap

object ScalaResultsCollectorIG {

  val logger = Logger[ScalaResultsCollectorIG.type]

  object MyJsonProtocol4ScalaResultsCollectorIG extends DefaultJsonProtocol {
    //Explicit type added here now
    implicit val caseClassTadDataInfoFromJSONSchemaFormat: JsonFormat[TadDataInfoFromJSON] = jsonFormat13(TadDataInfoFromJSON)

    implicit object friendListJsonFormat extends RootJsonFormat[TadDataInfoFromJSONList] {
      def read(value: JsValue) = TadDataInfoFromJSONList(value.convertTo[List[TadDataInfoFromJSON]])

      override def hashCode(): Int = super.hashCode()

      override def write(f: TadDataInfoFromJSONList) = {
        JsArray(f.items.map(_.toJson).toVector) //serializationError("not supported") //f.items.toJson
      }
    }

  }

  sealed abstract class FileDirRegExPattern(val tadFileNamePattern: String, val posteriorDatafilePatternStr: String, val posteriorDir: String, val sampleSizeFile: String) extends FileDirRegExPattern.Value

  object FileDirRegExPattern extends Enum4EMPColectResults[FileDirRegExPattern] {
    val HUMAN_MICROBIOME = new FileDirRegExPattern("^44xSR(S711891|R15897XX)_(TAD|tad)(_.*){0,1}\\.sample$", "^posterior-.*\\.csv$", "s_\\d{1,8}K_\\d{6}-\\d{4}-\\d{2}", "") {}
    val REAL_DATA_S = new FileDirRegExPattern(/*"""^([a-zA-Z0-9]*_?[a-zA-Z0-9]*)_?(TAD|tad)?\.
    (sample)$"""*/ """(?!^old.*)^([a-zA-Z0-9]*_?[a-zA-Z0-9]*)_?(TAD|tad)?_?[a-zA-Z0-9]*.*\.(sample)$""", """(?!^old)^posterior-s-(\w)-(\w*)-([a-zA-Z0-9]*_?(\w*)?)\.(\w*)_(\d{6}).*-(\d{4})_(\d{2}).*\.(\w{3,})$""", """s-\d{1,8}K-(\w*)-(\w*)-(\d+\.\d+)-(\d+\.\d+)-((\d{6})-(\d{4})_(\d{2}))$""", """(?i)^sample_size_posterior-(\w)-(\w)-(\w{1,})-((\w{1,})\.(\w{3,}))_(\d{6})-(\d{4})_(\d{2})_correct_Coverage_(\d*)_%o_([a-z]*)_(\d{6})-(\d{4})_(\d{2})\.csv$""") {}
    val REAL_DATA_C = new FileDirRegExPattern("^([a-zA-Z0-9]*)_(TAD|tad)\\.(sample)$", "^posterior-.*\\.csv$", "c_\\d{1,8}K_\\d{6}-\\d{4}-\\d{2}", "") {}
    //**************
    val REAL_DATA_S_OLD = new FileDirRegExPattern("^([a-zA-Z0-9]*)_(TAD|tad)\\.(sample)$", "^posterior-.*\\.csv$", "s_\\d{1,8}K_\\d{6}-\\d{4}-\\d{2}", "") {}
    val REAL_DATA_C_OLD = new FileDirRegExPattern("^([a-zA-Z0-9]*)_(TAD|tad)\\.(sample)$", "^posterior-.*\\.csv$", "c_\\d{1,8}K_\\d{6}-\\d{4}-\\d{2}", "") {}

  }


  def
  main(args: Array[String]): Unit = {
    val dirList = List(
      "/media/sf_data/nps409/Other/EBI Results From Topsy/emp_dev_scala_runs/TAD_DATA_Real_Out_4_var_distr/IG_Distr/synth/synth_sample_r_0.1_d_180124_1219-52_r2_0.3_d2_180226_1037-05-e01",
      "/mnt/topsy/share/nobackup/nps409/EBI/emp_dev_scala_runs/TAD_DATA_Real_Out_4_var_distr/IG_Distr/synth/synth_sample_r_0.1_d_180124_1219-52_r2_0.3_d2_180226_1037-05/",
      "/mnt/topsy/share/nobackup/nps409/EBI/emp_dev_scala_runs/TAD_DATA_Real_Out_4_var_distr/IG_Distr/synth/synth_sample_r_0.1_d_180124_1219-52_r2_0.05_d2_180226_1032-17/",
      "/mnt/topsy/share/nobackup/nps409/EBI/emp_dev_scala_runs/TAD_DATA_Real_Out_4_var_distr/IG_Distr/synth/synth_sample_r_0.1_d_180124_1219-52_r2_0.2_d2_180225_1431-36",
      "/mnt/topsy/share/nobackup/nps409/EBI/emp_dev_scala_runs/TAD_DATA_Real_Out_4_var_distr/IG_Distr/synth/synth_sample_r_0.1_d_180124_1219-52_r2_0.1_d2_180124_1228-40/",
      "/mnt/topsy/share/nobackup/nps409/EBI/emp_dev_scala_runs/TAD_DATA_Real_Out_4_var_distr/IG_Distr/synth/synth_sample_r_0.1_d_180124_1219-52_r2_0.1_d2_180124_1228-40",
      "/media/sf_Newcastle-Project/src/Projects/Quince-C/R/DATA_GENERATOR/CQ_Data_Generator_170627_2302/Results_TAD_60_pc_v01",
      "/media/sf_data/nps409/Other/Dropbox/Newcastle-Project/src/Projects/Quince-C/R/DATA_GENERATOR/CQ_Data_Generator_170627_2302/Results_TAD_10_pc",
      "/media/sf_data/nps409/Work2Sync/Projects/emp_dev_scala_runs/TAD_DATA_Synthetic_Out/QC_paper/QC_170213_stopped_w_topsy",
      "/media/sf_data/nps409/Work2Sync/Projects/emp_dev_scala_runs/TAD_DATA_Synthetic_Out/QC_paper/QC_170213_test4CCode",
      "/media/sf_data/nps409/Work2Sync/Projects/emp_dev_scala_runs/CQ_TAD_DATA_Real_Out_v2_stopped",
      "/media/sf_data/nps409/Work2Sync/Projects/emp_dev_scala_runs/TAD_DATA_Synthetic_Out/Topsy/CQ_TAD_DATA_Real_Out_v2/",
      "/media/sf_data/nps409/Work2Sync/Projects/emp_dev_scala_runs/TAD_DATA_Synthetic_Out/Topsy/QC_Paper_170319_covar_hetero/",
      "/media/sf_data/nps409/Work2Sync/Projects/emp_dev_scala_runs/TAD_DATA_Synthetic_Out/Topsy/QC_Paper_170319_covar_hetero_test",
      "/media/sf_data/nps409/Work2Sync/Projects/emp_dev_scala_runs/TAD_DATA_Synthetic_Out/Topsy/QC_Paper_170319_covar_hetero_test_1"
    )

    val mainDir = dirList(0) // dirList(3) // dirList(1)
    //    val mainDir = dirList(0) // dirList(3) // dirList(1)
    val mainDir14 = "/media/sf_data/nps409/Work2Sync/Projects/emp_dev_scala_runs/TAD_DATA_Synthetic_Out/Topsy/QC_Paper_170319_covar_hetero/"
    val mainDir13 = "/media/sf_data/nps409/Work2Sync/Projects/emp_dev_scala_runs/TAD_DATA_Synthetic_Out/Topsy/QC_Paper_170219/"
    var mainDir12 = "/media/sf_data/nps409/Work2Sync/Projects/emp_dev_scala_runs/TAD_DATA_Synthetic_Out/Topsy/QC_Paper_170219_short"
    val mainDir10 = "/media/sf_data/nps409/Work2Sync/Projects/emp_dev_scala_runs/TAD_DATA_Synthetic_Out/QC_paper/QC_170213"
    val mainDir9 = "/media/sf_data/nps409/Work2Sync/Projects/emp_dev_scala_runs/TAD_DATA_Real_Out/RESULTS_TOPSY_170123/TAD_DATA_Real_Out"
    //# /media/sf_data/nps409/Work2Sync/Projects/emp_dev_scala_runs/TAD_DATA_Synthetic_Out/QC_paper/QC_170213/
    val mainDir7: String = "/data/nps409/Work2Sync/Projects/emp_dev_scala_runs/TAD_DATA_Real_Out/AUTO_RUN_/**/REAL"
    val mainDir8 = "C:\\data\\EBI_Data\\hron\\TAD_DATA_Real_Out\\AUTO_RUN_REAL_170116"
    val mainDir6: String = "/media/sf_data/nps409/Work2Sync/Projects/emp_dev_scala_runs/TAD_DATA_Real_Out/AUTO_RUN_REAL"
    val mainDir5: String = "/media/sf_data/nps409/Work2Sync/Projects/emp_dev_scala_runs/TAD_DATA_Real_Out/AUTO_RUN_TEST/"
    //"/media/sf_data/nps409/Work2Sync/Projects/emp_dev_scala_runs/TAD_DATA_Real_Out/AUTO_RUN_TEST/"
    val mainDir4: String = "/media/sf_data/nps409/Work2Sync/Projects/emp_dev_scala_runs/TAD_DATA_Synthetic_Out/SRS711891_Synthetic_Out/AutoEval160816_160630_1636-40_correct/"
    val mainDir1: String = "/media/sf_data/nps409/Work2Sync/Projects/emp_dev_scala_runs/TAD_DATA_Synthetic_Out/SRS711891_Synthetic_Out/AutoEval160816_160630_1636-40_correct/"
    val mainDir2: String = "/media/sf_data/nps409/Work2Sync/Projects/emp_dev_scala_runs/TAD_DATA_Synthetic_Out/SRS711891_Synthetic_Out/AutoEval160816_160630_1444-36"
    val mainDir3: String = "/media/sf_data/nps409/Work2Sync/Projects/emp_dev_scala_runs/TAD_DATA_Synthetic_Out/SRS711891_Synthetic_Out/AutoEval_160629_1555-27_160816"

    // =============================================================================
    println("")

    collectAndWriteResults(mainDir, FileDirRegExPattern.REAL_DATA_S)
    //***********************************************************************************************
  }


  def collectAndWriteResults(mainDir: String, dataType: FileDirRegExPattern): Unit = {
    //***  ****
    // Use synthetic human biome data
    val nLDist90pc_statInfo = StatisticsInformation(0, 0, 0, 0, 0)
    // ---------------------------------------------------------------
    val outputFile = mainDir + File.separator + s"aggregatedResults_Scala_" + MCMC.DATE_STR + ".csv"
    val sampleDirs: List[File] = getSubdirsWPattern(mainDir,"""(?!^old)(^.*)""")
    println(s"Sample Dirs ${sampleDirs.mkString(" | ")} in collectAndWriteResults")

    val oFile = new File(outputFile)
    val dt = basics.ScalaResultsCollectorIG.getDataSchema()
    val rootDirNameOnly = (new File(mainDir)).getName
    val info2PassDown = new InfoPassedDown(dataGroupLabel = rootDirNameOnly)

    val dtR = sampleDirs.foldLeft(dt.get)((dtI: DataTable, f) =>
      collectResultsDataSample(f, dtI, nLDist90pc_statInfo, dataType, info2PassDown)
    )
    DataTableReadWrite.writeCsv(dtR, oFile)
    println(s"output to file = ${oFile}")
    println("d")
  }


  def getSubdirsWPattern(dir: String, paternStr: String = """.*""") = {
    val d = new File(dir)
    val res = if (d.exists && d.isDirectory) {
      d.listFiles.filter(f => f.isDirectory && paternStr.r.findFirstIn(f.getName).isDefined).toList
    } else {
      List[File]()
    }
    res
  }

  def getSubdirs(dir: String, pattern: String) = {
    val d = new File(dir)
    if (d.exists && d.isDirectory) {
      d.listFiles.filter(f => f.isDirectory && pattern.r.findFirstIn(f.getName).isDefined).toList
    } else {
      List[File]()
    }
  }

  def getDirOfPosteriors(dir: String, pattern: String = ".*"): List[File] = {
    getSubdirs(dir, pattern)
  }

  def getAllDirsWithPosteriors(dir_1: String, dirWithPostPattern: String = """^s-\d{1,5}K-\d{6}-\w{4}-\w{2}"""): List[File] = {
    val dirsOfPosteriors = getDirOfPosteriors(dir_1, dirWithPostPattern)
    dirsOfPosteriors

  }

  def getOtuInfo(dir: String, patternOTUFile: String = FileDirRegExPattern.REAL_DATA_S.tadFileNamePattern): OtuInfo = {
    val f = new File(dir)
    val matchedFiles: List[File] = if (f.exists && f.isDirectory) {
      f.listFiles.filter(f => f.isFile && patternOTUFile.r.findFirstIn(f.getName).isDefined).toList //
    } else {
      List[File]()
    }
    val otuFileName: Try[File] = matchedFiles.size match {
      case n: Int if (n > 0) => Success(matchedFiles(0))
      case _ => Failure(new Exception(s"Input tad data file not found \n with pattern in directory $patternOTUFile \n in directory $dir"))

    }
    println(s"$otuFileName")
    val colTypes = Map("Abundance" -> Int, "Num.Species" -> Int)
    val tadDate = otuFileName match {
      case Success(f) => AbundanceIO.readTADDAta(f)

      case Failure(e) => throw e
    }
    //DataTableReadWrite.readCsv("TADS", new FileReader(otuFileName), colTypes)

    val nL = MetropolisHMCMC.getL(tadDate.get)
    //tadDate.map(a => a.get(0).getOrElse(-1).asInstanceOf[Int] * a.get(1).getOrElse(-1).asInstanceOf[Int]).sum
    val nD = MetropolisHMCMC.getD(tadDate.get) //tadDate.map(a => a.get(1).getOrElse(-1).asInstanceOf[Int]).sum


    println(s"L = $nL")
    println(s"D = $nD")
    println(s"$otuFileName")

    OtuInfo(nL, nD)
  }

  def getNumOfIterations(file: File): Option[Int] = {
    //     file.getName
    val p = "(s|c)-(\\d{1,4})K-.*".r
    val nIteration = file.getName match {
      case p(nIterationStr) => Option(tryStr2Int(nIterationStr).getOrElse(0) * 1000)
      case _ => None
    }
    nIteration
  }


  def extracCoveratFromPath(dir: String): Double = {
    val name = new File(dir).getName
    val pattern = ".*_(\\d{1,2})_pc_.*".r

    val coverage: Option[Double] = name match {
      case pattern(c: String) => tryStr2Double(c)
      case _ => None
    }
    //val pattern(coverageStr) = name
    println(s"coverage = ${
      coverage.getOrElse(Double.NaN)
    }")
    println("Koniec")
    //    val coverage = tryStr2Double(coverageStr)
    coverage.getOrElse(0)
  }

  def getInfoFromFileNameOfPosteriors(fileNameOnlyWPosteriors: String): (Option[Int], Option[String]) = {
    val pattern = """^posterior-.*_TAD_.*_pc_D_(\d{1,}\.{0,1}\d{0,4})_([a-zA-Z]{1,})_.*\.csv$""".r
    println(s"fileNameOnlyWPosteriors = $fileNameOnlyWPosteriors")
    //    val pattern (nD,distribution) = fileNameOnlyWPosteriors
    val (nD, distribution): (Option[Int], Option[String]) = fileNameOnlyWPosteriors match {
      case pattern(nD, distribution) => (tryStr2Double(nD) match {
        case Some(x: Double) => Some(x.toInt)
        case None => None //throw new Exception("Extracted value is not integer")
      }, Some(distribution))
      case _ => (None, None)
    }
    (nD, distribution)
  }

  /**
    *
    * @param dt
    * @param f
    * @param coverage
    * @param numOfIterations
    * @param otuInfo
    * @param nLDist90pcStatInfo
    * @param dataType
    * @param dataSetName
    * @param tadInfo
    * @param fileNameWithPosteriors
    * @param sampleSizePattern
    * @param fileNameWithSampleSize
    * @param posteriorParamsData
    * @param sampleSizeData
    * @param nSEstO
    * @param info2PassDown
    * @return
    */
  def getStatisticsFromPosteriorParamsIG(dt: DataTable, f: File, coverage: Double, numOfIterations: Option[Int], otuInfo: OtuInfo, nLDist90pcStatInfo: StatisticsInformation, dataType: FileDirRegExPattern, dataSetName: String, tadInfo: TadDataInfoFromJSON, fileNameWithPosteriors: File, sampleSizePattern: String, fileNameWithSampleSize: Option[File], posteriorParamsData: DataTable, sampleSizeData: Option[DataTable], nSEstO: Try[StatisticsInformation], info2PassDown: InfoPassedDown): DataTable = {
    // sampleSizeData: Try[DataTable]

    nSEstO match {
      //TODO: 170424 quietly capture error
      case Success(nSEst: StatisticsInformation) => {
        println(nSEst)

        val numInteration0 = extractLastOfColumn(posteriorParamsData, "nIter")
        val numInteration: Int = numInteration0 match {
          case Success(myInt: Int) => myInt
          case Failure(e) => throw new Exception(e.getMessage + "\n" + s" for file with posterior $fileNameWithPosteriors ")
        }

        val muEstO = extractStats4Variable(posteriorParamsData, 10000, "alpha")
        val muEst: StatisticsInformation = muEstO match {
          case Success(est: StatisticsInformation) => est
          case Failure(e) => throw new Exception(e.getMessage + "\n" + s" for file with posterior $fileNameWithPosteriors ")
        }
        //sigma
        val varianceEst0 = extractStats4Variable(posteriorParamsData, 10000, "beta")
        val varianceEst: StatisticsInformation = varianceEst0 match {
          case Success(est: StatisticsInformation) => est
          case Failure(e) => throw new Exception(e.getMessage + "\n" + s" for file with posterior $fileNameWithPosteriors ")
        }


        val datanD1 = otuInfo.nD
        // *************************************************************
        val effectiveSizeNL: Int = sampleSizeData match {
          case Some(dt: DataTable) =>
            utils.EffectiveSampleSize.getEffectiveSampleSize2(dt, "nL_Recommended", scala.Double)
          case None =>
            println(s"Effective sample size not caluclated")
            0
        }
        val effectiveSizeMu = utils.EffectiveSampleSize.getEffectiveSampleSize2(posteriorParamsData, "alpha", scala.Double)
        val effectiveSizeSigma = utils.EffectiveSampleSize.getEffectiveSampleSize2(posteriorParamsData, "beta", scala.Double)
        val effectiveSizeS = utils.EffectiveSampleSize.getEffectiveSampleSize2(posteriorParamsData, "S", scala.Double)
        val valCIWidthS = (nSEst.percentile97_5 - nSEst.percentile2_5).toInt
        val valCIWidthL = 0
        //(nLEst.percentile97_5 - nLEst.percentile2_5).toInt
        val valCIWidthMu = (muEst.percentile97_5 - muEst.percentile2_5)
        val valCIWidthSigma = (varianceEst.percentile97_5 - varianceEst.percentile2_5)
        val valCIWidthVariance = scala.math.pow(valCIWidthSigma, 2)
        val dirName = f.getName
        val pattern = dataType.posteriorDir.r
        val (distributionName, dataSetDate, optimizationMethod) = dirName match {
          case pattern(distribution, optimizationMethod, tollerance, strictTollerance, dataSetDate, _*) => println(distribution + ", " + optimizationMethod + ", " + tollerance + ", " + strictTollerance + ", " + dataSetDate + ", "); (distribution, dataSetDate, optimizationMethod)
          case _ => ("distributionUnknown", "datasetDateUnknown", "optimizationMethodUnknown")
        }
        val sampleSizeFNOnly = fileNameWithSampleSize match {
          case Some(f) => f.getName
          case None => "UnknownSampleSizeFileName"
        }
        val sampleSizePatternExpr = sampleSizePattern.r
        val coverageOpt: Option[Double] = sampleSizeFNOnly match {
          case sampleSizePatternExpr(a1, a2, a3, a4, a5, a6, a7, a8, a9, coverage, _*) => tryStr2Double(coverage)
          case _ => Some(0.0)
        }

        val numAccepted = 0
        val acceptanceRatio = 0.0

        val coverageForSampleSize = coverageOpt.getOrElse(0.0) / 1000
        //    // -------------------------------------------------------------
        //    val dc29 = new DataColumn[Double]("coverageForSampleSize", Iterable.empty[Double])
        //    val dc30 = new DataColumn[Int]("numIterations", Iterable.empty[Int])
        //    val dc31 = new DataColumn[Int]("numAccepted", Iterable.empty[Int])
        //    val dc32 = new DataColumn[Double]("acceptanceRatio", Iterable.empty[Double])
        //************************************
        val x = dt.rows.add(Seq(DataValue(dataSetName), DataValue(dataSetDate), DataValue(info2PassDown.runStamp), DataValue(info2PassDown.dataGroupLabel), DataValue(coverage), DataValue(numOfIterations.getOrElse(0)), DataValue(otuInfo.nD), DataValue(otuInfo.nL),
          DataValue(nSEst.median.toInt), DataValue(nSEst.percentile2_5.toInt), DataValue(nSEst.percentile97_5.toInt),
          DataValue(0 /*nLEst.median.toInt*/), DataValue(0 /*nLEst.percentile2_5.toInt*/), DataValue(0 /*nLEst.percentile97_5.toInt*/),
          DataValue(muEst.median), DataValue(muEst.percentile2_5), DataValue(muEst.percentile97_5),
          DataValue(varianceEst.median), DataValue(varianceEst.percentile2_5), DataValue(varianceEst.percentile97_5),
          DataValue(nLDist90pcStatInfo.median), DataValue(nLDist90pcStatInfo.percentile2_5), DataValue(nLDist90pcStatInfo.percentile97_5),
          DataValue(effectiveSizeNL), DataValue(effectiveSizeMu), DataValue(effectiveSizeSigma), DataValue(effectiveSizeS)
          , DataValue(valCIWidthS), DataValue(valCIWidthL), DataValue(valCIWidthMu), DataValue(valCIWidthVariance), DataValue(valCIWidthSigma), DataValue(coverageForSampleSize), DataValue(numInteration), DataValue(numAccepted), DataValue(acceptanceRatio), DataValue(tadInfo.meanLog), DataValue(tadInfo.varLog), DataValue(tadInfo.S), DataValue(tadInfo.dateStr1), DataValue(tadInfo.distribution), DataValue(tadInfo.coverage), DataValue(tadInfo.S)
        ))
        println("")
        x.get
      }
      // TODO: 170523 REWRITE WITHOUT THROWIN ERRROR
      case Failure(e) => {
        println(e.getMessage + "\n" + s" for file with posterior $fileNameWithPosteriors ");
        dt
      }
    }


  }



  def getStatisticsFromPosteriorParamsIG(dt: DataTable, f: File, coverage: Double, numOfIterations: Option[Int], otuInfo: OtuInfo, nLDist90pcStatInfo: StatisticsInformation, dataType: FileDirRegExPattern, dataSetName: String, tadInfo: TadDataInfoFromJSON, fileNameWithPosteriors: File, sampleSizePattern: String, fileNameWithSampleSize: Option[File], posteriorParamsData: DataTable, sampleSizeData: Try[DataTable] , nSEstO: Try[StatisticsInformation], info2PassDown: InfoPassedDown): DataTable = {
    // sampleSizeData: Try[DataTable]

    nSEstO match {
      //TODO: 170424 quietly capture error
      case Success(nSEst: StatisticsInformation) => {
        println(nSEst)

        val numInteration0 = extractLastOfColumn(posteriorParamsData, "nIter")
        val numInteration: Int = numInteration0 match {
          case Success(myInt: Int) => myInt
          case Failure(e) => throw new Exception(e.getMessage + "\n" + s" for file with posterior $fileNameWithPosteriors ")
        }

        val muEstO = extractStats4Variable(posteriorParamsData, 10000, "alpha")
        val muEst: StatisticsInformation = muEstO match {
          case Success(est: StatisticsInformation) => est
          case Failure(e) => throw new Exception(e.getMessage + "\n" + s" for file with posterior $fileNameWithPosteriors ")
        }
        //sigma
        val varianceEst0 = extractStats4Variable(posteriorParamsData, 10000, "beta")
        val varianceEst: StatisticsInformation = varianceEst0 match {
          case Success(est: StatisticsInformation) => est
          case Failure(e) => throw new Exception(e.getMessage + "\n" + s" for file with posterior $fileNameWithPosteriors ")
        }


        val datanD1 = otuInfo.nD
        // *************************************************************
        val effectiveSizeNL: Int = sampleSizeData match {
          case Success(dt: DataTable) => utils.EffectiveSampleSize.getEffectiveSampleSize2(dt, "nL_Recommended", scala.Double)
          case Failure(e) =>
            println(e.getMessage)
            0
        }
        val effectiveSizeMu = utils.EffectiveSampleSize.getEffectiveSampleSize2(posteriorParamsData, "alpha", scala.Double)
        val effectiveSizeSigma = utils.EffectiveSampleSize.getEffectiveSampleSize2(posteriorParamsData, "beta", scala.Double)
        val effectiveSizeS = utils.EffectiveSampleSize.getEffectiveSampleSize2(posteriorParamsData, "S", scala.Double)
        val valCIWidthS = (nSEst.percentile97_5 - nSEst.percentile2_5).toInt
        val valCIWidthL = 0
        //(nLEst.percentile97_5 - nLEst.percentile2_5).toInt
        val valCIWidthMu = (muEst.percentile97_5 - muEst.percentile2_5)
        val valCIWidthSigma = (varianceEst.percentile97_5 - varianceEst.percentile2_5)
        val valCIWidthVariance = scala.math.pow(valCIWidthSigma, 2)
        val dirName = f.getName
        val pattern = dataType.posteriorDir.r
        val (distributionName, dataSetDate, optimizationMethod) = dirName match {
          case pattern(distribution, optimizationMethod, tollerance, strictTollerance, dataSetDate, _*) => println(distribution + ", " + optimizationMethod + ", " + tollerance + ", " + strictTollerance + ", " + dataSetDate + ", "); (distribution, dataSetDate, optimizationMethod)
          case _ => ("distributionUnknown", "datasetDateUnknown", "optimizationMethodUnknown")
        }
        val sampleSizeFNOnly = fileNameWithSampleSize match {
          case Some(f) => f.getName
          case None => "UnknownSampleSizeFileName"
        }
        val sampleSizePatternExpr = sampleSizePattern.r
        val coverageOpt: Option[Double] = sampleSizeFNOnly match {
          case sampleSizePatternExpr(a1, a2, a3, a4, a5, a6, a7, a8, a9, coverage, _*) => tryStr2Double(coverage)
          case _ => Some(0.0)
        }

        val numAccepted = 0
        val acceptanceRatio = 0.0

        val coverageForSampleSize = coverageOpt.getOrElse(0.0) / 1000
        //    // -------------------------------------------------------------
        //    val dc29 = new DataColumn[Double]("coverageForSampleSize", Iterable.empty[Double])
        //    val dc30 = new DataColumn[Int]("numIterations", Iterable.empty[Int])
        //    val dc31 = new DataColumn[Int]("numAccepted", Iterable.empty[Int])
        //    val dc32 = new DataColumn[Double]("acceptanceRatio", Iterable.empty[Double])
        //************************************
        val x = dt.rows.add(Seq(DataValue(dataSetName), DataValue(dataSetDate), DataValue(info2PassDown.runStamp), DataValue(info2PassDown.dataGroupLabel), DataValue(coverage), DataValue(numOfIterations.getOrElse(0)), DataValue(otuInfo.nD), DataValue(otuInfo.nL),
          DataValue(nSEst.median.toInt), DataValue(nSEst.percentile2_5.toInt), DataValue(nSEst.percentile97_5.toInt),
          DataValue(0 /*nLEst.median.toInt*/), DataValue(0 /*nLEst.percentile2_5.toInt*/), DataValue(0 /*nLEst.percentile97_5.toInt*/),
          DataValue(muEst.median), DataValue(muEst.percentile2_5), DataValue(muEst.percentile97_5),
          DataValue(varianceEst.median), DataValue(varianceEst.percentile2_5), DataValue(varianceEst.percentile97_5),
          DataValue(nLDist90pcStatInfo.median), DataValue(nLDist90pcStatInfo.percentile2_5), DataValue(nLDist90pcStatInfo.percentile97_5),
          DataValue(effectiveSizeNL), DataValue(effectiveSizeMu), DataValue(effectiveSizeSigma), DataValue(effectiveSizeS)
          , DataValue(valCIWidthS), DataValue(valCIWidthL), DataValue(valCIWidthMu), DataValue(valCIWidthVariance), DataValue(valCIWidthSigma), DataValue(coverageForSampleSize), DataValue(numInteration), DataValue(numAccepted), DataValue(acceptanceRatio), DataValue(tadInfo.meanLog), DataValue(tadInfo.varLog), DataValue(tadInfo.S), DataValue(tadInfo.dateStr1), DataValue(tadInfo.distribution), DataValue(tadInfo.coverage), DataValue(tadInfo.S)
        ))
        println("")
        x.get
      }
      // TODO: 170523 REWRITE WITHOUT THROWIN ERRROR
      case Failure(e) => {
        println(e.getMessage + "\n" + s" for file with posterior $fileNameWithPosteriors ");
        dt
      }
    }


  }


  def extractStats4Variable(posteriorData: DataTable, nSkip: Int, columnName: String): Try[StatisticsInformation] = {
    val nSTmp = posteriorData.columns.getAs[Double](columnName)
    // get vector of doubles
    val nSTmp0: Vector[Double] = nSTmp.toOption match {
      case Some(x: DataColumn[Double]) => {
        println(s"column info = ${x.toString()} column name ${x.name}");
        x.data
      }
      case None => throw new Exception(s"could't extract column $columnName")
    }
    //    val tmp50: Vector[Double] = nSTmp0.map(_.toDouble)
    val nSTmp1 = nSTmp0.drop(nSkip)

    if (nSTmp1.size > 0) {
      val nSMedian = DescriptiveStats.percentile(nSTmp1, 0.5)
      val nSPercentile2_5 = DescriptiveStats.percentile(nSTmp1, 0.025)
      val nSPercentile97_5 = DescriptiveStats.percentile(nSTmp1, 0.975)
      val (nSMean, _, _) = DescriptiveStats.meanAndCov(nSTmp1, nSTmp1)
      val nSVariance = DescriptiveStats.cov(nSTmp1, nSTmp1)
      //    (nSMedian: Double, nSPercentile2_5: Double, nSPercentile97_5: Double, nSMean: Double, nSVariance.asInstanceOf[Double]: Double)
      Success(StatisticsInformation(nSMedian, nSPercentile2_5, nSPercentile97_5, nSMean, nSVariance.asInstanceOf[Double]))
    } else
      Failure(new Exception(s"Data missing for $columnName in posterior data after skipping $nSkip rows"))
  }

  def collectResultsEstimatesLevel(dt: DataTable, f: File, coverage: Double, numOfIterations: Option[Int], otuInfo: OtuInfo, nLDist90pcStatInfo: StatisticsInformation, dataType: FileDirRegExPattern, dataSetName: String, tadInfo: TadDataInfoFromJSON, info2PassDown: InfoPassedDown): DataTable = {
    // ======= Posterior  =======================
    val posteriorProbabPattern: String = dataType.posteriorDatafilePatternStr
    //"^posterior-.*\\.csv$"
    val dtOut: DataTable = getFileNameWithPosteriors(f, List(posteriorProbabPattern, "^posterior-s-(\\w)-(\\w*)-([a-zA-Z0-9]*_?(\\w*)?)(-|_).*\\.csv$", "^posterior-s-(\\w)-(\\w*)-([a-zA-Z0-9]*_?(\\w*)?)-(\\d{2})_(\\d{2}-\\d{2}-\\d{2}_\\d{4}-\\d{2})\\.(\\w{3})_(\\d{6})-(\\d{4}_\\d{2})\\.csv$")) match {
      case Some(fileNameWithPosteriors) => {
        println(s"fileWPosterior = $fileNameWithPosteriors")
        val fileNameOnlyWPosteriors = fileNameWithPosteriors.getName
        println(s"fileOnlyWPosterior = $fileNameOnlyWPosteriors")
        val (nD, distribution) = getInfoFromFileNameOfPosteriors(fileNameOnlyWPosteriors)
        //*** Samples size file ****
        val sampleSizePattern: String = dataType.sampleSizeFile
        val fileNameWithSampleSize = getFileNameWithPosteriors(f, sampleSizePattern)
        println(s"samle size file name ${fileNameWithSampleSize}")
        //===========================
        //    val (nSMedian: Double, nSPercentile2_5: Double, nSPercentile97_5: Double, nSMean, nSVariance) = extractStats4Variable(fileNameWithPosteriors, 10000, "S")
        val posteriorParamsDataSafe: Try[DataTable] = readPosteriorData(fileNameWithPosteriors, ListMap("nIter" -> Double, "S" -> Double, "alpha" -> Double, "beta" -> Double))
        val newDT: DataTable = posteriorParamsDataSafe match {
          case Success(posteriorParamsData) =>
            val sampleSizeData: Try[DataTable] = readSampleSizeData(fileNameWithSampleSize)
            val nSEstO: Try[StatisticsInformation] = extractStats4Variable(posteriorParamsData, 10000, "S")
            val newDT: DataTable = getStatisticsFromPosteriorParamsIG(dt, fileNameWithPosteriors, coverage, numOfIterations, otuInfo, nLDist90pcStatInfo, dataType, dataSetName, tadInfo, fileNameWithPosteriors, sampleSizePattern, fileNameWithSampleSize, posteriorParamsData, sampleSizeData, nSEstO, info2PassDown)
            newDT
          case Failure(e) =>
            println(e.getMessage)
            dt
        }
        newDT
      }
      case None => {
        println(s"File not in directory $f")
        logger.error(s"No file  with posterior distribution of parameters found! in  directoryt ${f.getCanonicalPath}")
        // TODO: 170523 REWRITE WITHOUT THROWIN ERRROR
        //throw new Exception(s"No file  with posterior distribution of parameters found! in  directoryt ${f.getCanonicalPath}")
        dt
      }
    }
    dtOut
  }


  def getFileNameWithPosteriors(dir: File, filePatternStr: List[String]): Option[File] = {
    val files: List[File] = getSubFiles(dir, filePatternStr)
    println(s"Number of files found ${files.size} in $dir with pattern  $filePatternStr")
    val f = files.lift(0)
    f
  }

  def getFileNameWithPosteriors(dir: File, filePatternStr: String = "^posterior-.*\\.csv$") = {
    val files: List[File] = getSubFiles(dir, filePatternStr)
    files.lift(0)
  }

  def extractDataForPosteriorFolder(sampleSubDir: File, dataTable: DataTable, nLDist90pcStatInfo: StatisticsInformation, dataType: FileDirRegExPattern, tadInfo: TadDataInfoFromJSON, allDirsOfPosteriors: List[File], firstDirOfPosteriors0: Option[File], otuInfo: OtuInfo, dataSetName: String,
                                    info2PassDown: InfoPassedDown, dirWithPostPattern: String ): DataTable = {
    val firstDirOfPosteriors = firstDirOfPosteriors0 match {
      case Some(x) => x
      case None => throw new Exception(s"No folder with posterior file found in directory ${sampleSubDir.getAbsolutePath}!")
    }
    //getFirstDirWithPosteriors(sampleSubDir.getAbsolutePath, dirWithPostPattern)
    // ---------------------------------

    val numOfIterations: Option[Int] = getNumOfIterations(sampleSubDir)
    val coverage = dataType match {
      case FileDirRegExPattern.REAL_DATA_S => extracCoveratFromPath(sampleSubDir.getAbsolutePath)
      case _ => 0
    }
    //val posteriorProbabPattern: String = "^posterior-.*\\.csv$"
    //    val patterns = List("^posterior-s-(\\w)-(\\w*)-([a-zA-Z0-9]*_?(\\w*)?)(-|_).*\\.csv$")
    val fileNameWithPosteriors = getFileNameWithPosteriors(sampleSubDir, List(dataType.posteriorDatafilePatternStr, "^posterior-s-(\\w)-(\\w*)-([a-zA-Z0-9]*_?(\\w*)?)-(\\d{2})_(\\d{2}-\\d{2}-\\d{2}_\\d{4}-\\d{2})\\.(\\w{3})_(\\d{6})-(\\d{4}_\\d{2})\\.csv$", "^posterior-s-(\\w)-(\\w*)-([a-zA-Z0-9]*_?(\\w*)?)(-|_).*\\.csv$", "^posterior-s-(\\w)-(\\w*)-([a-zA-Z0-9]*_?(\\w*)?)(-|_).*\\.csv$"
    )) match {
      case Some(file) => file
      case None => {
        println(s"File matching pattern ${dataType.posteriorDatafilePatternStr} not in directory " +
          s"$firstDirOfPosteriors matching  pattern ${dirWithPostPattern}")
        logger.error(s"File matching pattern ${dataType.posteriorDatafilePatternStr} not in " +
          s"directory" +
          s" $firstDirOfPosteriors matching  pattern ${dirWithPostPattern}" )
        // throw new Exception(s"No file with posterior distribution of parameters found!")
      }
    }
    val runStamp = sampleSubDir.getName
    val info2PassDownNew = info2PassDown.copy(runStamp = runStamp)


    println(s"sampleSubDir = $sampleSubDir")
    val fileNameOnlyWPosteriors = sampleSubDir.getName
    println(s"sampleSubDir = $sampleSubDir")
    val (nD, distribution) = getInfoFromFileNameOfPosteriors(fileNameOnlyWPosteriors)

    println(s"nD = $nD")
    println(s"distribution = $distribution")
    println(s"")
    val dt3 = collectResultsEstimatesLevel(dataTable, sampleSubDir, coverage, numOfIterations, otuInfo, nLDist90pcStatInfo, dataType, dataSetName, tadInfo, info2PassDownNew)
    println("")
    dt3
  }


  def collectResultsDataSample(sampleSubDir: File, dataTable: DataTable, nLDist90pcStatInfo: StatisticsInformation, dataType: FileDirRegExPattern, info2PassDown: InfoPassedDown) /*: DataTable*/ = {
    println("sdaf")
    val dirWithPostPattern: String ="""(?!^old.*)(^s-\d{1,8}K-.*$)"""
    val tadInfo0: Option[TadDataInfoFromJSON] = readTADInfoJSONInDir(sampleSubDir.getCanonicalPath)
    val tadInfo = tadInfo0 match {
      case Some(info) => info
      case None => TadDataInfoFromJSON("", "", 0, 0, 0d, 0, 0d, 0d, 0d, "", "", "", "")
    }
    val allDirsOfPosteriors = getAllDirsWithPosteriors(sampleSubDir.getAbsolutePath, dirWithPostPattern)
    val firstDirOfPosteriors0 = allDirsOfPosteriors.lift(0) //first item in List
    val otuInfo: OtuInfo = getOtuInfo(sampleSubDir.getAbsolutePath, FileDirRegExPattern.REAL_DATA_S.tadFileNamePattern)
    val dataSetName = sampleSubDir.getName //
    println(s"Folders ${allDirsOfPosteriors.mkString(" | ")} in collectResultsDataSample")
    val dtR = allDirsOfPosteriors.foldLeft(dataTable)((dtI: DataTable, sampleSubDir) => extractDataForPosteriorFolder(sampleSubDir, dtI, nLDist90pcStatInfo, dataType, tadInfo, allDirsOfPosteriors, firstDirOfPosteriors0, otuInfo, dataSetName, info2PassDown, dirWithPostPattern))
    dtR

  }

  case class OtuInfo(nL: Int, nD: Int)

  case class StatisticsInformation(median: Double, percentile2_5: Double, percentile97_5: Double, mean: Double = 0, variance: Double = -1.0)

  case class InfoPassedDown(dataGroupLabel: String = "", runStamp: String = "")


  case class TadDataInfoFromJSON(dateStr: String, inputDir: String, nL: Int, nD: Int, coverage: Double, S: Int, meanLog: Double, varLog: Double, sdLog: Double, dsName: String, dateStr1: String, distribution: String, estimatedBy: String)

  case class TadDataInfoFromJSONList(items: List[TadDataInfoFromJSON])

  def readTADInfoJSONInDir(subdirStr: String): Option[TadDataInfoFromJSON] = {
    import basics.ScalaResultsCollectorIG.MyJsonProtocol4ScalaResultsCollectorIG._
    // params_per_file_sampleSize_100_pc_syntheticSampleSizes_Community__lognormal_170207_1942-44_17-02-07_2010-22.json
    val patternStr = "^params_per_file.*json$"
    val subDir = new File(subdirStr)
    val jcsonFiles: Seq[File] = basics.ResultsCollector.getSubFiles(subDir, patternStr)
    val info: Option[TadDataInfoFromJSON] = jcsonFiles.lift(0) match {
      case Some(f) => {
        val jsonFile = jcsonFiles.lift(0).get
        val jsonFileString0 = jsonFile.getCanonicalFile
        val jsonFileString = jsonFileString0 //"/media/sf_data/nps409/Work2Sync/Projects/emp_dev_scala_runs/TAD_DATA_Synthetic_Out/Topsy/QC_Paper_170319_covar_hetero_test_1/Brazil_0_9_170207/params_per_file_sampleSize_100_pc_syntheticSampleSizes_Community__lognormal_170207_1942-44_17-02-07_2010-22-fixed.json"
        val fileContents: String = Source.fromFile(jsonFileString).getLines.mkString
        println(fileContents)
        val myObject0: Try[TadDataInfoFromJSONList] = Try(fileContents.parseJson.convertTo[TadDataInfoFromJSONList])
        val info: Option[TadDataInfoFromJSON] = myObject0 match {
          case Success(infoJson) => {
            println(s"$infoJson")
            println("*****************************************************************************************")
            println(s"${infoJson.items(0)}")
            println("---------------------------------------------------------------")
            infoJson.items.lift(0)
          }
          case Failure(e) => {
            println(s"No json file in dir $subdirStr");
            println(s"Error ${e}");
            None
          }
        }
        info
      }
      case _ => None
    }
    info
  }

  def getDataSchema(): Try[DataTable] = {
    val dc_dsName = new DataColumn[String]("dataSetName", Iterable.empty[String])
    val dc_dsDate = new DataColumn[String]("dataSetDate", Iterable.empty[String])
    val dc_runStamp = new DataColumn[String]("runStamp", Iterable.empty[String])
    val dc_dsStamp = new DataColumn[String]("dsStamp", Iterable.empty[String])
    val dc_dsCoverage = new DataColumn[Double]("coverageInDataset", Iterable.empty[Double])
    val dc_nIter = new DataColumn[Int]("nIterations", Iterable.empty[Int])
    val dc_D = new DataColumn[Int]("D", Iterable.empty[Int])
    val dc_L = new DataColumn[Int]("L", Iterable.empty[Int])
    val dc_Median_S = new DataColumn[Int]("median_S", Iterable.empty[Int])
    val dc_S_0_025_pc = new DataColumn[Int]("S_0.025_pc", Iterable.empty[Int])
    val dc_S_0_975_pc = new DataColumn[Int]("S_0.975_pc", Iterable.empty[Int])
    val dc_Median_L = new DataColumn("median_L", Iterable.empty[Int])
    val dc_L_0_025_pc = new DataColumn("L_0.025_pc", Iterable.empty[Int])
    val dc_L_0_975_pc = new DataColumn[Int]("L_0.975_pc", Iterable.empty[Int])
    val dc_Median_Mu = new DataColumn[Double]("median_mu", Iterable.empty[Double])
    val dc_Mu_0_025_pc = new DataColumn[Double]("mu_0.025_pc", Iterable.empty[Double])
    val dc_Mu_0_975_pc = new DataColumn[Double]("mu_0.975_pc", Iterable.empty[Double])
    val dc_median_variance = new DataColumn[Double]("median_variance", Iterable.empty[Double])
    val dc_variance_0_025_pc = new DataColumn[Double]("variance_0.025_pc", Iterable.empty[Double])
    val dc_variance_0_975_pc = new DataColumn[Double]("variance_0.975_pc", Iterable.empty[Double])
    val dc_nLMedianFor90pcOfS = new DataColumn[Double]("nLMedianFor90pcOfS", Iterable.empty[Double])
    val dc_nL25PromilleFor90pcOfS = new DataColumn[Double]("nL25PromilleFor90pcOfS", Iterable.empty[Double])
    val dc_nL975PromilleFor90pcOfS = new DataColumn[Double]("nL975PromilleFor90pcOfS", Iterable.empty[Double])
    val dc_nLEffectiveSize = new DataColumn[Int]("nLEffectiveSize", Iterable.empty[Int])
    val dc_muEffectiveSize = new DataColumn[Int]("muEffectiveSize", Iterable.empty[Int])
    val dc_sigmaEffectiveSize = new DataColumn[Int]("sigmaEffectiveSize", Iterable.empty[Int])
    val dc_nSEffectiveSize = new DataColumn[Int]("nSEffectiveSize", Iterable.empty[Int])
    val dc_CI_width_S = new DataColumn[Int]("CI_width_S", Iterable.empty[Int])
    val dc_CI_width_L = new DataColumn[Int]("CI_width_L", Iterable.empty[Int])
    val dc_CI_width_mu = new DataColumn[Double]("CI_width_mu", Iterable.empty[Double])
    val dc_CI_width_variance = new DataColumn[Double]("CI_width_variance", Iterable.empty[Double])
    val dc_CI_width_sigma = new DataColumn[Double]("CI_width_sigma", Iterable.empty[Double])
    val dc_coverageForSampleSize = new DataColumn[Double]("coverageForSampleSize", Iterable.empty[Double])
    val dc_numIterations = new DataColumn[Int]("numIterations", Iterable.empty[Int])
    val dc_numAccepted = new DataColumn[Int]("numAccepted", Iterable.empty[Int])
    val dc_acceptanceRatio = new DataColumn[Double]("acceptanceRatio", Iterable.empty[Double])
    val dc_true_mu = new DataColumn[Double]("true_mu", Iterable.empty[Double])
    val dc_true_variance = new DataColumn[Double]("true_variance", Iterable.empty[Double])
    val dc_true_S = new DataColumn[Int]("true_S", Iterable.empty[Int])
    val dc_dateStr1 = new DataColumn[String]("dateStr1", Iterable.empty[String])
    val dc_distribution = new DataColumn[String]("distribution", Iterable.empty[String])
    val dc_coverage_config = new DataColumn[Double]("coverage_config", Iterable.empty[Double])
    val dc_S_config = new DataColumn[Int]("S_config", Iterable.empty[Int])
    //************
    val dt = DataTable("Results", Seq(dc_dsName, dc_dsDate, dc_runStamp, dc_dsStamp, dc_dsCoverage, dc_nIter, dc_D, dc_L, dc_Median_S, dc_S_0_025_pc, dc_S_0_975_pc, dc_Median_L, dc_L_0_025_pc, dc_L_0_975_pc, dc_Median_Mu, dc_Mu_0_025_pc, dc_Mu_0_975_pc, dc_median_variance, dc_variance_0_025_pc, dc_variance_0_975_pc, dc_nLMedianFor90pcOfS, dc_nL25PromilleFor90pcOfS, dc_nL975PromilleFor90pcOfS, dc_nLEffectiveSize, dc_muEffectiveSize, dc_sigmaEffectiveSize, dc_nSEffectiveSize, dc_CI_width_S, dc_CI_width_L, dc_CI_width_mu, dc_CI_width_variance, dc_CI_width_sigma, dc_coverageForSampleSize, dc_numIterations, dc_numAccepted, dc_acceptanceRatio, dc_true_mu, dc_true_variance, dc_true_S, dc_dateStr1, dc_distribution, dc_coverage_config, dc_S_config))
    dt
  }

}

