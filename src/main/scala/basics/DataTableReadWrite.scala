//import com.github.martincooper.datatable.{DataColumn, DataTable}
package basics

import com.github.martincooper.datatable._
import java.io.{File, FileReader}

import com.github.tototoshi.csv._
import org.scalactic.Fail

import scala.collection.immutable
//import org.scalatest.fixture
//
import scala.collection.JavaConverters._
import scala.collection.immutable.ListMap
import scala.util.{Failure, Success, Try}

/**
  * Created by peter on 16/08/16.
  */


object StringCol

object DataTableReadWrite {

  def convertStringTo[T](str: String, col: Object): Try[DataValue] = {
    val r2: Try[DataValue] = col match {
      case Double => Try(DataValue(str.toDouble))
      case Int => Try(DataValue(str.toInt))
      case StringCol => Try(DataValue(str))
      case _ => println("Unknown type of COLUMN!"); DataValue("Unknown type"); Failure(new Exception("Unknown type of COLUMN!"))
    }
    r2
  }

  def addRow2DS4CPosteriors(rowL: List[String], idx: Int, nRows: Int, dtI: DataTable, colTypes: ListMap[String, Object]): _root_.com.github.martincooper.datatable.DataTable = {
    val row: Seq[String] = rowL.take(colTypes.size)
    assert(row.size == colTypes.toList.size, s"Rows size =${row.size} and expected  row size = ${colTypes.toList.size} Line = ${row.mkString("[", ",", "]")} Header = ${colTypes.toList.mkString("[", ",", "]")}")
    val rDT: Seq[Try[DataValue]] = row.indices.map((i: Int) => {
      val colType: Object = colTypes.values.toIndexedSeq(i)
      convertStringTo(row(i), colType)
    })
    val rDT2 = rDT.collect { case Success(value) => value }
    val nDT: Try[DataTable] = if (rDT2.size == colTypes.keySet.size) {
      val r: Try[Try[DataTable]] = Try(dtI.rows.add(rDT2))
      r.toOption match {
        case Some(dtTry) => dtTry
        case _ => {
          println(s"${colTypes.keySet.mkString("[", ",", "]")}")
          println(s"${row.mkString("[", ",", "]")}")
          Try(dtI)
        }
      }
    }
    else {
      println(s"${colTypes.keySet.mkString("[", ",", "]")}")
      println(s"${row.mkString("[", ",", "]")}")
      println(s"*************************************************************************")
      Try(dtI)
    }
    if (idx % 10000 == 1) println(s"Row  $idx/$nRows,  ${100 * idx / nRows} %")
    nDT.get
  }

  /**
    * Not working
    *
    * @param name
    * @param file
    * @param colTypes
    * @return
    */
  def readCsvWOHeaderByColumns(name: String, file: FileReader, colTypes: ListMap[String, Object]) = {
    val colEntries: Seq[(String, Object)] = colTypes.toIndexedSeq
    val reader = CSVReader.open(file)
    val all: Seq[List[String]] = reader.all()
    //    Columns
    reader.close()
    //    val header = colTypes.toList.map(p => new DataColumn[p._2.type ](p._1, Iterable.empty[p._2.type ])) //.map()
    val header = colTypes.toList.map(pair => {
      colTypes(pair._1) match {
        case StringCol => new DataColumn[String](pair._1, Iterable.empty[String])
        case Int => new DataColumn[Int](pair._1, Iterable.empty[Int])
        case Double => new DataColumn[Double](pair._1, Iterable.empty[Double])
      }
    }
    )
    //    val dc_NIter = new DataColumn[Double]("nIter", Iterable.empty[Double])
    val dt: Try[DataTable] = DataTable("Results", header) //6-items
    println("")
    val colSets: immutable.Iterable[Seq[String]] = colTypes.zipWithIndex.map(cti => all.map(r => r(cti._2)))
    //    colTypes.in
    val dataCols: Iterable[DataColumn[_ >: String with Int with Double]] = colSets.zipWithIndex.map({ pair => {
      val colValues: Option[Seq[String]] = Option(pair._1)
      val colSampVal = colValues match {
        case Some(colValues) if (colValues.size > 0) => Some(colValues(0))
        case _ => None
      }
      // println(s"${pair._1} value ${colSampVal} type ${colTypes(pair._1)}");
      colTypes(colEntries(pair._2)._1) match {
        case StringCol => new DataColumn[String](colEntries(pair._2)._1, pair._1)
        case Int => new DataColumn[Int](colEntries(pair._2)._1, pair._1 map { x =>
          Try(x.toInt).toOption.getOrElse(-99)
        })
        case Double => new DataColumn[Double](colEntries(pair._2)._1, pair._1 map { x => {
          Try(x.toDouble).toOption.getOrElse(-99.0)
        }

        })
      }
    }
    })
    //*****
    val dtNew: DataTable = DataTable("Posteriors", dataCols).get
    // *** ****
    // -------------
    dtNew
  }

  def readCsvWOHeaderByRow(name: String, file: FileReader, colTypes: ListMap[String, Object]) = {
    val reader = CSVReader.open(file)
    val all: Seq[List[String]] = reader.all()

    //    Columns
    reader.close()
    //    val header = colTypes.toList.map(p => new DataColumn[p._2.type ](p._1, Iterable.empty[p._2.type ])) //.map()
    val header = colTypes.toList.map(pair => {
      colTypes(pair._1) match {
        case StringCol => new DataColumn[String](pair._1, Iterable.empty[String])
        case Int => new DataColumn[Int](pair._1, Iterable.empty[Int])
        case Double => new DataColumn[Double](pair._1, Iterable.empty[Double])
      }
    }
    )
    //    val dc_NIter = new DataColumn[Double]("nIter", Iterable.empty[Double])
    val dt: Try[DataTable] = DataTable("Results", header) //6-items
    println("")
    //    colTypes.in
    //    println(s"Total ${all.size} rows")
    val dtNew: DataTable = all.zipWithIndex.foldLeft(dt.get)((dtI: DataTable, rowWithIdx: (List[String], Int)) => addRow2DS4CPosteriors(rowWithIdx._1, rowWithIdx._2, all.size, dtI, colTypes))
    // *** ****
    //dt.get.rows.add(Seq(DataValue(0.0)))  //add(Seq(DataValue(0.0)))
    //    ------------------------
    val a = dtNew.foreach(r => r.values.foreach(e => println(e)))
    //    -----------
    // -------------
    dtNew
  }


  def readCsvSafe(name: String, file: FileReader, colTypes: Map[String, Object]): DataTable = {
    val reader = CSVReader.open(file)
    val all: Seq[Map[String, String]] = reader.allWithHeaders()
    reader.close()

    val ks = colTypes.keys
    //    val colSet4 = ks map {key => {val selRows = all.filter(row => row.size>1 && ks.forall(colName=>row.contains(colName)));(key, selRows map { row => row(key) })}}
    //    val colSet3 = ks map { key => (key, all map { row => row(key) }) }
    val keysInData = all(0).keys
    val colSet = ks map { key => (key, all.filter(r => ks.forall(key => r.contains(key))).map(row => row(key))) }
    val dataCols: Iterable[DataColumn[_ >: String with Int with Double]] = colSet map { pair => {
      val colValues = Option(pair._2)
      val colSampVal = colValues match {
        case Some(colValues) if (colValues.size > 0) => Some(colValues(0))
        case _ => None
      }
      // println(s"${pair._1} value ${colSampVal} type ${colTypes(pair._1)}");
      colTypes(pair._1) match {
        case StringCol => new DataColumn[String](pair._1, pair._2)
        case Int => new DataColumn[Int](pair._1, pair._2 map { x =>
          Try(x.toInt).toOption.getOrElse(-99)
        })
        case Double => new DataColumn[Double](pair._1, pair._2 map { x => {
          Try(x.toDouble).toOption.getOrElse(-99.0)
        }

        })
      }
    }
    }
    DataTable(name, dataCols).get
  }


  def readCsvUnsave(name: String, fileName: File, colTypes: Map[String, Object]): DataTable = {
    val file: FileReader = new FileReader(fileName)
    val reader: CSVReader = CSVReader.open(file)
    val all: Seq[Map[String, String]] = reader.allWithHeaders()
    reader.close()
    //    require(all.size>0,s"File ${file.toString} is empty")
    val ks = colTypes.keys
    //    val colSet4 = ks map {key => {val selRows = all.filter(row => row.size>1 && ks.forall(colName=>row.contains(colName)));(key, selRows map { row => row(key) })}}
    //    val colSet3 = ks map { key => (key, all map { row => row(key) }) }
    val keysInData = all(0).keys
    val colSet = ks map { key => (key, all.filter(r => ks.forall(key => r.contains(key))).map(row => row(key))) }
    val dataCols: Iterable[DataColumn[_ >: String with Int with Double]] = colSet map { pair => {
      val colValues = Option(pair._2)
      val colSampVal = colValues match {
        case Some(colValues) if (colValues.size > 0) => Some(colValues(0))
        case _ => None
      }
      // println(s"${pair._1} value ${colSampVal} type ${colTypes(pair._1)}");
      colTypes(pair._1) match {
        case StringCol => new DataColumn[String](pair._1, pair._2)
        case Int => new DataColumn[Int](pair._1, pair._2 map { x =>
          Try(x.toInt).toOption.getOrElse(-99)
        })
        case Double => new DataColumn[Double](pair._1, pair._2 map { x => {
          Try(x.toDouble).toOption.getOrElse(-99.0)
        }

        })
      }
    }
    }
    DataTable(name, dataCols).get
  }

  /**
    *
    * @param name
    * @param fileName
    * @param colTypes
    * @return
    */
  def readCsv(name: String, fileName: File, colTypes: Map[String, Object]): Try[DataTable] = {
    val file: FileReader = new FileReader(fileName)
    val reader: CSVReader = CSVReader.open(file)
    val all: Seq[Map[String, String]] = reader.allWithHeaders()
    reader.close()
    //**********************************************
    val ds: Try[DataTable] = if (all.size > 0) {

      //    require(all.size>0,s"File ${file.toString} is empty")
      val ks = colTypes.keys
      //    val colSet4 = ks map {key => {val selRows = all.filter(row => row.size>1 && ks.forall(colName=>row.contains(colName)));(key, selRows map { row => row(key) })}}
      //    val colSet3 = ks map { key => (key, all map { row => row(key) }) }
      val keysInData = all(0).keys
      val colSet = ks map { key => (key, all.filter(r => ks.forall(key => r.contains(key))).map(row => row(key))) }
      val dataCols: Iterable[DataColumn[_ >: String with Int with Double]] = colSet map { pair => {
        val colValues = Option(pair._2)
        val colSampVal = colValues match {
          case Some(colValues) if (colValues.size > 0) => Some(colValues(0))
          case _ => None
        }
        // println(s"${pair._1} value ${colSampVal} type ${colTypes(pair._1)}");
        colTypes(pair._1) match {
          case StringCol => new DataColumn[String](pair._1, pair._2)
          case Int => new DataColumn[Int](pair._1, pair._2 map { x =>
            Try(x.toInt).toOption.getOrElse(-99)
          })
          case Double => new DataColumn[Double](pair._1, pair._2 map { x => {
            Try(x.toDouble).toOption.getOrElse(-99.0)
          }

          })
        }
      }
      }
      DataTable(name, dataCols)

    } else {
      Failure(new Exception(s"Fie ${fileName.getName}"))
    }

    reader.close()
    ds
  }

  /**
    *
    * @param df
    * @param out
    */
  def writeCsv(df: DataTable, out: File): Unit = {
    val writer = Try(CSVWriter.open(out))
    writer match {
      case Success(w: CSVWriter) => {
        w.writeRow(df.columns.map {
          _.name
        })
        df.foreach {
          (r: DataRow) => w.writeRow(r.values)
        }
        w.close()
      }
      case Failure(e) => new Exception(s"$e")
    }
  }

}

