package basics;


object IntegrateTest extends App {
  implicit class PowerInt(i: Double) {
      def ** (n: Double): Double = scala.math.pow(i, n).intValue
  }
  val myInteg =  IntegrationWithTrapezoid()
  var resTrapez = myInteg.calculate(x  => x,0.0,10.0, 100)
  println("resTrapez "+resTrapez)

  val myIntegSimpson =  IntegrationWithSimpson()
  var resSimpson = myIntegSimpson.calculate(x  => x,0.0,10.0, 100)
  println("resSimpson "+resSimpson)


}
