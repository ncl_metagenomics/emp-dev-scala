package basics

import com.github.tototoshi.csv._


object CompareNLLs {
  val mcmcMetro = new MetropolisHMCMC()

  def main(args: Array[String]) = {
    //    val qOutFileN = "data/nLL_debug-copy2.csv"
    val abundanceFileN = "data/T2_160503/posterior-c_ERR315852_TAD__80_pc_D_199.2_lognormal_160429_1934-26_16-04-30_2021-59_160503-2305_33_0.sample"//"data/Brazil.sample"
    //val qOutFileN = "data/nLL_debug-copy_short.csv"
    //val qOutFileN = "data/nLL_debug-copy2.csv"
    //    val qOutFileN = "data/nLL_debug-UnitTest.csv"
    //val qOutFileN = "data/nLL_debug-copy2_shorter.csv"
    //******
    //val qOutFileN = "data/nLL_debug_160128_2205.csv"
    val qOutFileN = "data/T2_160503/nLL_debug_160503.csv"//"data/nLL_debug_160128_2303.csv"

    val relError = 1 / 10000000
    val absError = 0.00001

    val res = getFailedValues(qOutFileN, abundanceFileN, relError, absError)
    val res2 = addDifferences(res)

    //******
    writeIncorectNLL("output/T2_160503/incorretNLLs.csv", res2)

    println(s"${res.length - 1} incorrect records found!!!")
    println("test")
    // ********************

  }

  def writeIncorectNLL(filename: String, data: List[((Boolean, Double, CompareNLLs.Parameters), (Double, Double))]) {
    val writer = CSVWriter.open(filename) //"out.csv"
    //nIter","dMDash","dVDash","nSDash","dNLLDash
    val header = List("nIter", "mu", "sigma", "V", "S", "nLLQunce", "nLLScala", "nLLDev", "tnLLDiffQuince", "tnLLDiffScala")
    writer.writeRow(header)
    //val line = data.map(e=>writer.writeRow(List(e._3.nIter,e._3.dMDash,e._3.dVDash,e._3.nSDash,e._3.dNLLDash,e._2)))
    data.map(e => writer.writeRow(List(e._1._3.nIter, e._1._3.dMDash, Math.sqrt(e._1._3.dVDash), e._1._3.dVDash, e._1._3.nSDash,
      e._1._3.dNLLDash, e._1._2, e._1._3.dNLLDash - e._1._2, e._2._1, e._2._2)))
    writer.close()
  }

  case class Parameters(nIter: Int, dMDash: Double, dVDash: Double, nSDash: Int, dNLLDash: Double)
  //val heads = """(nIter","dMDash","dVDash","nSDash","dNLLDash)""".r

  /*  def ~=(x: Double, y: Double, precision: Double) = {
    if ((x - y).abs < precision) true else false
  }*/

  case class Precision(val p: Double)

  implicit class DoubleWithAlmostEquals(val d: Double) extends AnyVal {
    def ~=(d2: Double)(implicit p: Precision) = (d - d2).abs < p.p
  }

  implicit val precision = Precision(0.001) // Precision(0.000000001)

  /*val f03 = xs flatMap {
    case s: Int => { var c = s * s; if (s % 30 == 0) Some(Map(s -> c)) else None }
    case _ => None
  }*/

  def getFailedValues(qOutFileN: String, abundanceFileN: String, relError: Double, absError: Double): Array[(Boolean, Double, Parameters)] = {
    val data = MetropolisHMCMC.readData(abundanceFileN)
    //    val tuples = io.Source.fromFile(qOutFileN).getLines map (l => getFailedValue(l, data))
    val tuples: Iterator[(Boolean, Double, Parameters)] = io.Source.fromFile(qOutFileN).getLines.map(l => getFlaggedValue(l, data, relError, absError)).filter(_._1)
    var res = tuples.toArray
    res
  }

  /**
   * @param line
   * @param data
   */
  def getFlaggedValue(line: String, data: Array[(Int, Int)], relError: Double, absError: Double): (Boolean, Double, Parameters) = {
    val pars: Parameters = parseLine(line)
    val sigma = Math.sqrt(pars.dVDash)
    val nL = MetropolisHMCMC.getD(data.drop(1))
    //val correctNLL = 639.408178
    val correctNLL = mcmcMetro.nLogLikelihood(pars.dMDash, Math.sqrt(pars.dVDash), pars.nSDash, data, nL)
    //val isInCorrect = !(pars.dNLLDash ~= correctNLL)
    val isInCorrect = !(((pars.dNLLDash - correctNLL).abs < pars.dNLLDash.abs * relError) && (pars.dNLLDash - correctNLL).abs < absError)
    val res = (isInCorrect, correctNLL, pars)
    if (pars.nIter % 200 == 0) {
      println(s"processing record ${pars.nIter} - nLL value is ${if (isInCorrect) "incorrect" else "correct"}")
    }
    if (isInCorrect && false) { println(s"processing record ${pars.nIter} - nLL value is incorrect, diff = ${pars.dNLLDash - correctNLL}, rel = diff = ${(pars.dNLLDash - correctNLL) / pars.dNLLDash}") }
    res
  }

  def parseLine(line: String): Parameters = {
    val data = line.split(",")
    val nIter = try {
      data(0).toInt
    } catch {
      case e: NumberFormatException => { println(s"nIter = ${data(0)} "); 0 }
    }
    val dMDash = data(1).toDouble
    val dVDash = data(2).toDouble
    val nSDash = data(3).toInt
    val dNLLDash = data(4).toDouble
    new Parameters(nIter, dMDash, dVDash, nSDash, dNLLDash)
  }

  def movingDiff(values: List[Double], period: Int): List[Double] = {
    values.sliding(2).toList.map(e => e(0) - e(1))
  }

  def addDifferences(res: Array[(Boolean, Double, CompareNLLs.Parameters)]) = {
    // first is NLL from scala code and second is Quince's NLL
    var diffScala0 = res map { e => (e._2, e._3.dNLLDash) } sliding (2) map { e => (e(0)._1 - e(1)._1, e(0)._2 - e(1)._2) }
    var diffScala = List((0.0, 0.0)) ::: (diffScala0.toList)
    var combRes = (res, diffScala).zipped.toList
    println(s"Orig array length ${res.length}")
    println(s"Diff array length ${diffScala0.length}")
    println(s"New (ammended) Diff array length ${diffScala0.length}")
    combRes
  }

  /**
   * nIter","dMDash","dVDash","nSDash","dNLLDash
   */

}
