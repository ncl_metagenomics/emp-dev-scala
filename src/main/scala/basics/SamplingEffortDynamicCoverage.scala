package basics


import org.apache.commons.math3._
import org.apache.commons.math3.analysis.solvers._
import Math.exp
import Math.log

import scala.annotation.tailrec
import com.github.tototoshi.csv.CSVWriter
import com.github.tototoshi.csv.defaultCSVFormat
import java.io.IOException
import java.io.File
import com.typesafe.scalalogging.LazyLogging

import scala.util.{Failure, Success, Try}


/**
  * Created by peter on 14/06/16.
  */
object SamplingEffortDynamicCoverage extends LazyLogging {


  def main(args: Array[String]) = {
    val options = getProgramParameters(args) //List('arg1, 'arg2)
    println(options)
    println(s"mcmcf = ${options('mcmcf).toString()}")

    val postParamFileN = options('mcmcf).toString()
    val abundDataFileN = options('tadf).toString()

    //    val tCoverage: Double = (options('coverage)) match {
    //      case s: String => s.toDouble
    //      case d: Double => d
    //      case i: Int => i.toDouble
    //      case _ => throw new Error("coverage must be Double!"); Double.NaN
    //    }

    val nDTestDS = (options('D)) match {
      case s: String => s.toDouble
      case d: Double => d
      case i: Int => i.toDouble
      case _ => throw new Error("coverage must be Double!"); Double.NaN
    }

    val outputFile = SamplingEffort.constructOutputFileNameAllOutputs(postParamFileN, "dynamic_S")
    val outputFile2 = SamplingEffort.constructOutputFileName4Correct(postParamFileN, "dynamic_S")
    // outputFile2
    // outputFile2
    // outputFile2
    //  -mcmcf "/media/sf_Dropbox/Newcastle-Project/src/Projects/Quince-C/Output/sad_samples/q_GOS_logNormal_160304_1549_all.out__160304-1711.sample"
    // -tadf "/media/sf_Newcastle-Project/src/Projects/Quince-C/Output/sad_samples/GOS.sample"
    println(s"Output written to $outputFile")
    val abuData = SamplingEffort.readData(abundDataFileN)
    val nD = abuData.map(x => x._2).par.sum
    val nL = abuData.map((x) => x._1 * x._2).par.sum
    assert(nL > 0, s"Positive number of individuals but $nL found!")
    assert(nD > 0, s"Positive number of species reqired but $nD found!")
    val dropNumerator = 2
    val dropDenominator = 5
    val numRows2Skip = 0
    val (dataPosterior, head) = SamplingEffort.readDataPosteriorEstimates(postParamFileN, numRows2Skip)
    println(s"${dataPosterior(1).mu}, ${dataPosterior(1).variance}, ${dataPosterior(1).nCommunityTaxa}")

    val tollerance = 1.1 //relative error for difference of algorihms
    val tighterTollerance = 1.05
    val mu = -1.6
    val variance = 6.0
    //targeted coverage
    //    val tCoverage = 0.9 //targeted coverage
    //    val targetFractionUnseen = 1 - tCoverage
    //    val lnTargetP = Math.log(targetFractionUnseen)
    val lnPInit = SamplingEffort.logPLogNormal.calculateWithRLikeScala(mu, variance, 0)

    println(s"*** Estimate *******")

    //    val nSampleEst: SamplingEffort.SampleSizeEstimateDS = SamplingEffort.getSampleSizeValue4DynamicCoverage(mu, variance, nL, nDTestDS, tollerance, tighterTollerance)
    println(s"Initial: nL = $nL, lnPInit = $lnPInit, c = ${1 - Math.exp(lnPInit)}")
    //    println(s"Target: lnTargetP = $lnTargetP, c = ${1 - Math.exp(lnTargetP)}")
    //    println(s"Solution: required sampl size = ${nSampleEst.nL}, muS=${nSampleEst.nL}, c = ${nSampleEst.nL}")
    // *********************************
    //    println(s"Solution required sample size as multiple of actual sample size = ${nSampleEst.nL}")
    implicit val fwCSV = CSVWriter.open(outputFile2)
    fwCSV.writeRow(List("mu", "variance", "nL_Recommended", "nL_Observed", "multipleOfActualSampleSize", "coverage", "mu_orig", "nL_orig"))
    val (res: Array[Try[SamplingEffort.SampleSizeEstimateDS]], time:Long) = SamplingEffort.profile(SamplingEffort.getSampleSizeDistributionDynamicCoverage(dataPosterior, mu, variance, nL, nDTestDS, tollerance, tighterTollerance))
    fwCSV.close()
    //val outSampleFN: String = outPutFN(postParamFileN)
    // val (res: (ParamsPS, Double), time) = profile(mcmcMetro.calculate(mu0, variance0, s0, data, _nL = Some(nL), nIter = nIter, estimateWhat, version, outfileName))
    SamplingEffort.writeOutput(res, outputFile)
    println(s"Output written to $outputFile")
    println(s"Time of calculation = ${time * 1E-9} sec")
    println("Finished estimating of sampling effort")
  }

  def getProgramParameters(args: Array[String]) = {
    val required = List() //List('arg1, 'arg2)
    // Optional arguments by flag which map to a key in options
    val optional = Map("-tadf" -> 'tadf, "-mcmcf" -> 'mcmcf,  "-D"->'D)
    // Default options that are passed in
    val definedParameters: Map[Symbol, Any] = Map()
    // Parse options based on the command line args
    val options = parseOptions(args.toList, required, optional, definedParameters, 0)
    options
  }


  def parseOptions(args: List[String], required: List[Symbol], optional: Map[String, Symbol], definedParameters: Map[Symbol, Any], iterations: Int): Map[Symbol, Any] = {
    val usage =
      """
						Usage: parser [-v] [-f file] [-s sopt] ...
						Where:
						Required parameters:\n"
						-mcmcf input file name for file with MCMC iterations
						-tadf input file name with
            -D number of species
      						"""

    if (args.length == 0 && iterations < 1) println(usage)
    args match {
      // Empty list
      case Nil => definedParameters

      // Keyword arguments
      case "-s" :: value :: tail =>
        parseOptions(tail, required, optional, definedParameters ++ Map(optional("-s") -> value.toInt), iterations + 1)
      case "-s0" :: value :: tail =>
        parseOptions(tail, required, optional, definedParameters ++ Map(optional("-s0") -> value.toInt), iterations + 1)
      case key :: value :: tail if optional.get(key) != None =>
        parseOptions(tail, required, optional, definedParameters ++ Map(optional(key) -> value), iterations + 1)

      // Positional arguments
      case value :: tail if required != Nil =>
        parseOptions(tail, required.tail, optional, definedParameters ++ Map(required.head -> value), iterations + 1)

      // Exit if an unknown argument is received
      case _ =>
        printf("unknown argument(s): %s\n", args.mkString(", "))
        sys.exit(1)
    }
  }

}
