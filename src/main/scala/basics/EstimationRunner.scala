package basics

object EstimationRunner extends App {
  val fileName = "/media/sf_data/nps409/Other/Dropbox/Newcastle-Project/src/Projects/Quince-C/Data/Brazil.sample"
  val mcmcMetro = new MetropolisHMCMC()
  val data = MetropolisHMCMC.readData(fileName)
  // Initial values
  val mu0 = 1.0
  val sigma0 = 1.0
  val s0 = 100.0
  val estimateWhat = "all"
  val nL = Option(5)
  val nIter = 5
  val distribution = "lognormal"
  mcmcMetro.calculate(mu0, sigma0, s0, data, nL, nIter, estimateWhat, "s", "out.csv", distribution, None)
}
