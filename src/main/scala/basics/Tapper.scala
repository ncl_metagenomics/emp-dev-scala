import scala.language.implicitConversions


class Tapper[A](tapMe: A) {
  def tap(f: (A) => Unit): A = {
    f(tapMe)
    tapMe
  }
}

object Tap {
  implicit def any2Tapper[A](toTap: A): Tapper[A] = new Tapper(toTap)
}