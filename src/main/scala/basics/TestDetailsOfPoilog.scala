package basics

import com.github.tototoshi.csv.CSVWriter
import com.github.tototoshi.csv.defaultCSVFormat
import utils.sampling._


object TestDetailsOfPoilog {
  def main(args: Array[String]) = {
    val quinceLogPByRecord = "output/T2_160526_2331/debug_poilog_vals_160526_2355.csv" //"data/nLL_debug_by_record_160128_2303.csv"
    val relError = 1 / 10000000
    val absError = 0.00001
    val outFileName = "output/T2_160528_2331/debug_poilog_vals_added_160527_1443a.csv"
    val outFileName2 = "output/T2_160528_2331/debug_poilog_vals_added_160527_1443b.csv"
    val fw2 = getWriter(outFileName2)

    val header = List("n", "mu", "v", "sigma", "maxFScala", "lowerBoundScala", "upperBoundV", "p_poilog_PS", "maxFPS", "lowerBoundPS", "upperBoundPS", "p_poilogPS")
    fw2.writeRow(header)
    val res: List[Map[String, Any]] = addPSRPoilogDebugValues(quinceLogPByRecord, relError, absError, fw2)
    writeAll("output/T2_160526_2331/debug_poilog_vals_added_160527_1443.csv", res)
    fw2.close()
  }

  def addPSRPoilogDebugValues(qOutFileN: String, relError: Double, absError: Double, fw: CSVWriter): List[Map[String, Any]] = {
    val lines = io.Source.fromFile(qOutFileN).getLines()
    val headerStr = lines.next()
    val header = headerStr.split(",")
    val resMap = lines.map(l => addPoilogRAndScalaValues(l, relError, absError, header, fw)) //.filter(_._1)
    resMap.toList
  }

  def getWriter(filename: String) = {
    val f = new java.io.File(filename)
    val fDir = f.getParent
    (new java.io.File(fDir)).mkdirs()
    CSVWriter.open(filename)
  }

  case class Parameters(nIter: Int, dMDash: Double, dVDash: Double, nA: Int, dLogP: Double)

  def writeAll(filename: String, data: List[Map[String, Any]]) = {
    val f = new java.io.File(filename)
    val fDir = f.getParent
    (new java.io.File(fDir)).mkdirs()
    val writer = CSVWriter.open(filename)
    //nIter,dMDash,dVDash,nSDash,nA,dLogP
    val header = List("n, mu, variance, maxF, lowerBound, upperBound, P_poilogWOConst, P_poilog")
    writer.writeRow(header)
    // "n"->n,"mu"->mu,"maxF"->maxF,"lowerBound"->lowerBound,"upperBound"->upperBound,"p_poilogWOConst"->p_poilogWOConst,"p_poilog"->p_poilog
    data.map(e => writer.writeRow(List(e.get("n"), e.get("mu"))))
    writer.close()
    println(s"Writing to filename $filename")
  }

  def addPoilogRAndScalaValues(line: String, relError: Double, absError: Double, header: Array[String], fw: CSVWriter): Map[String, Any] = {
    val pars: Map[String, Any] = parseLine(line, header)
    val ll = new LogLikelihoodPoissonLogNormal()
    val mu = pars.get("mu").orNull.asInstanceOf[Double]
    val v = pars.get("variance").orNull.asInstanceOf[Double]
    val sigma = Math.sqrt(v)
    val n = pars.get("n").orNull.asInstanceOf[Integer]
    val lowerBoundV = pars.get("lowerBound").orNull.asInstanceOf[Double]
    val upperBoundV = pars.get("upperBound").orNull.asInstanceOf[Double] // upperBound
    val p_poilogWOConst = pars.get("p_poilogWOConst").orNull.asInstanceOf[Double]
    val p_poilog = pars.get("p_poilog").orNull.asInstanceOf[Double]
    val maxFV = pars.get("maxF").orNull.asInstanceOf[Double]
    //************************************************************
    val maxFScala = Poilog.maxF(n, mu, v)
    val lowerBoundScala = Poilog.lower(n, maxFScala, mu, v)
    val upperBoundScala = Poilog.upper(n, maxFScala, mu, v)
    val p_poilogScala = Poilog.dpoilog(n, mu, sigma)

    //************************************************************
    val maxFPS = PoilogRScala.getMaxFPS(mu, v, n)
    val lowerBoundPS = PoilogRScala.getLowerBoundPS(n, maxFPS, mu, v)
    val upperBoundPS = PoilogRScala.getUpperBoundPS(n, maxFPS, mu, v)
    val p_poilogPS = PoilogRScala.getPSPoilog(mu, v, n)
    //------------------------------------------------------------
    // ***** calculation *****

    val res = Map[String, Any]("n" -> n, "mu" -> mu, "v" -> v, "sigma" -> sigma, "maxF_Scala" -> maxFV, "lowerBoundScala" -> lowerBoundV, "upperBoundScala" -> upperBoundV,
      "p_poilog_Scala" -> p_poilog, "maxFPS" -> maxFPS, "lowerBoundPS" -> lowerBoundPS, "upperBoundPS" -> upperBoundPS, "p_poilogPS" -> p_poilogPS)
    val tmp = List("n", "mu", "v", "sigma", "maxFScala", "lowerBoundScala", "upperBoundScala", "p_poilog_Scala", "maxFPS", "lowerBoundPS", "upperBoundPS", "p_poilogPS")
    fw.writeRow(List(n, mu, sigma, v, maxFScala, lowerBoundScala, upperBoundScala, p_poilogScala, maxFPS, lowerBoundPS, upperBoundPS, p_poilogPS))
    //    if (pars.nIter % 200 == 0) {
    //      println(s"processing record ${} - logP value is ${if (isInCorrect) "incorrect" else "correct"}")
    //    }
    res
  }

  def parseLine(line: String, header: Array[String]): Map[String, Any] = {
    // nIter,dMDash,dVDash,nSDash,nA,dLogP
    val data = line.split(",")
    val n = try {
      data(0).toInt
    } catch {
      case e: NumberFormatException => { println(s"nIter = ${data(0)} "); 0 }
    }
    //nIter,dMDash,dVDash,nSDash,nA,dLogP
    val mu = data(1).toDouble
    val variance = data(2).toDouble
    val maxF = data(3).toDouble
    val lowerBound = data(4).toDouble
    val upperBound = data(5).toDouble
    val p_poilogWOConst = data(6).toDouble
    val p_poilog = data(7).toDouble
    val m = Map[String, Any]("n" -> n, "mu" -> mu, "variance" -> variance, "maxF" -> maxF, "lowerBound" -> lowerBound, "upperBound" -> upperBound, "p_poilogWOConst" -> p_poilogWOConst, "p_poilog" -> p_poilog)
    m
    //new Parameters(nIter, dMDash, dVDash, nA, logP)
  }

}
