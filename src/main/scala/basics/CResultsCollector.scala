package basics

import java.io.{File, FileReader, FileWriter}

import basics.ResultsCollector._
import com.github.martincooper.datatable.{DataColumn, DataTable, DataValue}
import utils.{CScriptGenerator, MCMCIO}
import utils.ScalaScriptGenerator.{dateStr, generateShScript, getCovareFileName}

import scala.collection.immutable.ListMap
import scala.util.{Failure, Success, Try}

/**
  * Created by peter on 05/06/17.
  */
object CResultsCollector {


  val synthDataPatterns: DataInfoAndPatterns = DataInfoAndPatterns("synthetic", """posteriorParams_c\.csv_0\.sample""", """sample.*\.csv""", """c(_|-).*""")
  val realDataPatterns: DataInfoAndPatterns = DataInfoAndPatterns("real", """posteriorParams_c\.csv_0\.sample""", """sample.*\.csv""", """c(_|-).*""")

  def main(args: Array[String]): Unit = {

    val dirList = List(
      "/media/sf_data/nps409/Other/EBI Results From Topsy/emp_dev_scala_runs/TAD_DATA_Real_Out_4_var_distr/IG_Distr/synth/Script_CQ_100_pc_v00_synth_sample_r_0.1_d_180124_1219-52_r2_0.3_d2_180226_1037-05_180302_0348-02",
      "/mnt/topsy/share/nobackup/nps409/EBI/emp_dev_scala_runs/TAD_DATA_Real_Out_4_var_distr/IG_Distr/synth/Script_CQ_100_pc_v00_synth_sample_r_0.1_d_180124_1219-52_r2_0.3_d2_180226_1037-05_180302_0348-02",
      "/mnt/topsy/share/nobackup/nps409/EBI/emp_dev_scala_runs/TAD_DATA_Real_Out_4_var_distr/IG_Distr/synth/Script_CQ_100_pc_v00_synth_sample_r_0.1_d_180124_1219-52_r2_0.05_d2_180226_1032-17_180302_0332-03",
      "/mnt/topsy/share/nobackup/nps409/EBI/emp_dev_scala_runs/TAD_DATA_Real_Out_4_var_distr/IG_Distr/synth/Script_CQ_100_pc_v00_synth_sample_r_0.1_d_180124_1219-52_r2_0.2_d2_180225_1431-36_180302_0309-22",
      "/mnt/topsy/share/nobackup/nps409/EBI/emp_dev_scala_runs/TAD_DATA_Real_Out_4_var_distr/IG_Distr/synth/Script_CQ_100_pc_v00_180130_0139-47",
      "/mnt/topsy/share/nobackup/nps409/EBI/emp_dev_scala_runs/TAD_DATA_Real_Out_4_var_distr/Script_CQ_100_pc_v00_171117_0230-00",
      "/mnt/topsy/share/nobackup/nps409/EBI/emp_dev_scala_runs/TAD_DATA_Real_Out_4_var_distr/Script_CQ_100_pc_v00_171106_1711-57",
      "/media/sf_Newcastle-Project/src/Projects/Quince-C/R/DATA_GENERATOR/CQ_Data_Generator_170627_2302/Results_TAD_60_pc_v01",
      "/media/sf_data/nps409/Other/Dropbox/Newcastle-Project/src/Projects/Quince-C/R/DATA_GENERATOR/CQ_Data_Generator_170627_2302/Results_TAD_10_pc",
      "/media/sf_data/nps409/Other/Dropbox/Newcastle-Project/src/Projects/Quince-C/R/DATA_GENERATOR/CQ_Data_Generator_170627_2302/Results_TAD_10_pc",
      "/media/sf_data/nps409/Work2Sync/Projects/emp_dev_scala_runs/TAD_DATA_Synthetic_Out/QC_paper/QC_170213_stopped_w_topsy",
      "/media/sf_data/nps409/Work2Sync/Projects/emp_dev_scala_runs/TAD_DATA_Synthetic_Out/QC_paper/QC_170213_test4CCode"
    )

    val mainDir = dirList(0).trim // dirList(3) // dirList(1)

    //    collectAndWriteResults(mainDir, synthDataPatterns)
    collectAndWriteResults(mainDir, realDataPatterns)
  }


  def processSingleDataDir(dsDir: String, scriptDir: String, jarFile: String, jarDependencyFile: String, params: CScriptGenerator.Params): Option[String] = {
    val dataFileFileStr: String = getCovareFileName(dsDir, """(?i).*TAD.*\.csv$""")
    println(s"dataFileFileStr = $dataFileFileStr")
    val varCovarFileStr: String = getCovareFileName(dsDir, """(?i).*varCovar.*adjusted.*partial.*\.csv$""")
    println(s"varCovarFileStr = $varCovarFileStr")
    // *** Dataset name ***
    val dataFileNamePartialOnly = (new File(dsDir)).getName()
    // *** Scriptr File path
    val scriptPath4DS: File = (new File(scriptDir, s"${dataFileNamePartialOnly}"))
    val scriptPath4DS2: File = (new File(scriptPath4DS, s"s-${dataFileNamePartialOnly}_${dateStr}"))
    // , "s-${dataFileNamePartialOnly}_${dateStr}"
    scriptPath4DS2.mkdirs()
    val scriptNameOutput = (new File(scriptPath4DS2, s"${dataFileNamePartialOnly}_${dateStr}.sh"))
    println(s"scriptNameOutput = $scriptNameOutput")
    // *** Relative Paths ****
    val relPathDataStr = MCMCIO.absoluteToRelativePath(scriptPath4DS2.getAbsolutePath, dataFileFileStr)
    println(s"relPathDataStr = ${relPathDataStr.getOrElse("??????")}")
    val relPathVarCovarStr = MCMCIO.absoluteToRelativePath(scriptPath4DS2.getAbsolutePath, varCovarFileStr)
    println(s"relPathVarCovarStr  = ${relPathVarCovarStr.getOrElse("????")} ")
    val jarFileRelative = MCMCIO.absoluteToRelativePath(scriptPath4DS2.getAbsolutePath, jarFile)
    val jarDependencyFileRelative = MCMCIO.absoluteToRelativePath(scriptPath4DS2.getAbsolutePath, jarDependencyFile)
    //  *** Script Content ****
    //    val script = generate(relPathDataStr.getOrElse("???"), relPathVarCovarStr.getOrElse("???"), jarFileRelative.getOrElse("???"), jarDependencyFileRelative.getOrElse("????"),params)
    // *** RElative to script root directory
    // scriptNameOutput
    // scriptDir
    val relativeScript = MCMCIO.absoluteToRelativePath(scriptDir, scriptNameOutput.getAbsolutePath)
    val fw = new FileWriter(scriptNameOutput)
    //    fw.write(script)
    fw.close()
    //    println(s"script = $script")
    println("--------------------------------------------------------------------------------------------------------------")
    relativeScript
  }


  def getDataTableSchema(): Try[DataTable] = {
    val dc_DSName = new DataColumn[String]("dataSetName", Iterable.empty[String])
    val dc_DSDate = new DataColumn[String]("dataSetDate", Iterable.empty[String])
    val dc_CompletedIters = new DataColumn[Int]("CompletedIterations", Iterable.empty[Int])

    val dc_MedianMu = new DataColumn("median_mu", Iterable.empty[Double])
    val dc_LBMu = new DataColumn[Double]("mu_0.025_pc", Iterable.empty[Double])
    val dc_UBMu = new DataColumn[Double]("mu_0.975_pc", Iterable.empty[Double])

    val dc_MedianVar = new DataColumn[Double]("median_variance", Iterable.empty[Double])
    val dc_LBVar = new DataColumn[Double]("variance_0.025_pc", Iterable.empty[Double])
    val dc_UBVAr = new DataColumn[Double]("variance_0.975_pc", Iterable.empty[Double])

    val dc_MedianS = new DataColumn[Int]("median_S", Iterable.empty[Int])
    val dc_LBS = new DataColumn[Int]("S_0.025_pc", Iterable.empty[Int])
    val dc_UBS = new DataColumn[Int]("S_0.975_pc", Iterable.empty[Int])

    val dc_MedianL = new DataColumn[Option[Int]]("median_L", Iterable.empty[Option[Int]])
    val dc_LBL = new DataColumn("L_0.025_pc", Iterable.empty[Option[Int]])
    val dc_UBL = new DataColumn("L_0.975_pc", Iterable.empty[Option[Int]])

    val dc_MuEffectiveSize = new DataColumn[Int]("muEffectiveSize", Iterable.empty[Int])
    val dc_SigmaEffectiveSize = new DataColumn[Int]("sigmaEffectiveSize", Iterable.empty[Int])
    val dc_nSEffectiveSize = new DataColumn[Int]("nSEffectiveSize", Iterable.empty[Int])


    val dt = DataTable("Results",
      Seq(
        dc_DSName, dc_DSDate, dc_CompletedIters,
        dc_MedianMu, dc_LBMu, dc_UBMu,
        dc_MedianVar, dc_LBVar, dc_UBVAr,
        dc_MedianS, dc_LBS, dc_UBS,
        dc_MedianL, dc_LBL, dc_UBL,
        dc_MuEffectiveSize, dc_SigmaEffectiveSize, dc_nSEffectiveSize
      )

    )

    dt
  }

  /**
    *
    * @param file
    * @return
    */
  def readPosteriorData(file: File) = {
    //    throw new Exception("Not implemented yet!")
    val colTypes = ListMap("nIter" -> Double, "mu" -> Double, "variance" -> Double, "S" -> Double, "nLL" -> Double)
    //it was acceptance
    // nIter,mu,variance,S,nLL,acceptance,n_accepted
    //    val tadData = DataTableReadWrite.readCsvWOHeader("ParamsPosteriorDistribution", new FileReader(file), colTypes)
    println(s"Working for file ${file.getAbsoluteFile}")
    val tadData = DataTableReadWrite.readCsvWOHeaderByColumns("ParamsPosteriorDistribution", new FileReader(file), colTypes)
    //    val tadData = DataTableReadWrite.readCsvWOHeader("ParamsPosteriorDistribution", new FileReader(file), colTypes)
    tadData
  }

  def collectResultsEstimatesLevel(dt: DataTable, f: File, coverage: Double, numOfIterations: Option[Int], otuInfo: OtuInfo, nLDist90pcStatInfo: StatisticsInformation, dataType: DataInfoAndPatterns, dataSetName: String, tadInfo: TadDataInfoFromJSON): DataTable = {
    // ======= Posterior  =======================
    val posteriorProbabPattern: String = dataType.posteriorDatafilePatternStr
    //"^posterior-.*\\.csv$"
    val dtOut: DataTable = getFileNameWithPosteriors(f, List("posteriorParams_c.*_0\\.sample", posteriorProbabPattern,"""posterior-c.*\.sample$""")) match {
      case Some(fileNameWithPosteriors) => {
        println(s"fileWPosterior = $fileNameWithPosteriors")
        val fileNameOnlyWPosteriors = fileNameWithPosteriors.getName

        println(s"fileOnlyWPosterior = $fileNameOnlyWPosteriors")
        val (nD, distribution) = getInfoFromFileNameOfPosteriors(fileNameOnlyWPosteriors)

        //*** Samples size file ****
        val sampleSizePattern: String = dataType.sampleSizeFilePattern
        //"^sample_size_posterior-.*\\.csv$"
        val fileNameWithSampleSize = getFileNameWithPosteriors(f, sampleSizePattern)

        println(s"samle size file name ${fileNameWithSampleSize}")
        //========================review http://www.tennis-warehouse.com/Reviews/BPA/BPAReview.html ===

        val posteriorParamsData: DataTable = readPosteriorData(fileNameWithPosteriors)
        val sampleSizeData: Try[DataTable] = readSampleSizeData(fileNameWithSampleSize)
        val nSEstO: Try[ResultsCollector.StatisticsInformation] = extractStats4Variable(posteriorParamsData, 10000, "S")
        val newDT: DataTable = nSEstO match {
          //TODO: 170424 quietly capture error
          case Success(nSEst: StatisticsInformation) => {
            println(nSEst)
            val numInteration0 = extractLastOfColumn(posteriorParamsData, "nIter")
            val numInteration: Int = numInteration0 match {
              case Success(myInt: Int) => myInt
              case Failure(e) =>
                throw new Exception(e.getMessage + "\n" + s" for file with posterior $fileNameWithPosteriors ")
            }

            val muEstO = extractStats4Variable(posteriorParamsData, 10000, "mu")
            val muEst: ResultsCollector.StatisticsInformation = muEstO match {
              case Success(est: StatisticsInformation) => est
              case Failure(e) => throw new Exception(e.getMessage + "\n" + s" for file with posterior $fileNameWithPosteriors ")
            }
            //sigma
            val varianceEst0 = extractStats4Variable(posteriorParamsData, 10000, "variance")
            val varianceEst: ResultsCollector.StatisticsInformation = varianceEst0 match {
              case Success(est: StatisticsInformation) => est
              case Failure(e) => throw new Exception(e.getMessage + "\n" + s" for file with posterior $fileNameWithPosteriors ")
            }
            //sigma

            val nLEstO = sampleSizeData match {
              case Success(f) => extractStats4Variable(f, 10000, "nL_Recommended")
              case Failure(e) =>
                println(s"Problem to read file ${fileNameWithSampleSize}. Error $e.getMessage().")
                println(s"Error $e.getMessage().")
                Success(StatisticsInformation(0, 0, 0, 0, 0))
            }

            //            val nLEstO = sampleSizeData match {
            //              case Some(f) => extractStats4Variable(f, 10000, "nL_Recommended")
            //              case None => Success(StatisticsInformation(0, 0, 0, 0, 0))
            //            }
            // StatisticsInformation(nSMedian, nSPercentile2_5, nSPercentile97_5, nSMean, nSVariance.asInstanceOf[Double])
            val nLEst: ResultsCollector.StatisticsInformation = nLEstO match {
              case Success(est: StatisticsInformation) => est
              case Failure(e) =>
                throw new Exception(e.getMessage + "\n" + s" for file with posterior $fileNameWithPosteriors ")
            }
            val datanD1 = otuInfo.nD
            // *************************************************************
            val effectiveSizeNL: Int = sampleSizeData match {
              case Success(dt: DataTable) => utils.EffectiveSampleSize.getEffectiveSampleSize2(dt, "nL_Recommended", scala.Double)
              case Failure(e) =>
                println(e.getMessage)
                0
            }
            val effectiveSizeMu: Int = utils.EffectiveSampleSize.getEffectiveSampleSize2(posteriorParamsData, "mu", scala.Double)
            val effectiveSizeSigma: Int = utils.EffectiveSampleSize.getEffectiveSampleSize2(posteriorParamsData, "variance", scala.Double)
            val effectiveSizeS: Int = utils.EffectiveSampleSize.getEffectiveSampleSize2(posteriorParamsData, "S", scala.Double)
            val valCIWidthS = (nSEst.percentile97_5 - nSEst.percentile2_5).toInt
            val valCIWidthL = (nLEst.percentile97_5 - nLEst.percentile2_5).toInt
            val valCIWidthMu = (muEst.percentile97_5 - muEst.percentile2_5)
            val valCIWidthSigma = (varianceEst.percentile97_5 - varianceEst.percentile2_5)
            val valCIWidthVariance = scala.math.pow(valCIWidthSigma, 2)
            val dirName = f.getName
            val pattern = dataType.posteriorDirPattern.r
            val (distributionName, dataSetDate, optimizationMethod) = dirName match {
              case pattern(distribution, optimizationMethod, tollerance, strictTollerance, dataSetDate, _*) => println(distribution + ", " + optimizationMethod + ", " + tollerance + ", " + strictTollerance + ", " + dataSetDate + ", "); (distribution, dataSetDate, optimizationMethod)
              case _ => ("distributionUnknown", "datasetDateUnknown", "optimizationMethodUnknown")
            }
            val sampleSizeFNOnly = fileNameWithSampleSize match {
              case Some(f) => f.getName
              case None => "UnknownSampleSizeFileName"
            }
            val sampleSizePatternExpr = sampleSizePattern.r
            val coverageOpt: Option[Double] = sampleSizeFNOnly match {
              case sampleSizePatternExpr(a1, a2, a3, a4, a5, a6, a7, a8, a9, coverage, _*) => tryStr2Double(coverage)
              case _ => Some(0.0)
            }

            val coverageForSampleSize = coverageOpt.getOrElse(0.0) / 1000
            //    // -------------------------------------------------------------
            //    val dc29 = new DataColumn[Double]("coverageForSampleSize", Iterable.empty[Double])
            //    val dc30 = new DataColumn[Int]("numIterations", Iterable.empty[Int])
            //    val dc31 = new DataColumn[Int]("numAccepted", Iterable.empty[Int])
            //    val dc32 = new DataColumn[Double]("acceptanceRatio", Iterable.empty[Double])
            //************************************
            // todo: 170609 work here
            val x = dt.rows.add(Seq(
              DataValue(dataSetName), DataValue(dataSetDate), DataValue(numInteration),
              DataValue(muEst.median), DataValue(muEst.percentile2_5), DataValue(muEst.percentile97_5),
              DataValue(varianceEst.median), DataValue(varianceEst.percentile2_5), DataValue(varianceEst.percentile97_5),
              DataValue(nSEst.median.toInt), DataValue(nSEst.percentile2_5.toInt), DataValue(nSEst.percentile97_5.toInt),
              DataValue(Option(nLEst.median.toInt)), DataValue(Option(nLEst.percentile2_5.toInt)), DataValue(Option(nLEst.percentile97_5.toInt)),
              DataValue(effectiveSizeMu.toInt),
              DataValue(effectiveSizeSigma.toInt),
              DataValue(effectiveSizeS.toInt)

            ))
            println("")
            x.get
          }
          // TODO: 170523 REWRITE WITHOUT THROWIN ERRROR
          case Failure(e) => {
            println(e.getMessage + "\n" + s" for file with posterior $fileNameWithPosteriors ");
            dt
          }
        }
        newDT

      }
      case None => {
        println(s"File not in directory $f")
        logger.error(s"No file  with posterior distribution of parameters found! in  directoryt ${f.getCanonicalPath}")
        // TODO: 170523 REWRITE WITHOUT THROWIN ERRROR
        //throw new Exception(s"No file  with posterior distribution of parameters found! in  directoryt ${f.getCanonicalPath}")
        dt
      }
    }

    dtOut
  }

  /**
    *
    * @param sampleSubDir
    * @param dataTable
    * @param nLDist90pcStatInfo
    * @param tadInfo
    * @param allDirsOfPosteriors
    * @param firstDirOfPosteriors0
    * @param otuInfo
    * @param dataSetName
    * @param dataType
    * @return
    */

  def extractDataForPosteriorFolder(sampleSubDir: File, dataTable: DataTable, nLDist90pcStatInfo: StatisticsInformation, tadInfo: TadDataInfoFromJSON, allDirsOfPosteriors: List[File], firstDirOfPosteriors0: Option[File], otuInfo: OtuInfo, dataSetName: String, dataType: DataInfoAndPatterns): DataTable = {
    val firstDirOfPosteriors = firstDirOfPosteriors0 match {
      case Some(x) => x
      case None => throw new Exception(s"No folder with posterior file found in directory ${sampleSubDir.getAbsolutePath}!")
    }

    //**
    val numOfIterations: Option[Int] = getNumOfIterations(sampleSubDir)
    val coverage = dataType match {
      case FileDirRegExPattern.REAL_DATA_S => extracCoveratFromPath(sampleSubDir.getAbsolutePath)
      case _ => 0
    }
    //val posteriorProbabPattern: String = "^posterior-.*\\.csv$"
    //    val patterns = List("^posterior-s-(\\w)-(\\w*)-([a-zA-Z0-9]*_?(\\w*)?)(-|_).*\\.csv$")
    val fileNameWithPosteriors = getFileNameWithPosteriors(sampleSubDir, List("^posteriorParams.*0\\.sample$", dataType.posteriorDatafilePatternStr
    )) match {
      case Some(file) => file
      case None => {
        println(s"File with pattern ${dataType.posteriorDatafilePatternStr} not in directory $firstDirOfPosteriors")
        logger.error(s"File with pattern ${dataType.posteriorDatafilePatternStr} not in directory $firstDirOfPosteriors")
        // throw new Exception(s"No file with posterior distribution of parameters found!")
      }
    }
    println(s"sampleSubDir = $sampleSubDir")
    val fileNameOnlyWPosteriors = sampleSubDir.getName
    println(s"sampleSubDir = $sampleSubDir")
    val (nD, distribution) = getInfoFromFileNameOfPosteriors(fileNameOnlyWPosteriors)


    println(s"nD = $nD")
    println(s"distribution = $distribution")
    println(s"")

    //    val dirsOfPosteriors = getDirOfPosteriors(sampleSubDir.getAbsolutePath, """^s-\d{1,5}K-\d{6}-\w{4}-\w{2}""")
    // duplicated iteration in posterior folders
    //    val dt2 = allDirsOfPosteriors.foldLeft(dataTable)((dtI, f) => collectResultsEstimatesLevel(dtI, f, coverage, numOfIterations, otuInfo, nLDist90pcStatInfo, dataType, dataSetName, tadInfo))
    //
    val dt3 = collectResultsEstimatesLevel(dataTable, sampleSubDir, coverage, numOfIterations, otuInfo, nLDist90pcStatInfo, dataType, dataSetName, tadInfo)
    println("")
    dt3
  }


  def collectResultsDataSample(sampleSubDir: File, dataTable: DataTable, nLDist90pcStatInfo: ResultsCollector.StatisticsInformation, dataType: DataInfoAndPatterns): _root_.com.github.martincooper.datatable.DataTable = {
    //    val dirWithPostPattern: String = """^s-\d{1,5}K-\d{6}-\w{4}-\w{2}"""
    val dirWithPostPattern: String = dataType.posteriorDirPattern

    val tadInfo0: Option[TadDataInfoFromJSON] = readTADInfoJSONInDir(sampleSubDir.getCanonicalPath)
    val tadInfo = tadInfo0 match {
      case Some(info) => info
      case None => TadDataInfoFromJSON("", "", 0, 0, 0d, 0, 0d, 0d, 0d, "", "", "", "")
    }
    val allDirsOfPosteriors = getAllDirsWithPosteriors(sampleSubDir.getAbsolutePath, dirWithPostPattern)
    val firstDirOfPosteriors0 = allDirsOfPosteriors.lift(0) //first item in List

    val otuInfo: OtuInfo = getOtuInfo(sampleSubDir.getAbsolutePath, FileDirRegExPattern.REAL_DATA_S.tadFileNamePattern)
    val dataSetName = sampleSubDir.getName //
    println(s"Number of subdirs ${allDirsOfPosteriors.size}")
    val dtR = allDirsOfPosteriors.foldLeft(dataTable)((dtI: DataTable, sampleSubDir) => extractDataForPosteriorFolder(sampleSubDir, dtI, nLDist90pcStatInfo, tadInfo, allDirsOfPosteriors, firstDirOfPosteriors0, otuInfo, dataSetName, dataType))
    dtR

  }

  def collectAndWriteResults(mainDir: String, dataType: DataInfoAndPatterns) = {
    //    Initialize statistics info
    val nLDist90pc_statInfo = ResultsCollector.StatisticsInformation(0, 0, 0, 0, 0)
    // output File Path
    val outputFile = mainDir + File.separator + s"aggregatedResults_C_Code_" + MCMC.DATE_STR + ".csv"
    // Directories of datasets
    val sampleDirs: List[File] = getSubdirsWPattern(mainDir,""".*""")
    sampleDirs.sortWith((f1, f2) => f1.getName.compareTo(f2.getName) < 0)
    // output file for aggregated results
    val oFile = new File(outputFile)
    // Data Frame Schema
    val dt = getDataTableSchema()
    val dtR = sampleDirs.foldLeft(dt.get)((dtI: DataTable, f) => collectResultsDataSample(f, dtI, nLDist90pc_statInfo, dataType))

    DataTableReadWrite.writeCsv(dtR, oFile)

    // graphCollectedResults(oFile)
    println(s"output to file = ${oFile}")
    println("d")

  }


}

case class DataInfoAndPatterns(dataType: String, posteriorDatafilePatternStr: String, sampleSizeFilePattern: String, posteriorDirPattern: String)
