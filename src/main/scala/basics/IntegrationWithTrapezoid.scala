/**
 * @author peter
 */
package basics
import breeze.integrate
// import org.apache.commons.math3.analysis.integration.TrapezoidIntegrator

case class IntegrationWithTrapezoid() extends Integral { 
  def calculate(f:Double=>Double, a:Double,b:Double, nSegments: Integer):Double = {    
    integrate.trapezoid(f, a, b, nSegments)
  }
}

object IntegrateTest00 extends App {
 val myInteg00 =  IntegrationWithTrapezoid()
 myInteg00.calculate(x=>x,1.0,5.0,10)
 
}