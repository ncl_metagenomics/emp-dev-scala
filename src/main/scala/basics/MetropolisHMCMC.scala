package basics

import utils.DateTimeUtils
import utils.DateTimeUtils._
import java.util.Locale
import java.util.concurrent.ThreadLocalRandom

import com.github.tototoshi.csv.{CSVWriter, defaultCSVFormat}
import basics.MetropolisHMCMCObj.{ParamsPS, ParamsQuince}
import breeze.linalg.{DenseMatrix, DenseVector, diag}
import breeze.numerics.sqrt
import breeze.stats.distributions.{Gaussian, Poisson}
import org.joda.time.DateTime
import org.slf4j.LoggerFactory

import scala.annotation.tailrec
import scala.collection.mutable.ListBuffer

//import scala.collection.parallel.ParIterableLike.Foreach

trait UsefullFunctions {
  /**
    * Returns total number of observed species/taxa from abundance data in form of TAD
    */
  def getD(data: Array[(Int, Int)]): Int =
    data.map(x => x._2).par.sum


  /**
    * Total number of observed individuals from abundance data in form of TAD
    *
    * @param data
    * @return
    */
  def getL(data: Array[(Int, Int)]): Int = data.map((x) => x._1 * x._2).par.sum


  /**
    * Reads csv file of abundance with header on the first line, where abundance is first column and number of taxa is second column
    *
    * @param fileName
    * @return
    */
  def readDataASCSVTAD(fileName: String): Array[(Int, Int)] = {
    val heads = """(\S+)\s*,\s*(\S+)""".r
    val nums = """(\d+),(\d+)""".r
    val lines = io.Source.fromFile(fileName).getLines
    val heads(h1, h2) = lines.next()
    val tuples = lines collect {
      case nums(abundance, nTaxa) => (abundance.toInt, nTaxa.toInt)
    }
    tuples.toArray

  }


  /**
    * Reads input file in the form of space separated Quince's format:
    * Row 1:  ${number of lines}
    * Other rows :${abundance} ${number of species}
    *
    * @param fileName
    * @return
    */
  def readData(fileName: String): Array[(Int, Int)] = {
    val nums = """(\d+) (\d+)""".r
    val heads = """(\d+)""".r
    val tuples = io.Source.fromFile(fileName).getLines collect {
      case nums(label, num) => (label.toInt -> num.toInt)
      case heads(num) => (num.toInt -> num.toInt)
    }
    tuples.toArray.drop(1)
  }

}


object MetropolisHMCMC extends UsefullFunctions /*extends MetropolisHMCMC */ {
  val g = new Gaussian(0, 1) // Gaussian standard normal distribution
}


case class MetropolisHMCMC(thinningInterval: Int = 10, sigmaParamA: Double = 0.1, val sigmaParamB: Double = 0.2, val sigmaS: Int = 400) {
  //  val g = breeze.stats.distributions.Gaussian(0, 1)
  val ThinningInterval = thinningInterval
  val SigmaParamA = sigmaParamA
  val SigmaParamB = sigmaParamB
  val SigmaS = sigmaS
  val SLICE2 = 1000
  val logger = LoggerFactory.getLogger(this.getClass)
  val ll = new LogLikelihoodPoissonLogNormal()

  // utils.ReadingExperimentParameters.InputParametersMCMCAndSamplingEffort


  /**
    * Dispatches calls  for various versions of function with MH MCMC code to fit different distrigutions. Intializes and closes output.
    *
    * @param mu0
    * @param variance
    * @param _s0
    * @param data
    * @param _nL
    * @param nIter
    * @param estimateWhat
    * @param version - version distribution to fit
    * @param outfileName
    * @param distribution
    * @param choleskyLowerTriangleOfVarCovarMatrx
    * @return
    */
  def calculate(mu0: Double = 1.0, variance: Double = 1.0, _s0: Double, data: Array[(Int, Int)], _nL: Option[Int] = None, nIter: Int = 250, estimateWhat: String, version: String, outfileName: String, distribution: String, choleskyLowerTriangleOfVarCovarMatrx: Option[DenseMatrix[Double]]): (ParamsPS, Double) = {
    // getting value of sample size
    val nL: Int = _nL match {
      case Some(a) => _nL.get
      case None => MetropolisHMCMC.getD(data.drop(0))
    }

    if (variance < 0) {
      throw new Exception(s"")
    }
    // starting initial value (number of observed species) has to be larger (or equal) than observed value
    val s0 = if (_s0 < nL) nL else _s0
    val nLL = nLogLikelihood(mu0, variance, s0, data, nL) // Needs to be tested

    // Propose new Parameters
    val paramsOld = ParamsPS(mu0, variance, s0.floor.toInt, 0)
    val paramsVariance = ParamsVariance(SigmaParamA, SigmaParamB, SigmaS)
    val seed = 127L
    //    try
    //      ThreadLocalRandom.current().setSeed(13)
    //    catch {
    //      case e: Exception => e.printStackTrace
    //    }
    //val nThreads = 12
    val writer = CSVWriter.open(outfileName)
    val header = List("nIter", "mu", "variance", "S", "nLL", "acceptance", "n_accepted")
    writer.writeRow(header)
    writer.writeRow(List(0, paramsOld.mu, variance, paramsOld.S, nLL, -1))
    logger.debug(0 + "," + paramsOld.mu + "," + paramsOld.variance + "," + paramsOld.S + "," + nLL + "," + 0.0d)
    //all, mu, sigma, S
    //Run MCMC loop of chosen type
    println("\n*** Posterior parameter estimation ***")
    val res: (ParamsPS, Double) = version match {
      case "s" => {
        distribution match {
          case "lognormal" => estimatorLogNormalPS(paramsOld, nLL, paramsVariance, data, nIter, nIter, writer, nL, estimateWhat, DateTime.now(), choleskyLowerTriangleOfVarCovarMatrx)
          case "inverseG" => estimatorInverseGaussianPS(paramsOld, nLL, paramsVariance, data, nIter, nIter, writer, nL, estimateWhat)
          case "sichel" => estimatorSichelPS(paramsOld, nLL, paramsVariance, data, nIter, nIter, writer, nL, estimateWhat)
          case "logt" => estimatorInverseGaussianPS(paramsOld, nLL, paramsVariance, data, nIter, nIter, writer, nL, estimateWhat)
          case _ => {
            println(s"$estimateWhat is not correct value for estimateWhat ");
            System.exit(1);
            (ParamsPS(0.0, 0.0, 0, 0), 0.0)
          }
        }
      }
      case _ => {
        println(s"$estimateWhat is not correct value for estimateWhat ");
        System.exit(1);
        (ParamsPS(0.0, 0.0, 0, 0), 0.0)
      }
    }
    writer.close()
    res
  }

  def haveAllGoodValues(parametersNew: ParamsPS, nL: Int): (Boolean) = {
    val boolFlag = ListBuffer.empty[Boolean]
    boolFlag += true //mu
    boolFlag += true //sigma
    if (parametersNew.S < 1 || parametersNew.S < nL || parametersNew.variance <= 0) {
      boolFlag += false
    } else {
      boolFlag += true
    }

    // var oneFails = boolFlag.exists { x => x == false }
    (boolFlag.forall { x => x })
  }

  def haveAllGoodValuesQuince(parametersNew: ParamsQuince, nL: Int): (Boolean) = {
    val boolFlag = ListBuffer.empty[Boolean]
    boolFlag += true //mu
    boolFlag += true //sigma
    if (parametersNew.S < 1 || parametersNew.S < nL) {
      boolFlag += false
    } else
      boolFlag += true

    var oneFails = boolFlag.exists { x => x == false }
    (boolFlag.forall { x => x })
  }

  /**
    * Estimation of parameter by storing mu where mu = E[ln(x)], and recalculating m = E[x]  when needed for calculation of proposed new values
    */
  @tailrec
  final def estimatorLogNormalPS(parameters: ParamsPS, nLL: Double, paramsVariance: ParamsVariance, data: Array[(Int, Int)],
                                 index: Int, nIter: Int, writer: CSVWriter, nL: Int, estimateWhat: String, runningParams: utils.ReadingExperimentParameters.InputParametersMCMCAndSamplingEffort): (ParamsPS, Double) = {
    if (index == 0)
      (parameters, nLL)
    else {
      val parametersNew: ParamsPS = estimateWhat match {
        case "all" => getNormalDProposalWithCorrelationMatrix(parameters, paramsVariance, None)
        case "mu" => getProposalMuOnly(parameters, paramsVariance)
        case "sigma" => getProposalSigmaOnly(parameters, paramsVariance)
        case "S" => getProposalTotalSOnly(parameters, paramsVariance, nL)
        case _ => {
          println(s"$estimateWhat is not correct value for estimateWhat ");
          System.exit(1);
          ParamsPS(0.0, 0.0, 0, 0)
        }
      }
      val goodValues: (Boolean) = haveAllGoodValues(parametersNew, nL)
      //----------------------
      val (dNLLDash, lnAcceptance, lnRu) = if (goodValues) {
        val vDash = parametersNew.variance
        val dNLLDash = nLogLikelihood(parametersNew.mu, parametersNew.variance, parametersNew.S, data, nL)
        var lnAcceptance = (nLL - dNLLDash)
        val lnRu = Math.log(ThreadLocalRandom.current().nextDouble())
        (dNLLDash, lnAcceptance, lnRu)
      } else {
        (Double.NaN, Double.NaN, Double.NaN)
      }
      val iterIdx = nIter - index + 1
      //Accepting/Rejecting values
      val newValues = if (goodValues && lnRu <= lnAcceptance) {
        (parametersNew, dNLLDash)
      } else
        (parameters, nLL)
      writeOutputs(iterIdx, writer, newValues, lnAcceptance, runningParams.thinningInterval)
      // Call recusively for nIter iterations
      estimatorLogNormalPS(newValues._1, newValues._2, paramsVariance, data, index - 1, nIter, writer, nL, estimateWhat, runningParams)
    }

  }

  @tailrec
  final def estimatorLogNormalPS(parameters: ParamsPS, nLL: Double, paramsVariance: ParamsVariance, data: Array[(Int, Int)],
                                 index: Int, nIter: Int, writer: CSVWriter, nL: Int, estimateWhat: String): (ParamsPS, Double) = {
    if (index == 0)
      (parameters, nLL)
    else {
      val parametersNew: ParamsPS = estimateWhat match {
        case "all" => getNormalDProposalWithCorrelationMatrix(parameters, paramsVariance, None)
        case "mu" => getProposalMuOnly(parameters, paramsVariance)
        case "sigma" => getProposalSigmaOnly(parameters, paramsVariance)
        case "S" => getProposalTotalSOnly(parameters, paramsVariance, nL)
        case _ => {
          println(s"$estimateWhat is not correct value for estimateWhat ");
          System.exit(1);
          ParamsPS(0.0, 0.0, 0, 0)
        }
      }
      val goodValues: (Boolean) = haveAllGoodValues(parametersNew, nL)
      //----------------------
      val (dNLLDash, lnAcceptance, lnRu) = if (goodValues) {
        val vDash = parametersNew.variance
        val dNLLDash = nLogLikelihood(parametersNew.mu, parametersNew.variance, parametersNew.S, data, nL)
        var lnAcceptance = (nLL - dNLLDash)
        val lnRu = Math.log(ThreadLocalRandom.current().nextDouble())
        (dNLLDash, lnAcceptance, lnRu)
      } else {
        (Double.NaN, Double.NaN, Double.NaN)
      }
      val iterIdx = nIter - index + 1
      if (iterIdx % 20 == 1) println(s"${10000 * iterIdx / nIter}/10,000 completed")
      //Accepting/Rejecting values
      val newValues = if (goodValues && lnRu <= lnAcceptance) {
        (parametersNew, dNLLDash)
      } else
        (parameters, nLL)
      writeOutputs(iterIdx, writer, newValues, lnAcceptance)
      // Call recusively for nIter iterations
      estimatorLogNormalPS(newValues._1, newValues._2, paramsVariance, data, index - 1, nIter, writer, nL, estimateWhat)
    }

  }

  private val runtime = Runtime.getRuntime()

  import runtime.{totalMemory, freeMemory, maxMemory}

  val locale = new java.util.Locale("us", "US")

  val formatterInteger = java.text.NumberFormat.getIntegerInstance(locale)

  @tailrec
  final def estimatorLogNormalPS(parameters: ParamsPS, nLL: Double, paramsVariance: ParamsVariance, data: Array[(Int, Int)], toDoIndex: Int, nIter: Int, writer: CSVWriter, nL: Int, estimateWhat: String, startDate: DateTime, choleskyLowerTriangleOfVarCovarMatrix: Option[DenseMatrix[Double]]): (ParamsPS, Double) = {

    if (toDoIndex == 0)
      (parameters, nLL)
    else {
      val parametersNew: ParamsPS = estimateWhat match {
        //        case "all" => getNormalDProposalWithCorrelationMatrix(parameters, paramsVariance, choleskyLowerTriangleOfCorrelationMatrix)
        case "all" => getNormalDProposalWithVarCovarMatrix(parameters, paramsVariance, choleskyLowerTriangleOfVarCovarMatrix)
        case "mu" => getProposalMuOnly(parameters, paramsVariance)
        case "sigma" => getProposalSigmaOnly(parameters, paramsVariance)
        case "S" => getProposalTotalSOnly(parameters, paramsVariance, nL)
        case _ => {
          println(s"$estimateWhat is not correct value for estimateWhat ");
          System.exit(1);
          ParamsPS(0.0, 0.0, 0, 0)
        }
      }
      val goodValues: (Boolean) = haveAllGoodValues(parametersNew, nL)
      //----------------------
      val (dNLLDash, lnAcceptance, lnRu) = if (goodValues) {
        val vDash = parametersNew.variance
        val dNLLDash = nLogLikelihood(parametersNew.mu, parametersNew.variance, parametersNew.S, data, nL)
        var lnAcceptance = (nLL - dNLLDash)
        val lnRu = Math.log(ThreadLocalRandom.current().nextDouble())
        (dNLLDash, lnAcceptance, lnRu)
      } else {
        (Double.NaN, Double.NaN, Double.NaN)
      }
      val doneIdx = nIter - toDoIndex + 1
      if (doneIdx % 100 == 1) {
        val now = DateTime.now()
        val timeDiff = now.getMillis - startDate.getMillis
        val estsMillis = toDoIndex * timeDiff / doneIdx
        val estimated = now.plusMillis(estsMillis.toInt)
        val estimatedTimeStr = estimated.toString("yyyy-MM-dd__HH:mm:ss")
        println(f"${100 * doneIdx.toDouble / nIter}%1.2f %% of $nIter completed at ${DateTime.now().toString("yyyy-MM-dd__HH:mm:ss")} | ETC ${estimatedTimeStr} | total allocated memory = ${formatterInteger.format(totalMemory / 1e+6)} Mb | max memory = ${formatterInteger.format(maxMemory / 1e+6)} Mbytes| | remaining memory = ${"%1.3f".format((maxMemory - totalMemory) / 1e+6)} Mb | allocated but free memory = ${formatterInteger.format(freeMemory / 1e+6)} Mb |Total free memory = ${formatterInteger.format((maxMemory - totalMemory() + freeMemory()) / 1e+6)}")
        if (doneIdx <= 0) {
          if (doneIdx < 0) {
            println(s"doneIdx<0 with value $doneIdx")
          }
        }
      }
      //Accepting/Rejecting values
      val newValues = if (goodValues && lnRu <= lnAcceptance) {
        (parametersNew, dNLLDash)
      } else
        (parameters, nLL)
      writeOutputs(doneIdx, writer, newValues, lnAcceptance)
      // Call recusively for nIter iterations
      estimatorLogNormalPS(newValues._1, newValues._2, paramsVariance, data, toDoIndex - 1, nIter, writer, nL, estimateWhat, startDate, choleskyLowerTriangleOfVarCovarMatrix)
    }

  }


  /**
    * Estimation of parameter by storing mu where mu = E[ln(x)], and recalculating m = E[x]  when needed for calculation of proposed new values
    */
  @tailrec
  final def estimatorInverseGaussianPS(parameters: ParamsPS, nLL: Double, paramsVariance: ParamsVariance, data: Array[(Int, Int)],index: Int, nIter: Int, writer: CSVWriter, nL: Int, estimateWhat: String): (ParamsPS, Double) = {
    if (index == 0)
      (parameters, nLL)
    else {
      val parametersNew: ParamsPS = estimateWhat match {
        case "all" => getNormalDProposalWithCorrelationMatrix(parameters, paramsVariance, None)
        case "mu" => getProposalMuOnly(parameters, paramsVariance)
        case "sigma" => getProposalSigmaOnly(parameters, paramsVariance)
        case "S" => getProposalTotalSOnly(parameters, paramsVariance, nL)
        case _ => {
          println(s"$estimateWhat is not correct value for estimateWhat ");
          System.exit(1);
          ParamsPS(0.0, 0.0, 0, 0)
        }
      }
      val goodValues: (Boolean) = haveAllGoodValues(parametersNew, nL)
      //----------------------
      val (dNLLDash, lnAcceptance, lnRu) = if (goodValues) {
        val vDash = parametersNew.variance
        val dNLLDash = nLogLikelihood(parametersNew.mu, parametersNew.variance, parametersNew.S, data, nL)
        var lnAcceptance = (nLL - dNLLDash)
        val lnRu = Math.log(ThreadLocalRandom.current().nextDouble())
        (dNLLDash, lnAcceptance, lnRu)
      } else {
        (Double.NaN, Double.NaN, Double.NaN)
      }
      val iterIdx = nIter - index + 1
      //Accepting/Rejecting values
      val newValues = if (goodValues && lnRu <= lnAcceptance) {
        (parametersNew, dNLLDash)
      } else
        (parameters, nLL)
      writeOutputs(iterIdx, writer, newValues, lnAcceptance)
      // Call recusively for nIter iterations
      estimatorInverseGaussianPS(newValues._1, newValues._2, paramsVariance, data, index - 1, nIter, writer, nL, estimateWhat)
    }

  }


  /**
    * Estimation of parameter by storing mu where mu = E[ln(x)], and recalculating m = E[x]  when needed for calculation of proposed new values
    */
  @tailrec
  final def estimatorSichelPS(parameters: ParamsPS, nLL: Double, paramsVariance: ParamsVariance, data: Array[(Int, Int)],
                              index: Int, nIter: Int, writer: CSVWriter, nL: Int, estimateWhat: String): (ParamsPS, Double) = {
    if (index == 0)
      (parameters, nLL)
    else {
      val parametersNew: ParamsPS = estimateWhat match {
        case "all" => getNormalDProposalWithCorrelationMatrix(parameters, paramsVariance, None)
        case "mu" => getProposalMuOnly(parameters, paramsVariance)
        case "sigma" => getProposalSigmaOnly(parameters, paramsVariance)
        case "S" => getProposalTotalSOnly(parameters, paramsVariance, nL)
        case _ => {
          println(s"$estimateWhat is not correct value for estimateWhat ");
          System.exit(1);
          ParamsPS(0.0, 0.0, 0, 0)
        }
      }
      val goodValues: (Boolean) = haveAllGoodValues(parametersNew, nL)
      //----------------------
      val (dNLLDash, lnAcceptance, lnRu) = if (goodValues) {
        val vDash = parametersNew.variance
        val dNLLDash = nLogLikelihood(parametersNew.mu, parametersNew.variance, parametersNew.S, data, nL)
        var lnAcceptance = (nLL - dNLLDash)
        val lnRu = Math.log(ThreadLocalRandom.current().nextDouble())
        (dNLLDash, lnAcceptance, lnRu)
      } else {
        (Double.NaN, Double.NaN, Double.NaN)
      }
      val iterIdx = nIter - index + 1
      //Accepting/Rejecting values
      val newValues = if (goodValues && lnRu <= lnAcceptance) {
        (parametersNew, dNLLDash)
      } else
        (parameters, nLL)
      writeOutputs(iterIdx, writer, newValues, lnAcceptance)
      // Call recusively for nIter iterations
      estimatorSichelPS(newValues._1, newValues._2, paramsVariance, data, index - 1, nIter, writer, nL, estimateWhat)
    }

  }

  def writeOutputs(iterIdx: Int, writer: com.github.tototoshi.csv.CSVWriter, newValues: (MetropolisHMCMCObj.ParamsPS, Double), lnAcceptance: Double) = {
    if (iterIdx % ThinningInterval == 0) {
      writer.writeRow(List(iterIdx, newValues._1.mu, newValues._1.variance, newValues._1.S, newValues._2, lnAcceptance, newValues._1.nAccepted))
    }
    if (iterIdx % (ThinningInterval * 100) == 0) {
      writer.flush()
    }
    if (iterIdx % SLICE2 == 0) {
      logger.debug(iterIdx + "," + newValues._1.mu + "," + newValues._1.variance + "," + newValues._1.S + "," + newValues._2, lnAcceptance)
    }
  }

  def writeOutputs(iterIdx: Int, writer: com.github.tototoshi.csv.CSVWriter, newValues: (MetropolisHMCMCObj.ParamsPS, Double), lnAcceptance: Double, thinningInterval: Int) = {
    if (iterIdx % thinningInterval == 0) {
      writer.writeRow(List(iterIdx, newValues._1.mu, newValues._1.variance, newValues._1.S, newValues._2, lnAcceptance, newValues._1.nAccepted))
    }
    if (iterIdx % (thinningInterval * 100) == 0) {
      writer.flush()
    }
    if (iterIdx % (Math.min(100 * thinningInterval, 1000)) == 0) {
      logger.debug(iterIdx + "," + newValues._1.mu + "," + newValues._1.variance + "," + newValues._1.S + "," + newValues._2, lnAcceptance)
    }
  }

  /**
    * Estimation of parameter by storing ln(m) where m = E[x], and recalculating mu = E[ln(x)] when needed for calculation of nLL (negative loglikelihood)
    */
  @tailrec
  final def estimatorQuince(parameters: MetropolisHMCMCObj.ParamsQuince, nLL: Double, paramsVariance: ParamsVariance, data: Array[(Int, Int)],
                            index: Int, nIter: Int, writer: CSVWriter, nL: Int, estimateWhat: String): (ParamsQuince, Double) = {
    if (index == 0)
      (parameters, nLL)
    else {
      val parametersNew: ParamsQuince = estimateWhat match {
        case "all" => getProposalQuince(parameters, paramsVariance)
        case "mu" => getProposalMuOnlyQuince(parameters, paramsVariance)
        case "sigma" => getProposalSigmaOnlyQuince(parameters, paramsVariance)
        case "S" => getProposalTotalSOnlyQuince(parameters, paramsVariance, nL)
        case _ => {
          println(s"$estimateWhat is not correct value for estimateWhat ");
          System.exit(1);
          ParamsQuince(0.0, 0.0, 0, 0)
        }
      }
      val goodValues: (Boolean) = haveAllGoodValuesQuince(parametersNew, nL)
      //----------------------
      val (dNLLDash, lnAcceptance, lnRu) = if (goodValues) {
        val vDash = parametersNew.variance
        val mu = parametersNew.lnm - 0.5 * vDash
        val dNLLDash = nLogLikelihood(mu, parametersNew.variance, parametersNew.S, data, nL)
        var lnAcceptance = (nLL - dNLLDash)
        val lnRu = Math.log(ThreadLocalRandom.current().nextDouble())
        (dNLLDash, lnAcceptance, lnRu)
      } else {
        (Double.NaN, Double.NaN, Double.NaN)
      }
      val iterIdx = nIter - index + 1
      //Accepting/Rejecting new values
      val newValues = if (goodValues && lnRu < lnAcceptance) {
        (parametersNew, dNLLDash)
      } else
        (parameters, nLL)

      if (iterIdx % ThinningInterval == 0) {
        val mu = newValues._1.lnm - 0.5 * newValues._1.variance
        writer.writeRow(List(iterIdx, mu, newValues._1.variance, newValues._1.S, newValues._2, lnAcceptance, newValues._1.nAccepted))
      }
      if (iterIdx % SLICE2 == 0) {
        val mu = newValues._1.lnm - 0.5 * newValues._1.variance
        logger.debug(iterIdx + "," + mu + "," + newValues._1.variance + "," + newValues._1.S + "," + newValues._2, lnAcceptance)
      }
      estimatorQuince(newValues._1, newValues._2, paramsVariance, data, index - 1, nIter, writer, nL, estimateWhat)
    }

  }

  /**
    *
    */

  def getProposalLnMu(mu: Double, sigmaOfmu: Double): Double = {
    val rndGauss = ThreadLocalRandom.current().nextGaussian()
    rndGauss * sigmaOfmu + mu
  }

  /**
    *
    */

  def getProposalSigma(variance: Double, sigmaOfSigma: Double): Double = {
    val rndGauss = ThreadLocalRandom.current().nextGaussian()
    val newVariance = rndGauss * sigmaOfSigma + (variance)
    newVariance
  }

  //getProposalSigma

  /**
    * Propose value of total number of species
    */
  def getSAdditiveProposal(nTotalSpecies: Int, sigma: Int): Int = {
    val rndGauss = ThreadLocalRandom.current().nextGaussian()
    val tmp: Double = rndGauss * sigma + nTotalSpecies.toDouble
    tmp.round.toInt
  }

  /**
    * Propose value of other parameters:mu-location, sigma-shape
    */
  def getNormalDProposalWithCorrelationMatrix(paramsOld: ParamsPS, paramsVariance: ParamsVariance, choleskyLowerTriangleOfCorrelationMatrix: Option[DenseMatrix[Double]]): ParamsPS = {
    // Sampling from Gaussian distribution
    choleskyLowerTriangleOfCorrelationMatrix match {
      case Some(r: DenseMatrix[Double]) => getMultivariateNormalDProposalFromCorrelationMatrix(paramsOld, paramsVariance, r)
      case None => getIndepentNormalDProposal(paramsOld, paramsVariance)
    }
  }

  /**
    * Propose value of other parameters:mu-location, sigma-shape
    */
  def getNormalDProposalWithVarCovarMatrix(paramsOld: ParamsPS, paramsVariance: ParamsVariance, choleskyLowerTriangleOfCorrelationMatrix: Option[DenseMatrix[Double]]): ParamsPS = {
    // Sampling from Gaussian distribution
    choleskyLowerTriangleOfCorrelationMatrix match {
      case Some(r: DenseMatrix[Double]) => getMultivariateNormalDProposalFromVarianceCovarianceMatrix(paramsOld, r)
      case None => getIndepentNormalDProposal(paramsOld, paramsVariance)
    }
  }

  /**
    * Returns multivariate normal proposal for estimated parameters
    *
    * @param paramsOld
    * @param paramsVariance
    * @param choleskyLowerTriangleOfCorrelationMatrix
    * @return
    */
  def getMultivariateNormalDProposalFromCorrelationMatrix(paramsOld: ParamsPS, paramsVariance: ParamsVariance, choleskyLowerTriangleOfCorrelationMatrix: DenseMatrix[Double]): ParamsPS = {
    // Sampling from Gaussian distribution
    // TODO: 170228 replace code for getMultivariateNormalDProposal
    //val z = ThreadLocalRandom.current().nextGaussian()
    val zs0 = MetropolisHMCMC.g.sample(3).toArray
    val stdMatrix = diag(DenseVector(sqrt(paramsVariance.sigmaMu), sqrt(paramsVariance.sigmaVar), sqrt(paramsVariance.sigmaS)))
    val covMatrix = stdMatrix * choleskyLowerTriangleOfCorrelationMatrix * stdMatrix
    val zs: DenseMatrix[Double] = (DenseVector[Double](zs0: _*)).asDenseMatrix.t
    val lnM0 = paramsOld.mu + 0.5 * paramsOld.variance
    val oldValues: DenseMatrix[Double] = DenseVector[Double](lnM0, paramsOld.variance, paramsOld.S).asDenseMatrix.t
    val b = covMatrix * zs
    val newValues: DenseMatrix[Double] = oldValues + b

    // *** REWRITE THIS
    val mu = newValues.valueAt(0) - 0.5 * newValues.valueAt(1) // mean of log(X)~N(mu,variance)

    // *** --- END ---- ***
    ParamsPS(mu, newValues.valueAt(1), newValues.valueAt(2).toInt, paramsOld.nAccepted + 1)
  }

  /**
    * Returns multivariate normal proposal for estimated parameters
    *
    * @param paramsOld
    * @param choleskyLowerTriangleOfCorrelationMatrix
    * @return
    */
  def getMultivariateNormalDProposalFromVarianceCovarianceMatrix(paramsOld: ParamsPS, choleskyLowerTriangleOfCorrelationMatrix: DenseMatrix[Double]): ParamsPS = {
    // Sampling from Gaussian distribution
    // TODO: 170309 modify code for variance-covarian matrix - previously(correlation matrix)
    //val z = ThreadLocalRandom.current().nextGaussian()
    val zs0 = MetropolisHMCMC.g.sample(3).toArray
    //    val stdMatrix = diag(DenseVector(sqrt(paramsVariance.sigmaMu), sqrt(paramsVariance.sigmaVar), sqrt(paramsVariance.sigmaS)))
    //    val covMatrix = stdMatrix * choleskyLowerTriangleOfCorrelationMatrix * stdMatrix
    //    choleskyLowerTriangleOfCorrelationMatrix
    val zs: DenseMatrix[Double] = (DenseVector[Double](zs0: _*)).asDenseMatrix.t
    val lnM0 = paramsOld.mu + 0.5 * paramsOld.variance
    val oldValues: DenseMatrix[Double] = DenseVector[Double](lnM0, paramsOld.variance, paramsOld.S).asDenseMatrix.t
    val b = choleskyLowerTriangleOfCorrelationMatrix * zs
    val newValues: DenseMatrix[Double] = oldValues + b
    // *** REWRITE THIS
    val mu = newValues.valueAt(0) - 0.5 * newValues.valueAt(1) // mean of log(X)~N(mu,variance)
    // *** --- END ---- ***
    ParamsPS(mu, newValues.valueAt(1), newValues.valueAt(2).toInt, paramsOld.nAccepted + 1)
  }


  /**
    * Returns independent normal proposals for estimated parameters
    *
    * @param paramsOld
    * @param paramsVariance
    * @return
    */
  def getIndepentNormalDProposal(paramsOld: ParamsPS, paramsVariance: ParamsVariance): ParamsPS = {
    // Sampling from Gaussian distribution
    val lnM0 = paramsOld.mu + 0.5 * paramsOld.variance
    val lnM = getProposalLnMu(lnM0, paramsVariance.sigmaMu)
    val variance = getProposalSigma(paramsOld.variance, paramsVariance.sigmaVar)
    val mu = lnM - 0.5 * variance // mean of log(X)~N(mu,variance)

    val lS = getSAdditiveProposal(paramsOld.S.toInt, paramsVariance.sigmaS.toInt)
    ParamsPS(mu, variance, lS, paramsOld.nAccepted + 1)
  }

  /**
    * Propose value of other parameters:mu-location, sigma-shape
    */
  def getProposalQuince(paramsOld: ParamsQuince, paramsVariance: ParamsVariance): ParamsQuince = {
    // Sampling from Gaussian distribution
    val lnM0 = paramsOld.lnm
    val lnM = getProposalLnMu(lnM0, paramsVariance.sigmaMu)
    val variance = getProposalSigma(paramsOld.variance, paramsVariance.sigmaVar)
    val mu = lnM
    val lS = getSAdditiveProposal(paramsOld.S.toInt, paramsVariance.sigmaS.toInt)
    ParamsQuince(lnM, variance, lS, paramsOld.nAccepted + 1)
  }

  /**
    * Propose value of other parameters:mu-location, sigma-shape
    */
  def getProposalTotalSOnly(paramsOld: ParamsPS, paramsVariance: ParamsVariance, nL: Int): ParamsPS = {
    // Sampling from Gaussian distribution
    val lS = getSAdditiveProposal(paramsOld.S.toInt, paramsVariance.sigmaS.toInt)
    ParamsPS(paramsOld.mu, paramsOld.variance, lS, paramsOld.nAccepted + 1)
  }

  //getProposalTotalSOnly

  /**
    * Propose value of other parameters:mu-location, sigma-shape
    */
  def getProposalTotalSOnlyQuince(paramsOld: ParamsQuince, paramsVariance: ParamsVariance, nL: Int): ParamsQuince = {
    // Sampling from Gaussian distribution
    val lS = getSAdditiveProposal(paramsOld.S.toInt, paramsVariance.sigmaS.toInt)
    ParamsQuince(paramsOld.lnm, paramsOld.variance, lS, paramsOld.nAccepted + 1)
  }

  // getProposalTotalSOnlyQuince

  def getProposalSigmaOnly(paramsOld: ParamsPS, paramsVariance: ParamsVariance): ParamsPS = {
    // Sampling from Gaussian distribution for shape parameter sigma
    val variance = getProposalSigma(paramsOld.variance, paramsVariance.sigmaVar)
    ParamsPS(paramsOld.mu, variance, paramsOld.S.toInt, paramsOld.nAccepted + 1)
  }

  def getProposalSigmaOnlyQuince(paramsOld: ParamsQuince, paramsVariance: ParamsVariance): ParamsQuince = {
    // Sampling from Gaussian distribution for shape parameter sigma
    val variance = getProposalSigma(paramsOld.variance, paramsVariance.sigmaVar)
    ParamsQuince(paramsOld.lnm, variance, paramsOld.S.toInt, paramsOld.nAccepted + 1)
  }

  def getProposalMuOnly(paramsOld: ParamsPS, paramsVariance: ParamsVariance): ParamsPS = {
    // Sampling from Gaussian distribution
    val lnM0 = paramsOld.mu + 0.5 * paramsOld.variance
    val lnM = getProposalLnMu(lnM0, paramsVariance.sigmaMu)
    val variance = paramsOld.variance
    val mu = lnM - 0.5 * variance

    val lS = paramsOld.S.toInt
    ParamsPS(mu, variance, lS, paramsOld.nAccepted + 1)
  }

  def getProposalMuOnlyQuince(paramsOld: ParamsQuince, paramsVariance: ParamsVariance): ParamsQuince = {
    // Sampling from Gaussian distribution
    val lnM0 = paramsOld.lnm
    val lnM = getProposalLnMu(lnM0, paramsVariance.sigmaMu)
    val variance = paramsOld.variance

    val lS = paramsOld.S.toInt
    ParamsQuince(lnM, variance, lS, paramsOld.nAccepted + 1)
  }

  /**
    * Returns total number of observed individuals
    */
  def getNumOfIndividuals(data: Array[(Int, Int)]): Int =
    data.map(x => x._1 * x._2).par.sum


  def nLogLikelihood(mu: Double, variance: Double, s: Double, data: Array[(Int, Int)], nL: Int): Double = {
    def sumPartLL(x: (Int, Int)) = {
      // println("(" + x._1 + ", " + x._2 + ")")
      val a = ll.calculateWithRLikeScala(mu, variance, x._1) * x._2
      val b = -ll.lnFactorial(x._2)
      val res = a + b
      res
    }

    val a = {
      val a0 = s.toLong - nL
      if (a0 < 0.0) {
        println(s"D has negative value $a0")
        println("")
      }
      ll.logFactorial(BigInt(a0))
    }
    val b = ll.logFactorial(BigInt(s.toLong))
    val log0 = ll.calculateWithRLikeScala(mu, variance, 0)
    val res00 = data.drop(1).map(x => sumPartLL(x)).sum
    var c = (s - nL) * log0
    val res = res00 + c - a + b
    -res
  }

  def readData1(fileName: String): (Array[Int], Array[Array[Int]]) = {
    val content = io.Source.fromFile(fileName).getLines.map(line => line.split(" "))
    val header = content.next.map(_.toInt)
    val res = content.take(2).map(_.map(_.toInt))
    (header, res.toArray)
  }

  def readData_A(fileName: String): (Array[Int], Array[Array[Int]]) = {
    val content = io.Source.fromFile(fileName).getLines.map(line => line.split(" "))
    val header = content.next.map(_.toInt)
    val res = content.take(2).map(_.map(_.toInt))
    (header, res.toArray)
  }

  def readData2(fileName: String): Array[Array[Int]] = {
    val intPairs = io.Source.fromFile(fileName).getLines.map { line =>
      line.split(" ").take(2).map(_.toInt)
    }
    intPairs.toArray
  }


  case class ParamsVariance(val sigmaMu: Double = 0.1, val sigmaVar: Double = 0.1, val sigmaS: Int = 100) {}

}


object MetropolisHMCMCObj {

  case class ParamsPS(val mu: Double, val variance: Double, val S: Int, val nAccepted: Int)

  case class ParamsQuince(val lnm: Double, val variance: Double, val S: Int, val nAccepted: Int)

  //  object ParamsPS{
  //    implicit def toParamsQuince(p:ParamsPS):ParamsQuince = {val lnM = p.mu + 0.5*p.variance ; ParamsQuince(lnM,p.variance,p.S,p.nAccepted)} 
  //  }
  //   object ParamsQuince{
  //    implicit def toParamsQuince(p:ParamsQuince):ParamsPS = {val  = p.lnm - 0.5*p.variance ; ParamsPS(mu,p.variance,p.S,p.nAccepted)}
  //  }
}
