package basics

import java.io.FileWriter

import basics.ResultsCollector.StatisticsInformation
import breeze.io.{CSVWriter => BreezeCSVWriter}
import com.github.tototoshi.csv.{CSVWriter, defaultCSVFormat}
import net.jcazevedo.moultingyaml._
import org.joda.time.DateTime
import utils.MyYamlParamsProtocolForRunner._

import scala.sys.process._
import scala.util.{Failure, Success, Try}

object ExperimentCCodeRunner {
  val dateStr = MCMC.DATE_STR

  def main(args: Array[String]): Unit = {
    val options = MCMC.getProgramOptions(args)
    val version = options('version).toString()
    val estimateWhat = options('mod).toString()
    val fileNameTAD: String = options('in).toString()
    val outDirectoryName0: String = options('out).toString()
    val outFileNamePosteriorParams = MCMC.modifyCCodeFileName(fileNameTAD, outDirectoryName0, version, estimateWhat)
    val outputYamlFileN = MCMC.constructYamlFileName4CCode(fileNameTAD, outDirectoryName0, version, estimateWhat)
//    val mu0: Double = options('mu0).toString().toDouble //1
    //var sigma0: Double = Math.sqrt(options('variance0).toString().toDouble) //1
//    val variance0 = options('variance0).toString().toDouble
    val nIter: Int = options('s).toString().toInt //cast2Int(options('s))
    //********************
    val mcmcMetro = new MetropolisHMCMC()
    val data = MetropolisHMCMC.readData(fileNameTAD)
    //****************
    val nL = data.map((x) => x._1 * x._2).par.sum
    val nD = MetropolisHMCMC.getD(data.drop(1))
//    val s01: Try[Int] = Try(options('s0).toString().toInt )//match { case Some(x: Int) => x } // nL * 2
//    val s0 = estimateWhat match {
//      case "all" => if (s01 < nD * 1.5) nD * 1.5 else s01
//      case "S" => if (s01 < (nD * 1.5)) nD * 1.5 else s01
//      case _ => s01
//    }
    val tCoverage: Double = (options('coverage)) match {
      case s: String => s.toDouble
      case d: Double => d
      case i: Int => i.toDouble
      case _ => throw new Error("coverage must be Double!"); Double.NaN
    }

    // MetroLogNormal -in """$inputPath""" -v -out """$outputPath"""  -s 1000000
    val currentPath = System.getProperty("user.dir")
    println(s"Current directory ${currentPath}")

    val dirContents = "ls".!!
    println(s"Dir content $dirContents")

    // var res = Seq("MetroLogNormal -in ",fileNameTAD," -v - out ",outDirectoryName0," -s ",nIter).!
    val myCommand0 = Seq("./MetroLogNormal", "-in", fileNameTAD.toString(), "-v", "-out", outFileNamePosteriorParams.toString(), "-s", nIter.toString())
    // var myCommand3 = Seq("sh","-s","MetroLogNormal","-in",fileNameTAD.toString(),"-v","-out",outDirectoryName0.toString(),"-s",nIter.toString())
    val de = myCommand0.mkString(" ")
    println(de)
    println(s"Current directory ${currentPath}")

//    println(s"Input Parameters: file = ${fileNameTAD}, mu0 = ${mu0},  variancer0 = ${variance0}, s0 = ${s0}, nIter = ${nIter}")
    // Running MCMC computation
    val ((lines2: Stream[String], buffer: StringBuffer), time) = MCMC.profile(cCodeCall(myCommand0))
    val linesAsStr = lines2.toString()
    println(s"C output $linesAsStr")
    println(s"C output ${buffer.toString}")
    //println(s"Estimate: mu = ${res._1.mu}, v = ${res._1.variance}, S = ${res._1.S}, NLL = ${res._2}")

    val outputFileName0 = s"${outFileNamePosteriorParams}_0.sample"
    println(s"Thread 0 output $outputFileName0")
    println(s"File written into $outFileNamePosteriorParams")
    println(s"Time  of posterior dist params calculation = ${time * 1E-9} sec")

    val outParams = utils.Params(Double.NaN, Double.NaN, Integer.MIN_VALUE, Integer.MIN_VALUE, Double.NaN, time * 1E-9, nIter,  PosteriorParamsStat(StatisticsInformation(0.0,0.0,0.0,0.0,0.0),StatisticsInformation(0.0,0.0,0.0,0.0,0.0),StatisticsInformation(0.0,0.0,0.0,0.0,0.0)))
    //constructYamlFileName
    val yamlP = outParams.toYaml
    val fw = new FileWriter(outputYamlFileN)
    fw.write(yamlP.prettyPrint)
    fw.close()
    // ********** sample size calculation ***********
    // **
    // -------------------------------------------------------------------------------------------
    val abundDataFileN = fileNameTAD
    val postParamFileN = outFileNamePosteriorParams
    val outputFileSamplingEffort = SamplingEffort.constructOutputFileNameAllOutputs(postParamFileN, tCoverage, SamplingEffortRunnerInSubdir.DEFAULT_ROOT_FINDING_METHOD)
    val outputFileSamplingEffort2 = SamplingEffort.constructOutputFileName4Correct(postParamFileN, tCoverage, "rootFindingMethod")
    val outputIncorrectFile: String = SamplingEffort.constructIncorrectOutputFileName(postParamFileN, tCoverage, "rootFindingMethod")

    val abuData = data

    val dropNumerator = 2
    val dropDenominator = 5
    val numRows2Skip = 0
    val (dataPosterior, head) = SamplingEffort.readDataPosteriorEstimates(outputFileName0, numRows2Skip, false)
    println(s"${dataPosterior(1).mu}, ${dataPosterior(1).variance}, ${dataPosterior(1).nCommunityTaxa}")

    val tollerance = 1.1 //relative error for difference of algorihms
    val tighterTollerance = 1.05
    val mu = -1.6
    val variance = 6.0
    //targeted coverage
    //    val tCoverage = 0.9 //targeted coverage
    val targetFractionUnseen = 1 - tCoverage
    val lnTargetP = Math.log(targetFractionUnseen)
    val lnPInit = SamplingEffort.logPLogNormal.calculateWithRLikeScala(mu, variance, 0)

    println(s"*** Estimate Sampling Effort *******")

    val nSampleEstW = SamplingEffort.getSampleSizeValueWithBrentPegasus(mu, variance, nL, tCoverage, tollerance, tighterTollerance,DateTime.now())

    println(s"Initial: nL = $nL, lnPInit = $lnPInit, c = ${1 - Math.exp(lnPInit)}")
    println(s"Target: lnTargetP = $lnTargetP, c = ${1 - Math.exp(lnTargetP)}")

    nSampleEstW match {
      case Success(nSampleEst) => {
        println(s"Solution: required sampl size = ${nSampleEst.nL}, muS=${nSampleEst.mu}")
        println(s"Solution required sample size as multiple of actual sample size = ${nSampleEst.nL}")
      }
      case Failure(e)=> println(e.getMessage)
    }



    implicit val fwCSV = SamplingEffort.OutputCSVWriters(CSVWriter.open(outputFileSamplingEffort2), CSVWriter.open(outputIncorrectFile))
    fwCSV.correctOutput.writeRow(List("mu", "variance", "nL_Recommended", "nL_Observed", "multipleOfActualSampleSize", "coverage", "mu_orig", "nL_orig"))
    fwCSV.incorrectOutput.writeRow(List("mu", "variance", "nL_Recommended", "nL_Observed", "multipleOfActualSampleSize", "coverage", "mu_orig", "nL_orig"))
    val (res2: Array[Try[SamplingEffort.SampleSizeEstimateDS]], time2: Long) = SamplingEffort.profile(SamplingEffort.getSampleSizeDistribution(dataPosterior, mu, variance, nL, tCoverage, tollerance, tighterTollerance))
    fwCSV.correctOutput.close()
    fwCSV.incorrectOutput.close()
    SamplingEffort.writeOutput(res2, outputFileSamplingEffort)

    val outParamsWithCoverage = utils.ParamsWithCoverage(outParams.muLastVal, outParams.varianceLastVal, outParams.nSLastVal, outParams.nAccepted, time2 * 1e-9, outParams.nIter, tCoverage, s"sampling effort for ${tCoverage * 100}% of species")
    val yamlPWCoverage = outParamsWithCoverage.toYaml
    println(yamlPWCoverage.prettyPrint)
    val outputYamlWCovrgFileN = MCMC.constructAnyFileName(fileNameTAD, outDirectoryName0, s"sample_size-c_", version, estimateWhat, "yaml")
    val fwYamlWCoverage = new FileWriter(outputYamlWCovrgFileN)
    fwYamlWCoverage.write(yamlPWCoverage.prettyPrint)
    fwYamlWCoverage.close()


    println(s"Output written to $outputFileSamplingEffort")
    println(s"Time  of posterior dist params calculation = ${time * 1E-9} sec")
    println(s"Time of sampling effort calculation = ${time2 * 1E-9} seconds")
    println("Finished estimating of sampling effort")
  }

  def cCodeCall(myCommand: Seq[String]): (Stream[String], StringBuffer) = {
    println(myCommand)
    val buffer = new StringBuffer()
    //val cmd = Seq(myCommand)
    //  val lines = cmd lines_! ProcessLogger(buffer append _)
    val lines2 = myCommand lineStream_! ProcessLogger(buffer append _)
    (lines2, buffer)
  }

  def convertStreamToString(is: java.io.InputStream): String = {
    val s: java.util.Scanner = new java.util.Scanner(is).useDelimiter("\\A")
    if (s.hasNext()) s.next() else ""
  }

}
