package basics

import java.lang.Math.{PI, log, pow}

import ch.qos.logback.classic.LoggerContext
import ch.qos.logback.core.util.StatusPrinter
import com.typesafe.scalalogging._
import org.apache.commons.math3.analysis.differentiation._
import org.apache.commons.math3.analysis.solvers._
import org.apache.commons.math3.special.Gamma
import org.apache.commons.math3.util.CombinatoricsUtils._
import org.slf4j.LoggerFactory
import utils.sampling.{Poilog, PoilogRScala}
import scala.annotation.tailrec

class LogLikelihoodPoissonLogNormal extends LogLikelihood with LazyLogging {
  //val loggerOPTBrackets = LoggerFactory.getLogger("OPT_BRACKETS")
  val loggerOPTBrackets = Logger("OPT_BRACKETS")

  //val lc: LoggerContext = LoggerFactory.getILoggerFactory().asInstanceOf[LoggerContext];
  //  // print logback's internal status
  //StatusPrinter.print(lc);

  val tighterAbsoluteAccuracy = 1E-5
  //Math.log(1.05)
  val basicAbsoluteAccuracy = 1E-4
  //Math.log(1.1)
  val pegasusAccuracy = 1E-4
  val nonBracketing: UnivariateSolver = new BrentSolver(basicAbsoluteAccuracy)
  val pegassusSolver = new PegasusSolver(tighterAbsoluteAccuracy)
  val solver = new NewtonRaphsonSolver(pegasusAccuracy)

  /**
    * Calculates without multiplicative constant
    */
  def calculateModfiedChris(mu: Double, variance: Double, n: Int): Double = if (n < 100) calculateWithIntegral(mu, variance, n) else calculateWithApproximation(mu, variance, n)

  /**
    * Approximation as used by Quince's Code
    * to calculate lnLikelihood
    */
  def calculateWithApproximation(mu: Double, varianceLnX: Double, n: Int): Double = {
    val v = varianceLnX
    val t = pow(log(n) - mu, 2) //ln_e
    val t3 = pow(log(n) - mu, 3)
    val ll0 = -0.5 * log(2 * PI * varianceLnX) - log(n) - (t / (2 * varianceLnX))
    val ll1 = ll0 + log(1.0 + 1.0 / (2.0 * n * varianceLnX) * (t / (varianceLnX) + log(n) - mu - 1.0)
      + 1.0 / (6.0 * n * n * v * v * v) * (3.0 * v * v - (3.0 * v - 2.0 * v * v) * (mu - log(n))
      - 3.0 * v * t + t3));
    ll1
  }

  /**
    * Numerical Integration using NewtonRaphsonSolver
    */
  def calculateWithIntegral(mu: Double, varianceLnX: Double, n: Int): Double = {
    val est = mu + n * (varianceLnX)
    val lnNFact = lnFactorial(n) //if (n < 50) logFactorial(n) else Gamma.logGamma(n+1)
    val derivExp = new DerivExponent(n, mu, varianceLnX)
    val U = n + (mu / (varianceLnX) - 1.0) / (1 + 1 / (varianceLnX))

    val integRange: (Double, Double) = if (est > varianceLnX) {
      var argDistributionMax = 0.0
      if (math.abs(U) > 1e-7) {
        val baseSol = solver.solve(1000000, derivExp, 0, U) // maximum of distribution
        // derivExp.value(y)
        //val (nL, nU) = getBrackets(derivExp, 0, U, baseSol)
        val nL = 0
        val nU = U
        //val baseSol: Double = nonBracketing.solve(10000, derivExp, nL, nU, (nL + nU) / 2);
        argDistributionMax = if (derivExp.value(nL) * derivExp.value(nU) < 0) {
          try {
            UnivariateSolverUtils.forceSide(1000000, derivExp, pegassusSolver, baseSol, nL, nU, AllowedSolution.LEFT_SIDE);
          } catch {
            case e: org.apache.commons.math3.exception.NoBracketingException => {
              println(s"Parameter values mu = $mu, variance = $varianceLnX, num_species = $n ")
              println(s"Bracket values nL = $nL, nU =  $nU, derivExp(nL) = ${derivExp.value(nL)}, derivExp(nU) = ${derivExp.value(nU)}")
              loggerOPTBrackets.error(s"Parameter values mu = $mu, variance = $varianceLnX, num_species = $n ", e)
              loggerOPTBrackets.error(s"Bracket values nL = $nL, nU =  $nU, derivExp(nL) = ${derivExp.value(nL)}, derivExp(nU) = ${derivExp.value(nU)}")
              println("exception caught: " + e)
              Double.NaN
            }
          }

        } else baseSol
      }
      val myVar = getVar(argDistributionMax, varianceLnX)
      val A = argDistributionMax - 25 * myVar
      val B = argDistributionMax + 25 * myVar
      (A, B)
    } else {
      var argDistributionMax = 0.0
      var L = est - varianceLnX
      if (Math.abs(U - L) > 1e-7) {
        try {
          val baseSol = solver.solve(1000000, derivExp, L, U) // maximum of distribution
          //val baseSol: Double = nonBracketing.solve(10000, derivExp, L, U, (L + U) / 2);
          //val (nL, nU) = getBrackets(derivExp, L, U, baseSol)
          val nL = L
          val nU = U
          argDistributionMax = if (derivExp.value(nL) * derivExp.value(nU) < 0) {
            UnivariateSolverUtils.forceSide(100000, derivExp, pegassusSolver, baseSol, nL, nU, AllowedSolution.LEFT_SIDE);
          } else baseSol
        } catch {
          case e: Exception => println(s"exception caught: ${e}, L = ${L}, U = ${U}, n = ${n}, mu = ${mu}, varianceLnX = ${varianceLnX}.")
        }
      } else {
        argDistributionMax = 0.5 * (L + U)
      }
      val myVar = getVar(argDistributionMax, varianceLnX)
      val A = argDistributionMax - 25 * myVar
      val B = argDistributionMax + 25 * myVar
      (A, B)
    }
    val errRel = if (n < 10) 1e-12 else 1e-7
    val myIntegSimpson = IntegrationWithSimpson()
    var resSimpson = myIntegSimpson.calculate(pdfPoissonLognormal(n, mu, varianceLnX), integRange._1, integRange._2, 1000)
    val dLogFac1 = log(2 * PI * varianceLnX)
    log(resSimpson) - lnNFact - 0.5 * dLogFac1
  }

  def lnFactorial(n: Int): Double = factorialLog(n)

  /**
    * PDF of PoissonLognormal without multiplicaive  constant
    * Substitution X->ln(X)
    */
  def pdfPoissonLognormal(n: Int, mu: Double, varianceLnX: Double)(x: Double): Double =
  Math.exp(x * n - Math.exp(x) - 0.5 * (x - mu) * (x - mu) / (varianceLnX))

  def getVar(x: Double, variance: Double) = Math.sqrt(1 / (1 / (variance) + Math.exp(x)))

  /**
    * Calculate with R
    */
  def calculateWithR(mu: Double, variance: Double, n: Int): Double = {
    val p = PoilogRScala.getPoilog2(mu, variance, n)
    log(p)
  }

  /**
    * Calculate with R like Scala version
    * returns ln(PoissonLognormal(n,mu,variance))
    */
  def calculateWithRLikeScala(mu: Double, variance: Double, n: Int): Double = {
    if(variance<0){
      throw new Exception("")
    }
    assert(variance>=0, "")
    val sigma = Math.sqrt(variance)
    val logP0 = try {
      Math.log(Poilog.dpoilog(n, mu, sigma))
    } catch {
      case t: Throwable => t.printStackTrace(); println(s"variance = $variance, mu = $mu, n = $n"); logger.error(s"${t.toString}"); logger.error(s"variance = $variance, mu = $mu, n = $n", t)
    }
    logP0 match {
      case a: Double => a
      case _ =>  throw new Exception(s"a = $logP0")
    }
  }

  def calculateWithRLikeScala4N0(mu: Double, variance: Double): Double = {
    val n = 0
    val sigma = Math.sqrt(variance)
    val logP0 = try {
      Math.log(Poilog.dpoilog(n, mu, sigma))
    } catch {
      case t: Throwable => t.printStackTrace(); println(s"variance = $variance, mu = $mu, n = $n"); logger.error(s"${t.toString}"); logger.error(s"variance = $variance, mu = $mu, n = $n", t)
    }
    logP0 match {
      case a: Double => a
    }
  }

  /**
    * Approximation as described in Bulmer(1974) paper which uses Taylor expansion upto 2nd powers.
    * Gives realative error smaller than 1e-3 for n>10 and usual mu and sigma values.
    */
  def calculateWithApproximation2(mu: Double, sigma: Double, n: Int): Double = 5.0

  /**
    * Numerical Integration using
    */
  def calculateWithIntegral01(mu: Double, sigma: Double, n: Int): Double = {
    val est = mu + n * (sigma * sigma)
    val lnNFact = lnFactorial(n) //if (n < 50) logFactorial(n) else Gamma.logGamma(n+1)
    // **************************************************************
    //UnivariateFunction function = // some user defined function object
    val relativeAccuracy = 1.0e-12
    val absoluteAccuracy = 1.0e-8
    val nonBracketing: UnivariateSolver = new BrentSolver(relativeAccuracy, absoluteAccuracy)
    //val  baseRoot = nonBracketing.solve(100, function, 1.0, 5.0);
    // val c = UnivariateSolverUtils.forceSide(100, function,
    //                                      new PegasusSolver(relativeAccuracy, absoluteAccuracy),
    //                                     baseRoot, 1.0, 5.0, AllowedSolution.LEFT_SIDE);
    // ---------------------------------------------------------------------------

    val solver = new NewtonRaphsonSolver(1E-6)
    val derivExp = new DerivExponent(n, mu, sigma)
    val U = n + (mu / (sigma * sigma) - 1.0) / (1 + 1 / (sigma * sigma))

    val integRange: (Double, Double) = if (est > sigma * sigma) {
      var argDistributionMax = 0.0
      if (math.abs(U) > 1e-7) {
        //val baseSol: Double = nonBracketing.solve(10000, derivExp, 0, U, (U) / 2);
        //argDistributionMax  =  UnivariateSolverUtils.forceSide(100000, derivExp, new PegasusSolver(Math.log), baseSol, 0, U, AllowedSolution.LEFT_SIDE);
        argDistributionMax = solver.solve(1000000, derivExp, 0, U) // maximum of distribution
      }
      val myVar = getVar(argDistributionMax, sigma)
      val A = argDistributionMax - 25 * myVar
      val B = argDistributionMax + 25 * myVar
      (A, B)
    } else {
      var argDistributionMax = 0.0
      var L = est - sigma * sigma
      val sigmaL = 0 //this was missing
      if ((U - L) > 1e-7) {
        try {
          //argDistributionMax = solver.solve(1000000000, derivExp, L, U) // maximum of distribution
          //val  baseRoot = nonBracketing.solve(100, function, 1.0, 5.0);
          val baseRoot = nonBracketing.solve(10000, derivExp, L, U)
          argDistributionMax = UnivariateSolverUtils.forceSide(100, derivExp,
            new PegasusSolver(relativeAccuracy, absoluteAccuracy),
            baseRoot, L, U, AllowedSolution.LEFT_SIDE)

        } catch {
          case e: Exception => println(s"exception caught: ${e}, L = ${L}, U = ${U}, n = ${n}, mu = ${mu}, sigma = ${sigma}.")
        }
      } else {
        argDistributionMax = 0.5 * (L + U)
      }
      val myVar = getVar(argDistributionMax, sigma)
      val A = argDistributionMax - 25 * myVar
      val B = argDistributionMax + 25 * myVar
      (A, B)
    }
    val errRel = if (n < 10) 1e-12 else 1e-7
    val myIntegSimpson = IntegrationWithSimpson()
    var resSimpson = myIntegSimpson.calculate(pdfPoissonLognormal(n, mu, sigma), integRange._1, integRange._2, 1000)
    log(resSimpson) - lnNFact - 0.5 * log(2 * PI * sigma * sigma)
  }

  def logFactorial2(n: BigInt): Double = logBigInteger(factorial(n))

  def logFactorial3(n: BigInt): Double = if (n < 50) {
    if (n > 0) logBigInteger(factorial(n)) else 0.0
  } else Gamma.logGamma((n.toDouble + 1.0))

  /**
    * Computes the natural logarithm of a BigInteger. Works for really big
    * integers (practically unlimited)
    *
    * @param x Argument, positive integer
    * @return Natural logarithm, as in <tt>Math.log()</tt>
    */

  def logBigInteger(x: BigInt): Double = {
    val blex = x.bitCount - 1022
    val x2 = if (blex > 0) x << blex else x
    val res: Double = Math.log(x.doubleValue())
    if (blex > 0) res + blex * Math.log(2.0) else res
  }

  def factorial(n: BigInt): BigInt = {
    @scala.annotation.tailrec
    def factorial(fact: BigInt, n: BigInt): BigInt =
      if (n == 1) fact else factorial(fact * n, n - 1)
    if (n < 0) {
      println("factorial of " + n)
      assert(n >= 0)
    }
    factorial(1, n)
  }

  //  /**
  //   * PDF of PoissonLognormal with multiplicaive  constant
  //   */  
  //  def logPdfPoissonLognormalWithConsts(n: Int, mu: Double, varianceLnX: Double)(x: Double): Double = {
  //    // Without constants
  //    val pdfWOConst= pdfPoissonLognormal(n,mu,varianceLnX)(x)
  //    val lnNFact = lnFactorial(n) 	
  //    val dLogFac1 = log(2 * PI * varianceLnX)
  //    log(pdfWOConst) - lnNFact - 0.5 * dLogFac1
  //  }

  def logFactorial(n: BigInt): Double = factorialLog(n.toInt)

  def factorial3(n: Int): Long = {
    println("Factorial of " + n)
    org.apache.commons.math3.util.CombinatoricsUtils.factorial(n)
  }

  def factorial2(n: BigInt): BigInt = {
    if (n == 0)
      return 1
    else
      return n * factorial(n - 1)
  }

  /**
    * Differential of exponent of Poisson-lognormal Distribution
    */
  def diffOfExp(x: Double): Double = 5

  private def getBrackets(derivExp: DerivExponent, L: Double, U: Double, baseSol: Double): (Double, Double) = {
    @tailrec
    def iterate(derivExp: DerivExponent, nL: Double, nU: Double): (Double, Double) = {
      if (derivExp.value(nL) > 0) {
        //println(s"f($nL)= ${derivExp.value(nL)}, f($nU)= ${derivExp.value(nU)}")
        iterate(derivExp, nL - Math.max(1e-10, Math.abs(nL) * 0.05), nU)
      } else if (derivExp.value(nU) < 0 && nU < baseSol * 1.5) {
        //println(s"f($nL)= ${derivExp.value(nL)}, f($nU)= ${derivExp.value(nU)}")
        iterate(derivExp, nL, nU * 1.05)
      } else {
        (nL, nU)
      }
    }

    if ((derivExp.value(U) < 0) || (derivExp.value(L) > 0)) {
      val nU = baseSol
      val nL = L
      iterate(derivExp, nL, nU)
    } else {
      //println(s"f($L)= ${derivExp.value(L)}, f($U)= ${derivExp.value(U)}")
      (L, U)
    }

  }

  class MyFunction extends org.apache.commons.math3.analysis.UnivariateFunction {
    def value(x: Double): Double = {
      return (5.0)
    }
  }

}

case class DerivExponent(n: Int, mu: Double, variance: Double) extends UnivariateDifferentiableFunction {
  def value(y: Double): Double = {
    n * y - Math.exp(y) + (y - mu) / (variance)
  }

  /**
    * f(y)=n-exp(y)-(y-mi)/(sigma*sigma)
    * d/dy(f(y) = -exp(y)-1/(sigma*sigma)
    *
    */
  def value(t: DerivativeStructure): DerivativeStructure = {
    t.exp().multiply(-1).add(n).subtract(t.subtract(mu).divide(variance))
  }

}

object Test extends App {

  val llLogN = new LogLikelihoodPoissonLogNormal()
  println("Result ", llLogN.calculateWithIntegral(-1.78, 6, 10))
}