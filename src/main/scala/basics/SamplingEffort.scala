package basics

import java.io.{File, IOException}
import java.lang.System.nanoTime
import java.util.Arrays._

import com.github.tototoshi.csv.{CSVWriter, defaultCSVFormat}
import com.typesafe.scalalogging.LazyLogging
import joptsimple._
import org.apache.commons.math3.analysis.solvers._
import org.joda.time.DateTime
import utils.Params

import scala.annotation.tailrec
import scala.util.{Failure, Success, Try}


/**
  * Finds distribution of sample size that will covers c% of species
  * tst
  */

object SamplingEffort extends LazyLogging {
  def constructOutputFileNameAllOutputs(fileName: String, coverage: Double, rootFindingMethod: String, scalaOrC: String, outParams: Params) = {
    val sep = "_"
    val parent = new java.io.File(fileName).getParent
    val newParent = parent + sepPath + scalaOrC + sep + (outParams.nIter / 1000) + "K" + sep + outParams.time_posterior_calc_sec
    // val newParent = parent + java.io.File.separator + ".." + java.io.File.separator + "output-sampleSize"
    val shortName = new java.io.File(fileName).getName
    val newShortName = "allResults_sample_size_" + shortName.replaceAll("\\.[^.]*$", "") + "_all_" + (coverage * 1000).toInt + "_%o_" + rootFindingMethod + "_" + MCMC.DATE_STR + ".csv" //sCode_sVersion_
    var outDirPath: File = new java.io.File(newParent)
    outDirPath.mkdirs
    val newShortNameCanonical = outDirPath.getCanonicalPath()
    val newFullFN = newParent + java.io.File.separator + newShortNameCanonical
    newFullFN
  }


  val DEFAULT_RELATIVE_TOLERANCE = 1.1
  val DEFAULT_ABSOLUTE_TOLERANCE = 1e-3
  val DEFAULT_STRICT_TOLERANCE = 1.05
  val DEFAULT_COVERAGE: Double = 0.9
  val logPLogNormal = new LogLikelihoodPoissonLogNormal()

  def getSampleSizeValueWithBisection(mu: Double, variance: Double, nL: Int, tCoverage: Double, relativeAccuracy: Double, tighterRelativeAccuracy: Double): SampleSizeEstimateDS = {
    assert(!mu.isInfinity && !mu.isNaN && nL > 0, s"mu=$mu,nL=$nL")
    if (nL <= 0) {
      logger.error(s"nL<=0")
    }
    val maxIters = 100
    val targetFractionUnseen = 1 - tCoverage
    val lnTargetP = Math.log(targetFractionUnseen)
    val devFromPTargetCalc = new DevFromPTargetCalculator(variance, lnTargetP)
    val lnPInit = logPLogNormal.calculateWithRLikeScala(mu, variance, 0)
    val searchStep = 2
    if (nL.toInt <= 0) {
      logger.error(s"nL<0, $nL")
    }
    val searchLeft = lnPInit < lnTargetP
    val r1: Try[(Try[Int], Double, Double, Int, Boolean)] = Try(findAcceptableMu(mu, variance, nL.toInt, lnPInit, lnTargetP, 0, maxIters, searchStep, searchLeft, mu, nL, lnPInit))

    val (nL2: Int, lnP2: Double, mu2: Double, nIterations: Int, brackets: Boolean) = r1 match {
      case Success(r) => r
      case Failure(e) => println("Info from the exception: " + e.getMessage); logger.error(e.getMessage); (0, 0.0, 0.0, 0, false)
    }
    val logAbsoluteAccuracy = Math.log(relativeAccuracy)

    val logTighterAbsoluteAccuracy = Math.log(tighterRelativeAccuracy)

    val bsSolver = new BisectionSolver(logTighterAbsoluteAccuracy)
    //***************************************************************
    val baseNewMu: Double = if (brackets) {
      assert(!mu.isInfinite && !mu.isNaN, s"mu=$mu")
      assert(mu.isInstanceOf[Double], s"mu=$mu")
      Try(bsSolver.solve(100000, devFromPTargetCalc, mu, mu2, (mu + mu2) / 2)) match {
        case Success(r) => r
        case Failure(e) => logger.error(e.toString);
          logger.error(s"mu=$mu, mu2=$mu2, variance=$variance,logPInit=$lnPInit,lnPTargetP=$lnTargetP,nL.toInt-$nL,nL2=$nL2  ");
          logger.error(s"assigning and returning new baseNewMu = NaN");
          Try(bsSolver.solve(1000, devFromPTargetCalc, mu)) match {
            case Success(r) => r
            case Failure(e) => logger.error(e.toString); logger.error(s"mu=$mu, mu2=$mu2, variance=$variance,logPInit=$lnPInit,lnPTargetP=$lnTargetP,nL.toInt-$nL,nL2=$nL2  "); logger.error(s"assigning and returning new baseNewMu = NaN"); Double.NaN
          }
      }
      //nonBracketing.solve(10000, lnP0Calculator, mu)
    } else {
      mu
    }

    val muSolution = baseNewMu
    //----------------------------------------------------------

    val nLSolution = getNewL(nL, mu, muSolution)
    val lnPSolution: Double = Try(if (brackets) logPLogNormal.calculateWithRLikeScala(muSolution, variance, 0) else logPLogNormal.calculateWithRLikeScala(mu, variance, 0)) match {
      case Success(lnPSol) => lnPSol
      case Failure(e) => {
        println("Info from the exception: " + e.getMessage);
        logger.error(e.getMessage);
        logger.error(s"Input parameters muSolution=$muSolution variance=$variance give error for P_0(mu,variance)")
        Double.NaN
      }
    }
    val coverageNew = 1 - Math.exp(lnPSolution)
    if (Math.abs(tCoverage - coverageNew) > 0.01) {
      println(s"Input parameters muSolution=$muSolution, variance=$variance, number of individuals ${nL},  incorrect resulting coverage ${coverageNew}, while goal is ${tCoverage} ");
      logger.error(s"Input parameters muSolution=$muSolution, variance=$variance, number of individuals ${nL}, incorrect resulting coverage ${coverageNew}, while goal is ${tCoverage}.")
    }
    // println(s"required sampl size = $nLSolution")
    val multipleOfActualSampleSize = nLSolution / nL
    SampleSizeEstimateDS(nLSolution.toInt, muSolution, variance, multipleOfActualSampleSize, nLSolution.toInt, coverageNew, nL)
  }

  def runSingleSamplingEffort(paramsAll: InputParamsAll): String = {
    val mcmcFileName = paramsAll.mcmcFileName.getAbsolutePath
    //***********************
    val postParamFileN: String = mcmcFileName
    val abundDataFileN: String = paramsAll.tadFileName.getAbsolutePath
    //    val absoluteAccuracy: Double = absTolleranceOption.value(optionsSet).toDouble //1.1 //relative error for difference of algorihms
    val tighterRelativeAccuracy: Double = paramsAll.relTolleranceStrict
    val relTollerance: Double = paramsAll.relTollerance
    //    val relativeAccuracy: Double = relTolleranceOption.value(optionsSet).toDouble
    val relativeAccuracy = relTollerance
    // NEW PARAMETERS ABOVE
    // ****************************************
    println(s"mcmcf = ${mcmcFileName}")
    //
    val tCoverage: Double = paramsAll.coverage
    val outputFile: String = constructOutputFileNameAllOutputs(postParamFileN, tCoverage, paramsAll.rootFindingMethod)
    val correctOnlyOutputFile2: String = constructOutputFileName4Correct(postParamFileN, tCoverage, paramsAll.rootFindingMethod)
    val outputIncorrectFile: String = constructIncorrectOutputFileName(postParamFileN, tCoverage, paramsAll.rootFindingMethod)
    // ***********************************
    // ***
    // ***********************************
    println(s"Output written to $outputFile")
    val abuData = readData(abundDataFileN)
    val nD = abuData.map(x => x._2).par.sum
    val nL = abuData.map((x) => x._1 * x._2).par.sum
    assert(nL > 0, s"Positive number of individuals but $nL found!")
    assert(nD > 0, s"Positive number of species reqired but $nD found!")
    val dropNumerator = 2
    val dropDenominator = 5
    val numRows2Skip = 0
    val (dataPosterior, head) = readDataPosteriorEstimates(postParamFileN, numRows2Skip)
    println(s"${dataPosterior(1).mu}, ${dataPosterior(1).variance}, ${dataPosterior(1).nCommunityTaxa}")

    val mu = -1.6
    val variance = 6.0
    //targeted coverage
    //    val tCoverage = 0.9 //targeted coverage
    val targetFractionUnseen = 1 - tCoverage
    val lnTargetP = Math.log(targetFractionUnseen)
    val lnPInit = logPLogNormal.calculateWithRLikeScala(mu, variance, 0)

    println(s"*** Estimate *******")

    val nSampleEstW: Try[SampleSizeEstimateDS] = paramsAll.rootFindingMethod match {
      case "Pegasus" => getSampleSizeValueWithBrentPegasus(mu, variance, nL, tCoverage, relativeAccuracy, tighterRelativeAccuracy,DateTime.now())
      case "Bisection" => Try(getSampleSizeValueWithBisection(mu, variance, nL, tCoverage, relativeAccuracy, tighterRelativeAccuracy))
      case _ => throw new Exception(s"")
    }

    println(s"relativeTollerance = $relTollerance, stricter relative tollerance = $tighterRelativeAccuracy")
    println(s"Initial: nL = $nL, lnPInit = $lnPInit, c = ${1 - Math.exp(lnPInit)}")
    println(s"Target: lnTargetP = $lnTargetP, c = ${1 - Math.exp(lnTargetP)}")
    nSampleEstW match {
      case Success(nSampleEst) => {
        println(s"Solution: required sampl size = ${nSampleEst.nL}, muS=${nSampleEst.mu}, c = ${nSampleEst.coverage}")
        println(s"Solution required sample size as multiple of actual sample size = ${nSampleEst.nL}")
      }
      case Failure(e) => println(e.getMessage)
    }
    implicit val outputFilewriters: OutputCSVWriters = OutputCSVWriters(CSVWriter.open(correctOnlyOutputFile2), CSVWriter.open(outputIncorrectFile))
    outputFilewriters.correctOutput.writeRow(List("mu", "variance", "nL_Recommended", "nL_Observed", "multipleOfActualSampleSize", "coverage", "mu_orig", "nL_orig"))
    outputFilewriters.incorrectOutput.writeRow(List("mu", "variance", "nL_Recommended", "nL_Observed", "multipleOfActualSampleSize", "coverage", "mu_orig", "nL_orig"))
    val (res: Array[Try[SampleSizeEstimateDS]], time: Long) = profile(getSampleSizeDistribution(dataPosterior, mu, variance, nL, tCoverage, relativeAccuracy, tighterRelativeAccuracy))
    outputFilewriters.correctOutput.close()
    outputFilewriters.incorrectOutput.close()
    //val outSampleFN: String = outPutFN(postParamFileN)
    // val (res: (ParamsPS, Double), time) = profile(mcmcMetro.calculate(mu0, variance0, s0, data, _nL = Some(nL), nIter = nIter, estimateWhat, version, outfileName))
    writeOutput(res, outputFile)
    println(s"Output written to $outputFile")
    println(s"Initial: nL = $nL, lnPInit = $lnPInit, c = ${1 - Math.exp(lnPInit)}")
    println(s"relativeTollerance = $relTollerance, stricter relative tollerance = $tighterRelativeAccuracy")
    println(s"Target: lnTargetP = $lnTargetP, c = ${1 - Math.exp(lnTargetP)}")

    nSampleEstW match {
      case Success(nSampleEst) => {
        println(s"Solution: required sampl size = ${nSampleEst.nL}, muS=${nSampleEst.mu}, c = ${nSampleEst.coverage}")
      }
      case Failure(e) => println(e.getMessage)

    }
    println(s"Time of calculation = ${time * 1E-9} sec, or ${time * 1E-9 / 60} minutes or, ${time * 1E-9 / 3600} hours.")
    println("Finished estimating of sampling effort")
    s"Result for posterior $outputFile"
  }

  def main(args: Array[String]): Unit = {
    val parser: OptionParser = new OptionParser() {
      acceptsAll(asList("h", "?"), "show help").forHelp()
    }
    val classString = Class.forName("java.lang.String");
    val mcmcvalOption = parser.accepts("mcmcf", "path to file with posterior distribution of parameters").withRequiredArg().ofType(classString)
    val tadfOption = parser.accepts("tadf").withRequiredArg().ofType(classString)
    val cOption: ArgumentAcceptingOptionSpec[java.lang.Double] = parser.acceptsAll(asList("c", "coverage"), "species coverage").withRequiredArg().ofType(java.lang.Double.TYPE).defaultsTo(DEFAULT_COVERAGE)
    val absTolleranceOption: ArgumentAcceptingOptionSpec[java.lang.Double] = parser.accepts("absTollerance", "absolute numerical error for root finding").withRequiredArg().ofType(java.lang.Double.TYPE).defaultsTo(DEFAULT_ABSOLUTE_TOLERANCE)
    val relTolleranceOption: ArgumentAcceptingOptionSpec[java.lang.Double] = parser.accepts("relTollerance", "relative numeric error for root finding").withRequiredArg().ofType(java.lang.Double.TYPE).defaultsTo(DEFAULT_RELATIVE_TOLERANCE)
    val strictTolleranceOption: ArgumentAcceptingOptionSpec[java.lang.Double] = parser.accepts("strictTollerance", "strict relative numeric error for root finding").withRequiredArg().ofType(java.lang.Double.TYPE).defaultsTo(DEFAULT_STRICT_TOLERANCE)
    //parser.acceptsAll(asList("h", "?"), "show help").forHelp();
    val optionsSet: OptionSet = parser.parse(args: _*)
    parser.printHelpOn(System.out)
    val mcmcFileName = mcmcvalOption.value(optionsSet)
    //***********************
    val postParamFileN: String = mcmcvalOption.value(optionsSet).toString
    val abundDataFileN: String = tadfOption.value(optionsSet).toString
    val absoluteAccuracy: Double = absTolleranceOption.value(optionsSet).toDouble
    //1.1 //relative error for difference of algorihms
    val tighterRelativeAccuracy: Double = strictTolleranceOption.value(optionsSet).toDouble
    //1.05
    val relTollerance: Double = relTolleranceOption.value(optionsSet).toDouble
    val relativeAccuracy: Double = relTolleranceOption.value(optionsSet).toDouble

    // NEW PARAMETERS ABOVE
    // ****************************************
    println(s"mcmcf = ${mcmcFileName}")
    //
    val tCoverage: Double = cOption.value(optionsSet) //.asInstanceOf[Double]


    val paramsAll: InputParamsAll = InputParamsAll(new File(postParamFileN), new File(abundDataFileN), tCoverage, relTollerance, tighterRelativeAccuracy)
    SamplingEffort.runSingleSamplingEffort(paramsAll)
  }

  def getProgramParameters(args: Array[String]) = {
    val required = List()
    //List('arg1, 'arg2)
    // Optional arguments by flag which map to a key in options
    val optional = Map("-tadf" -> 'tadf, "-mcmcf" -> 'mcmcf, "-c" -> 'coverage)
    // Default options that are passed in
    val definedParameters: Map[Symbol, Any] = Map()
    // Parse options based on the command line args
    val options = parseOptions(args.toList, required, optional, definedParameters, 0)
    options
  }

  def constructOutputFileNameAllOutputs(fileName: String, coverage: Double, rootFindingMethod: String): String = {
    val parent = new java.io.File(fileName).getParent
    val newParent = parent
    // val newParent = parent + java.io.File.separator + ".." + java.io.File.separator + "output-sampleSize"
    val shortName = new java.io.File(fileName).getName
    val newShortName = "allResults_sample_size_" + shortName.replaceAll("\\.[^.]*$", "") + "_all_" + (coverage * 1000).toInt + "_%o_" + rootFindingMethod + "_" + MCMC.DATE_STR + ".csv" //sCode_sVersion_
    (new java.io.File(newParent)).mkdirs
    val newFullFN = newParent + java.io.File.separator + newShortName
    newFullFN
  }

  def constructOutputFileNameAllOutputs(fileName: String, info: String, coverage: Double): String = {
    val parent = new java.io.File(fileName).getParent
    val newParent = parent
    // val newParent = parent + java.io.File.separator + ".." + java.io.File.separator + "output-sampleSize"
    val shortName = new java.io.File(fileName).getName
    val newShortName = info + shortName.replaceAll("\\.[^.]*$", "") + "_" + (coverage * 1000).toInt + "_%o_" + MCMC.DATE_STR + ".csv" //sCode_sVersion_
    (new java.io.File(newParent)).mkdirs
    val newFullFN = newParent + java.io.File.separator + newShortName
    newFullFN
  }

  def constructOutputFileNameAllOutputs(fileName: String, coverage: String): String = {
    val parent = new java.io.File(fileName).getParent
    val newParent = parent
    // val newParent = parent + java.io.File.separator + ".." + java.io.File.separator + "output-sampleSize"
    val shortName = new java.io.File(fileName).getName
    val newShortName = "sample_size_" + shortName.replaceAll("\\.[^.]*$", "") + "_" + coverage + "_%o_" + MCMC.DATE_STR + ".csv" //sCode_sVersion_
    (new java.io.File(newParent)).mkdirs
    val newFullFN = newParent + java.io.File.separator + newShortName
    newFullFN

  }

  def constructOutputFileNameAllOutputs(fileName: String, info: String, coverage: String): String = {
    val parent = new java.io.File(fileName).getParent
    val newParent = parent
    // val newParent = parent + java.io.File.separator + ".." + java.io.File.separator + "output-sampleSize"
    val shortName = new java.io.File(fileName).getName
    val newShortName = info + shortName.replaceAll("\\.[^.]*$", "") + "_all_" + coverage + "_%o_" + MCMC.DATE_STR + ".csv" //sCode_sVersion_
    (new java.io.File(newParent)).mkdirs
    val newFullFN = newParent + java.io.File.separator + newShortName
    newFullFN
  }

  val sepPath = File.separator

  def  constructOutputFileName4Correct(fileName: String, coverage: Double, rootFindingMethod: String): String = {
    val p = "_"
    val filePath: File = new java.io.File(fileName).getCanonicalFile
    val parent = filePath.getParent
    val newParent = parent
    // val newParent = parent + java.io.File.separator + ".." + java.io.File.separator + "output-sampleSize"
    val shortName = filePath.getName
    val newShortName = "sample_size_" + shortName.replaceAll("\\.[^.]*$", "") + "_correct_Coverage_" + (coverage * 1000).toInt + "_%o_" + rootFindingMethod + "_" + MCMC.DATE_STR + ".csv" //sCode_sVersion_
    (new java.io.File(newParent)).mkdirs
    val newFullFN = newParent + java.io.File.separator + newShortName
    newFullFN
  }

  def constructIncorrectOutputFileName(fileName: String, coverage: Double, rootFindingMethod: String): String = {
    val parent = new java.io.File(fileName).getCanonicalFile.getParent
    val newParent = parent
    // val newParent = parent + java.io.File.separator + ".." + java.io.File.separator + "output-sampleSize"
    val shortName = new java.io.File(fileName).getName
    val newShortName = "No_solution_for_sample_size_" + shortName.replaceAll("\\.[^.]*$", "") + "_incorrect_coverage_" + (coverage * 1000).toInt + "_%o_" + rootFindingMethod + "_" + MCMC.DATE_STR + ".csv" //sCode_sVersion_
    (new java.io.File(newParent)).mkdirs
    val newFullFN = newParent + java.io.File.separator + newShortName
    newFullFN
  }

  def constructOutputFileName4Correct(fileName: String, coverage: String): String = {
    val parent = new java.io.File(fileName).getParent
    val newParent = parent
    // val newParent = parent + java.io.File.separator + ".." + java.io.File.separator + "output-sampleSize"
    val shortName = new java.io.File(fileName).getName
    val newShortName = "sample_size_" + shortName.replaceAll("\\.[^.]*$", "") + "_" + coverage + "_%o_" + MCMC.DATE_STR + ".csv" //sCode_sVersion_
    (new java.io.File(newParent)).mkdirs
    val newFullFN = newParent + java.io.File.separator + newShortName
    newFullFN
  }

  def constructOutputFileName4QuinceCode(fileName: String): String = {
    val parent = new java.io.File(fileName).getParent
    val baseName = new java.io.File(fileName).getName
    val newParent = parent
    // val newParent = parent + java.io.File.separator + ".." + java.io.File.separator + "output-sampleSize"
    val shortName = new java.io.File(fileName).getName
    val newShortName = "sample_size_" + "_" + MCMC.DATE_STR + ".csv" //sCode_sVersion_
    (new java.io.File(newParent)).mkdirs
    val newFullFN = newParent + java.io.File.separator + newShortName
    newFullFN
  }

  def writeOutput(res: Array[Try[SamplingEffort.SampleSizeEstimateDS]], outSampleSizeFN: String) = {
    val writer = CSVWriter.open(outSampleSizeFN)
    writer.writeRow(List("mu", "variance", "nL_Recommended", "nL_Observed", "multipleOfActualSampleSize", "coverage"))
    res.foreach {
      xW: Try[SampleSizeEstimateDS] =>
        xW match {
          case Success(x: SampleSizeEstimateDS) => {
            writer.writeRow(List(x.mu, x.variance, x.nL, x.nLObserved, x.multipleOfActualSampleSize, x.coverage))
          }
          case Failure(e) => println(e.getMessage)
        }
    }
    writer.close()
  }

  /**
    * find L_* for which P_* = P_target
    *
    * @param muObserve   - estimated mean of logX~N(mu,variance)
    * @param varianceObserved
    * @param lnPObserved - log(nu)
    * @param lnTargetP   = ln(1-c), where c is fraction of observed species in sample
    * @param nLObserved  - number of observed microorganizms in sample
    * @return
    * L_* is sample size allow to observellg desired fraction of species
    */
  @tailrec
  def findAcceptableMu(muObserve: Double, varianceObserved: Double, nLObserved: Double, lnPObserved: Double, lnTargetP: Double, nIterations: Int, maxIters: Int, searchStep: Double, searchLeft: Boolean, muNew: Double, nLNew: Double, lnPNew: Double): (Try[Int], Double, Double, Int, Boolean) = {
    if (nLObserved <= 0) {
      logger.error("nL<=0")
    }

    assert(!lnTargetP.isInfinity && !lnTargetP.isNaN)
    //    val lnP2 = logPLogNormal.calculateWithRLikeScala(mu2, variance, 0)
    val nL1 = if (searchLeft) (0.0).max(nLNew / searchStep) else (nLNew * searchStep)
    if (nL1 < 1) {
      logger.error("nL1<=0 , nL1=$nL1")
    }
    val mu1 = convertL2M(nLObserved, muObserve, nL1)
    if (mu1.isInfinity || mu1.isNaN) {
      logger.error(s"mu1=$mu1, mu=$mu1,nL=$nL1")
    }
    val lnP1 = logPLogNormal.calculateWithRLikeScala(mu1, varianceObserved, 0)
    if (lnP1.isInfinity || lnPObserved.isInfinity || nL1 < 1) {
      logger.error("lnP or lnP2 is infinite or nL1 <1!")
      logger.error(s"mu=$mu1, variance=$varianceObserved, lnTargetP=$lnTargetP, nL=$nL1,lnP=$lnP1, nIterations=$nIterations, maxIters=$maxIters")
      logger.error(s"mu2=$mu1, nL2=$nL1,lnP2=$lnP1,")
    }

    val diff: Double = if (searchLeft) {
      lnTargetP - lnP1
    } else (lnP1 - lnTargetP)

    if ((lnTargetP - lnP1) * (lnTargetP - lnPObserved) > 0 && nIterations < maxIters && nL1 >= 1) {
      findAcceptableMu(muObserve, varianceObserved, nLObserved, lnPObserved, lnTargetP, nIterations + 1, maxIters, searchStep, searchLeft, mu1, nL1, lnP1)
    } else {
      if ((nIterations >= maxIters || nL1 <= 1) && (lnPObserved - lnTargetP) * (lnP1 - lnTargetP) > 0) {
        logger.error("No BracketsFound!")
        logger.error(s"(lnP - lnTargetP)=${(lnP1 - lnTargetP)}  and   (lnP2 - lnTargetP) = ${(lnP1 - lnTargetP)} ")
        logger.error(s"mu=$mu1, variance=$varianceObserved, lnTargetP=$lnTargetP, nL=$nL1,lnP=$lnP1, nIterations=$nIterations, maxIters=$maxIters")
        logger.error(s"mu1=$mu1, nL2=$nL1,lnP1=$lnP1,")
        if (nL1 >= 1) findAcceptableMu(muObserve, varianceObserved, nLObserved, lnPObserved, lnTargetP, nIterations + 1, maxIters, searchStep * 10, searchLeft, mu1, nL1, lnP1)
        else
          (Failure(new Exception(s"No solution for L found for target P_t = ${Math.exp(lnTargetP)}, mu1 = ${mu1}, variance = ${varianceObserved},  P1 = ${Math.exp(lnP1)}")), lnP1, mu1, nIterations, false)
      } else {
        (Success(nL1.toInt), lnP1, mu1, nIterations, true)
      }
    }
  }

  def convertL2M(nL: Double, mu: Double, nLNew: Double): Double = {
    Math.log(nLNew / nL) + mu
  }

  /**
    * Finds distribution of sample size that will covers c% of species for posterior distributions of parameters of lognormal distribution
    */
  def getSampleSizeValueWithBrentPegasus(mu: Double, variance: Double, nL: Double, tCoverage: Double, relativeAccuracy: Double, tighterRelativeAccuracy: Double,startDateTime:DateTime): Try[SampleSizeEstimateDS] = {
    assert(!mu.isInfinity && !mu.isNaN && nL > 0, s"mu=$mu,nL=$nL")
    if (nL <= 0) {
      logger.error(s"nL<=0")
    }
    val maxIters = 100
    val targetFractionUnseen = 1 - tCoverage
    val lnTargetP = Math.log(targetFractionUnseen)
    val devFromPTargetCalc = new DevFromPTargetCalculator(variance, lnTargetP)
    val lnPInit = logPLogNormal.calculateWithRLikeScala(mu, variance, 0)
    val searchStep = 2
    if (nL.toInt <= 0) {
      logger.error(s"nL<0, $nL")
    }
    val searchLeft = lnPInit < lnTargetP
    val r1: (Try[Int], Double, Double, Int, Boolean) = findAcceptableMu(mu, variance, nL.toInt, lnPInit, lnTargetP, 0, maxIters, searchStep, searchLeft, mu, nL, lnPInit)
    //    println(s"Actual sample size: nL = $nL, lnPInit = $lnPInit, c = ${1 - Math.exp(lnPInit)}")
    //    println(s"lnTargetP = $lnTargetP, c = ${1 - Math.exp(lnTargetP)}")
    //    println(s"Sufficiennt sample size : nL2 = $nL2, mu2=$mu2,  lnP2 = $lnP2, c = ${1 - Math.exp(lnP2)}")
    val (nL2: Try[Int], lnP2: Double, mu2: Double, nIterations: Int, brackets: Boolean) = r1
    /* match {
         case Success(r) => r
         case Failure(e) => println("Info from the exception: " + e.getMessage); logger.error(e.getMessage); (0, 0.0, 0.0, 0, false)
       }*/
    val logAbsoluteAccuracy = Math.log(relativeAccuracy)

    val logTighterAbsoluteAccuracy = Math.log(tighterRelativeAccuracy)

    val nonBracketing: UnivariateSolver = new BrentSolver(logAbsoluteAccuracy);
    val baseNewMu: Try[Double] = if (brackets) {
      assert(!mu.isInfinite && !mu.isNaN, s"mu=$mu")
      assert(mu.isInstanceOf[Double], s"mu=$mu")
      if (mu < mu2)
        Try(nonBracketing.solve(100000, devFromPTargetCalc, mu, mu2, (mu + mu2) / 2))
      else Try(nonBracketing.solve(100000, devFromPTargetCalc, mu2, mu, (mu + mu2) / 2))
      //nonBracketing.solve(10000, lnP0Calculator, mu)
    } else {
      Failure(new Exception("Bracket not found"))
    }
    //    println(s"base new mu = $baseNewMu")
    //    println("----")
    //    val nLBaseSolution = getNewL(nL, mu, baseNewMu)
    //    println(s"base required sampl size = $nLBaseSolution")
    //****************

    val muSolutionW: Try[Double] = baseNewMu match {
      case Success(baseNewMu) => {
        val p4BaseMu = Math.exp(logPLogNormal.calculateWithRLikeScala(baseNewMu, variance, 0))
        val muMin = if (brackets) baseNewMu * 0.99 else Double.MinValue
        val muMax = if (brackets) baseNewMu * 1.01 else Double.MaxValue
        val startMu = if (brackets) baseNewMu else mu
        val muSolution = Try(if (brackets) UnivariateSolverUtils.forceSide(1000000, devFromPTargetCalc, new PegasusSolver(logTighterAbsoluteAccuracy), startMu, muMin, muMax, AllowedSolution.ANY_SIDE) else Double.NaN)
        muSolution
      }
      case Failure(e) => {
        logger.error(e.toString)
        Failure(new Exception(e.getMessage + ""))
      }
    }
    //    println(s"final new mu = $muSolution")
    val res: Try[SampleSizeEstimateDS] = muSolutionW match {
      case Success(muSolution) => {
        val nLSolution = getNewL(nL, mu, muSolution)
        val lnPSolution: Double = Try(if (brackets) logPLogNormal.calculateWithRLikeScala(muSolution, variance, 0) else logPLogNormal.calculateWithRLikeScala(mu, variance, 0)) match {
          case Success(lnPSol) => lnPSol
          case Failure(e) => {
            println("Info from the exception: " + e.getMessage);
            logger.error(e.getMessage);
            logger.error(s"Input parameters muSolution=$muSolution variance=$variance give error for P_0(mu,variance)")
            Double.NaN
          }
        }
        val coverageNew = 1 - Math.exp(lnPSolution)
        if (Math.abs(tCoverage - coverageNew) > 0.01) {
          println(s"Input parameters muSolution=$muSolution, variance=$variance, number of individuals ${nL},  incorrect resulting coverage ${coverageNew}, while goal is ${tCoverage} ");
          logger.error(s"Input parameters muSolution=$muSolution, variance=$variance, number of individuals ${nL}, incorrect resulting coverage ${coverageNew}, while goal is ${tCoverage}.")
        }
        // println(s"required sampl size = $nLSolution")
        val multipleOfActualSampleSize = nLSolution / nL
        Try(SampleSizeEstimateDS(nLSolution.toInt, muSolution, variance, multipleOfActualSampleSize, nLSolution.toInt, coverageNew, nL))
      }
      case Failure(e) => Failure(new Exception(e.getMessage + ""))
    }
    res
  }


  /**
    * Finds distribution of sample size that will covers c% of species for posterior distributions of parameters of lognormal distribution
    * Remove when changes in getSampleSizeValueWithBrentPegasus ar completed and tested!
    */
  def getSampleSizeValueWithBrentPegasusOld(mu: Double, variance: Double, nL: Double, tCoverage: Double, relativeAccuracy: Double, tighterRelativeAccuracy: Double): SampleSizeEstimateDS = {
    assert(!mu.isInfinity && !mu.isNaN && nL > 0, s"mu=$mu,nL=$nL")
    if (nL <= 0) {
      logger.error(s"nL<=0")
    }
    val maxIters = 100
    val targetFractionUnseen = 1 - tCoverage
    val lnTargetP = Math.log(targetFractionUnseen)
    val devFromPTargetCalc = new DevFromPTargetCalculator(variance, lnTargetP)
    val lnPInit = logPLogNormal.calculateWithRLikeScala(mu, variance, 0)
    val searchStep = 2
    if (nL.toInt <= 0) {
      logger.error(s"nL<0, $nL")
    }
    val searchLeft = lnPInit < lnTargetP
    val r1: (Try[Int], Double, Double, Int, Boolean) = findAcceptableMu(mu, variance, nL.toInt, lnPInit, lnTargetP, 0, maxIters, searchStep, searchLeft, mu, nL, lnPInit)
    //    println(s"Actual sample size: nL = $nL, lnPInit = $lnPInit, c = ${1 - Math.exp(lnPInit)}")
    //    println(s"lnTargetP = $lnTargetP, c = ${1 - Math.exp(lnTargetP)}")
    //    println(s"Sufficiennt sample size : nL2 = $nL2, mu2=$mu2,  lnP2 = $lnP2, c = ${1 - Math.exp(lnP2)}")
    val (nL2: Try[Int], lnP2: Double, mu2: Double, nIterations: Int, brackets: Boolean) = r1
    /* match {
         case Success(r) => r
         case Failure(e) => println("Info from the exception: " + e.getMessage); logger.error(e.getMessage); (0, 0.0, 0.0, 0, false)
       }*/
    val logAbsoluteAccuracy = Math.log(relativeAccuracy)

    val logTighterAbsoluteAccuracy = Math.log(tighterRelativeAccuracy)

    val nonBracketing: UnivariateSolver = new BrentSolver(logAbsoluteAccuracy);
    val baseNewMu: Double = if (brackets) {
      assert(!mu.isInfinite && !mu.isNaN, s"mu=$mu")
      assert(mu.isInstanceOf[Double], s"mu=$mu")
      Try(nonBracketing.solve(100000, devFromPTargetCalc, mu, mu2, (mu + mu2) / 2)) match {
        case Success(r) => r
        case Failure(e) => logger.error(e.toString);
          logger.error(s"mu=$mu, mu2=$mu2, variance=$variance,logPInit=$lnPInit,lnPTargetP=$lnTargetP,nL.toInt-$nL,nL2=$nL2  ");
          logger.error(s"assigning and returning new baseNewMu = NaN");
          Try(nonBracketing.solve(1000, devFromPTargetCalc, mu)) match {
            case Success(r) => r
            case Failure(e) => logger.error(e.toString); logger.error(s"mu=$mu, mu2=$mu2, variance=$variance,logPInit=$lnPInit,lnPTargetP=$lnTargetP,nL.toInt-$nL,nL2=$nL2  "); logger.error(s"assigning and returning new baseNewMu = NaN"); Double.NaN
          }
      }
      //nonBracketing.solve(10000, lnP0Calculator, mu)
    } else {
      mu
    }
    //    println(s"base new mu = $baseNewMu")
    //    println("----")
    //    val nLBaseSolution = getNewL(nL, mu, baseNewMu)
    //    println(s"base required sampl size = $nLBaseSolution")
    val p4BaseMu = Math.exp(logPLogNormal.calculateWithRLikeScala(baseNewMu, variance, 0))
    val muMin = if (brackets) baseNewMu * 0.99 else Double.MinValue
    val muMax = if (brackets) baseNewMu * 1.01 else Double.MaxValue
    val startMu = if (brackets) baseNewMu else mu
    //****************
    val muSolution = Try(if (brackets) UnivariateSolverUtils.forceSide(1000000, devFromPTargetCalc, new PegasusSolver(logTighterAbsoluteAccuracy), startMu, muMin, muMax, AllowedSolution.ANY_SIDE) else Double.NaN) match {
      case Success(muS) => muS
      case Failure(e) => {
        println("Info from the exception: " + e.getMessage);
        logger.error(e.getMessage);
        logger.error(s"Passing mu=$mu as solution for P_target for sample mu=$mu and variance=$variance")
        e.getMessage;
        mu
      }
    }
    //    println(s"final new mu = $muSolution")
    val nLSolution = getNewL(nL, mu, muSolution)
    val lnPSolution: Double = Try(if (brackets) logPLogNormal.calculateWithRLikeScala(muSolution, variance, 0) else logPLogNormal.calculateWithRLikeScala(mu, variance, 0)) match {
      case Success(lnPSol) => lnPSol
      case Failure(e) => {
        println("Info from the exception: " + e.getMessage);
        logger.error(e.getMessage);
        logger.error(s"Input parameters muSolution=$muSolution variance=$variance give error for P_0(mu,variance)")
        Double.NaN
      }
    }
    val coverageNew = 1 - Math.exp(lnPSolution)
    if (Math.abs(tCoverage - coverageNew) > 0.01) {
      println(s"Input parameters muSolution=$muSolution, variance=$variance, number of individuals ${nL},  incorrect resulting coverage ${coverageNew}, while goal is ${tCoverage} ");
      logger.error(s"Input parameters muSolution=$muSolution, variance=$variance, number of individuals ${nL}, incorrect resulting coverage ${coverageNew}, while goal is ${tCoverage}.")
    }
    // println(s"required sampl size = $nLSolution")
    val multipleOfActualSampleSize = nLSolution / nL
    SampleSizeEstimateDS(nLSolution.toInt, muSolution, variance, multipleOfActualSampleSize, nLSolution.toInt, coverageNew, nL)
  }

  /**
    * Finds distribution of sample size that will covers c% of species for posterior distributions of parameters of lognormal distribution
    */
  def getSampleSizeValue4DynamicCoverage(mu: Double, variance: Double, nL: Double, nS: Double, nD: Double, absoluteAccuracy: Double, tighterTollerance: Double): Try[SampleSizeEstimateDS] = {
    assert(!mu.isInfinity && !mu.isNaN && nL > 0, s"mu=$mu,nL=$nL")
    if (nL <= 0) {
      logger.error(s"nL<=0")
    }
    val maxIters = 100

    val tCoverage = nD / nS
    val targetFractionUnseen = 1 - tCoverage
    val lnTargetP = Math.log(targetFractionUnseen)
    val devFromPTargetCalc = new DevFromPTargetCalculator4DynamicS(variance, lnTargetP)
    val lnPInit = logPLogNormal.calculateWithRLikeScala(mu, variance, 0)
    /*val (nL2:Int, lnP2:Double, mu2: Double, nIterations: Int, brackets: Boolean) = Try(findAcceptableMu(mu, variance, lnPInit, lnTargetP, nL.toInt, 0, maxIters)) match {
      //case Success(r:(Int,Double,Double,Int,Boolean)) =>println(s"r._1 = ${r._1}"); r
      case Success((nL2:Int, lnP2:Double, mu2: Double, nIterations: Int, brackets: Boolean)) =>println(s"nL2 = ${nL2}");(nL2, lnP2, mu2, nIterations, brackets)
      case Failure(e) => println("Info from the exception: " + e.getMessage); logger.error(e.getMessage)
    }*/

    //    val rest1 = findAcceptableMu(mu, variance, lnPInit, lnTargetP, nL.toInt,0,maxIters)
    val searchStep = 2
    if (nL.toInt <= 0) {
      logger.error(s"nL<0, $nL")
    }
    val searchLeft = lnPInit < lnTargetP
    val r1: Try[(Try[Int], Double, Double, Int, Boolean)] = Try(findAcceptableMu(mu, variance, nL.toInt, lnPInit, lnTargetP, 0, maxIters, searchStep, searchLeft, mu, nL, lnPInit))
    //    println(s"Actual sample size: nL = $nL, lnPInit = $lnPInit, c = ${1 - Math.exp(lnPInit)}")
    //    println(s"lnTargetP = $lnTargetP, c = ${1 - Math.exp(lnTargetP)}")
    //    println(s"Sufficiennt sample size : nL2 = $nL2, mu2=$mu2,  lnP2 = $lnP2, c = ${1 - Math.exp(lnP2)}")
    val (nL2: Int, lnP2: Double, mu2: Double, nIterations: Int, brackets: Boolean) = r1 match {
      case Success(r) => r
      case Failure(e) => println("Info from the exception: " + e.getMessage); logger.error(e.getMessage); (0, 0.0, 0.0, 0, false)
    }
    val logAbsoluteAccuracy = Math.log(absoluteAccuracy)

    val logTighterAbsoluteAccuracy = Math.log(tighterTollerance)

    val nonBracketing: UnivariateSolver = new BrentSolver(logAbsoluteAccuracy);
    val baseNewMu: Double = if (brackets) {
      assert(!mu.isInfinite && !mu.isNaN, s"mu=$mu")
      assert(mu.isInstanceOf[Double], s"mu=$mu")
      Try(nonBracketing.solve(1000, devFromPTargetCalc, mu, mu2, (mu + mu2) / 2)) match {
        case Success(r) => r
        case Failure(e) => logger.error(e.toString);
          logger.error(s"mu=$mu, mu2=$mu2, variance=$variance,logPInit=$lnPInit,lnPTargetP=$lnTargetP,nL.toInt-$nL,nL2=$nL2  ");
          logger.error(s"assigning and returning new baseNewMu = NaN");
          Try(nonBracketing.solve(1000, devFromPTargetCalc, mu)) match {
            case Success(r) => r
            case Failure(e) => logger.error(e.toString); logger.error(s"mu=$mu, mu2=$mu2, variance=$variance,logPInit=$lnPInit,lnPTargetP=$lnTargetP,nL.toInt-$nL,nL2=$nL2  "); logger.error(s"assigning and returning new baseNewMu = NaN"); Double.NaN
          }
      }
      //nonBracketing.solve(10000, lnP0Calculator, mu)
    } else {
      mu
    }
    //    println(s"base new mu = $baseNewMu")
    //    println("----")
    //    val nLBaseSolution = getNewL(nL, mu, baseNewMu)
    //    println(s"base required sampl size = $nLBaseSolution")
    val muMin = if (brackets) mu else Double.MinValue
    val muMax = if (brackets) mu2 else Double.MaxValue
    val startMu = if (brackets) (mu2 + mu) / 2 else mu
    //****************
    val muSolution = Try(if (brackets) UnivariateSolverUtils.forceSide(100000, devFromPTargetCalc, new PegasusSolver(logTighterAbsoluteAccuracy), startMu, muMin, muMax, AllowedSolution.ANY_SIDE) else Double.NaN) match {
      case Success(muS) => muS
      case Failure(e) => {
        println("Info from the exception: " + e.getMessage);
        logger.error(e.getMessage);
        logger.error(s"Passing mu=$mu as solution for P_target for sample mu=$mu and variance=$variance")
        e.getMessage;
        mu
      }
    }
    //    println(s"final new mu = $muSolution")
    val nLSolution = getNewL(nL, mu, muSolution)
    val lnPSolution: Double = Try(if (brackets) logPLogNormal.calculateWithRLikeScala(muSolution, variance, 0) else logPLogNormal.calculateWithRLikeScala(mu, variance, 0)) match {
      case Success(lnPSol) => lnPSol
      case Failure(e) => {
        println("Info from the exception: " + e.getMessage);
        logger.error(e.getMessage);
        logger.error(s"Input parameters muSolution=$muSolution variance=$variance give error for P_0(mu,variance)")
        Double.NaN
      }
    }
    val coverageNew = 1 - Math.exp(lnPSolution)
    // println(s"required sampl size = $nLSolution")
    val multipleOfActualSampleSize = nLSolution / nL
    Try(SampleSizeEstimateDS(nLSolution.toInt, muSolution, variance, multipleOfActualSampleSize, nLSolution.toInt, coverageNew, nL, tCoverage))
  }

  private val runtime = Runtime.getRuntime()
  import runtime.{ totalMemory, freeMemory, maxMemory }

  val locale = new java.util.Locale("us", "US")

  val formatterInteger = java.text.NumberFormat.getIntegerInstance(locale)



  /**
    * Finds distribution of sample size that will covers c% of species for posterior distributions of parameters of lognormal distribution
    */
  def getSampleSizeValue02(mu: Double, variance: Double, n: Int, nL: Double, mu2: Double, nL2: Double, targetFractionUnseen: Double, absoluteAccuracy: Double, tighterTollerance: Double): SampleSizeEstimateDS = {
    val logAbsoluteAccuracy = Math.log(absoluteAccuracy)
    val logTighterAbsoluteAccuracy = Math.log(tighterTollerance)
    val logPTarget: Double = Math.log(targetFractionUnseen)
    val lnP0Calculator = new DevFromPTargetCalculator(variance, logPTarget)

    val nonBracketing: UnivariateSolver = new BrentSolver(logAbsoluteAccuracy);
    val baseNewMu: Double = nonBracketing.solve(10000, lnP0Calculator, mu, mu2, (mu + mu2) / 2);
    println(s"base new mu = $baseNewMu")
    println("----")
    val nLBaseSolution = getNewL(nL, mu, baseNewMu)
    println(s"base required sampl size = $nLBaseSolution")
    //****************
    val muSolution = UnivariateSolverUtils.forceSide(100000, lnP0Calculator, new PegasusSolver(logTighterAbsoluteAccuracy), baseNewMu, mu, mu2, AllowedSolution.ANY_SIDE);
    println(s"final new mu = $muSolution")
    val nLSolution = getNewL(nL, mu, muSolution)
    println(s"required sampl size = $nLSolution")
    val multipleOfActualSampleSize = nLSolution / nL
    SampleSizeEstimateDS(nLSolution.toInt, muSolution, variance, multipleOfActualSampleSize, nLSolution.toInt, Double.NaN, nL)
  }

  def getNewL(observedL: Double, observedMu: Double, estimatedMmu: Double): Double = {
    observedL * Math.exp(estimatedMmu - observedMu)
  }

  def getNewMu(observedL: Double, estimatedMuForData: Double, estimatedL: Double): Double = {
    estimatedMuForData + Math.log(estimatedL / observedL)
  }

  /**
    * Finds sample size that will covers (1-targetFractionUnseen)% of species  for specific parameters of lognormal distribution
    */
  def getSampleSizeDistribution(data: Array[MCTraceRecord], mu: Double, variance: Double, nL: Int, tCoverage: Double, relativeAccuracy: Double, tighterRelativeAccuracy: Double)(implicit fwsCSV: OutputCSVWriters): Array[Try[SampleSizeEstimateDS]] = {
    val dataLength: Int = data.length
    var iteration: Int = 0
    val startDateTime = DateTime.now()
    //Array(nIter, mu, sigma, S, nLL, acceptance, n_accepted)
    val res = data collect {
      case (r: MCTraceRecord) => {
        if (iteration+1 % 100 == 1) {
          val now = DateTime.now()
          val toDoIdx = dataLength-iteration
          val timeDiff = now.getMillis-startDateTime.getMillis
          val iter = if(iteration==0) 1 else iteration
          val estsMillis = toDoIdx *timeDiff/iter
          val estimated = now.plusMillis(estsMillis.toInt)
          val estimatedTimeStr = estimated.toString("yyyy-MM-dd__HH:mm:ss")
          println(s"iteration $iteration / $dataLength | ${"%1.3f".format(100 * iteration.toDouble / dataLength.toDouble)} % completed at ${DateTime.now().toString("yyyy-MM-dd__HH:mm:ss")} | ETC ${estimatedTimeStr}  | total allocated memory = ${formatterInteger.format(totalMemory/1e+6 )} Mb | max memory = ${formatterInteger.format(maxMemory/1e+6)} Mbytes| | remaining memory = ${"%1.3f".format((maxMemory - totalMemory)/1e+6)} Mb | allocated but free memory = ${formatterInteger.format(freeMemory/1e+6)} Mb |Total free memory = ${formatterInteger.format((maxMemory-totalMemory()+freeMemory())/1e+6)}")
//          println(f"${100*doneIdx.toDouble/nIter}%1.2f %% of $nIter completed at ${DateTime.now().toString("yyyy-MM-dd__HH:mm:ss")} | total memory = ${formatterInteger.format(totalMemory/1e+6 )} Mb | max memory = ${formatterInteger.format(maxMemory/1e+6)} Mbytes| remaining memory = ${maxMemory - totalMemory} | free memory = ${formatterInteger.format(freeMemory/1e+6)} Mb | ETC ${estimatedTimeStr}")
        }
        iteration += 1
        val r2W = getSampleSizeValueWithBrentPegasus(r.mu, r.variance, nL, tCoverage, relativeAccuracy, tighterRelativeAccuracy,startDateTime)

        r2W match {
          case Success(r2) => {
            fwsCSV.correctOutput.writeRow(List(r2.mu, r2.variance, r2.nL, r2.nLObserved, r2.multipleOfActualSampleSize, r2.coverage, mu, nL))
          }
          case Failure(e) => {
            fwsCSV.incorrectOutput.writeRow(List(r.mu, r.variance, 0, nL, 0, tCoverage, mu, nL))
          }
        }

        //        if (r2.nL >= 1 && !r2.mu.isNaN && (abs(log(r2.coverage) - log(tCoverage)) < log(tighterRelativeAccuracy)))
        //          fwsCSV.correctOutput.writeRow(List(r2.mu, r2.variance, r2.nL, r2.nLObserved, r2.multipleOfActualSampleSize, r2.coverage, mu, nL))
        //        else
        //          fwsCSV.incorrectOutput.writeRow(List(r2.mu, r2.variance, r2.nL, r2.nLObserved, r2.multipleOfActualSampleSize, r2.coverage, mu, nL))
        r2W
      }
    }
    res.toArray
  }

  /**
    * Finds sample size that will covers (1-targetFractionUnseen)% of species  for specific parameters of lognormal distribution
    */
  def getSampleSizeDistributionDynamicCoverage(data: Array[MCTraceRecord], mu: Double, variance: Double, nL: Int, nDTestDS: Double, absoluteAccuracy: Double, tighterTollerance: Double)(implicit fwCSV: CSVWriter = CSVWriter.open("")): Array[Try[SampleSizeEstimateDS]] = {
    val dataLength: Int = data.length
    var iteration: Int = 0
    //Array(nIter, mu, sigma, S, nLL, acceptance, n_accepted)
    val res = data collect { case (r: MCTraceRecord) => {
      if (iteration % 100 == 0) {
        println(s"iteration $iteration / $dataLength, ${100 * iteration.toDouble / dataLength.toDouble} %")
      }
      iteration += 1
      val r2W = getSampleSizeValue4DynamicCoverage(r.mu, r.variance, nL, r.nCommunityTaxa, nDTestDS, absoluteAccuracy, tighterTollerance)
      r2W match {
        case Success(r2) => {
          if (r2.nL >= 1 && !r2.mu.isNaN) fwCSV.writeRow(List(r2.mu, r2.variance, r2.nL, r2.nLObserved, r2.multipleOfActualSampleSize, r2.coverage, mu, nL))
          Try(r2)
        }
        case Failure(e) => println(e.getMessage); Failure(e)
      }
    }
    }
    res.toArray
  }

  /**
    * Finds sample size that will covers (1-targetFractionUnseen)% of species  for specific parameters of lognormal distribution
    */
  def getSampleSizeDistribution2(mu: Double, variance: Double, n: Int, nL: Double, mu2: Double, nL2: Double, targetFractionUnseen: Double, tollerance: Double) = {

    val logTollerance = Math.log(1 + tollerance)
    val logPTarget: Double = Math.log(targetFractionUnseen)

    /**
      *
      */
    @tailrec
    def loop(mu: Double, lM1: Double, logPM1: Double, lM2: Double, logPM2: Double, fractionUnseen: Double): (Double, Double) = {
      val logP: Double = logPLogNormal.calculateWithRLikeScala(mu, variance, n)
      val lN: Double = lM1 + (logPTarget - logPM1) * (lM2 - lM1) / (logPM2 - logPM1)
      val muN: Double = mu + Math.log(lN) - Math.log(lM2)
      val logPN: Double = logPLogNormal.calculateWithRLikeScala(muN, variance, lN.toInt)
      println(s"target log likelihood value = $logPTarget")
      println(s"log likelihood value = $logP")
      println(s"Required sample size = $logP")

      if (Math.abs(logPN - logPTarget) < logTollerance) {
        (lN, logPN)
      } else {
        loop(muN, lN, logPN, lM1, logPM1, logPTarget)
      }
    }

    val lM2: Double = n
    val lM1: Double = 2 * n
    val logPM1 = 0.0
    val logPM2: Double = 0.0

    val (nL, logP) = loop(mu, lM1, logPM1, lM2, logPM2, logPTarget)
    println(s"target log likelihood value = $logPTarget")
    println(s"log likelihood value = $logP")
    println(s"Required sample size = $logP")

  }

  def readData(fileName: String): Array[(Int, Int)] = {
    val nums = """(\d+) (\d+)""".r
    val heads = """(\d+)""".r
    val lines = io.Source.fromFile(fileName).getLines
    lines.drop(1)
    val tuples = lines collect {
      case nums(label, num) => (label.toInt -> num.toInt)
      case heads(num) => (num.toInt -> num.toInt)
    }
    tuples.toArray
  }

  def readDataPosteriorEstimates(fileName: String, dropNumerator: Double, dropDenominator: Double): (Array[MCTraceRecord], Array[String]) = {
    if (!(new File(fileName)).exists) {
      throw new IOException("File " + fileName + " does not exists!")
    }
    val lines = io.Source.fromFile(fileName).getLines
    val heads = lines.next().toString().split(",")
    val t0: Iterator[MCTraceRecord] = lines collect { case line: String => extractValues(line) }
    val t0L: List[MCTraceRecord] = t0.toList
    val length = t0L.length
    val n2Drop: Int = (length * dropNumerator / dropDenominator).toInt
    val res = t0L.drop(n2Drop)
    (res.toArray, heads)
  }

  // nIter,mu,sigma,S,nLL,acceptance,n_accepted
  // 250000.0,-1.6,6.0,7053.0,279.90783301871124,-2.470024369053135,183969.0

  def readDataPosteriorEstimates(fileName: String, nLines2Skip: Int): (Array[MCTraceRecord], Array[String]) = {
    if (!(new File(fileName)).exists) {
      throw new IOException("File " + fileName + " does not exists!")
    }
    val lines = io.Source.fromFile(fileName).getLines
    val heads = lines.next().toString().split(",")
    val t0: Iterator[MCTraceRecord] = lines collect { case line: String => extractValues(line) }
    val t0L: List[MCTraceRecord] = t0.toList
    val n2Drop: Int = (nLines2Skip).toInt
    val res = t0L.drop(n2Drop)
    (res.toArray, heads)
  }

  def readDataPosteriorEstimates(fileName: String, nLines2Skip: Int, withHeaders: Boolean): (Array[MCTraceRecord], Array[String]) = {
    if (!(new File(fileName)).exists) {
      throw new IOException("File " + fileName + " does not exists!")
    }
    val lines = io.Source.fromFile(fileName).getLines
    val heads = if (withHeaders) {
      lines.next().toString().split(",")
    } else {
      Array("")
    }
    val t0: Iterator[MCTraceRecord] = lines collect { case line: String => extractValues(line) }
    val t0L: List[MCTraceRecord] = t0.toList

    val n2Drop: Int = (nLines2Skip).toInt
    val res = t0L.drop(n2Drop)
    (res.toArray, heads)
  }

  def extractValues(line: String): MCTraceRecord = {
    val a: Array[String] = line.toString().split(",")
    MCTraceRecord(a(1).toDouble, a(2).toDouble, a(3).toDouble)
  }

  def readDataPosteriorEstimates(fileName: String, dropNumerator: Double, dropDenominator: Double, withHeaders: Boolean): (Array[MCTraceRecord], Array[String]) = {
    if (!(new File(fileName)).exists) {
      throw new IOException("File " + fileName + " does not exists!")
    }
    val lines = io.Source.fromFile(fileName).getLines
    val heads = if (withHeaders) {
      lines.next().toString().split(",")
    } else {
      Array("")
    }
    val t0: Iterator[MCTraceRecord] = lines collect { case line: String => extractValues(line) }
    val t0L: List[MCTraceRecord] = t0.toList
    val length = t0L.length
    val n2Drop: Int = (length * dropNumerator / dropDenominator).toInt
    val res = t0L.drop(n2Drop)
    (res.toArray, heads)
  }

  def parseOptions(args: List[String], required: List[Symbol], optional: Map[String, Symbol], definedParameters: Map[Symbol, Any], iterations: Int): Map[Symbol, Any] = {
    val usage =
      """
						Usage: parser [-v] [-f file] [-s sopt] ...
						Where:
						Required parameters:\n"
						-mcmcf input file name for file with MCMC iterations
						-tadf input file name with
            -c is decimal number, required coverage of species = D/S, D - number of species in the sample, S - total number of species
      						"""

    if (args.length == 0 && iterations < 1) println(usage)
    args match {
      // Empty list
      case Nil => definedParameters

      // Keyword arguments
      case "-s" :: value :: tail =>
        parseOptions(tail, required, optional, definedParameters ++ Map(optional("-s") -> value.toInt), iterations + 1)
      case "-s0" :: value :: tail =>
        parseOptions(tail, required, optional, definedParameters ++ Map(optional("-s0") -> value.toInt), iterations + 1)
      case key :: value :: tail if optional.get(key) != None =>
        parseOptions(tail, required, optional, definedParameters ++ Map(optional(key) -> value), iterations + 1)

      // Positional arguments
      case value :: tail if required != Nil =>
        parseOptions(tail, required.tail, optional, definedParameters ++ Map(required.head -> value), iterations + 1)

      // Exit if an unknown argument is received
      case _ =>
        printf("unknown argument(s): %s\n", args.mkString(", "))
        sys.exit(1)
    }
  }

  def profile[R](code: => R, t: Long = nanoTime) = (code, nanoTime - t)

  case class OutputCSVWriters(correctOutput: CSVWriter, incorrectOutput: CSVWriter)

  case class InputParamsPartial(mainDir: File = new File("/media/sf_data/nps409/Work2Sync/Projects/emp_dev_scala_runs/TAD_DATA_Synthetic_Out/SRS711891_Synthetic_Out/AutoEval160816_160630_1444-36"), coverage: Double = 0.9, relTollerance: Double = 1.05, relTolleranceStrict: Double = 1.001, rootFindingMethod: String = "Pegasus")

  case class InputParamsAll(mcmcFileName: File = new File("/media/sf_data/nps409/Work2Sync/Projects/emp_dev_scala_runs/TAD_DATA_Synthetic_Out/SRS711891_Synthetic_Out/AutoEval160816_160630_1444-36"), tadFileName: File = new File("/media/sf_data/nps409/Work2Sync/Projects/emp_dev_scala_runs/TAD_DATA_Synthetic_Out/SRS711891_Synthetic_Out/AutoEval160816_160630_1444-36"), coverage: Double = 0.9, relTollerance: Double = 1.05, relTolleranceStrict: Double = 1.001, rootFindingMethod: String = "Pegasus")

  case class SampleSizeEstimateDS(nL: Int, mu: Double, variance: Double, multipleOfActualSampleSize: Double, nLSolution: Int, coverage: Double, nLObserved: Double, coverageTarget: Double = 0.0)

  case class Tollerance(relTollerance: Double, absTollerance: Double)

  //val n = 0
  class DevFromPTargetCalculator(variance: Double, logPTarget: Double) extends org.apache.commons.math3.analysis.UnivariateFunction {
    def value(mu: Double): Double = {
      assert(!mu.isInfinity && !mu.isNaN && !logPTarget.isInfinity && !logPTarget.isNaN, s"mu=$mu, logTarget=$logPTarget ")
      val pNew = logPLogNormal.calculateWithRLikeScala(mu, variance, n = 0)
      assert(!pNew.isInfinity && !pNew.isNaN)
      val diff = pNew - logPTarget
      diff
    }
  }

  class DevFromPTargetCalculator4DynamicS(variance: Double, logPTarget: Double) extends org.apache.commons.math3.analysis.UnivariateFunction {
    def value(mu: Double): Double = {
      assert(!mu.isInfinity && !mu.isNaN && !logPTarget.isInfinity && !logPTarget.isNaN, s"mu=$mu, logTarget=$logPTarget ")
      val pNew = logPLogNormal.calculateWithRLikeScala(mu, variance, n = 0)
      assert(!pNew.isInfinity && !pNew.isNaN)
      pNew - logPTarget
    }
  }

  case class ScalaEstimatesDS()

  case class MCTraceRecord(mu: Double, variance: Double, nCommunityTaxa: Double)

}
