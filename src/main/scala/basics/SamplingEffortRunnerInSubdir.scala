package basics

import java.io._
import java.nio.file.Files
import java.util.Arrays._

import joptsimple.{ArgumentAcceptingOptionSpec, OptionParser, OptionSet, OptionSpec, OptionSpecBuilder}
import SamplingEffort.{InputParamsAll, InputParamsPartial}
import joptsimple.util.RegexMatcher
import utils.{Enum4EMP, Enum4EMPSampleSize4Directory}

import scala.collection.mutable.ListBuffer
import scala.concurrent.duration.Duration
import scala.concurrent.{Await, Future}
import scala.reflect.internal.Required
import scala.concurrent.ExecutionContext.Implicits.global


/**
  * Created by peter on 03/10/16.
  */
object SamplingEffortRunnerInSubdir {


  val DEFAULT_TARGET_COVERAGE = 0.9

  val DEFAULT_RELATIVE_TOLLERANCE = 1.005

  val DEFAULT_STRICT_RELATIVE_TOLLERANCE = 1.001

  val DEFAULT_ROOT_FINDING_METHOD = "Pegasus"

  def getInputParams(args: Array[String]): SamplingEffort.InputParamsPartial = {
    val parser: OptionParser = new OptionParser() {
      acceptsAll(asList("h", "?"), "show help").forHelp()
    }
    val classString = Class.forName("java.lang.String");
    val mainDirSpec = parser.accepts("dir", "directory - calculate for all subdirs").withRequiredArg().ofType(classString)
    val rootFindingMethod = parser.accepts("rootFindingMethod", "root finding method").withRequiredArg().withValuesConvertedBy(RegexMatcher.regex(".*")).defaultsTo(DEFAULT_ROOT_FINDING_METHOD)
    val targetCoverageOption: ArgumentAcceptingOptionSpec[java.lang.Double] = parser.acceptsAll(asList("c", "coverage"), "species coverage").withRequiredArg().ofType(java.lang.Double.TYPE).defaultsTo(DEFAULT_TARGET_COVERAGE)
    val relTolleranceSpec: ArgumentAcceptingOptionSpec[java.lang.Double] = parser.acceptsAll(asList("relTollerance"), "relative tollerance for ").withRequiredArg().ofType(java.lang.Double.TYPE).defaultsTo(DEFAULT_RELATIVE_TOLLERANCE)
    val strictTolleranceOption: ArgumentAcceptingOptionSpec[java.lang.Double] = parser.accepts("strictTollerance", "strict relative numeric error for root finding").withRequiredArg().ofType(java.lang.Double.TYPE).defaultsTo(DEFAULT_STRICT_RELATIVE_TOLLERANCE)

    parser.acceptsAll(asList("h", "?"), "show help").forHelp();

    val optionsSet: OptionSet = parser.parse(args: _*)

    parser.printHelpOn(System.out)


    val method = rootFindingMethod.value(optionsSet).toString
    val mainDir = mainDirSpec.value(optionsSet).toString
    val targetCoverage = targetCoverageOption.value(optionsSet)
    val relTollerance = relTolleranceSpec.value(optionsSet)
    val relStrictTollerance = strictTolleranceOption.value(optionsSet)
    InputParamsPartial(new java.io.File(mainDir), targetCoverage, relTollerance, relStrictTollerance, method)
  }

  def main(args: Array[String]): Unit = {
    val params: InputParamsPartial = getInputParams(args)
    //    val mainDir: String = "/media/sf_data/nps409/Work2Sync/Projects/emp_dev_scala_runs/TAD_DATA_Synthetic_Out/SRS711891_Synthetic_Out/AutoEval160816_160630_1444-36"
    print("")
    browsDataSubfolders(params)

  }

  sealed abstract class FileDirRegExPattern(val tadFileNamePattern: String, val posteriorDatafilePatternStr:String,val posteriorDir:String) extends FileDirRegExPattern.Value

  object FileDirRegExPattern extends Enum4EMPSampleSize4Directory[FileDirRegExPattern] {
    val HUMAN_MICROBIOME = new FileDirRegExPattern("^44xSR(S711891|R15897XX)_(TAD|tad)(_.*){0,1}\\.sample$","^posterior-.*\\.csv$","s_\\d{1,8}K_\\d{6}-\\d{4}-\\d{2}") {}
    val REAL_DATA_S = new FileDirRegExPattern("^([a-zA-Z0-9]*)_(TAD|tad)\\.(sample)$","^posterior-.*\\.csv$","s_\\d{1,8}K_\\d{6}-\\d{4}-\\d{2}") {}
    val REAL_DATA_C = new FileDirRegExPattern("^([a-zA-Z0-9]*)_(TAD|tad)\\.(sample)$","^posterior-.*\\.csv$","c_\\d{1,8}K_\\d{6}-\\d{4}-\\d{2}") {}
    //  posteriorDatafilePatternStr = "^posterior-.*\\.csv$"

    //    val DEFAULT_VALUE = CSV
  }


  def browsDataSubfolders(params: InputParamsPartial): Unit = {
    val mainDir = params.mainDir.getAbsolutePath
    val sampleDirs: List[File] = getDataSubdirs(mainDir)
    sampleDirs.sortWith((f1, f2) => f1.getName.compareTo(f2.getName) < 0)

    def processADataSubfolderLoc = processADataSubfolderBase(_: File, _: InputParamsPartial)(FileDirRegExPattern.REAL_DATA_S.tadFileNamePattern)

    val dtR = sampleDirs.foreach(processADataSubfolderLoc(_, params))
    print("")
  }


  def getDataSubdirs(mainDir: String): List[File] = {
    val dataDirs = new File(mainDir)
    val res = if (dataDirs.exists && dataDirs.isDirectory) {
      dataDirs.listFiles.filter(_.isDirectory).toList
    } else {
      List[File]()
    }
    val res2 = res.sortBy(_.getName)
    res2
  }

  def getSingleFileNameWithPattern(dir: File, filePatternStr: String = "") = {
    val files = getSubFiles(dir, filePatternStr)
    files.lift(0)
  }

  def getSubFiles(dir: File, filePatternStr: String) = {
    val d = dir
    val res = if (d.exists && d.isDirectory) {
      d.listFiles.filter(f => f.isFile && filePatternStr.r.findFirstIn(f.getName).isDefined).toList
    } else {
      List[File]()
    }
    res.sortBy(_.getName)
  }

  def processPosteriorDataDir(posteriorParamsDir: File, posteriorProbabPattern: String = "^posterior-.*\\.csv$"): Unit = {
    //    val posteriorProbabPattern: String = "^posterior-.*\\.csv$"
    //    val fileNameWithPosteriors = getFileNamesWithPosteriors(posteriorParamsDir, posteriorProbabPattern) match {
    //      case Some(files:List[File]) => files
    //      case None => {
    //        println(s"File not in directory $posteriorParamsDir")
    //        throw new Exception(s"Now file with posterior distribution of parameters found!")
    //      }
    //    }
  }

  def processDataPosteriorFile(posteriorDataFile: File, fL: ListBuffer[Future[String]], tadFile: File, params: InputParamsPartial): ListBuffer[Future[String]] = {
    val paramsAll: InputParamsAll = InputParamsAll(posteriorDataFile, tadFile, params.coverage, params.relTollerance, params.relTolleranceStrict, params.rootFindingMethod)
    val lsOfFutures: Future[String] = scala.concurrent.Future {
      SamplingEffort.runSingleSamplingEffort(paramsAll)
    }
    println("")
    fL += lsOfFutures
  }

  def processDataPosteriorFiles(posteriorDir: File, tadFile: File, params: InputParamsPartial): Unit = {
    val posteriorDatafilePatternStr = "^posterior-.*\\.csv$"
    val posteriorDataFiles = getFileNamesWithPosteriors(posteriorDir, posteriorDatafilePatternStr)
    val sampleSizeFutures: ListBuffer[Future[String]] = scala.collection.mutable.ListBuffer.empty[Future[String]]
    val res = posteriorDataFiles.foldLeft(sampleSizeFutures)((fL: ListBuffer[Future[String]], f: File) => processDataPosteriorFile(f, fL, tadFile, params))
    val fs = Future.sequence(res.toList)
    val res2 = Await.result(fs, Duration.Inf)
    println("--- Finished for all file in main directory")
  }


  def processADataSubfolder(dataDir: File, params: InputParamsPartial): Unit = {
    val posteriorDataDirs = if (dataDir.exists && dataDir.isDirectory) {
      dataDir.listFiles.filter(_.isDirectory).toList.sortBy(_.getName)
    } else {
      List[File]()
    }

    println("")
    val tadDataPattern: String = "^44xSR(S711891|R15897XX)_(TAD|tad)(_.*){0,1}\\.sample$"
    val fileNameWithTADData = getSingleFileNameWithPattern(dataDir, tadDataPattern) match {
      case Some(file) => file
      case None => {
        println(s"File not in directory $dataDir")
        throw new Exception(s"No file with $tadDataPattern with posterior distribution of parameters found in $dataDir!")
      }
    }

    posteriorDataDirs.filter(_.isDirectory).foreach(processDataPosteriorFiles(_, fileNameWithTADData, params))
    //    val firstTADFileFull = tadFiles.lift(0)
    println("")
  }

  def processADataSubfolderBase(dataDir: File, params: InputParamsPartial)(tadFilePattern: String): Unit = {
    val posteriorDataDirs = if (dataDir.exists && dataDir.isDirectory) {
      dataDir.listFiles.filter(_.isDirectory).toList.sortBy(_.getName)
    } else {
      List[File]()
    }

    println("")
    // val tadDataPattern: String = "^44xSR(S711891|R15897XX)_(TAD|tad)(_.*){0,1}\\.sample$"
    val fileNameWithTADData = getSingleFileNameWithPattern(dataDir, tadFilePattern) match {
      case Some(file) => file
      case None => {
        println(s"File not in directory $dataDir")
        throw new Exception(s"No file with $tadFilePattern with posterior distribution of parameters found in $dataDir!")
      }
    }

    posteriorDataDirs.filter(f=>f.isDirectory && FileDirRegExPattern.REAL_DATA_S.posteriorDir.r.findFirstIn(f.getName).isDefined ).foreach(processDataPosteriorFiles(_, fileNameWithTADData, params))
    //    val firstTADFileFull = tadFiles.lift(0)
    println("")
  }


  def getFileNameWithPosteriors(dir: File, filePatternStr: String = "^posterior-.*\\.csv$") = {
    val files = getSubFiles(dir, filePatternStr)
    files.lift(0)
  }

  def getFileNamesWithPosteriors(dir: File, filePatternStr: String = "^posterior-.*\\.csv$"): List[File] = {
    getSubFiles(dir, filePatternStr)
  }

  def getSubdirs(dir: String) = {
    val d = new File(dir)
    val res = if (d.exists && d.isDirectory) {
      d.listFiles.filter(_.isDirectory).toList
    } else {
      List[File]()
    }
    res
  }

}
