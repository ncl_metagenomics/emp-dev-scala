package basics

import java.io.File

import scala.annotation.tailrec
import scala.util.Try

object CopyAllFlilesFromTo {

  def copyFilesWithPattern(inputDirectoryName: String, outDirectoryName0: String, strings: List[String]): Seq[Try[Long]] = {
    //    FileUtils.getDateAsString()
    //    FileUtils.copyFileToDir()
    val filesList: List[File] = basics.ResultsCollector.getSubFiles(inputDirectoryName, strings)
    filesList.map(f => utils.FileUtils.copyFileToDir(f.getCanonicalPath, outDirectoryName0))
  }


  def copyFiles(dirFrom: String, dirTo: String, patterns: List[String]): Unit = {
    patterns match {
      case pFile :: Nil => {
        print(s"File pattern $pFile")
        copyFilesWithPattern(dirFrom, dirTo, List[String](pFile))


      }
      case pDir :: tail => {
        println(s"dir pattern $pDir");
        val dirs = basics.ScalaResultsCollectorIG.getSubdirsWPattern(dirFrom, pDir)
        println(dirs.mkString("\n"))
        for {
          dir <- dirs

        } {
          var dirNameOnly = dir.getName
          var fullToDir = dirTo + File.separator + dirNameOnly
          println(s"$fullToDir")
          copyFiles(dir.getAbsolutePath, fullToDir, tail)
        }

        //        dirs.foreach(dir=>copyFiles(dir, dirTo, tail))

      }
      case _ => throw new Exception("could copy files")

    }
    //  Try(copyScripts(inputDirectoryName, outDirectoryName0, List(""".*\.sh$ """, """.*\.dmtcp$""")))

  }

  def main(args: Array[String]): Unit = {
    val dirsFrom = List(
      "/mnt/topsy/share/nobackup/nps409/EBI/emp_dev_scala_runs/TAD_DATA_Real_Out_4_var_distr/IG_Distr/synth/synth_sample_r_0.1_d_180124_1219-52_r2_0.3_d2_180226_1037-05/",
      "/mnt/topsy/share/nobackup/nps409/EBI/emp_dev_scala_runs/TAD_DATA_Real_Out_4_var_distr/IG_Distr/synth/synth_sample_r_0.1_d_180124_1219-52_r2_0.05_d2_180226_1032-17/",
      "/mnt/topsy/share/nobackup/nps409/EBI/emp_dev_scala_runs/TAD_DATA_Real_Out_4_var_distr/IG_Distr/synth/synth_sample_r_0.1_d_180124_1219-52_r2_0.2_d2_180225_1431-36/"
    )

    val dirsTo = List(
      "/mnt/topsy/share/nobackup/nps409/EBI/emp_dev_scala_runs/TAD_DATA_Real_Out_4_var_distr/IG_Distr/synth/Script_CQ_100_pc_v00_synth_sample_r_0.1_d_180124_1219-52_r2_0.3_d2_180226_1037-05_180302_0348-02/",
      "/mnt/topsy/share/nobackup/nps409/EBI/emp_dev_scala_runs/TAD_DATA_Real_Out_4_var_distr/IG_Distr/synth/Script_CQ_100_pc_v00_synth_sample_r_0.1_d_180124_1219-52_r2_0.05_d2_180226_1032-17_180302_0332-03/",
      "/mnt/topsy/share/nobackup/nps409/EBI/emp_dev_scala_runs/TAD_DATA_Real_Out_4_var_distr/IG_Distr/synth/Script_CQ_100_pc_v00_synth_sample_r_0.1_d_180124_1219-52_r2_0.2_d2_180225_1431-36_180302_0309-22/"
    )

    val dirFrom = dirsFrom(0)


    val dirTo = dirsTo(0)
    val patterns = List[String](".*", "^tad_.*\\.(sample|csv)$")
    copyFiles(dirFrom, dirTo, patterns)
  }

}
