import java.io.File

import ExperimentScalaRunner.{constructOutputDirName, copyScripts, createSoftLinkToDir}
import basics.{MCMC, MetropolisHMCMC}
import breeze.linalg.{DenseMatrix, DenseVector, diag}
import breeze.stats.distributions.{Gaussian, MarkovChain, MultivariateGaussian, Uniform}
import com.github.tototoshi.csv.CSVWriter
import numerics.IGMetropolisMCMC
import numerics.IGMetropolisMCMC._
import utils.ReadingExperimentParameters
import utils.ReadingExperimentParameters.{InputParametersMCMCAndSamplingEffort, InputParametersMCMCGeneral}

import scala.util.{Failure, Success, Try}
import numerics.mcmc._
import numerics.mcmc.thinning.ThinnableSyntax._
import numerics.mcmc.thinning.DefaultThinnable._
import utils.Statistics4MCMC.{getCholeskyLowerTriangular, getRescaledCovariance}


object ExperimentScalaRunner4IG {

  def time[A](f: => A) = {
    val s = System.nanoTime
    val ret = f
    println("time: " + (System.nanoTime - s) / 1e6 + "ms")
    ret
  }

  def copyUsefullFiles(inputDirectoryName: String, outDirectoryName0: String) = {
    Try(copyScripts(inputDirectoryName, outDirectoryName0, List(""".*\.sh$ """, """.*\.dmtcp$""")))
    Try(copyScripts(inputDirectoryName, inputDirectoryName, List("""output\.txt$""", """error\.txt$""")))
    Try(copyScripts(inputDirectoryName, outDirectoryName0, List(""".*\.log$""")))
  }

  def createSoftLinks(inputDirectoryName: String, outDirectoryName0: String) = {
    Try(createSoftLinkToDir(outDirectoryName0, inputDirectoryName))
  }


  def writeIGTableHeader(outFileNamePosteriorParams: String): CSVWriter = {
    val writer = CSVWriter.open(outFileNamePosteriorParams)
    val header = List("nIter", "S", "alpha", "beta")
    writer.writeRow(header)
    writer
  }

  def writeInitialIGParameters(csvWriter: CSVWriter, paramsInit: DenseVector[Double]) = {
    csvWriter.writeRow(List(-1) ++ paramsInit.toArray.toList
    )
  }


  def executeExperimentScalaRunner(args: Array[String]): Unit = {
    import numerics.mcmc.thinning.ThinnableSyntax._
    import numerics.mcmc.thinning.DefaultThinnable._

    // val clOptions: InputParametersMCMCAndSamplingEffort = ReadingExperimentParameters.getInputParams4MCMCAndSamplingEffort(args)

    val clOptionsGeneral: InputParametersMCMCGeneral = ReadingExperimentParameters.getInputGeneralParams4MCMCA(args)
    // ****************************************************************************
    // ***
    // ****************************************************************************hg

    val distribution = clOptionsGeneral.distribution
    //"ig"
    var fileNameTADFile = clOptionsGeneral.tadFileName //"src/main/resources/Data/test02.sample"
    val fileNameTAD = fileNameTADFile.getAbsolutePath
    val fileNameOnlyTAD = fileNameTADFile.getName ///"test02.sample"
    //    fileNameTAD = "src/main/resources/Data/test.sample"
    val nIter = clOptionsGeneral.nMCMCIterations //10
    //    val thinSkip = 2
    // Specific Parameters

    // **********
    val data: Array[(Int, Int)] = (if (fileNameOnlyTAD.matches(""".*\.csv$""")) Success(MetropolisHMCMC.readDataASCSVTAD(fileNameTAD))
    else if (fileNameOnlyTAD.matches(""".*\.sample""")) Success(MetropolisHMCMC.readData(fileNameTAD))
    else Failure(new Exception("Input file must be *.csv or *.sample file"))).get
    val nD = basics.MetropolisHMCMC.getD(data)

    //8888888888888888888888888888888
    def trasformValue(distribution: String, value: Double): Double = {
      val res: Double = distribution match {
        case "ig" => value
        case "igln" => scala.math.log(value)
        case _ => throw new Exception("Unknown distribution. transformValue()"); 0
      }
      res
    }

    //**************************
    val nSInit0: Double = distribution match {
      case "ig" => clOptionsGeneral.nSpeciesInit match {
        case Success(nS0: Double) if nS0 >= nD => trasformValue(distribution, nS0)
        case Success(nS0: Double) if nS0 < nD => trasformValue(distribution, nD * 1.1)
        case Failure(e) => trasformValue(distribution, 2 * nD) //2 * nD
      }
      case "igln" => clOptionsGeneral.nSpeciesInit match {
        case Success(nS0: Double) if scala.math.exp(nS0) >= nD => nS0
        case Success(nS0: Double) if scala.math.exp(nS0) < nD => trasformValue(distribution, nD * 1.1)
        case Failure(e) => trasformValue(distribution, 2 * nD) //2 * nD
      }

    }
    // todo: 171106 work here
    val nSInit: Double = nSInit0
    val nSInitExp = scala.math.exp(nSInit)
    //********************;

    val resTry: Unit = distribution match {
      case s: String if s.equalsIgnoreCase("ig") => println("IG distribution chosen.")

        def isProposalValid(t: DenseVector[Double]): Boolean = t(0) > nD && t(1) > 0 && t(2) > 0
        // ***************************
        // *** Get Program Parameters for IG
        // ***************************

        val clOptions2ParamsDistribution: ReadingExperimentParameters.Input42ParametersDistribution = ReadingExperimentParameters.getInput_4_2_ParamsDistribution(args)

        val clOptionsIGDistribution: ReadingExperimentParameters.InputParams_IG_Distribution = ReadingExperimentParameters.getInput_IG_Distribution(args)

        //*** General parameters aducstment
        /**
          * Parameters array : number_of_species, mean, shape
          */
        val paramsInit = DenseVector(nSInit /*clOptionsGeneral.nSpeciesInit.getOrElse(2*nD).toDouble*/ , clOptionsIGDistribution.alpha0, clOptionsIGDistribution.beta0) // DenseVector(2 * nD, 3.2, 3.5)

        val varCovarFileName: Option[File] = clOptions2ParamsDistribution.varCovarFile
        // List[String]("S", "alpha", "beta")
        val covarColumns = Map("S" -> Double, "alpha" -> Double, "beta" -> Double)
        val covarianceMatrix: DenseMatrix[Double] = getVarCovar(distribution, clOptions2ParamsDistribution, varCovarFileName, covarColumns)

        printParamInfo2Console(nSInit, clOptionsIGDistribution)

        // *** Getting values ***
        //        clOptionsGeneral.
        //todo:171105 work here
        // *** Output File Name Construction ***
        //        val outDirectoryName0: String = ExperimentScalaRunner.constructOutputDirName(clOptionsGeneral,clOptions2ParamsDistribution,clOptionsIGDistribution, clOptionsGeneral.outDir.getAbsolutePath)
        val outDirectoryName00: String = clOptionsGeneral.outDir.getAbsolutePath
        val outDirectoryName0: String = ExperimentScalaRunner.constructOutputDirName(clOptionsGeneral, outDirectoryName00)
        val outFileNamePosteriorParams = MCMC.modifyFileName(fileNameTAD, outDirectoryName0, clOptionsGeneral.version, clOptionsGeneral.estimateWhat)

        // =========================================================================
        //*** Function for evaluating conditional log likelihood
        def posLogLScalaJNA: DenseVector[Double] => Try[Double] = numerics.IGMetropolisMCMC.posLogLikelihood(_)(data)

        // *** Create Soft Links  ***
        val inputDirectoryName: String = new File(".").getCanonicalFile().getAbsolutePath

        println(s"Current directorry= ${inputDirectoryName}")
        copyUsefullFiles(inputDirectoryName, outDirectoryName0)
        createSoftLinks(inputDirectoryName, outDirectoryName0)
        println(s"Current directorry= ${inputDirectoryName}")

        Try(createSoftLinkToDir(outDirectoryName0, inputDirectoryName))
        //*** write table header

        val csvWriter: CSVWriter = writeIGTableHeader(outFileNamePosteriorParams)
        writeInitialIGParameters(csvWriter, paramsInit)
        // ****************************************
        // *** MCMC loop for estimation
        // ****************************************
        val res = runMCMC(clOptionsGeneral, nIter, isProposalValid _, paramsInit, covarianceMatrix, posLogLScalaJNA, csvWriter)
        printFinishingInfo2Console(paramsInit, outFileNamePosteriorParams)
      case s: String if s.equalsIgnoreCase("igln") =>
        println("IG with natural logarithm ln/log_e transformation distribution chosen.")

        val nD = MetropolisHMCMC.getD(data)


        def isProposalValid(t: DenseVector[Double]): Boolean = {
          val r = t(0) > Math.log(nD) && t(0) < 21 && t(1) > -7 && t(1) < 10 && t(2) > -7 && t(2) < 10;
          r
        }

        val clOptions2ParamsDistribution: ReadingExperimentParameters.Input42ParametersDistribution = ReadingExperimentParameters.getInput_4_2_ParamsDistribution(args)

        val clOptionsIGDistribution: ReadingExperimentParameters.InputParams_IG_Distribution = ReadingExperimentParameters.getInput_IG_Distribution(args)

        val paramsInit = DenseVector(nSInit /*clOptionsGeneral.nSpeciesInit.getOrElse(2*nD).toDouble*/ , clOptionsIGDistribution.alpha0, clOptionsIGDistribution.beta0) // DenseVector(2 * nD, 3.2, 3.5)

        val varCovarFileName: Option[File] = clOptions2ParamsDistribution.varCovarFile

        val covarColumns = Map("ln_S" -> Double, "ln_alpha" -> Double, "ln_beta" -> Double)

        val covarianceMatrix: DenseMatrix[Double] = getVarCovar(distribution, clOptions2ParamsDistribution, varCovarFileName, covarColumns)
        printParamInfo2Console(nSInit, clOptionsIGDistribution)
        //======================================================================================
        val outDirectoryName00: String = clOptionsGeneral.outDir.getAbsolutePath
        val outDirectoryName0: String = ExperimentScalaRunner.constructOutputDirName(clOptionsGeneral, outDirectoryName00)

        val outFileNamePosteriorParams = MCMC.modifyFileName(fileNameTAD, outDirectoryName0, clOptionsGeneral.version, clOptionsGeneral.estimateWhat)

        // =========================================================================
        // =========================================================================
        //*** Function for evaluating conditional log likelihood
        def posLogLScalaJNAWithLogParams: DenseVector[Double] => Try[Double] = numerics.IGMetropolisMCMC.posLogLikelihoodWithLogParams(_)(data)(nD)

        // *** Create Soft Links  ***
        val inputDirectoryName: String = new File(".").getCanonicalFile().getAbsolutePath

        println(s"Current directorry= ${inputDirectoryName}")
        copyUsefullFiles(inputDirectoryName, outDirectoryName0)
        createSoftLinks(inputDirectoryName, outDirectoryName0)
        println(s"Current directorry= ${inputDirectoryName}")

        Try(createSoftLinkToDir(outDirectoryName0, inputDirectoryName))
        //*** write table header

        val csvWriter: CSVWriter = writeIGTableHeader(outFileNamePosteriorParams)
        //        writeInitialIGParameters(csvWriter, paramsInit)
        // ****************************************
        // *** MCMC loop for estimation
        // ****************************************
        //todo:180326 work here
        val res = runMCMCWithLnTransform(clOptionsGeneral, nIter, isProposalValid _, paramsInit, covarianceMatrix, posLogLScalaJNAWithLogParams, csvWriter)
        printFinishingInfo2Console(paramsInit, outFileNamePosteriorParams)

      case _ =>
        Failure(new Exception("Unsupported distribution. Allowed values ig (Inverse Gaussian)"))

    }
    println(s"${resTry}")
    println("------------------------------------------------------------")
    println("")
  }

  private def printFinishingInfo2Console(paramsInit: DenseVector[Double], outFileNamePosteriorParams: String) = {
    println("------------------- Parameters --------------------------------------Ł---")
    println(s"${paramsInit.toString()}")
    println("------------------------------------------------------------")
    println(s"output written into ${outFileNamePosteriorParams}")
    println("------------------------- Output Dir -----------------------------------")
  }

  private def runMCMC(clOptionsGeneral: InputParametersMCMCGeneral, nIter: Int, isProposalValid: DenseVector[Double] => Boolean, paramsInit: DenseVector[Double], covarianceMatrix: DenseMatrix[Double], posLogLScalaJNA: DenseVector[Double] => Try[Double], csvWriter: CSVWriter) = {
    MarkovChainMG.metropolis[DenseVector[Double]](
      paramsInit,
      (x: DenseVector[Double]) => MultivariateGaussian(x, covarianceMatrix)
    )(
      (x) => posLogLScalaJNA(x)
    )(
      x => isProposalValid(x)
    ).steps.zipWithIndex.take(nIter).toStream.thin(clOptionsGeneral.thinningInterval).toIterator.foreach(x => csvWriter.writeRow((List(x._2) ::: x._1.toArray.toList)))
  }

  private def runMCMCWithLnTransform(clOptionsGeneral: InputParametersMCMCGeneral, nIter: Int, isProposalValid: DenseVector[Double] => Boolean, paramsInit: DenseVector[Double], covarianceMatrix: DenseMatrix[Double], posLogLScalaJNA: DenseVector[Double] => Try[Double], csvWriter: CSVWriter) = {
    MarkovChainMG.metropolis[DenseVector[Double]](
      paramsInit,
      (x: DenseVector[Double]) => MultivariateGaussian(x, covarianceMatrix)
    )(
      (x) => posLogLScalaJNA(x)
    )(
      x => isProposalValid(x)
    ).steps.zipWithIndex.take(nIter).toStream.thin(clOptionsGeneral.thinningInterval).toIterator.foreach(x => printRowWithLnTranformation(csvWriter, x))
  }

  def printRowWithLnTranformation(csvWriter: CSVWriter, row: (DenseVector[Double], Int), numParams: Int = 3): Unit = {
    //    println(s"Num cols in row ${row._1.length}")
    val parMatrix = row._1(0 to numParams - 1)
    val parList0: List[Double] = parMatrix.toArray.toList.map(x => scala.math.exp(x)).toList
    val parList: List[Double] = List(parList0(0).round.toDouble) ::: parList0.slice(1, parList0.length)
    //    val endList: List[Double] = row._1(3 to row._1.size).toArray.toList
    //    val rowOut = List(row._2)::: parList:::endList
    val rowOut = List(row._2) ::: parList
    println(rowOut.mkString(", "))
    csvWriter.writeRow(rowOut)
  }

  private def printParamInfo2Console(nSInit: Double, clOptionsIGDistribution: ReadingExperimentParameters.InputParams_IG_Distribution) = {
    println("***************************************************")
    println("init parameters:")
    println(s"Used parameter nSInit = ${nSInit}")
    println(s"Used parameter alpha0 = ${clOptionsIGDistribution.alpha0}")
    println(s"Used parameter beta0 = ${clOptionsIGDistribution.beta0}")
    println("===================================================")
  }

  private def getVarCovar(distribution: String, clOptions2ParamsDistribution: ReadingExperimentParameters.Input42ParametersDistribution, varCovarFileName: Option[File], covarColumns: Map[String, Double.type]) = {
    varCovarFileName match {
      case Some(f) => getVarCovarianceMatrix(varCovarFileName, distribution, clOptions2ParamsDistribution.varianceScalingFactor.get, colTypes = covarColumns) match {
        case Some(matrix) => matrix
        case None => throw new Exception("Couldn't read matrix"); diag(DenseVector(90000, 0.027308725397199522, 0.013288741148109576))
      }

      case None => throw new Exception(s"No var-covar file was provided"); diag(DenseVector(90000, 0.027308725397199522, 0.013288741148109576))

    }
  }

  def getVarCovarianceMatrix(correlationFile: Option[File], distribution: String, scalingFactor: List[Double], colTypes: Map[String, Double.type]): Option[DenseMatrix[Double]] = {
    //todo:170310 call appropriate conversion for distribution
    //    val colTypes: Map[String, Double.type] = Map("mu" -> Double, "variance" -> Double, "S" -> Double)
    // distribution: Lognormal
    val r: Option[DenseMatrix[Double]] = correlationFile match {
      case Some(fileName: File) => {
        //todo: 170312 work here
        val r: Option[DenseMatrix[Double]] = if (fileName.exists) {
          val covarianceMatrix0: Try[DenseMatrix[Double]] = Try(utils.Statistics4MCMC.readMatrix3By3(fileName, colTypes))
          val covarianceMatrix: DenseMatrix[Double] = covarianceMatrix0 match {
            case Success(covM) =>
              val scaledCovariance: DenseMatrix[Double] = getRescaledCovariance(covM, scalingFactor)
              scaledCovariance
            case Failure(e) => throw new Exception(s"Correlation matrix file name: ${fileName.getAbsolutePath}, colTypes = ${colTypes.toList}; ${e.getMessage}"); DenseMatrix(0d)

          }
          Some(covarianceMatrix)
        } else {
          throw new Exception(s"File ${fileName.getAbsolutePath} does not exist")
          None
        }
        r
        //        covarianceMatrix
        //        val covarianceMatrix: DenseMatrix[Double] = utils.Statistics4MCMC.readMatrix3By3(fileName, colTypes)
        //        scaledCovariance
        //        getCholeskyLowerTriangular(scaledCovariance)
      }
      case None => None
      case _ => throw new Error(s"Correlation matrix in file ${correlationFile}"); None
    }
    r
  }


  def main(args: Array[String]): Unit = {
    executeExperimentScalaRunner(args)
  }

}
