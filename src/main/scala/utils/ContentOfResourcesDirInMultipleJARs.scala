package utils

import java.io.File
import java.net.URL
import java.util
import java.util.jar.{JarEntry, JarFile}

import scala.util.{Failure, Success, Try}

/**
  * Created by peter on 23/09/16.
  */
object ContentOfResourcesDirInMultipleJARs {
  // ****************************************************
  def main(args: Array[String]): Unit = {
    listFilesOfResources("R")
    val rFileGraphsForSummary: Try[String] = getFullFileName("R", "graphsForSummary.R")
    println("Requested file is " + rFileGraphsForSummary.get + "!")
  }

  // **********************************************************************
  def getFullFileName(path: String, fileName: String): Try[String] = {
    val jarFile: File = new File(getClass().getProtectionDomain().getCodeSource().getLocation().getPath())
    //*********************************************
    val url: URL = this.getClass.getResource("/" + path) //class.getResource("/" + path)
    // ***********************************************************************************
    if (url != null) {
      val rFilesDir: File = new File(url.toURI())
      if (rFilesDir.exists() && rFilesDir.isDirectory) {
        val rFile: File = new File(rFilesDir.getAbsolutePath, fileName)
        if (rFile.exists()) Success(rFile.getAbsolutePath)
        else
          Failure(new Exception(s"File  ${rFile.getAbsolutePath} does not exist!"))
      } else Failure(new Exception(s"Directory ${rFilesDir.getAbsolutePath} does not exist!"))
    } else Failure(new Exception(s"url = $url"))
  }

  // **********************************************************************
  def listFilesOfResources(path: String = "R"): Unit = {
    //    val path: String = "R"
    val jarFile: File = new File(getClass().getProtectionDomain().getCodeSource().getLocation().getPath())

    if (jarFile.isFile()) {
      // Run with JAR file
      val jar: JarFile = new JarFile(jarFile);
      val entries: util.Enumeration[JarEntry] = jar.entries(); //gives ALL entries in jar
      println("Files in JAR file")
      while (entries.hasMoreElements) {
        val name: String = entries.nextElement().getName();
        if (name.startsWith(path + "/")) {
          //filter according to the path
          System.out.println(name);
        }
      }

      jar.close();
    } else {
      // Run with IDE
      val url: URL = this.getClass.getResource("/" + path) //class.getResource("/" + path)
      if (url != null) {

        val apps: File = new File(url.toURI())
        println("Files in class directory for IDE")
        apps.listFiles().foreach(f => System.out.println(f))

      }
    }
  }


}
