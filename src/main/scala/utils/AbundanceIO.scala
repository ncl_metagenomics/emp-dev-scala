/**
  * Provides IO for abundance data
  */
package utils

import java.io.File

import basics.MetropolisHMCMC

import scala.util.{Failure, Success, Try}


/**
  * Provides reading and writing to files abundance data in TAD, abundance and OTU forms.
  */
object AbundanceIO {


  //  val headerAbundance = List("abundance")
  //  val headerTAD = List[String]("Abundance", "Num.Species")

  def main(args: Array[String]): Unit = {

  }

  def outputFileName4CommunityData(datasetName: String, dataFormat: String = "Abundance", scopeOfData: String = "Community", isGeneratedOrSynth: String = "Synthetic", coverage: Double = 1d, tadDistribution: String = "lognormal", dateTimeStamp: String, fileNameSuffix: String = "csv"): String = {
    val sep: String = "_"
    datasetName + "_" + dataFormat + "_" + scopeOfData + "_" + isGeneratedOrSynth + sep + "%05.1f".format(coverage * 100) + "pc" + sep + tadDistribution + sep + dateTimeStamp + "." + fileNameSuffix
  }


  case class BaseNameSuffix(baseName: Option[String], suffix: Option[String])

  /**
    *
    * @param name
    * @return (Option(baseName),Option(suffix/extension))
    */
  def splitFileNameToBaseNameAndSuffix(name: String): BaseNameSuffix = {
    val p = """(.*)\.([a-zA-Z]+$)""".r
    val p(nameOnly: String, suffix: String) = name
    BaseNameSuffix(Option(nameOnly), Option(suffix))
  }

  val dirSep: String = File.separator
  val strSepInName: String = "_"

  def outputFileName4SubCommunityData(previousFileNameOnly: String, inputDir: String, coverageSubsampling: Double, timeStampSubSampling: String /*atasetName: String, dataFormat: String = "Abundance", scopeOfData: String = "Community", isGeneratedOrSynth: String = "Synthetic", coverage: Double = 1d, tadDistribution: String = "lognormal", dateTimeStamp: String, fileNameSuffix: String = "csv",timeStampOfSubSampling:String*/): String = {
    val fileNameParts = splitFileNameToBaseNameAndSuffix(previousFileNameOnly)
    val fN: String = fileNameParts.baseName.get + strSepInName + "sub" + strSepInName + coverageSubsampling * 100 + "pc" + strSepInName + timeStampSubSampling + "." + fileNameParts.suffix.get
    fN
  }

  def outputDirName(datasetName: String, dataFormat: String = "Abundance", scopeOfData: String = "Community", isGeneratedOrSynth: String = "Synthetic", coverage: Double = 1d, tadDistribution: String = "lognormal", dateTimeStamp: String, fileNameSuffix: String = "csv"): String = {
    val sep: String = "_"
    datasetName + "_" + dataFormat + "_" + scopeOfData + "_" + isGeneratedOrSynth + sep + "%05.1f".format(coverage * 100) + "pc" + sep + tadDistribution + sep + dateTimeStamp
  }

  def readAbundanceFile(fileName: String): Unit = {

  }

  def getOutputFilePath(outDir: String, datasetName: String, dataFormat: String = "Abundance", scopeOfData: String = "Community", isGeneratedOrSynth: String = "Synthetic", coverage: Double = 1d, tadDistribution: String = "lognormal", dateTimeStamp: String, fileNameSuffix: String = "csv"): String = {
    val fNOnly: String = AbundanceIO.outputFileName4CommunityData(datasetName, dataFormat, scopeOfData, isGeneratedOrSynth, coverage, tadDistribution, dateTimeStamp, fileNameSuffix)
    outDir + File.separator + fNOnly
  }

  def readTADDAta(fileNameTAD: String): Option[Array[(Int, Int)]] = {
    val f = new File(fileNameTAD)
    readTADDAta(f)
  }

  def readTADDAta(f: File): Option[Array[(Int, Int)]] = {
    val dirN = f.getParent
    val fileNameOnlyTAD: String = f.getName
    val data: Option[Array[(Int, Int)]] = (if (fileNameOnlyTAD.matches(""".*\.csv$""")) Success(MetropolisHMCMC.readDataASCSVTAD(f.getAbsolutePath))
    else if (fileNameOnlyTAD.matches(""".*\.sample""")) Success(MetropolisHMCMC.readData(f.getAbsolutePath))
    else Failure(new Exception("Input file must be *.csv or *.sample file"))).toOption
    //      if (fileNameOnlyTAD.matches(""".*\.csv$""")) Some(MetropolisHMCMCO.readDataASCSVTAD(f.getAbsolutePath))
//    else if (fileNameOnlyTAD.matches(""".*\.sample""")) Some(MetropolisHMCMCO.readDataASCSVTAD(f.getAbsolutePath))
//    else throw new Exception("""Incorrect suffix in input file name, must be either "csv" or "sample" """)
    data
  }


}


case class Parameters4AbundanceFileName(datasetName: String)


//trait Enum[A <: {def name : String}] {
//
//  trait Value {
//    self: A =>
//    _values :+= this
//  }
//
//  private var _values = List.empty[A]
//
//  def values = _values
//}


/*
{datasetName}_${dataFormat}_${scopeOfData}_${generated}_${Coverage of speciest}_pc_${distribution}_${DateTime}.csv
parts:


${datasetName} – possible value: 44xSRS711891
${dataFormat} – Abundance or TAD
${scopeOfData} – Community or Sample
${generated} – with values Real or Synthetic
${Coverage of species}– fractional coverage of species, double values
${distribution} – type of TAD distribution
${DateTime} – date and time stamp

 */
