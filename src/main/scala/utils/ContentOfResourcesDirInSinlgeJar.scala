package utils

import java.io.File
import java.net.URL
import java.util
import java.util.jar.{JarEntry, JarFile}

import scala.util.{Failure, Success, Try}


/**
  * Created by peter on 22/09/16.
  */
object ContentOfResourcesDirInSinlgeJar {

  def main(args: Array[String]): Unit = {
    listFilesOfResources("R")
    val rFileGraphsForSummary = getFullFileName("R", "graphsForSummary.R")

    listFilesOfResources("Data/44xSRS711891_TAD__50_pc_D_7747.5_lognormal_160630_1444-36_16-07-01_1801-30")
    val rFileGraphsForSummary2 = getFullFileName("Data/44xSRS711891_TAD__50_pc_D_7747.5_lognormal_160630_1444-36_16-07-01_1801-30", "44xSRS711891_TAD__50_pc_D_7747.5_lognormal_160630_1444-36_16-07-01_1801-30.csv")

    println("")

  }

  def getFullFileName(path: String, fileName: String): Try[File] = {
    val jarFile: File = new File(getClass().getProtectionDomain().getCodeSource().getLocation().getPath())
    //*********************************************
    val url: URL = this.getClass.getResource("/" + path) //class.getResource("/" + path)
    val rFilePath = if (url == null) {
      val rFilesDir: Try[File] = Try(new File(url.toURI()))
      val res = rFilesDir match {
        case Success(f) => Try[File](new File(f.getAbsolutePath, fileName))
        case Failure(e) =>
          println("url is probably null - maybe accessing file in JAR")
          println("Information from exception " + e.getMessage)
          Failure(new Exception("url is probably null - maybe accessing file in JAR.\\n" + e.getMessage))
      }
      res
    } else {
      val rFilesDir: File = new File(url.toURI())
      val rest:Try[File ] = if (rFilesDir.exists() && rFilesDir.isDirectory) {
        val rFile: File = new File(rFilesDir.getAbsolutePath, fileName)
        Try(rFile)
      } else Failure(new Exception(s"File $fileName does not exist in ${rFilesDir.getAbsolutePath}"))
      rest
    }
    rFilePath
  }


  def listFilesOfResources(path: String = "R"): Unit = {
    //    val path: String = "R"
    val jarFile: File = new File(getClass().getProtectionDomain().getCodeSource().getLocation().getPath())

    if (jarFile.isFile()) {
      // Run with JAR file
      val jar: JarFile = new JarFile(jarFile);
      val entries: util.Enumeration[JarEntry] = jar.entries(); //gives ALL entries in jar
      println("Files in JAR file")
      while (entries.hasMoreElements) {
        val name: String = entries.nextElement().getName();
        if (name.startsWith(path + "/")) {
          //filter according to the path
          System.out.println(name);
        }
      }

      jar.close();
    } else {
      // Run with IDE
      val url: URL = this.getClass.getResource("/" + path) //class.getResource("/" + path)
      if (url != null) {

        val apps: File = new File(url.toURI())
        println("Files in class directory for IDE")
        apps.listFiles().foreach(f => System.out.println(f))

      }
    }
  }
}
