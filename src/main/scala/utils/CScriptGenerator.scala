package utils

import java.io.{File, FileWriter}

import utils.ScalaScriptGenerator.{getSubDirs, getCovareFileName, dateStr}

/**
  * Created by peter on 02/06/17.
  */
object CScriptGenerator {

  case class Params(niters: Int, compiledCCodePath: String, mainScriptDir: String, dataFileNamePartialOnly: String,cCQCodeFN:String)

  def main(args: Array[String]): Unit = {

    val mainDataDirs = List[String](
      "/mnt/topsy/share/nobackup/nps409/EBI/emp_dev_scala_runs/TAD_DATA_Real_Out_4_var_distr/IG_Distr/synth/synth_sample_r_0.1_d_180124_1219-52_r2_0.3_d2_180226_1037-05",
      "/mnt/topsy/share/nobackup/nps409/EBI/emp_dev_scala_runs/TAD_DATA_Real_Out_4_var_distr/IG_Distr/synth/synth_sample_r_0.1_d_180124_1219-52_r2_0.05_d2_180226_1032-17",
      "/mnt/topsy/share/nobackup/nps409/EBI/emp_dev_scala_runs/TAD_DATA_Real_Out_4_var_distr/IG_Distr/synth/synth_sample_r_0.1_d_180124_1219-52_r2_0.2_d2_180225_1431-36",
      "/mnt/topsy/share/nobackup/nps409/EBI/emp_dev_scala_runs/TAD_DATA_Real_Out_4_var_distr/IG_Distr/synth/synth_sample_r_0.1_d_180124_1219-52_r2_0.3_d2_180226_1037-05",
      "/mnt/topsy/share/nobackup/nps409/EBI/emp_dev_scala_runs/TAD_DATA_Real_Out_4_var_distr/IG_Distr/synth/synth_sample_r_0.1_d_180124_1219-52_r2_0.05_d2_180226_1032-17",
      "/mnt/topsy/share/nobackup/nps409/EBI/emp_dev_scala_runs/TAD_DATA_Real_Out_4_var_distr/IG_Distr/synth/synth_sample_r_0.1_d_180124_1219-52_r2_0.2_d2_180225_1431-36",
      "/mnt/topsy/share/nobackup/nps409/EBI/emp_dev_scala_runs/TAD_DATA_Real_Out_4_var_distr/IG_Distr/synth/synth_sample_r_0.1_d_180124_1219-52_r2_0.1_d2_180124_1228-40",
      "/mnt/topsy/share/nobackup/nps409/EBI/emp_dev_scala_runs/TAD_DATA_Real_Out_4_var_distr/IG_Distr",
      "/mnt/topsy/share/nobackup/nps409/EBI/emp_dev_scala_runs/TAD_DATA_Synthetic_Out/CQ_Data_Generator_170627_2302/Results_TAD_10_pc",
      "/mnt/topsy/share/nobackup/nps409/EBI/emp_dev_scala_runs/TAD_DATA_Synthetic_Out/CQ_Data_Generator_170627_2302/Results_TAD_40_pc",
      "/mnt/topsy/share/nobackup/nps409/EBI/emp_dev_scala_runs/TAD_DATA_Synthetic_Out/CQ_Data_Generator_170627_2302/Results_TAD_15_pc",
      "/mnt/topsy/share/nobackup/nps409/EBI/emp_dev_scala_runs/TAD_DATA_Synthetic_Out/CQ_Data_Generator_170627_2302/Results_TAD_80_pc_v01",
      "/mnt/topsy/share/nobackup/nps409/EBI/emp_dev_scala_runs/TAD_DATA_Synthetic_Out/CQ_Data_Generator_170627_2302/Results_TAD_60_pc_v01",
      "/mnt/topsy/share/nobackup/nps409/EBI/emp_dev_scala_runs/TAD_DATA_Synthetic_Out/CQ_Data_Generator_170627_2302/Results_TAD_50_pc_v01",


      "/mnt/topsy/share/nobackup/nps409/EBI/emp_dev_scala_runs/TAD_DATA_Synthetic_Out/CQ_Data_Generator_170627_2302/Results_TAD_50_pc_v01/FS312b_c_0.5_synthetic_S_170704_1814-19",

      "/media/sf_data/nps409/Other/Dropbox/Newcastle-Project/src/Projects/Quince-C/R/DATA_GENERATOR/CQ_Data_Generator_170627_2302/Results_TAD_50_pc_v01/",
      "/media/sf_Newcastle-Project/src/Projects/Quince-C/R/DATA_GENERATOR/CQ_Data_Generator_170627_2302/Results_TAD_10_pc/",

      "/mnt/topsy/share/nobackup/nps409/EBI/emp_dev_scala_runs/TAD_DATA_Synthetic_Out/CQ_Data_Generator_170627_2302/Results_TAD_5_pc_v01",
      "/mnt/topsy/share/nobackup/nps409/EBI/emp_dev_scala_runs/TAD_DATA_Synthetic_Out/CQ_Data_Generator_170627_2302/Results_TAD_40_pc",

      "/media/sf_data/nps409/Work2Sync/Projects/emp_dev_scala_runs/TAD_DATA_Synthetic_Out/QC_paper/CQ_Data_Generator_170627_2302/Results_TAD_80_pc/",
      "/media/sf_data/nps409/Work2Sync/Projects/emp_dev_scala_runs/TAD_DATA_Synthetic_Out/QC_paper/QC_170213_stopped_w_topsy",
      "/media/sf_data/nps409/Work2Sync/Projects/emp_dev_scala_runs/TAD_DATA_Synthetic_Out/Topsy/QC_Paper_170319_covar_hetero"
    )

    //    val compiledCCodePath = "/mnt/topsy/share/nobackup/nps409/EBI/emp_dev_scala_runs/C_Code/Quince-C/MetroLogNormal/MetroLogNormal"
//    val compiledCCodePath = "/mnt/topsy/share/nobackup/nps409/EBI/emp_dev_scala_runs/C_Code/Quince-C/MetroIG/"
    val compiledCCodePath = "/media/sf_data/nps409/Other/Dropbox/Newcastle-Project/src/Projects/Quince-C/MetroIG_original/MetroIGStatic"


    val mainDataDir = mainDataDirs(0)
    val parentDirOnly = new File(mainDataDir).getName

    val parentDir: String = new File(mainDataDir).getParent

    val p = """.*_(\d{1,3})_pc.*""".r


    val samplingRateInDataset = mainDataDir match {
      case p(c) => c.toInt
      case _ => 100
    }
    val pV = """.*_v(\d{1,3}).*""".r

    val dataVersion = mainDataDir match {
      case pV(c) => c
      case _ => "00"
    }

    val mainScriptDir = new File(parentDir,s"""Script_CQ_${samplingRateInDataset}_pc_v${dataVersion}_${parentDirOnly}_${dateStr}""").getCanonicalPath

    val mainScriptDir00 = s"/mnt/topsy/share/nobackup/nps409/EBI/emp_dev_scala_runs/TAD_DATA_Synthetic_Out/CQ_Data_Generator_170627_2302/Script_TAD_${samplingRateInDataset}_pc_DMTCP_data_v${dataVersion}_Script_${dateStr}"
    //    val mainScriptDir = mainScriptDirs(0)

    // "/media/sf_data/nps409/Work2Sync/Projects/emp_dev_scala_runs/TAD_DATA_Synthetic_Out/QC_paper/QC_170213_stopped_w_topsy"
    val dataDirs: Seq[File] = getSubDirs(mainDataDir,""".*[a-zA-z].*""")
    println(s"DIRS = ${dataDirs.mkString(",")}")

    val jarFile = "/media/sf_data/nps409/Work2Sync/Projects/emp_dev_scala_runs/jars/emp_dev_scala-assembly-1.0-170530-2057_56.jar"
    val jarDependencyFile = "/media/sf_data/nps409/Work2Sync/Projects/emp_dev_scala_runs/jars/emp_dev_scala-assembly-1.0-deps.jar"

    FileUtils.copyFileToDir(compiledCCodePath, mainScriptDir)

    val los00: Seq[(Option[String], Option[String])] = dataDirs.map(f => processSingleDataDir(f.getAbsolutePath, mainScriptDir, jarFile, jarDependencyFile, CScriptGenerator.Params(2400000, compiledCCodePath, mainScriptDir, "","MetroIGStatic"), generateShellScript, generateDMTCP))
    val (lShellScriptName, lDMTCPScriptName) = los00.unzip
    // ************************************************
    val shellScriptEntries = lShellScriptName.collect { case Some(s) => val f = new File(s); val p = f.getParent; val n = f.getName; s"""(cd ${p}; bash $n &)""" }
    val qsubShellScriptEntries = lShellScriptName.collect { case Some(s) => val f = new File(s); val p = f.getParent; val n = f.getName; s"""(cd ${p}; qsub $n)""" }
    // -----------------------------------
    val shellDMTCPEntries = lShellScriptName.collect { case Some(s) => val f = new File(s); val p = f.getParent; val n = f.getName; s"""(cd ${p}; qsub $n)""" }

    // ************************************************

    val lineSep = sys.props("line.separator")
    // ************************************************
    val fwShellScriptFW: FileWriter = new FileWriter(new File(mainScriptDir,s"""list_Shell_Scripts_C_${dateStr}.sh"""))
    fwShellScriptFW.write(shellScriptEntries.mkString(sep = lineSep))
    fwShellScriptFW.close()
    // ************************************************ // ************************************************
    val fwQSubShellScriptFW: FileWriter = new FileWriter(new File(mainScriptDir,s"""list_QSub_Shell_Scripts_C_${dateStr}.sh"""))
    fwQSubShellScriptFW.write(qsubShellScriptEntries.mkString(sep = lineSep))
    fwQSubShellScriptFW.close()
    // ************************************************

    // ************************************************
    val fwDMTCPScript = new FileWriter(new File(mainScriptDir,s"""list_DMTCP_Scripts_C_${dateStr}.sh"""))
    fwDMTCPScript.write(shellDMTCPEntries.mkString(sep = lineSep))
    fwDMTCPScript.close()
    // ===========================================
    println(s"""Scripts written into ${mainScriptDir}""")
    println(s"""Runs CQ code on data  ${mainDataDir}""")
  }


  def processSingleDataDir(dsDir: String, scriptDir: String, jarFile: String, jarDependencyFile: String, params: Params, generatorShellScriptFnc: (String, String, String, String, CScriptGenerator.Params) => String, generatorDMTCPScriptFnc: (String, String, String, String, CScriptGenerator.Params) => String): (Option[String], Option[String]) = {
    val dataFileFileStr: String = getCovareFileName(dsDir, """(?i).*TAD.*\.sample$""")
    println(s"dataFileFileStr = $dataFileFileStr")
    val varCovarFileStr: String = getCovareFileName(dsDir, """(?i).*varCovar.*adjusted.*partial.*\.csv$""")
    println(s"varCovarFileStr = $varCovarFileStr")
    // *** Dataset name ***
    val dataFileNamePartialOnly = (new File(dsDir)).getName()
    // *** Scriptr File path
    val scriptPath4DS: File = (new File(scriptDir, s"${dataFileNamePartialOnly}"))
    val scriptPath4DS2: File = (new File(scriptPath4DS, s"c-${dataFileNamePartialOnly}_${dateStr}"))
    // , "s-${dataFileNamePartialOnly}_${dateStr}"
    scriptPath4DS2.mkdirs()
    val shellScriptNameOutput: File = (new File(scriptPath4DS2, s"c-${dataFileNamePartialOnly}_${dateStr}.sh"))
    println(s"shellScriptNameOutput = $shellScriptNameOutput")
    (new File(scriptPath4DS2, "AUTO_RESTART_PLEASE")).createNewFile() //s"""AUTO_RESTART_PLEASE"""
    val dmtcpScriptNameOutput: File = (new File(scriptPath4DS2, s"c-dmtcp_${dataFileNamePartialOnly}_${dateStr}.qsub"))
    println(s"dmtcpScriptNameOutput = $dmtcpScriptNameOutput")

    // *** Relative Paths ****
    //    val relPathDataStr = MCMCIO.absoluteToRelativePath(scriptDir, dataFileFileStr)
    val relPathDataStr: Option[String] = MCMCIO.absoluteToRelativePath(scriptPath4DS2.getAbsolutePath, dataFileFileStr)
    println(s"Processing dir: ${scriptDir}")
    val relPathDataParentDirStr = new File(relPathDataStr.get).getParent
    val a = MCMCIO.absoluteToRelativePath(params.mainScriptDir, scriptPath4DS2.getAbsolutePath)


    val outputScriptNameStr = dmtcpScriptNameOutput.getName

    println(s"relPathDataStr = ${relPathDataStr.getOrElse("??????")}")
    val relPathVarCovarStr = MCMCIO.absoluteToRelativePath(scriptPath4DS2.getAbsolutePath, varCovarFileStr)
    println(s"relPathVarCovarStr  = ${relPathVarCovarStr.getOrElse("????")} ")
    val jarFileRelative = MCMCIO.absoluteToRelativePath(scriptPath4DS2.getAbsolutePath, jarFile)
    val jarDependencyFileRelative = MCMCIO.absoluteToRelativePath(scriptPath4DS2.getAbsolutePath, jarDependencyFile)
    //  *** Script Content ****
    val paramsNew = Params(params.niters, params.compiledCCodePath, params.mainScriptDir, dataFileNamePartialOnly,params.cCQCodeFN)
    // ********************************************************
    val scriptShellScript = generatorShellScriptFnc(relPathDataStr.getOrElse("???"), relPathVarCovarStr.getOrElse("???"), jarFileRelative.getOrElse("???"), jarDependencyFileRelative.getOrElse("????"), paramsNew)
    //******************************************************
    // ********************************************************
    val scriptDMTCP = generatorDMTCPScriptFnc(relPathDataStr.getOrElse("???"), relPathVarCovarStr.getOrElse("???"), jarFileRelative.getOrElse("???"), jarDependencyFileRelative.getOrElse("????"), paramsNew)
    //******************************************************
    val dmtcpScript = generateDMTCP(relPathDataStr.getOrElse("???"), relPathVarCovarStr.getOrElse("???"), jarFileRelative.getOrElse("???"), jarDependencyFileRelative.getOrElse("????"), params)
    // *** RElative to script root directory
    // scriptNameOutput
    // scriptDir
    val relativeShellScript = MCMCIO.absoluteToRelativePath(scriptDir, shellScriptNameOutput.getAbsolutePath)
    val relativeDMTCPScript = MCMCIO.absoluteToRelativePath(scriptDir, dmtcpScriptNameOutput.getAbsolutePath)
    // ******************************************************
    val fw = new FileWriter(shellScriptNameOutput)
    fw.write(scriptShellScript)
    fw.close()
    // ========================================================// ******************************************************
    val fwDMTCP = new FileWriter(dmtcpScriptNameOutput)
    fwDMTCP.write(scriptDMTCP)
    fwDMTCP.close()
    // ========================================================
    //    println(s"script = $script")
    println("--------------------------------------------------------------------------------------------------------------")
    (relativeShellScript, relativeShellScript)
  }

  def generateShellScript(datasetRelativePath: String, varCovarRelativePath: String, jarFileRelative: String, jarDependencyFileRelative: String, params: CScriptGenerator.Params): String = {
    // *************
    val compiledCCodePath = params.compiledCCodePath
    val mainScriptDir = params.mainScriptDir
    /*FileUtils.copyFileToDir(compiledCCodePath,mainScriptDir)*/
    val copiledCodeNameOnly = new File(compiledCCodePath).getName
    val newCompiledCodeLocation = new File(mainScriptDir, copiledCodeNameOnly)
    //    params.
    val dsDir = ""
    val relPathDataStr = MCMCIO.absoluteToRelativePath(mainScriptDir, dsDir)
    //-------------
    val s = new StringBuilder()
    s.append(
      s"""#!/bin/bash
         |
         |# Directives to grid engine
         |#$$ -cwd                         # Use current working directory.
         |#$$ -S /bin/bash                 # Interpreting script is bash.
         |#$$ -l h_vmem=1G
         |#$$ -pe threaded 1               # Slots on the same node should use pe threaded.
         |#$$ -o output.txt                # Recommended.
         |#$$ # -l s_rt=96:00:00
         |#$$ -e error.txt                 # Recommended.
         |# Do not use -N if you are doing automatic resubmit.
         |
         |# Load required modules
         |# module load dmtcp
         |module load gsl
         |
         |dataStr=`date +%y%m%d-%H%M-%S`
         |sigmaA=0.2
         |sigmaB=0.2
         |sigmaS=15
         |nIter=${params.niters}
         |in="${datasetRelativePath}"
         |
        |
        |
        |${params.cCQCodeFN} -v -in "$${in}" -out "posteriorParams_c_$${dataStr}_"${params.dataFileNamePartialOnly} -s $${nIter}   >  >(tee -a stdout_$${dataStr}_${params.dataFileNamePartialOnly}.log) 2> >(tee -a "stderr_"$${dataStr}_${params.dataFileNamePartialOnly}.log >&2)
         |
        |
        """.stripMargin)


    s.toString()
  }


  def generateDMTCP(datasetRelativePath: String, varCovarRelativePath: String, jarFileRelative: String, jarDependencyFileRelative: String, params: CScriptGenerator.Params): String = {
    val s = new StringBuilder()
    s.append(
      s"""#!/bin/bash
         |
        |#----------------------------------------------------------------------------------#
         |# Grid engine job script for a java job with DMTCP checkpointing
         |# Notes:
         |#     - You should touch AUTO_RESTART_PLEASE in the working directory before
         |#       submitting this job if you wish to automatically re-submit.
         |#     - It is recommended to give named -o and -e output and error file names
         |#       so that output from different stages of the checkpointed job are
         |#       combined.
         |#     - Problems/comments/enquiries to Chris Graham: hope this works for you :-)
         |#----------------------------------------------------------------------------------#
         |
        |# Directives to grid engine
         |#$$ -cwd                         # Use current working directory.
         |#$$ -S /bin/bash                 # Interpreting script is bash.
         |#$$ -l h_vmem=1G
         |#$$ -pe threaded 1               # Slots on the same node should use pe threaded.
         |#$$ -o output.txt                # Recommended.
         |#   #-l s_rt=04:00:00
         |#$$ -e error.txt                 # Recommended.
         |# Do not use -N if you are doing automatic resubmit.
         |
         |# Load required modules
         |# module load dmtcp
         |load module dmtcp/000-2.5.0
         |module load gsl
         |
         |# The following line ensures that the DMTCP coordinator moves with the job when restarted
         |export DMTCP_HOST= $$(hostname)
         |export DMTCP_PORT=0
         |
         |# Set the checkpoint interval in seconds - adjust accordingly
         |export DMTCP_CHECKPOINT_INTERVAL=120
         |
         |
         |# Launch executable with checkpointing
         |if [ -e ./dmtcp_restart_script.sh ]
         |  then
         |    # The file dmtcp_restart_script.sh is used to restart the job
         |    echo " $$(date +%c) ($$JOB_ID) : Restarting job from checkpoint file on $$HOSTNAME, job ID $$JOB_ID."
         |    ./dmtcp_restart_script.sh
         |  else
         |
         |      extJAR="emp_dev_scala-assembly-1.0-deps.jar"
         |      # MyCode JAR
         |      mcJAR="emp_dev_scala-assembly-1.0-170530-2057_56.jar"
         |      jarDir="../../../../jars"
         |      rScalaJAR="/home/peter/R/x86_64-pc-linux-gnu-library/3.3/rscala/java/rscala_2.11-1.0.9.jar"
         |      # Combining
         |      extJAR="$${jarDir}/$${extJAR}"
         |      mcJAR="$${jarDir}/$${mcJAR}"
         |      mcJAR="${jarFileRelative}"
         |      extJAR="${jarDependencyFileRelative}"
         |      dataStr=`date +%y%m%d-%H%M-%S`
         |      # **********************************************************************************
         |      #
         |      nIter=${params.niters}
         |      initMu=-4.640013
         |      initVariance=6.481367
         |      initialS=9662
         |      varianceScalingFactor=0.1,0.1,0.1
         |      tadFile="$datasetRelativePath"
         |      varCovarFile="$varCovarRelativePath"
         |
         |      # **********************************************************************************
         |
         |      dmtcp_launch MetroLogNormalStatic_orig -v -in "$${tadFile}" -out "posteriorParams_c_$${dataStr}_"${params.dataFileNamePartialOnly} -s $${nIter}   >  >(tee -a stdout_$${dataStr}_${params.dataFileNamePartialOnly}.log) 2> >(tee -a "stderr_"$${dataStr}_${params.dataFileNamePartialOnly}.log >&2)
         |
         | # ----------------------------------------------------------------------------------------
         |
         |
         |fi
         |
         |exit_status=$$??         |
         |if [ $$exit_status -eq 0 ]
         |  then
         |    # Job has completed; clean up DMTCP files
         |    echo " $$(date +%c) ($$JOB_ID) : Job completed: cleaning up DMTCP checkpoint files"
         |    rm dmtcp_restart*sh
         |    rm -r ckpt*files
         |    rm ckpt*dmtcp
         |fi
         |
         |
         |
         |
        """.stripMargin)


    s.toString()

  }


}



