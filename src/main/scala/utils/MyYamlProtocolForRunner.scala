package utils

//import net.jcazevedo.moultingyaml._
import basics.MyYamlParamsProtocolForResultsCollector.yamlFormat3
import basics.{PosteriorParamsStat, ResultsCollector}
import basics.ResultsCollector.StatisticsInformation
import net.jcazevedo.moultingyaml.DefaultYamlProtocol
import basics.ResultsCollector.StatisticsInformation
//import scala.reflect.runtime.universe
import basics.MyYamlParamsProtocolForResultsCollector._

case class Params(val muLastVal: Double, val varianceLastVal: Double, val nSLastVal: Int, val nAccepted: Int, val nLLLastVal: Double, val time_posterior_calc_sec: Double, nIter: Int, posteriorParamsStat: basics.PosteriorParamsStat)
case class ParamsWithCoverage(val mu: Double, val variance: Double, val S: Int, val nAccepted: Int, val time_sec: Double, val nIter: Int, val coverage: Double,val comment:String)
case class ParamsSampleSize(val mu: Double, val variance: Double, val S: Int, val nAccepted: Int, val time_sec: Double)
case class ParamsMCMCInput(val initMuValue: Option[Double], val initVarianceValue: Option[Double], initiSValue: Option[Int], paramASigma: Option[Double], paramBSigma: Option[Double], paramSSigma: Option[Double], plannedIterations: Option[Int], thinning: Option[Int], varianceScalingFactor: Option[List[Double]], varCovarFile: Option[String])

object MyYamlParamsProtocolForRunner extends DefaultYamlProtocol {
  implicit val posteriorParamsStatFormat = yamlFormat3(PosteriorParamsStat )
	implicit val paramsSampleSizeFormat = yamlFormat5(ParamsSampleSize)
  implicit val paramsFormat = yamlFormat8(Params)
  implicit val paramsWithCoverageFormat = yamlFormat8(ParamsWithCoverage)
  implicit val protocolParamsMCMCInput = yamlFormat10(ParamsMCMCInput)
}

//case class Params2( meanLog: Option[Double],
//                   sdLog: Option[Double],
//                   coverage: Option[Double],
//                   varLog: Option[Double],
//                   S: Option[Double])


/*


 */
