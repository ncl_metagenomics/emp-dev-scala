package utils

import java.io.File

import basics.MCMC
import basics.SamplingEffort.OutputCSVWriters
import breeze.linalg.{DenseMatrix, DenseVector}
import utils.MCMCIO.correlationAndCovarianceForCSVData

import scala.util.{Failure, Success, Try}

/**
  * Created by peter on 11/04/17.
  */
object CovarianceOfPosteriorParams {

  def getFileNamefCovarianceMatrix(postParamFileN: String, dateStr: String, nameSpecifier: String): String = {
    val fileName = new File(postParamFileN).getName()
    val runDir = new File(postParamFileN).getParent()
    val dbNameDir = new File(runDir).getParentFile.getName
    println(s"postParamFileN = ${postParamFileN}")
    println(s"Parent directory: ${runDir}")
    println(s"Database directory name: ${dbNameDir}")
    val covarianceFileMame = s"varCovarMatrix_${nameSpecifier}_${dbNameDir}_${dateStr}.csv"
    val covarianceFilePath = new File(runDir, covarianceFileMame).getPath
    println("***************************")
    covarianceFilePath
  }


  def getFileNamefCovarianceMatrix4IG(postParamFileN: String, dateStr: String, nameSpecifier: String): (String, String) = {
    val fileName = new File(postParamFileN).getName()
    val runDir = new File(postParamFileN).getParent()
    val parentOfParentDir = (new File(runDir)).getParent
    val parentOfParentOfParentDir = (new File(parentOfParentDir)).getParent
    val dbNameDir = new File(runDir).getParentFile.getName
    val pattern = "^s-(.*)".r
    val dirInfo = (new File(runDir)).getName match {
      case pattern(appendix) => appendix
      case _ => ""
    }
    println(s"postParamFileN = ${postParamFileN}")
    println(s"Parent directory: ${runDir}")
    println(s"Database directory name: ${dbNameDir}")
    println(s"parentOfParentOfParentDir : ${parentOfParentOfParentDir}")
    // parentOfParentOfParentDir
    val covarianceFileName = s"varCovarMatrix_${nameSpecifier}_${dbNameDir}_Run_${dirInfo}_T_${dateStr}.csv"
    val covarianceFilePathRunDir = new File(runDir, covarianceFileName).getPath
    val covarianceFilePathDBDir = new File(parentOfParentDir, covarianceFileName).getPath
    println("***************************")
    (covarianceFilePathRunDir, covarianceFilePathDBDir)
  }


  def getFileNamefCovarianceMatrix4IGWLn(postParamFileN: String, dateStr: String, nameSpecifier: String): (String, String) = {
    val fileName = new File(postParamFileN).getName()
    val runDir = new File(postParamFileN).getParent()
    val parentOfParentDir = (new File(runDir)).getParent
    val parentOfParentOfParentDir = (new File(parentOfParentDir)).getParent
    val dbNameDir = new File(runDir).getParentFile.getName
    val pattern = "^s-(.*)".r
    val dirInfo = (new File(runDir)).getName match {
      case pattern(appendix) => appendix
      case _ => ""
    }
    println(s"postParamFileN = ${postParamFileN}")
    println(s"Parent directory: ${runDir}")
    println(s"Database directory name: ${dbNameDir}")
    println(s"parentOfParentOfParentDir : ${parentOfParentOfParentDir}")
    // parentOfParentOfParentDir
    val covarianceFileName = s"varCovarMatrix_Ln_${nameSpecifier}_${dbNameDir}_Run_${dirInfo}_T_${dateStr}.csv"
    val covarianceFilePathRunDir = new File(runDir, covarianceFileName).getPath
    val covarianceFilePathDBDir = new File(parentOfParentDir, covarianceFileName).getPath
    println("***************************")
    (covarianceFilePathRunDir, covarianceFilePathDBDir)
  }

  def main(args: Array[String]): Unit = {
    val postParamFileN2 = "/media/sf_data/nps409/Work2Sync/Project`s/emp_dev_scala_runs/TAD_DATA_Synthetic_Out/Topsy/QC_Paper_170219/Brazil_0_9_170207/s-1200K-lognormal-Pegasus-1.05-1.001-170212-1814_54/posterior-s-s-all-data_sampleSize_TAD_90_pc_D_24877.8_syntheticSampleSizes_lognormal_170207_1942-44_17-02-07_2010-22.csv_170212-1814_54.csv"
    // nIter,mu,variance,S,nLL,acceptance,n_accepted

    val postParamFileN = "/media/sf_data/nps409/Work2Sync/Projects/emp_dev_scala_runs/TAD_DATA_Synthetic_Out/Topsy/QC_Paper_170319_covar_hetero/GOS_90_170209/s-800K-lognormal-Pegasus-1.05-1.001-170327-1020_45/posterior-s-s-all-data_sampleSize_TAD_90_pc_D_8925.3_syntheticSampleSizes_lognormal_170209_0213-39_17-02-09_0219-42.csv_170327-1020_45.csv"
    calcAndSaveCovarianceWLognormal(postParamFileN)

  }

  def calcAndSaveCovarianceWLognormal(posteriorFileName: String): Try[String] = {
    val colNames = List[String]("mu", "variance", "S")
    val numRows2Skip = 10000
    //    val correlationAndCovariance = utils.MCMCIO.correlationAndCovarianceForCSVData(postParamFileN, colNames, numRows2Skip)
    //    val adjustedCovariance: DenseMatrix[Double] = utils.MCMCIO.getPartialAdjusteCovarinceFromPosteriors4Lognormal(posteriorFileName, colNames, numRows2Skip)
    val rTmp: Try[DenseMatrix[Double]] = utils.MCMCIO.getPartialAdjusteCovarinceFromPosteriors4Lognormal(posteriorFileName, colNames, numRows2Skip)
    val res: Try[String] = rTmp match {
      case Success(adjustedCovariance: DenseMatrix[Double]) =>
        println(s"Number of data points used ")
        val covarianceFileNames = getFileNamefCovarianceMatrix(posteriorFileName, MCMC.DATE_STR, nameSpecifier = "adjusted_partial_Scala")
        //    breeze.io.CSVWriter.write(new breeze.io.CSVWriter(""),adjustedCovariance)
        breeze.linalg.csvwrite(new File(covarianceFileNames), adjustedCovariance, separator = ',')
        Success(covarianceFileNames)
      case Failure(e) => Failure(e)
    }
    res
  }


  /**
    *
    * @param posteriorFileName
    * @param colNames
    * @param numRows2Skip
    * @return
    */
  def calcAndSaveCovarianceWLognormal(posteriorFileName: String, colNames: List[String] = List[String]("mu", "variance", "S"), numRows2Skip: Int = 10000): Try[String] = {
    // val colNames = List[String]("mu", "variance", "S")
    //    val numRows2Skip = 10000
    //    val correlationAndCovariance = utils.MCMCIO.correlationAndCovarianceForCSVData(postParamFileN, colNames, numRows2Skip)
    //    val adjustedCovariance: DenseMatrix[Double] = utils.MCMCIO.getPartialAdjusteCovarinceFromPosteriors4Lognormal(posteriorFileName, colNames, numRows2Skip)
    val rTmp: Try[DenseMatrix[Double]] = utils.MCMCIO.getPartialAdjusteCovarinceFromPosteriors4Lognormal(posteriorFileName, colNames, numRows2Skip)
    val res: Try[String] = rTmp match {
      case Success(adjustedCovariance: DenseMatrix[Double]) =>
        println(s"Number of data points used ")
        val covarianceFileNames = getFileNamefCovarianceMatrix(posteriorFileName, MCMC.DATE_STR, nameSpecifier = "adjusted_partial_Scala")
        //    breeze.io.CSVWriter.write(new breeze.io.CSVWriter(""),adjustedCovariance)
        breeze.linalg.csvwrite(new File(covarianceFileNames), adjustedCovariance, separator = ',')
        Success(covarianceFileNames)
      case Failure(e) => Failure(e)

    }
    res
  }

  /**
    *
    * @param posteriorFileName
    * @param numRows2Skip
    * @return
    */
  def calcAndSaveCovariance(posteriorFileName: String, distribution: String, numRows2Skip: Int = 10000): Try[String] = {

    val covarianceFileName: Try[String] = distribution.toLowerCase match {
      case "ig" =>
        //        val colNames: List[String] = List[String]("nIter", "S", "alpha", "beta")
        //        val colNames: List[String] = List[String]("nIter", "S", "alpha", "beta")
        val colNames: List[String] = List[String]("S", "alpha", "beta")
        val adjustedCovariance: Try[DenseMatrix[Double]] = utils.MCMCIO.getPartialAdjusteCovarinceFromPosteriors4IG(posteriorFileName, colNames, numRows2Skip)

        val res: Try[String] = adjustedCovariance match {
          case Success(covar) =>
            println(s"Number of data points used ")
            val (covarianceFilePathRunDir, covarianceFilePathDBDir) = getFileNamefCovarianceMatrix4IG(posteriorFileName, MCMC.DATE_STR, nameSpecifier = "adjusted_partial_Scala")
            //    breeze.io.CSVWriter.write(new breeze.io.CSVWriter(""),adjustedCovariance)

            val fw = new java.io.FileWriter(new File(covarianceFilePathRunDir))
            val fw2 = new java.io.FileWriter(new File(covarianceFilePathDBDir))
            utils.CSVWriter4Breeze.writeWithHeader(fw, covar, colNames, separator = ',')
            utils.CSVWriter4Breeze.writeWithHeader(fw2, covar, colNames, separator = ',')
            //        breeze.linalg.csvwrite(new File(covarianceFilePathRunDir), adjustedCovariance, separator = ',')
            //        breeze.linalg.csvwrite(new File(covarianceFilePathDBDir), adjustedCovariance, separator = ',')
            fw.close()
            fw2.close()

            Success(covarianceFilePathRunDir)

          case Failure(e) => println(e.getMessage); Failure(e)
        }
        res

      case "igln" =>
        println("")
        //        val covarColumns = Map("ln_S" -> Double, "ln_alpha" -> Double, "ln_beta" -> Double)

        val varColNamesOut: List[String] = List[String]("ln_S", "ln_alpha", "ln_beta")

        val varColNames: List[String] = List[String]("S", "alpha", "beta")

        val adjustedCovariance: Try[DenseMatrix[Double]] = utils.MCMCIO.getPartialAdjusteCovarinceFromPosteriors4IGWLn(posteriorFileName, varColNames, numRows2Skip)

        val res: Try[String] = adjustedCovariance match {
          case Success(covar) =>
            println(s"Number of data points used ")
            val (covarianceFilePathRunDir, covarianceFilePathDBDir) = getFileNamefCovarianceMatrix4IGWLn(posteriorFileName, MCMC.DATE_STR, nameSpecifier = "adjusted_partial_Scala")
            //    breeze.io.CSVWriter.write(new breeze.io.CSVWriter(""),adjustedCovariance)

            val fw = new java.io.FileWriter(new File(covarianceFilePathRunDir))
            val fw2 = new java.io.FileWriter(new File(covarianceFilePathDBDir))
            utils.CSVWriter4Breeze.writeWithHeader(fw, covar, varColNamesOut, separator = ',')
            utils.CSVWriter4Breeze.writeWithHeader(fw2, covar, varColNamesOut, separator = ',')
            println(s"""Output file ${covarianceFilePathRunDir}""")
            println(s"""Output file ${covarianceFilePathDBDir}""")

            fw.close()
            fw2.close()
            println(s"Results written into $covarianceFilePathDBDir")
            println(s"Results written into $covarianceFilePathRunDir")

            Success(covarianceFilePathRunDir)

          case Failure(e) => println(e.getMessage); Failure(e)
        }
        res

      case "" => new Exception("Unknown type of distribution")
        val s = s"No distribution was specified"
        println(s)
        Failure(new Exception(s))
      case _ => new Exception("Unknown type of distribution")
        val s = s"Unknown type of distributions specified $distribution"
        println(s)
        Failure(new Exception(s))
    }
    // val colNames = List[String]("mu", "variance", "S")
    //    val numRows2Skip = 10000
    //    val correlationAndCovariance = utils.MCMCIO.correlationAndCovarianceForCSVData(postParamFileN, colNames, numRows2Skip)
    covarianceFileName
  }


  def calcAndSaveCovarianceVarNames(posteriorFileName: String, distribution: String, numRows2Skip: Int = 10000, colNames: List[String]): Try[String] = {

    val covarianceFileName: Try[String] = distribution match {
      case "IG" =>
        //        val colNames: List[String] = List[String]("nIter", "S", "alpha", "beta")
        //        val colNames: List[String] = List[String]("nIter", "S", "alpha", "beta")

        val adjustedCovariance: Try[DenseMatrix[Double]] = utils.MCMCIO.getPartialAdjusteCovarinceFromPosteriors4IG(posteriorFileName, colNames, numRows2Skip)

        val res: Try[String] = adjustedCovariance match {
          case Success(covar) =>
            println(s"Number of data points used ")
            val (covarianceFilePathRunDir, covarianceFilePathDBDir) = getFileNamefCovarianceMatrix4IG(posteriorFileName, MCMC.DATE_STR, nameSpecifier = "adjusted_partial_Scala")
            //    breeze.io.CSVWriter.write(new breeze.io.CSVWriter(""),adjustedCovariance)

            val fw = new java.io.FileWriter(new File(covarianceFilePathRunDir))
            val fw2 = new java.io.FileWriter(new File(covarianceFilePathDBDir))
            utils.CSVWriter4Breeze.writeWithHeader(fw, covar, colNames, separator = ',')
            utils.CSVWriter4Breeze.writeWithHeader(fw2, covar, colNames, separator = ',')
            //        breeze.linalg.csvwrite(new File(covarianceFilePathRunDir), adjustedCovariance, separator = ',')
            //        breeze.linalg.csvwrite(new File(covarianceFilePathDBDir), adjustedCovariance, separator = ',')
            fw.close()
            fw2.close()

            Success(covarianceFilePathRunDir)

          case Failure(e) => println(e.getMessage); Failure(e)
        }
        res


      case "" => new Exception("Unknown type of distribution")
        val s = s"No distribution was specified"
        println(s)
        Failure(new Exception(s))
      case _ => new Exception("Unknown type of distribution")
        val s = s"Unknown type of distributions specified $distribution"
        println(s)
        Failure(new Exception(s))
    }
    // val colNames = List[String]("mu", "variance", "S")
    //    val numRows2Skip = 10000
    //    val correlationAndCovariance = utils.MCMCIO.correlationAndCovarianceForCSVData(postParamFileN, colNames, numRows2Skip)
    covarianceFileName
  }

}
