package utils

import java.io.{File, FileWriter, StringWriter, Writer}

import au.com.bytecode.opencsv.{CSVReader => OpenCSVReader, CSVWriter => OpenCSVWriter}
import breeze.linalg.DenseMatrix


/**
  * Created by peter on 13/04/17.
  */



object CSVWriter4Breeze {


  def write(output: Writer,
            mat: TraversableOnce[IndexedSeq[String]],
            separator: Char=',',
            quote: Char='"',
            escape: Char='\\') {
    val writer = new OpenCSVWriter(output, separator, quote, escape)
    import scala.collection.JavaConverters._
    mat match {
      case Seq(x @ _*) => writer.writeAll(x.map(_.toArray).asJava)
      case _ =>
        for(l <- mat) {
          writer.writeNext(l.toArray)
        }
    }
    writer.flush()
  }

  def writeWithHeader(output: Writer,
            matDense: DenseMatrix[Double],
           cols: List[String],
            separator: Char=',',
            quote: Char='"',
            escape: Char='\\') {
    val mat0 =  IndexedSeq.tabulate(matDense.rows,matDense.cols)(matDense(_,_).toString)
    val v: IndexedSeq[String] = cols.toIndexedSeq
    val mat:  TraversableOnce[IndexedSeq[String]] = List(v)++mat0
    val writer = new OpenCSVWriter(output, separator, quote, escape)
    import scala.collection.JavaConverters._
    mat match {
      case Seq(x @ _*) => writer.writeAll(x.map(_.toArray).asJava)
      case _ =>
        for(l <- mat) {
          writer.writeNext(l.toArray)
        }
    }
    writer.flush()
  }

  def writeFile(file: File,
                mat: IndexedSeq[IndexedSeq[String]],
                separator: Char=',',
                quote: Char='"',
                escape: Char='\\') {
    val out = new FileWriter(file)
    write(out, mat, separator, quote, escape)
    out.close()
  }

  def mkString(mat: IndexedSeq[IndexedSeq[String]],
               separator: Char=',',
               quote: Char='"',
               escape: Char='\\'):String = {
    val out = new StringWriter
    write(out, mat, separator, quote, escape)
    out.toString
  }

}
