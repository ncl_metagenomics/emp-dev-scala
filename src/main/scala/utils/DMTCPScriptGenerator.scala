package utils

/**
  * Created by peter on 19/04/17.
  */
object DMTCPScriptGenerator {
  val endOfLine: String = System.getProperty("line.separator")

  case class DMTCPParameters(initMu: Double, initVariance: Double, initS: Int, varCovarFile: String, varianceScalingFactor: List[Double],fileNameTAD:String)

  // varianceScalingFactor
  def getShellScript(dmtcp: DMTCPParameters): String = {""}

  def getDMTCPSCRIPT(dmtcp: DMTCPParameters): String = {
    val s = new StringBuilder()
    s.append(
      """#!/bin/bash
        |
        |#----------------------------------------------------------------------------------#
        |# Grid engine job script for a java job with DMTCP checkpointing
        |# Notes:
        |#     - You should touch AUTO_RESTART_PLEASE in the working directory before
        |#       submitting this job if you wish to automatically re-submit.
        |#     - It is recommended to give named -o and -e output and error file names
        |#       so that output from different stages of the checkpointed job are
        |#       combined.
        |#     - Problems/comments/enquiries to Chris Graham: hope this works for you :-)
        |#----------------------------------------------------------------------------------#
        |
        |# Directives to grid engine
        |#$ -cwd                         # Use current working directory.
        |#$ -S /bin/bash                 # Interpreting script is bash.
        |#$ -l h_vmem=18G
        |#$ -pe threaded 1               # Slots on the same node should use pe threaded.
        |#$ -o output.txt                # Recommended.
        |#   #-l s_rt=04:00:00
        |#$ -e error.txt                 # Recommended.
        |# Do not use -N if you are doing automatic resubmit.
        |
        |# Load required modules
        |module load dmtcp
        |
        |# The following line ensures that the DMTCP coordinator moves with the job when restarted
        |export DMTCP_HOST=$(hostname)
        |export DMTCP_PORT=0
        |
        |# Set the checkpoint interval in seconds - adjust accordingly
        |export DMTCP_CHECKPOINT_INTERVAL=300
        |
        |
        |# Launch executable with checkpointing
        |if [ -e ./dmtcp_restart_script.sh ]
        |  then
        |    # The file dmtcp_restart_script.sh is used to restart the job
        |    echo "$(date +%c) ($JOB_ID) : Restarting job from checkpoint file on $HOSTNAME, job ID $JOB_ID."
        |    ./dmtcp_restart_script.sh
        |  else
        |    # This is our first time running the job
        |    echo "$(date +%c) ($JOB_ID) : Running with checkpointing for first time on $HOSTNAME, job ID $JOB_ID."

        |""".stripMargin)


    s.append(
      """|
        |    extJAR="emp_dev_scala-assembly-1.0-deps.jar"
        |	# MyCode JAR
        |	mcJAR="emp_dev_scala-assembly-1.0-170317-1602_03.jar"
        |	jarDir="../../../../jars"
        |	rScalaJAR="/home/peter/R/x86_64-pc-linux-gnu-library/3.3/rscala/java/rscala_2.11-1.0.9.jar"
        |	# Combining
        |	extJAR="${jarDir}/${extJAR}"
        |	mcJAR="${jarDir}/${mcJAR}"
        |
        |      #**********************************************************************************
        |""".stripMargin)

    s.append(
      s"""|      nIter=400000
          |      initMu=${dmtcp.initMu}
          |      initVariance=${dmtcp.initVariance}
          |      initialS=${dmtcp.initS}
          |      varCovarFile="${dmtcp.varCovarFile}"
          |      varianceScalingFactor=${dmtcp.varianceScalingFactor.mkString("", ",", "")}
          |      tadFile="${dmtcp.fileNameTAD}"
          |     #*********************************************************************
          |""".stripMargin)

    s.append(
      """|      dmtcp_launch java -Xmx1000M -XX:ParallelGCThreads=1 -XX:-UseParallelGC -cp  "${extJAR}:${mcJAR}" ExperimentScalaRunner --in "${tadFile}" -s ${nIter} -c 0.9 --paramASigma 0.1 --paramBSigma 0.1  --SSigma 400 --initMu ${initMu} --initialS ${initialS} --initVariance ${initVariance} --varCovarFile "${varCovarFile}" --varianceScalingFactor ${varianceScalingFactor}
         |     #************************************************************************
         |    """.stripMargin)

    s.append(
      """
        |fi
        |
        |exit_status=$?
        |if [ $exit_status -eq 0 ]
        |  then
        |    # Job has completed; clean up DMTCP files
        |    echo "$(date +%c) ($JOB_ID) : Job completed: cleaning up DMTCP checkpoint files"
        |    rm dmtcp_restart*sh
        |    rm -r ckpt*files
        |    rm ckpt*dmtcp
        |fi
      """.stripMargin)

    //***************************************
    s.toString
  }

  def main(args: Array[String]): Unit = {
    val dmtcpParams = DMTCPParameters(0.3, 7.0, 20000, "C:/afc/eeee.csv", List[Double](0.1, 0.1, 0.1),"/path/to/tadfile")
    val doc = getDMTCPSCRIPT(dmtcpParams)
    println(doc)
    println("----")

  }

}

