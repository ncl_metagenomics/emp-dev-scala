package utils

import java.io.{File, FileReader}

import basics.DataTableReadWrite
import breeze.linalg.{DenseMatrix, _}
import breeze.numerics.sqrt
import com.github.martincooper.datatable.{DataRow, DataRowCollection, DataTable}
import utils.Statistics4MCMC._

import scala.util.Try


/**
  * Created by peter on 10/03/17.
  */
object Statistics4MCMC {

  def readMatrix3By3V2(fileName: String, colTypes: Map[String, Double.type]): DenseMatrix[Double] = {
    val filePath = new File(fileName)
    val matrix: DenseMatrix[Double] = readMatrix3By3(filePath, colTypes)
    //    println("*************************************")
    matrix
  }


  def choleskyUpperTriangular(matrix: DenseMatrix[Double]): DenseMatrix[Double] = {
    getCholeskyLowerTriangular(matrix).t
  }

  /**
    *
    * @param matrix
    * @return
    */
  def getCholeskyLowerTriangular(matrix: DenseMatrix[Double]): DenseMatrix[Double] = {
    import breeze.linalg.cholesky
    breeze.linalg.cholesky(matrix)
  }

  //  /**
  //    *
  //    * @param fileName
  //    */
  //  def getAdjustedCovarianceFromCSV(fileName: File): Unit = {
  //    val colTypes = Map("varA" -> Double, "varB" -> Double, "varS" -> Double)
  //    val correlationMatrix: DenseMatrix[Double] = readMatrix3By3(fileName, colTypes)
  //  }

  /**
    * Calculates Cholesky Lower triangular matrix of correlation matrix
    *
    * @param fileName
    * @return
    */
  def getCholeskyLowerTriangleOfCorrelationMatrix(fileName: File): DenseMatrix[Double] = {
    val correlationMatrix: DenseMatrix[Double] = readCorrelationMatrix3By3(fileName)
    //TODO: 170227 getCorrelationMatrix - read it from file instead of using haradcoded
    //    val cholesky = DenseMatrix((1d, 0d, 0d), (-0.8725896, 0.4884541, 0d), (-0.9481653, -0.1355943, 0.2873965))
    //    val correlation = DenseMatrix((1d, -0.8725896, -0.9481653), (-0.8725896, 1d, 0.7611276), (-0.9481653, 0.7611276, 1d))
    val choleskyLowerTriangularMatrix = getCholeskyLowerTriangular(correlationMatrix)
    choleskyLowerTriangularMatrix
  }


  /**
    * Calculates Cholesky Lower triangular matrix of covariance matrix
    *
    * @param fileName
    * @return
    */
  def getCholeskyLowerTriangleOfCovarianceMatrix(fileName: File): DenseMatrix[Double] = {
    val correlationMatrix: DenseMatrix[Double] = readCorrelationMatrix3By3(fileName)
    //TODO: 170227 getCorrelationMatrix - read it from file instead of using haradcoded
    //    val cholesky = DenseMatrix((1d, 0d, 0d), (-0.8725896, 0.4884541, 0d), (-0.9481653, -0.1355943, 0.2873965))
    //    val correlation = DenseMatrix((1d, -0.8725896, -0.9481653), (-0.8725896, 1d, 0.7611276), (-0.9481653, 0.7611276, 1d))
    val choleskyLowerTriangularMatrix = getCholeskyLowerTriangular(correlationMatrix)
    choleskyLowerTriangularMatrix
  }


  def dataFrameToArray(a: Array[Double], row: DataRow, colNames: List[String]): Array[Double] = {
    //TODO: 170303 Convert row to array
    val res01: List[Double] = for {
      colName <- colNames
    } yield {
      //      println(s"colName = $colName")
      val x = row.getAs[Double](colName).get
      x
    }
    val res02 = a ++ res01
    res02
  }

  /**
    *
    * @param file
    * @return
    */
  def readCorrelationMatrix3By3(file: File): DenseMatrix[Double] = {
    //TODO: 170302 Reading Correlation Matrix
    //    throw new Exception("Not implemented yet!")
    val colTypes = Map("varA" -> Double, "varB" -> Double, "varS" -> Double)
    val colNames: Set[String] = colTypes.keySet
    //it was acceptance
    // nIter,mu,variance,S,nLL,acceptance,n_accepted
    val tadDataS: Try[DataTable] = DataTableReadWrite.readCsv("VarianceOfParameters", file, colTypes)
    val tadData = tadDataS.get

    val numCols = tadData.columns.size
    val ca: Array[DataRow] = tadData.toArray
    val x1: DataRowCollection = tadData.rows
    val x2: List[DataRow] = x1.toList
    val x3: DataRow = x2(0)
    val x5: IndexedSeq[Any] = x3.values
    val x4: Double = x3.as[Double](0)
    //**********************
    val a = Array[Double]()
    val x6 = ca.foldLeft(a) { (a, r) => dataFrameToArray(a, r, colNames.toList) }
    //-----------------------
    val corM: DenseMatrix[Double] = new DenseMatrix[Double](tadData.rowCount, tadData.columns.size, x6)
    //*********************
    corM.t
  }

  /**
    *
    * @param file
    * @param colTypes
    * @return
    */
  def readMatrix3By3(file: File, colTypes: Map[String, Double.type]): DenseMatrix[Double] = {
    //TODO: 170302 Reading Correlation Matrix
    //    throw new Exception("Not implemented yet!")
    //    val colTypes = Map("varA" -> Double, "varB" -> Double, "varS" -> Double)
    val colNames: Set[String] = colTypes.keySet
    //it was acceptance
    // nIter,mu,variance,S,nLL,acceptance,n_accepted
    val tadDataS: Try[DataTable] = DataTableReadWrite.readCsv("VarianceOfParameters", file, colTypes)
    val tadData = tadDataS.get
    val numCols = tadData.columns.size
    val numRows = tadData.rowCount
    val ca: Array[DataRow] = tadData.toArray
    val x1: DataRowCollection = tadData.rows
    val x2: List[DataRow] = x1.toList
    if(x2.size==0){
      throw new Exception(s"""File either does not contain column ${colTypes.keys.mkString(sep=", ")}""")
    }
    val x3: DataRow = x2(0)
    val x5: IndexedSeq[Any] = x3.values
    val x4: Double = x3.as[Double](0)
    //**********************
    val a = Array[Double]()
    val x6 = ca.foldLeft(a) { (a, r) => dataFrameToArray(a, r, colNames.toList) }
    //-----------------------
    val corM: DenseMatrix[Double] = new DenseMatrix[Double](tadData.rowCount, tadData.columns.size, x6)
    //*********************
    corM.t
  }

  /**
    *
    *
    * @param varianceV
    */
  def getCovariance(varianceV: DenseVector[Double], corr: DenseMatrix[Double]): DenseMatrix[Double] = {
    val stdV: DenseVector[Double] = breeze.numerics.sqrt(varianceV)
    val stdM: DenseMatrix[Double] = diag(stdV)
    val covariance = stdM * corr * stdM
    covariance
  }

  /**
    * Var(X1)=Var(X)+1/4*VAR(Y)+STD(X)*STD(Y)*Rho(X,Y)
    *
    * @param varianceV
    * @param corrM
    * @return
    */
  def adjustVariance(varianceV: DenseVector[Double], corrM: DenseMatrix[Double]): DenseVector[Double] = {
    val newVarianceV = varianceV.copy
    newVarianceV(0) = varianceV(0) + 0.25 * varianceV(1) + corrM(0, 1) * sqrt(varianceV(0)) * sqrt(varianceV(1))
    println(s"*****************")
    newVarianceV
  }

  /**
    *
    * @param covariance
    */

  def covarianceToCorrelationAndVariance(covariance: DenseMatrix[Double]) = {
    val varianceV: DenseVector[Double] = diag(covariance)
    val stDevV = breeze.numerics.sqrt(varianceV)
    val stDevM = diag(stDevV)
    //    val invStDevV = stDevV.map(println(_))11
    val invStDev: List[Double] = stDevV.activeValuesIterator.map(x => {
      println(x);
      1 / x
    }).toList
    val tstInvStDev0 = (stDevV.activeValuesIterator.toList.zip(invStDev))
    //.map((x,y)=>x) //(x:(Double,Double)=>{x.})
    //.map((x:Double,y:Double)=>x) //.map((x,y)=>{println(s"$x,$y");x})
    val tstInvStDev = tstInvStDev0.map(x => x._1 * x._2)
    val tstInvStDevV = breeze.linalg.DenseVector(tstInvStDev0.toArray)
    val invStDevM: DenseMatrix[Double] = diag(DenseVector[Double](invStDev.toArray))
    val corrM: DenseMatrix[Double] = invStDevM * covariance * invStDevM
    //    val newVarianceV:DenseVector[Double] = adjustVariance(varianceV,corrM)
    //    val newCovariance:DenseMatrix[Double] = getCovariance(newVarianceV,corrM)
    //val auxInvStDev = val auxInvStDev0

    println("*************************")
    (varianceV, corrM)
  }

  /**
    * Adjusts covariance matrix for lognormal distribution
    *
    * @param covariance
    */
  def getAdustedAndScaledCovarianceForLognormal(covariance: DenseMatrix[Double], covarianceScaler: Double): (DenseVector[Double], DenseMatrix[Double]) = {
    //todo:170310 complete this
    val (variance, correlation) = covarianceToCorrelationAndVariance(covariance)
    val newVarianceV: DenseVector[Double] = adjustVariance(variance, correlation)
    val newVarianceScaledV = covarianceScaler * newVarianceV
    val newCovarianceScaled: DenseMatrix[Double] = getCovariance(newVarianceScaledV, correlation)
    (newVarianceScaledV, newCovarianceScaled)
  }
  /**
    * Adjusts covariance matrix for alternative parametrizetion of mu lognormal distribution
    *
    * @param covariance
    */
  def getAdustedCovarianceForLognormalBN(covariance: DenseMatrix[Double]): (DenseVector[Double], DenseMatrix[Double]) = {
    //todo:170310 complete this
    val (variance, correlation) = covarianceToCorrelationAndVariance(covariance)
    val adjustedVarianceV: DenseVector[Double] = adjustVariance(variance, correlation)
    val adjustedCovarianceScaled: DenseMatrix[Double] = getCovariance(adjustedVarianceV, correlation)
    (adjustedVarianceV, adjustedCovarianceScaled)
  }

  /**
    * Adjusts covariance matrix for lognormal distribution
    *
    * @param covariance
    */
  def getRescaledCovariance(covariance: DenseMatrix[Double], covarianceScaler: Double): DenseMatrix[Double] = {
    //170313 debug here
    val newCovarianceScaled: DenseMatrix[Double] = covariance * covarianceScaler
    newCovarianceScaled
  }


  /**
    * Adjusts covariance matrix for lognormal distribution
    *
    * @param covariance
    */
  def getRescaledCovariance(covariance: DenseMatrix[Double], covarianceScaler: List[Double]): DenseMatrix[Double] = {
    //todo:170314 debug here
    val covarianceScalerV: DenseVector[Double] = DenseVector(covarianceScaler.toArray)
    val covarianceScalerM = diag(covarianceScalerV)
    val stdScaler: DenseMatrix[Double] = breeze.numerics.sqrt(covarianceScalerM)
    val newCovarianceScaled: DenseMatrix[Double] = stdScaler * covariance * stdScaler
    newCovarianceScaled
  }


  def covarianceToCorrelationAndVarianceTest(covariance: DenseMatrix[Double]) = {

    val varianceV = diag(covariance)
    // val adjVarianceV = getCovariance(varianceV)
    val stDevV = breeze.numerics.sqrt(varianceV)
    //    val invStDevV = stDevV.map(println(_))11
    val invStDev: List[Double] = stDevV.activeValuesIterator.map(x => {
      println(x);
      1 / x
    }).toList
    val tstInvStDev0 = (stDevV.activeValuesIterator.toList.zip(invStDev))
    //.map((x,y)=>x) //(x:(Double,Double)=>{x.})
    //.map((x:Double,y:Double)=>x) //.map((x,y)=>{println(s"$x,$y");x})
    val tstInvStDev = tstInvStDev0.map(x => x._1 * x._2)
    val auxInvStDev = diag(DenseVector(invStDev))
    //val auxInvStDev = val auxInvStDev0
    println("*************************")
    (varianceV, DenseMatrix.zeros[Double](3, 3))
  }

}
