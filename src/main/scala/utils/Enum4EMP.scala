package utils

/**
  * Created by peter on 01/11/16.
  */
trait Enum4EMP[A <: {def name : String}] {

  trait Value {
    self: A =>
    _values :+= this
  }

  private var _values = List.empty[A]

  def values = _values
}

trait Enum4EMP2[A <: {def tadFileNamePattern: String; def posteriorDatafilePatternStr:String}] {

  trait Value {
    self: A =>
    _values :+= this
  }

  private var _values = List.empty[A]

  def values = _values
}


trait Enum4EMPColectResults[A <: {val tadFileNamePattern: String; val posteriorDatafilePatternStr: String; val posteriorDir: String; val sampleSizeFile:String}] {

  trait Value {
    self: A =>
    _values :+= this
  }

  private var _values = List.empty[A]

  def values = _values
}

// Enum4EMPColectResults

// Enum4EMPSampleSize4Directory
trait Enum4EMPSampleSize4Directory[A <: {val tadFileNamePattern: String; val posteriorDatafilePatternStr:String; val posteriorDir:String}] {

  trait Value {
    self: A =>
    _values :+= this
  }

  private var _values = List.empty[A]

  def values = _values
}