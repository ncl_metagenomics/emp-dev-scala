package utils

import net.jcazevedo.moultingyaml.DefaultYamlProtocol

import scala.reflect.runtime.universe

case class MyParams( meanLog: Option[Double],
                   sdLog: Option[Double],
                   coverage: Option[Double],
                   varLog: Option[Double],
                   S: Option[Double])

object MyYamlParamsProtocol extends DefaultYamlProtocol {
  implicit val paramsFormat = yamlFormat5(MyParams)
}