package utils

import basics._
import java.io.{File, FileReader}
import java.util
import java.util.Arrays._

import com.github.martincooper.datatable.DataTable
import com.github.tototoshi.csv.{CSVReader, CSVWriter, DefaultCSVFormat}
import joptsimple.{ArgumentAcceptingOptionSpec, OptionParser, OptionSet}

import scala.collection.immutable.{ListMap, Queue, HashMap => IMHashMap}
import scala.collection.mutable.{Queue => MQueue}
import scala.collection.mutable
import scala.tools.nsc.classpath.FlatClassPathEntries.entry2Tuple
import scala.util.Try

/**
  * Created by peter on 13/09/16.
  */
object AbundanceConversion {

  def convertTAD2Abundance(tadIn: Map[Long, Int]):List[Long]  = {
    val res = tadIn.foldLeft(
      (Queue[Long](), 0))(
      (ini, entry) => {
        // abundance class
        val res = ini._1
        val nSpeciesInserted = ini._2
        val abu = entry._1
        val nSpec = entry._2
        val a = Queue.fill(nSpec)(abu.toLong)
        (res++:a, nSpec + nSpeciesInserted)
      }
    )._1
    res.toList
  }

  case class InputParamsPartial(dir: String)

  def convertOTUs2TAD(lOTUs: List[Int]) = {
    val abu = convertOTUs2Abundance(lOTUs)
    val tad = convertAbundance2TAD(abu.values.toList)
    tad
  }

  def convertAbundance2TAD(lAbundance: List[Int]): scala.collection.mutable.HashMap[Int, Int] = {
    val tadMap = scala.collection.mutable.HashMap.empty[Int, Int]

    var res = lAbundance.foldLeft(tadMap)((tadMap: mutable.HashMap[Int, Int], otu: Int) => {
      if (tadMap.contains(otu)) {
        tadMap.put(otu, tadMap.get(otu).get + 1)
        tadMap
      }
      else {
        tadMap.put(otu, 1)

        tadMap
      }
    })
    res
  }

  /**
    * Converts abundance to TAD as Hashmap(abundance, number of species)
    *
    * @param lAbundance - list of Long values containing abundance per species
    * @return
    */
  def convertAbundance2TADLong(lAbundance: List[Long]): scala.collection.immutable.Map[Long, Int] = {
    val tadMap = scala.collection.mutable.HashMap.empty[Long, Int]

    val res = lAbundance.foldLeft(tadMap)((tadMap: mutable.HashMap[Long, Int], otu: Long) => {
      if (tadMap.contains(otu.toLong)) {
        tadMap.put(otu.toLong, tadMap.get(otu).get + 1)
        tadMap
      }
      else {
        tadMap.put(otu.toLong, 1)
        tadMap
      }
    })
    res.map(kv=>(kv._1,kv._2)).toMap
  }

  def convertOTUs2Abundance(lOTUs: List[Int]) = {
    val abundanceMap: scala.collection.mutable.HashMap[Int, Int] = scala.collection.mutable.HashMap.empty[Int, Int]
    val res = lOTUs.foldLeft(abundanceMap)((abuMap: mutable.HashMap[Int, Int], otu: Int) => {
      if (abuMap.contains(otu)) {
        abuMap.put(otu, abuMap.get(otu).get + 1)
        abuMap
      }
      else {
        abuMap.put(otu, 1)
        abuMap
      }
    })
    res
  }

  def getInputParams(args: Array[String]): InputParamsPartial = {
    val parser: OptionParser = new OptionParser() {
      acceptsAll(asList("h", "?"), "show help").forHelp()
    }
    val classString = Class.forName("java.lang.String");


    val mainDirSpec = parser.accepts("dir", "directory - calculate for all subdirs").withRequiredArg().ofType(classString)


    parser.acceptsAll(asList("h", "?"), "show help").forHelp();

    val optionsSet: OptionSet = parser.parse(args: _*)

    parser.printHelpOn(System.out)

    val mainDir = mainDirSpec.value(optionsSet).toString

    InputParamsPartial(mainDir)
  }

  def main(args: Array[String]): Unit = {
    var mainDir1 = "/media/sf_data/nps409/Work2Sync/Projects/emp_dev_scala_runs/TAD_DATA_Synthetic_Out/SRS711891_Synthetic_Out/44xSRS711891_synthetic__160630_1444-36_GENERATED/old_test"
    var mainDir2 = "/media/sf_data/nps409/Work2Sync/Projects/emp_dev_scala_runs/TAD_DATA_Synthetic_Out/SRS711891_Synthetic_Out/AutoEval_160629_1555-27_160816/test01"
    var mainDir3 = ""
    var params = getInputParams(args)
    var mainDir = params.dir
    // "44xSRS711891_OTU__Community__100_pc_lognormal_160630_1444-36.csv"
    doConversionsFromOTU(mainDir)
    println("")

  }

  /**
    * Converts OTU data into abundance and TAD data.
    *
    * @param mainDir directory with  otu file
    */
  def doConversionsFromOTU(mainDir: String) = {
    val filePattern4OTUDataStr: String =
      """^(\d{1,2}x[a-zA-Z0-9]*)_OTU_(.*)\.csv$"""
    val otuFile = ResultsCollector.getFileNameforPattern(new File(mainDir), filePattern4OTUDataStr)
    val outsS: Try[DataTable] = readOTUFile(otuFile.get)
    val outs = outsS.get
    val otuFileOnly = otuFile.get.getName
    val pattern = """^(\d{1,2}x[a-zA-Z0-9]*)_OTU_(.*)\.csv$""".r
    val pattern(part1, part2) = otuFileOnly
    // *** New file names ***
    val fileName4Abundance = mainDir + File.separator + part1 + "_Abundance_" + part2 + ".csv"
    val fileName4TADSample = mainDir + File.separator + part1 + "_tad_" + part2 + ".ssample"
    val fileName4TADCsv = mainDir + File.separator + part1 + "_tad_" + part2 + ".csv"

    //----------------
    val otusL = outs.columns.getAs[Int](0).get.data.toList
    val abu = convertOTUs2Abundance(otusL)
    // *****************************************
    val abundance: List[Int] = abu.values.toList
    val abundanceSorted = abundance.sorted
    val tadTmp: scala.collection.mutable.HashMap[Int, Int] = convertAbundance2TAD(abundance)
    val tad = ListMap(tadTmp.toSeq.sortWith(_._1 < _._1): _*)
    val (okAbu: Boolean, okTAD: Boolean, msg: String) = verifyResults(otusL, abu, tad)

    //val (allright:Boollean,message:String) = verifynLAndnD(tad2,abundanceSorted)
    // ****************************************
    writeAbundanceToFile(abundance, fileName4Abundance)
    writeTADToCSVFile(tad, fileName4TADCsv)
    writeTADToSsampleFile(tad, fileName4TADSample)

    println("")
  }

  def verifyResults(otusL: List[Int], abu: mutable.HashMap[Int, Int], tad: ListMap[Int, Int]): (Boolean, Boolean, String) = {
    val nLOtus = otusL.size
    val nDOtus = otusL.distinct.size
    val nLTAD = tad.map(a => a._1 * a._2).sum
    val nDTAD = tad.map(a => a._2).sum
    val nDAbu = abu.size
    val nLAbu = abu.values.sum

    val strBuilder = StringBuilder.newBuilder
    var allOK: Boolean = if (nLOtus != nLAbu) {
      strBuilder.append(s"nLOtus<>nLAbu where nlOtus = $nLOtus and nLAbu = $nLAbu");
      false
    } else
      true

    allOK = if (nDOtus != nDAbu) {
      strBuilder.append(s"nLOtus<>nLAbu where nDOtus = $nDOtus and nDAbu = $nDAbu");
      allOK && false
    } else allOK && true

    allOK = if (nLOtus != nLTAD) {
      strBuilder.append(s"nLOtus<>nLAbu where nLOtus = $nLOtus and nLTAD = $nLTAD");
      allOK && false
    } else allOK && true


    allOK = if (nDOtus != nDTAD) {
      strBuilder.append(s"nLOtus<>nLAbu where nDOtus = $nDOtus and nDTAD = $nDTAD");
      allOK && false
    } else allOK && true



    if (allOK) {
      strBuilder.clear();
      strBuilder.append("Everything OK!")
    }

    //**************************
    ((nLOtus == nLAbu && nDOtus == nDAbu), (nLOtus == nLTAD && nDOtus == nDTAD), strBuilder.toString())
  }

  def writeTADToSsampleFile(tad: ListMap[Int, Int], fileName4TADSample: String) = {
    implicit object MyFormat extends DefaultCSVFormat {
      override val delimiter = ' '
    }
    val csvWriter: CSVWriter = CSVWriter.open(fileName4TADSample)
    csvWriter.writeRow(List(tad.size))
    tad.foreach(a => csvWriter.writeRow(List(a._1, a._2)))
    csvWriter.close()

  }

  def writeTADToCSVFile(tad: ListMap[Int, Int], fileName4Abundance: String, colNames: List[String] = List("Abundance", "Num.Species")) = {
    val csvWriter: CSVWriter = CSVWriter.open(fileName4Abundance)
    csvWriter.writeRow(colNames)
    tad.foreach(a => csvWriter.writeRow(List(a._1, a._2)))
    csvWriter.close()
  }

  /**
    * Write abundance @abundance to files
    *
    * @param abundance
    * @param fileName4Abundance
    */
  def writeAbundanceToFile(abundance: List[Int], fileName4Abundance: String, colNames: List[String] = List("""abundance""")): Unit = {
    val csvWriter: CSVWriter = CSVWriter.open(fileName4Abundance)
    csvWriter.writeRow(colNames)
    abundance.foreach(a => csvWriter.writeRow(List(a)))
    csvWriter.close()
  }

  def readOTUFile(file: File) = {
    val colTypes = Map("otus" -> Int)
    val tadData = DataTableReadWrite.readCsv("TADS", file, colTypes)
    tadData
  }

  def saveTADasCSV(filePath: String): Unit = {
    val csvWriter = CSVWriter.open(filePath)

  }

  def saveTADasSample(filePath: String): Unit = {

  }

}
