package utils

import java.io.{File, IOException}
import java.nio.file.Path
import java.util

import breeze.stats.DescriptiveStats._
import basics.SamplingEffort.MCTraceRecord
import basics.{MetropolisHMCMC, SamplingEffort}
import breeze.linalg.{DenseMatrix, DenseVector}
import com.typesafe.scalalogging.{LazyLogging, Logger}
import utils.Statistics4MCMC.covarianceToCorrelationAndVariance

import scala.util.{Failure, Success, Try}


/**
  * Created by peter on 06/03/17.
  */

class MCMCIO {

}


object MCMCIO extends LazyLogging {
  val loggerInfo = Logger("utils.MCMCIO.info")


  def absoluteToRelativePath(basePath: String, targetPath: String): Option[String] = {
    val relative: Try[String] = Try(new File(basePath).toPath.relativize(new File(targetPath).toPath).toString)
    relative.toOption

  }


  /** *
    * Read csv file as Array of Array of Double-s
    *
    * @param fileName
    * @param colNamesToRead
    * @param nLines2Skip
    * @return
    */
  def readDataPosteriorEstimates(fileName: String, colNamesToRead: List[String], nLines2Skip: Int): (List[List[Double]], Array[String]) = {
    if (!(new File(fileName)).exists) {
      throw new IOException("File " + fileName + " does not exists!")
    }
    val lines = io.Source.fromFile(fileName).getLines
    val res: (List[List[Double]], Array[String]) = if (lines.hasNext) {
      val heads = lines.next().toString().split(",")
      val headIdx = colNamesToRead.map(heads.indexOf(_))
      val t0: Iterator[List[Double]] = lines collect { case line: String => extractValues(line, headIdx) }
      val t0L: List[List[Double]] = t0.toList
      val n2Drop: Int = (nLines2Skip).toInt
      val n = t0L.size
      val res = if (n < n2Drop) {
        val r0 = (n * 2 / 5).toInt
        val d0 = n - r0
        val r1 = 4
        val d1 = n - r1
        val r = Math.max(r0, r1)
        val n2Drop = n - r
        logger.error(s"Only $r values used to calculate covariance!")
        t0L.drop(n2Drop)
      } else {
        val r = n - n2Drop
        loggerInfo.info(s"Using $r values to calculate covariance.")
        t0L.drop(n2Drop)
      }
      (res, heads)
    } else {
      throw new Exception(s"File $fileName does not have enough lines - number lines = ${lines.length}")
      (List(List()), Array())
    }

    res
  }

  /** *
    * Read csv file as Array of Array of Double-s
    *
    * @param fileName
    * @param colNamesToRead
    * @param nLines2Skip
    * @return
    */
  def readDataPosteriorEstimatesSafer(fileName: String, colNamesToRead: List[String], nLines2Skip: Int): Try[(List[List[Double]], Array[String])] = {
    if (!(new File(fileName)).exists) {
      throw new IOException("File " + fileName + " does not exists!")
    }
    val lines: Iterator[String] = io.Source.fromFile(fileName).getLines
    val res: Try[(List[List[Double]], Array[String])] = if (lines.hasNext) {
      val heads: Array[String] = lines.next().toString().split(",")
      //***************************************************************************
      //      if(lines.size==heads.size){
      //
      //      }else{
      //
      //      }
      //---------------------------------------------------------------------------
      val headIdx = colNamesToRead.map(heads.indexOf(_))
      val t0: Iterator[List[Double]] = lines collect { case line: String => extractValues(line, headIdx) }
      val t0L: List[List[Double]] = t0.toList
      val n2Drop: Int = (nLines2Skip).toInt
      val n = t0L.size
      val res = if (n < n2Drop) {
        val r0 = (n * 2 / 5).toInt
        val d0 = n - r0
        val r1 = 4
        val d1 = n - r1
        val r = Math.max(r0, r1)
        val n2Drop = n - r
        logger.error(s"Only $r values used to calculate covariance!")
        t0L.drop(n2Drop)
      } else {
        val r = n - n2Drop
        loggerInfo.info(s"Using $r values to calculate covariance.")
        t0L.drop(n2Drop)
      }
      Success(res, heads)
    } else {
      println(s"File $fileName does not have enough lines - number lines = ${lines.length}")
      scala.util.Failure(new Exception(s"File $fileName does not have enough lines - number lines = ${lines.length}"))
    }

    res
  }


  val numRows2Skip = 0


  def parseDouble(s: String): Option[Double] = Try {
    s.toDouble
  }.toOption

  /**
    * Extract values from line
    *
    * @param line
    * @param colIdx
    * @return
    */
  def extractValues(line: String, colIdx: List[Int]): List[Double] = {
    val a: Array[String] = line.toString().split(",")
    val selectedValues = colIdx.map(i => a(i).toDouble)
    selectedValues
  }

  /**
    * Class to store output values
    *
    * @param covVar
    * @param correlation
    */
  case class CovVarResults(covVar: DenseMatrix[Double], correlation: DenseMatrix[Double])

  def main(args: Array[String]): Unit = {
    val postParamFileN2 = "/media/sf_data/nps409/Work2Sync/Projects/emp_dev_scala_runs/TAD_DATA_Synthetic_Out/Topsy/QC_Paper_170219/Brazil_0_9_170207/s-1200K-lognormal-Pegasus-1.05-1.001-170212-1814_54/posterior-s-s-all-data_sampleSize_TAD_90_pc_D_24877.8_syntheticSampleSizes_lognormal_170207_1942-44_17-02-07_2010-22.csv_170212-1814_54.csv"


    val postParamFileN = "/media/sf_data/nps409/Work2Sync/Projects/emp_dev_scala_runs/TAD_DATA_Synthetic_Out/Topsy/QC_Paper_170319_covar_hetero/GOS_90_170209/s-800K-lognormal-Pegasus-1.05-1.001-170327-1020_45/posterior-s-s-all-data_sampleSize_TAD_90_pc_D_8925.3_syntheticSampleSizes_lognormal_170209_0213-39_17-02-09_0219-42.csv_170327-1020_45.csv" +
      ""
    // nIter,mu,variance,S,nLL,acceptance,n_accepted
    val colNames = List[String]("mu", "variance", "S")
    val numRows2Skip = 10000
    val correlationAndCovariance = correlationAndCovarianceForCSVData(postParamFileN, colNames, numRows2Skip)
    println(s"------")
  }

  /**
    * Calculate covariance and correlation for variable from file with headers
    *
    *
    */
  //  private def correlationAndCovarianceForCSVData(postParamFileN: String, colNames: List[String]) = {
  //    val numRows2Skip=10000
  //    val (dataPosterior: List[List[Double]], head) = readDataPosteriorEstimates(postParamFileN, colNames, numRows2Skip)
  //    val dMatrix: DenseMatrix[Double] = arrayToDensMatrix(dataPosterior)
  //    val covariance: DenseMatrix[Double] = breeze.stats.covmat(dMatrix)
  //    // breeze.stats.DescriptiveStats.corrcoeff
  //    val correlationCoef = breeze.stats.corrcoeff(dMatrix)
  //    CovVarResults(covVar = covariance, correlation = correlationCoef)
  //  }

  /**
    *
    * @param postParamFileN
    * @param colNames
    * @param numRows2Skip
    * @return
    */
  private def correlationAndCovarianceForCSVData(postParamFileN: String, colNames: List[String], numRows2Skip: Int): CovVarResults = {
    val (dataPosterior: List[List[Double]], head) = readDataPosteriorEstimates(postParamFileN, colNames, numRows2Skip)
    val dMatrix: DenseMatrix[Double] = arrayToDensMatrix(dataPosterior)
    val covariance: DenseMatrix[Double] = breeze.stats.covmat(dMatrix)
    // breeze.stats.DescriptiveStats.corrcoeff
    val correlationCoef: DenseMatrix[Double] = breeze.stats.corrcoeff(dMatrix)
    CovVarResults(covVar = covariance, correlation = correlationCoef)
  }

  private def adjustedCorrelationAndCovarianceForCSVData4LogNormal(postParamFileN: String, colNames: List[String], numRows2Skip: Int): Try[CovVarResults] = {
    val res: Try[(List[List[Double]], Array[String])] = readDataPosteriorEstimatesSafer(postParamFileN, colNames, numRows2Skip)
    val rF: Try[CovVarResults] = res match {
      case Success((dataPosterior: List[List[Double]], head: Array[String])) =>
        //    val (dataPosterior: List[List[Double]], head) = readDataPosteriorEstimates(postParamFileN, colNames, numRows2Skip)
        // Adjust here
        val idxMu: Int = head.indexOf("mu") - 1
        val idxVar: Int = head.indexOf("variance") - 1
        // TODO: 170418 adjust input data

        //    val dataPosteriorAdjusted: Seq[List[Double]] = dataPosterior.map(row =>  List[Double](row(idxMu),row(idxVar)):::row.drop(2)).toList
        val dataPosteriorAdjusted = dataPosterior.map(row => List[Double](row(idxMu) + 0.5 * row(idxVar), row(idxVar)) ::: row.drop(2))

        // Adjuste here
        val dMatrix: DenseMatrix[Double] = arrayToDensMatrix(dataPosteriorAdjusted)
        val covariance: DenseMatrix[Double] = breeze.stats.covmat(dMatrix)
        // breeze.stats.DescriptiveStats.corrcoeff
        val correlationCoef: DenseMatrix[Double] = breeze.stats.corrcoeff(dMatrix)
        Success(CovVarResults(covVar = covariance, correlation = correlationCoef))
      case Failure(e) => Failure(e)
    }
    rF
  }

  /**
    * Inverse Gaussian
    *
    * @param postParamFileN
    * @param colNames
    * @param numRows2Skip
    * @return
    */
  private def unAdjustedCorrelationAndCovarianceForCSVData4IG(postParamFileN: String, colNames: List[String], numRows2Skip: Int): Try[CovVarResults] = {
    //Try[(List[List[Double]], Array[String])]
    val res: Try[(List[List[Double]], Array[String])] = readDataPosteriorEstimatesSafer(postParamFileN, colNames, numRows2Skip)

    val outRes = res match {
      case Success((dataPosterior: List[List[Double]], head)) =>
        //    val (dataPosterior: List[List[Double]], head) = readDataPosteriorEstimates(postParamFileN, colNames, numRows2Skip)
        // Adjust here
        //    val idxMu: Int = head.indexOf("mu") - 1
        //    val idxVar: Int = head.indexOf("variance") - 1
        // TODO: 170418 adjust input data

        //     val dataPosteriorAdjusted: Seq[List[Double]] = dataPosterior.map(row => row.drop(1))
        val dataPosteriorAdjusted: Seq[List[Double]] = dataPosterior

        // Adjuste here
        val dMatrix: DenseMatrix[Double] = arrayToDensMatrix(dataPosteriorAdjusted)
        val covariance: DenseMatrix[Double] = breeze.stats.covmat(dMatrix)
        // breeze.stats.DescriptiveStats.corrcoeff
        val correlationCoef: DenseMatrix[Double] = breeze.stats.corrcoeff(dMatrix)
        Success(CovVarResults(covVar = covariance, correlation = correlationCoef))
      case Failure(e) => println(e.toString); Failure(e)
    }
    outRes
  }

  // varColNamesOut: List[String]


private def unAdjustedCorrelationAndCovarianceForCSVData4IGWLn(postParamFileN: String, colNames: List[String], varColNamesOut: List[String] ,numRows2Skip: Int): Try[CovVarResults] = {
  //Try[(List[List[Double]], Array[String])]
  val res: Try[(List[List[Double]], Array[String])] = readDataPosteriorEstimatesSafer(postParamFileN, colNames, numRows2Skip)

  val outRes = res match {
    case Success((dataPosterior: List[List[Double]], head)) =>
      // TODO: 170418 adjust input data
      val dataPosteriorAdjusted: Seq[List[Double]] = dataPosterior.map(row => {
        val newRow = row.map(x => scala.math.log(x)) //.toList
        newRow
      })
      // Adjuste here
      val dMatrix: DenseMatrix[Double] = arrayToDensMatrix(dataPosteriorAdjusted)
      val covariance: DenseMatrix[Double] = breeze.stats.covmat(dMatrix)
      val correlationCoef: DenseMatrix[Double] = breeze.stats.corrcoeff(dMatrix)
      Success(CovVarResults(covVar = covariance, correlation = correlationCoef))
    case Failure(e) => println(e.toString); Failure(e)
  }
  outRes
  }

  private def unAdjustedCorrelationAndCovarianceForCSVData4IGWLn(postParamFileN: String, colNames: List[String], numRows2Skip: Int): Try[CovVarResults] = {
  //Try[(List[List[Double]], Array[String])]
  val res: Try[(List[List[Double]], Array[String])] = readDataPosteriorEstimatesSafer(postParamFileN, colNames, numRows2Skip)

  val outRes = res match {
    case Success((dataPosterior: List[List[Double]], head)) =>
      // TODO: 170418 adjust input data
      val dataPosteriorAdjusted: Seq[List[Double]] = dataPosterior.map(row => {
        val newRow = row.map(x => scala.math.log(x)) //.toList
        newRow
      })
      // Adjuste here
      val dMatrix: DenseMatrix[Double] = arrayToDensMatrix(dataPosteriorAdjusted)
      val covariance: DenseMatrix[Double] = breeze.stats.covmat(dMatrix)
      val correlationCoef: DenseMatrix[Double] = breeze.stats.corrcoeff(dMatrix)
      Success(CovVarResults(covVar = covariance, correlation = correlationCoef))
    case Failure(e) => println(e.toString); Failure(e)
  }
  outRes
  }


  /**
    * Inverse Gaussian
    *
    * @param postParamFileN
    * @param colNames
    * @param numRows2Skip
    * @return
    */
  private def adjustedCorrelationAndCovarianceForCSVData4IG(postParamFileN: String, colNames: List[String], numRows2Skip: Int): CovVarResults = {
    val (dataPosterior: List[List[Double]], head) = readDataPosteriorEstimates(postParamFileN, colNames, numRows2Skip)
    // Adjust here
    val idxMu: Int = head.indexOf("mu") - 1
    val idxVar: Int = head.indexOf("variance") - 1
    // TODO: 170418 adjust input data

    //    val dataPosteriorAdjusted: Seq[List[Double]] = dataPosterior.map(row =>  List[Double](row(idxMu),row(idxVar)):::row.drop(2)).toList
    val dataPosteriorAdjusted: Seq[List[Double]] = dataPosterior.map(row => List[Double](row(idxMu) + 0.5 * row(idxVar), row(idxVar)) ::: row.drop(2))

    // Adjuste here
    val dMatrix: DenseMatrix[Double] = arrayToDensMatrix(dataPosteriorAdjusted)
    val covariance: DenseMatrix[Double] = breeze.stats.covmat(dMatrix)
    // breeze.stats.DescriptiveStats.corrcoeff
    val correlationCoef: DenseMatrix[Double] = breeze.stats.corrcoeff(dMatrix)
    CovVarResults(covVar = covariance, correlation = correlationCoef)
  }

  /**
    * Adjustedd variance of mean using variance
    *
    * @param colNames
    * @param posteriorParamFileN
    */
  def getPartialAdjusteCovarinceFromPosteriors4Lognormal(posteriorParamFileN: String, colNames: List[String] = List[String]("mu", "variance", "S"), numRows2Skip: Int): Try[DenseMatrix[Double]] = {
//    val covarCorrResults: CovVarResults = adjustedCorrelationAndCovarianceForCSVData4LogNormal(posteriorParamFileN, colNames, numRows2Skip)
//    covarCorrResults.covVar


    val covarCorrResults: Try[CovVarResults] = adjustedCorrelationAndCovarianceForCSVData4LogNormal(posteriorParamFileN, colNames, numRows2Skip)
    val covVar: Try[DenseMatrix[Double]] = covarCorrResults match {

      case Failure(e) => Failure(e)
    }
    covVar
  }


  /**
    * Adjustedd variance of mean using variance
    *
    * @param colNames
    * @param posteriorParamFileN
    */
  def getPartialAdjusteCovarinceFromPosteriors4IG(posteriorParamFileN: String, colNames: List[String] = List[String]("mu", "variance", "S"), numRows2Skip: Int): Try[DenseMatrix[Double]] = {
    val covarCorrResults: Try[CovVarResults] = unAdjustedCorrelationAndCovarianceForCSVData4IG(posteriorParamFileN, colNames, numRows2Skip)
    val covVar: Try[DenseMatrix[Double]] = covarCorrResults match {
      case Success(cv) => Success(cv.covVar)
      case Failure(e) => Failure(e)
    }
    covVar
  }
// varColNamesOut

  def getPartialAdjusteCovarinceFromPosteriors4IGWLn(posteriorParamFileN: String, colNames: List[String] = List[String]("mu", "variance", "S"),  numRows2Skip: Int): Try[DenseMatrix[Double]] = {
    val covarCorrResults: Try[CovVarResults] = unAdjustedCorrelationAndCovarianceForCSVData4IGWLn(posteriorParamFileN, colNames, numRows2Skip)
    val covVar: Try[DenseMatrix[Double]] = covarCorrResults match {
      case Success(cv) => Success(cv.covVar)
      case Failure(e) => Failure(e)
    }
    covVar
  }

//  def getPartialAdjusteCovarinceFromPosteriors4IGWLn(posteriorParamFileN: String, colNames: List[String] = List[String]("mu", "variance", "S"), numRows2Skip: Int): Try[DenseMatrix[Double]] = {
//    val covarCorrResults: Try[CovVarResults] = unAdjustedCorrelationAndCovarianceForCSVData4IGWLn(posteriorParamFileN, colNames, numRows2Skip)
//    val covVar: Try[DenseMatrix[Double]] = covarCorrResults match {
//      case Success(cv) => Success(cv.covVar)
//      case Failure(e) => Failure(e)
//    }
//    covVar
//  }


  /*
   val colNames = List[String]("mu", "variance", "S")
    val correlationAndCovariance = correlationAndCovarianceForCSVData(postParamFileN, colNames)
   */


  /**
    * *
    *
    * @param dataPosterior
    * @return
    */
  def arrayToDensMatrix(dataPosterior: Seq[List[Double]]) = {
    DenseMatrix(dataPosterior.toArray: _*)
  }
}
