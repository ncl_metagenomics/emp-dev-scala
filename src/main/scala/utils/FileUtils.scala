package utils

import java.io.{FileInputStream, FileOutputStream, File => JavaFile}
import java.nio.file.Paths
import java.text.SimpleDateFormat
import java.util.Calendar

import basics.ResultsCollector.getSubdirs

import scala.util.{Failure, Success, Try}
import scala.language.postfixOps

object FileUtils {
  final val fString = "yyMMdd-HHmm_ss"
  final val fString2 = "yyMMdd_HHmm-ss"

  def getDirOfPosteriors(dir: String, pattern: String = ".*"): List[JavaFile] = {
    getSubdirs(dir, pattern)
  }


  /**
    *
    * @param from
    * @param to
    * @return
    */
  def copyFile(from: String, to: String): Try[Long] = {
    val src = new java.io.File(from)
    val dest = new java.io.File(to)
    val res = Try(new FileOutputStream(dest) getChannel() transferFrom(
      new FileInputStream(src) getChannel, 0, Long.MaxValue))
    res.transform(s => Success(s), e => Failure(new Exception(s"src = ${src.getCanonicalPath} ")))
  }

  /**
    *
    *
    */
  def copyFileToDir(from: String, toDir: String): Try[Long] = {
    val srcFileName = (new java.io.File(from)).getName()
    val fullOutputFN = Paths.get(toDir, srcFileName)
    //    val src = new JavaFile(from)
    println(s"copyFileToDir")
    println(s"From: ${from}")
    println(s"To: ${fullOutputFN.toString}")
    Try((new JavaFile(fullOutputFN.toString)).getCanonicalFile.getParentFile.mkdirs)
    copyFile(from, fullOutputFN.toString)
  }

  def getDateAsString(formatStr: String): String = {
    var today = Calendar.getInstance().getTime();
    var formatter = new SimpleDateFormat(formatStr);
    formatter.format(today);
  }

  def appendString2FileName(filenName: String, str2Append: String): String = {
    val f = new java.io.File(filenName)
    val parentDir = f.getParent
    println(s"*** Parts of parent file name: $parentDir")

    val newshortName: String = f.getName + "_" + str2Append + ".out"
    new JavaFile(parentDir, newshortName).getPath
  }
}
