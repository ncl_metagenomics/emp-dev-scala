package utils

import org.joda.time.Duration

/**
  * Created by nps409 on 13/12/16.
  */
object DateTimeUtils {
  val msInDay = (24 * 60 * 60 * 1000)
  val msInHour = (60 * 60 * 1000)
  val msInMin = (60 * 1000)
  val msInSec = (1000)

  case class TimeAsTimeTypes(days: Int = 0, hours: Int = 0, min: Int = 0, sec: Int = 0, ms: Int = 0)

  def secondsToDHM(start: org.joda.time.DateTime)(now: org.joda.time.DateTime): TimeAsTimeTypes = {
    val diff = now.getMillis - start.getMillis
    val dur = new Duration(start, now)
    val durMS: Long = dur.getMillis
    //*****************************************
    val remainder4Hours = diff % msInDay
    val diffDays = ((diff) / msInDay).toLong
    //***
    val diffHours = ((remainder4Hours) / msInHour).toLong
    val remainder4Minutes = (remainder4Hours) % msInHour
    //***
    val remainder4Seconds = (remainder4Minutes) % msInMin
    val diffMinutes = ((remainder4Minutes) / msInMin).toLong
    //***
    val diffSeconds = (remainder4Seconds / msInSec).toLong
    val remainder4MilliSeconds = remainder4Seconds % msInSec
    val diffMillisec = remainder4MilliSeconds

    TimeAsTimeTypes(diffDays.toInt, diffHours.toInt, diffMinutes.toInt, diffSeconds.toInt, diffMillisec.toInt)
  }

  def secondsToDHM(durationMilliSeconds: Long): TimeAsTimeTypes = {
    val diff = durationMilliSeconds
    //*****************************************
    val remainder4Hours = diff % msInDay
    val diffDays = (diff.toDouble / msInDay).toLong
    //***
    val diffHours = ((remainder4Hours) / msInHour).toLong
    val remainder4Minutes = (remainder4Hours) % msInHour
    //***
    val remainder4Seconds = (remainder4Minutes) % msInMin
    val diffMinutes = (remainder4Minutes.toDouble / msInMin).toLong
    //***
    val diffSeconds = (remainder4Seconds / msInSec).toLong
    val remainder4MilliSeconds = remainder4Seconds % msInSec
    val diffMillisec = remainder4MilliSeconds

    TimeAsTimeTypes(diffDays.toInt, diffHours.toInt, diffMinutes.toInt, diffSeconds.toInt, diffMillisec.toInt)
  }


  def durationToString(start: org.joda.time.DateTime)(now: org.joda.time.DateTime) = {
    val res = secondsToDHM(start)(now)
    s"${res.days} days - ${res.hours} hours - ${res.min} minutes - ${res.sec} seconds - ${res.ms} milliseconds"
  }

  def durationToString(durationMilliSecond: Long) = {
    def notNull2Str(value: Int, unit: String): String = {
      if (value > 0) s" - $value $unit" else ""
    }

    def notNul2StrStart(value: Int, unit: String): String = {
      if (value > 0) s"$value $unit" else ""
    }

    val res = secondsToDHM(durationMilliSecond)
    val sb = StringBuilder.newBuilder
    sb ++= notNul2StrStart(res.days, "days")
    sb ++= notNull2Str(res.hours, "hours")
    sb ++= notNull2Str(res.min, "minutes")
    sb ++= notNull2Str(res.sec, "seconds")
    // s"${res.days} days - ${res.hours} hours - ${res.min} minutes - ${res.sec} seconds - ${res.ms} milliseconds"
    sb.toString()
  }

}

