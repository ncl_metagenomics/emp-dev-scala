package utils

/**
  * Created by peter on 30/05/17.
  */
object RelativeFromAbsolutePath {
  def main(args: Array[String]): Unit = {
    val res = MCMCIO.absoluteToRelativePath(args(0),args(1))
    println(res)
  }
}
