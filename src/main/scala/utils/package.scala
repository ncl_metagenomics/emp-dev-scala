import scala.annotation.tailrec

/**
  * Created by peter on 14/11/16.
  */
package object utils {

  object ArrayUtil {

    def main(args: Array[String]): Unit = {
      val arr = Array(1,2,3)
      println(s"Input ${arr.mkString(", ")}")
      val res = toList1(arr)
      println(s"Result ${res.mkString(", ")}")
    }
    def toList[a](array: Array[a]): List[a] = {
      if (array == null || array.length == 0) Nil
      else if (array.length == 1) List(array(0))
      else array(0) :: toList(array.slice(1, array.length))
    }


    def toList1[a](iL: Array[a]): List[a] = {
      @tailrec
      def toListAcc[a](res: List[a], remains: Array[a]): List[a] = {
        if (remains == null) Nil
        else if (remains.length == 0) res
        else {
          toListAcc( remains(0) :: res, remains.slice(1, remains.length))
        }
      }
      toListAcc(List(iL(0)) , iL.slice(1, iL.length))
    }
  }
}
