package utils.sampling

import scala.util.{Failure, Success, Try}

object PoilogRScala {
  // val R = org.ddahl.rscala.RClient()


  val R = org.ddahl.rscala.RClient()
  val R2 = org.ddahl.rscala.RClient()

  def rPoilog(nS:Int ,mu: Double, sigma: Double): Long ={
    val R = org.ddahl.rscala.RClient()
    val nT:Try[Long] = Try(R.evalD0(s".libPaths(c(.libPaths(),'~/applications/Rpackages/'));library(poilog);rpoilog(S=$nS, mu=$mu, sig=$sigma)").asInstanceOf[Number].longValue) //R.evalI0(s"library(poilog);  rpoilog(S=$nS, mu=$mu, sig=$sigma)")
    R.exit()
    val n = nT match{
      case Success(n)=>n
      case Failure(e)=> println(s"Error ${e.getMessage}"); throw new Exception(e)
    }
    if(n<0) rPoilog(nS:Int ,mu: Double, sigma: Double) else n
  }

  def getPoilog(mu: Double, sigma: Double, n: Int): Double = {
    val p = R.evalD0(s"library(poilog);dpoilog(n=$n, mu=$mu, sig=$sigma)")
    p
  }

  def getPoilog2(mu: Double, v: Double, n: Int): Double = {
    val sigma = Math.sqrt(v)
    val p = R.evalD0(s"library(poilog);dpoilog(n=$n, mu=$mu, sig=$sigma)")
    p
  }

  def getPSPoilog(mu: Double, v: Double, n: Int): Double = {
    // val p = R2.evalD0(s"setwd('/media/sf_Dropbox/Newcastle-Project/src/Projects/Quince-C/R/poilogPS/');Rcpp::sourceCpp('src/bipoilog_s_cint.cpp');poilog(n=$n, mu=$mu, sig=$sigma)")
    val p = R2.evalD0(s"setwd('/media/sf_Dropbox/Newcastle-Project/src/Projects/Quince-C/R/poilogPS/');source('R/poilogpS.R');poilogPS(n=$n, mu=$mu, v=$v)")
    p
  }

  def getMaxFPS(mu: Double, v: Double, n: Int): Double = {    
    val p = R2.evalD0(s"setwd('/media/sf_Dropbox/Newcastle-Project/src/Projects/Quince-C/R/poilogPS/');source('R/poilogpS.R');maxfPS(n=$n, mu=$mu, v=$v)")
    p
  }

  def getLowerBoundPS(n: Int, maxVal: Double, mu: Double, v: Double): Double = {
    val r = R2.evalD0(s"setwd('/media/sf_Dropbox/Newcastle-Project/src/Projects/Quince-C/R/poilogPS/');source('R/poilogpS.R');lowerBoundPS(n=$n,m=$maxVal, mu=$mu, v=$v)")
    // lower(x,m, my, sig)
    r
  }

  def getUpperBoundPS(n: Int, maxVal: Double, mu: Double, v: Double): Double = {
    val r = R2.evalD0(s"setwd('/media/sf_Dropbox/Newcastle-Project/src/Projects/Quince-C/R/poilogPS/');source('R/poilogpS.R');upperBoundPS(n=$n,m=$maxVal, mu=$mu, v=$v)")
    // lower(x,m, my, sig)
    r
  }
  
  // lambda,  n,  mu,  sig,  fac
  def getMy_FPS(lambda: Double, n: Int,  mu: Double, v: Double,fac:Double): Double = {
    val r = R2.evalD0(s"setwd('/media/sf_Dropbox/Newcastle-Project/src/Projects/Quince-C/R/poilogPS/');source('R/poilogpS.R');my_fPS (n=$n,lambda=$lambda, mu=$mu, v=$v,fac=$fac)")
    // lower(x,m, my, sig)
    r
  }
  
  // my_fPS <- function( lambda,  n,  mu,  sig,  fac)

  def main(args: Array[String]) = {
    var n = 6
    var mu = 2
    var v = 16
    var sigma = Math.sqrt(v)

    var res = getPoilog(mu, sigma, n)
    println(s"P_poilog = $res for n=$n,mu=$mu,sigma=$sigma") //n, mu, sig

    var resDpoilog2 = getPoilog2(mu, v, n)
    println(s"P_poilog = $resDpoilog2 for n=$n,mu=$mu,sigma=$sigma") //n, mu, sig

    var resPSPoilog = getPSPoilog(mu,  v, n)
    println(s"P_SPoilog = $resPSPoilog for n=$n,mu=$mu,sigma=$sigma") //n, mu, sig
    println(s"Half P_SPoilog = ${resPSPoilog / 2} for n=$n,mu=$mu,sigma=$sigma") //n, mu, sig
    println(s"P_SPoilog/Poilog = ${resPSPoilog / resDpoilog2} for n=$n,mu=$mu,sigma=$sigma") //n, mu, sig

    println("**********************************************************************************************")

    n = 10
    mu = 2
    v = 16
    sigma = Math.sqrt(v)

    res = getPoilog(mu, sigma, n)
    println(s"P_poilog = $res for n=$n,mu=$mu,sigma=$sigma") //n, mu, sig

    resDpoilog2 = getPoilog2(mu, v, n)
    println(s"P_poilog = $resDpoilog2 for n=$n,mu=$mu,sigma=$sigma") //n, mu, sig

    resPSPoilog = getPSPoilog(mu,  v, n)
    println(s"P_SPoilog = $resPSPoilog for n=$n,mu=$mu,sigma=$sigma") //n, mu, sig
    println(s"Half P_SPoilog = ${resPSPoilog / 2} for n=$n,mu=$mu,sigma=$sigma") //n, mu, sig
    println(s"P_SPoilog/Poilog = ${resPSPoilog / resDpoilog2} for n=$n,mu=$mu,sigma=$sigma") //n, mu, sig

    println("**********************************************************************************************")

    n = 1
    mu = 2
    v = 16
    var fac=24
    sigma = Math.sqrt(v)

    res = getPoilog(mu, sigma, n)
    println(s"P_poilog = $res for n=$n,mu=$mu,sigma=$sigma") //n, mu, sig

    resDpoilog2 = getPoilog2(mu, v, n)
    println(s"P_poilog = $resDpoilog2 for n=$n,mu=$mu,sigma=$sigma, v=$v") //n, mu, sig

    resPSPoilog = getPSPoilog(mu, v, n)
    println(s"P_SPoilog = $resPSPoilog for n=$n,mu=$mu,sigma=$sigma, v=$v") //n, mu, sig
    println(s"Half P_SPoilog = ${resPSPoilog / 2} for n=$n,mu=$mu,sigma=$sigma, v=$v") //n, mu, sig
    println(s"P_SPoilog/Poilog = ${resPSPoilog / resDpoilog2} for n=$n,mu=$mu,sigma=$sigma, v=$v") //n, mu, sig 
    var m = getMaxFPS(mu, v, n)
    println(s"getmaxfPS = ${m} for n=$n,mu=$mu,sigma=$sigma, v=$v") //n, mu, sig 
    //var uB = (n:Int, m, mu, sigma)

    var lB = getLowerBoundPS(n, m, mu, sigma)
    println(s"lB = ${lB} for m=$m, n=$n,mu=$mu,sigma=$sigma, v=$v") //n, mu, sig

    var uB = getUpperBoundPS(n, m, mu, sigma)
    println(s"uB = ${uB} for m=$m, n=$n,mu=$mu,sigma=$sigma  , v=$v") //n, mu, sig
    // getMy_FPS
    var myF = getMy_FPS(m, n, mu, sigma,fac)
    println(s"myF = ${myF} for m=$m, n=$n,mu=$mu,sigma=$sigma, v=$v, fac=$fac") //n, mu, sig
  }

}