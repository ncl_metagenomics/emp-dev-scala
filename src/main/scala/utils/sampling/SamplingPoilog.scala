package utils.sampling

import java.io.{File, PrintWriter}

import breeze.stats.distributions.Uniform
import com.fasterxml.jackson.databind.{JsonNode, ObjectMapper}
import com.github.tototoshi.csv.CSVWriter
import play.libs.Json
import spray.json.DefaultJsonProtocol._
import spray.json.{RootJsonFormat, _}
import utils.{Enum4EMP, FileUtils}
import utils.debug.Tap._

import scala.collection.immutable.ListMap
import scala.collection.immutable
import scala.collection.mutable.HashMap
import scala.collection.mutable.ListBuffer


/**
  * Created by peter on 27/09/16.
  */
object SamplingPoilog {

  sealed trait SimpleTrait

  case class ParametersSamplingPoilog(S: Int = 15495, coverage: Double = 1.0, meanLog: Double = -0.86, varLog: Double = 8.91, dsName: String = "44xSRS711891", estimatedBy: String = "", distr: String = "lognormal", spec: String = "Community", date: String = "", nD: Long = 0l) extends SimpleTrait

  case class ParametersSamplingPoilog11(S: Int = 15495, coverage: Double = 1.0, meanLog: Double = -0.86, varLog: Double = 8.91, dsName: String = "44xSRS711891", estimatedBy: String = "", distr: String = "lognormal", spec: String = "Community", date: String = "", nD: Long = 0l, nL: Option[Long] = None) extends SimpleTrait {
    def this(p: ParametersSamplingPoilog, _nL: Option[Long]) = {
      this(p.S, p.coverage, p.meanLog, p.varLog, p.dsName, p.estimatedBy, p.distr, p.spec, p.date, p.nD, _nL)
    }
  }

  object ParametersSamplingPoilog {
    implicit val parametersJson: RootJsonFormat[ParametersSamplingPoilog] = jsonFormat10(ParametersSamplingPoilog.apply)
  }

  object ParametersSamplingPoilog11 {
    implicit val parametersJson11: RootJsonFormat[ParametersSamplingPoilog11] = jsonFormat11(ParametersSamplingPoilog11.apply)
  }


  //  object MyJsonProtocol extends DefaultJsonProtocol {
  //    //Explicit type added here now
  //    implicit val parametersJson : JsonFormat[Parameters] = jsonFormat10(Parameters.apply)
  //  }


  final val DATE_STR: String = FileUtils.getDateAsString(FileUtils.fString)
  val uniSampler = Uniform(0, 1)

  def writeListAsCSV[T](ts: List[T], header: List[String], fileName: String) = {
    val fw = CSVWriter.open(fileName)
    fw.writeRow(header)
    ts.foreach((a: T) => fw.writeRow(List(a)))
    fw.close()
  }

  def writeHashMapAsCSV[T0: Numeric, T1: Numeric](tadSelected: Map[T0, T1], tadSelectedPath: String, header: List[String]) = {
    val data: ListMap[T0, T1] = ListMap(tadSelected.toSeq.sortBy(_._1): _*)
    val fw = CSVWriter.open(tadSelectedPath)
    fw.writeRow(header)
    data.keys.foreach((k: T0) => fw.writeRow(List(k, data(k))))
    fw.close()
  }


  def extractOptions(): Unit = {

  }

  def main(args: Array[String]): Unit = {

    val allParams: utils.sampling.Parameters4Sampling = utils.ReadingExperimentParameters.getInputParams4SamplingCommunity(args)
    val parameters4Distribution: Parameters4Distribution = allParams.parameters4Distribution
    val paramsFN = allParams.paramsFN

    val parameters = ParametersSamplingPoilog(S = allParams.numberOfSpeciest, coverage = allParams.paramsFN.coverage, meanLog = parameters4Distribution.meanOfLogValue, varLog = parameters4Distribution.varianceOfLogValue, dsName = paramsFN.dataSetName, estimatedBy = "", distr = paramsFN.tadDistribution, spec = paramsFN.scopeOfData, date = DATE_STR, nD = 0l)


    //    val parameters = ParametersSamplingPoilog(S = 100, coverage = 1.0, meanLog = -0.86, varLog = 8.91, dsName = "44xSRS711891", estimatedBy = "", distr = "lognormal", spec = "Community", date = "", nD = 0l)
    val outputDirBasic = paramsFN.outDir
    val fNSep = "_"
    // ***********************************************************
    val outputDir = outputDirBasic + File.separator + paramsFN.dataSetName + "_" +paramsFN.isGeneratedOrSynth+"_"+"Out" + File.separator + s"${parameters.dsName}_${paramsFN.isGeneratedOrSynth}_${DATE_STR}_GENERATED"

    val outputDir2 = "/media/sf_data/nps409/Work2Sync/Projects/emp_dev_scala_runs/TAD_DATA_Synthetic_Out/TESTS_OF_SAMPLING" + File.separator + "SRS711891_Synthetic_Out" + File.separator + s"${parameters.dsName}_synthetic_${DATE_STR}_GENERATED"


    val outputDirPath: File = new File(outputDir)
    outputDirPath.mkdirs()
    // fNameAbundanceCsv <-     paste0(myParams$dsName,"_Abundance_"
    // ,spec,'_',myParams$coverage*100,'_pc_',myParams$distr,"_",dtStr,     ".csv")
    val abundanceFileName = parameters.dsName + "_Abundance_" + parameters.spec + "_" + parameters.coverage * 100 + "_pc_" + parameters.distr + "_" + DATE_STR + ".csv"
    val selectedCoverage: Double = parameters.coverage
    // 44xSRS711891_TAD__75_pc_D_11621.25_lognormal_160629_1555-27_16-08-24_2043-52.csv
    val selectedParams: ParametersSamplingPoilog = ParametersSamplingPoilog(parameters.S, selectedCoverage, parameters.meanLog, parameters.varLog, parameters.dsName, parameters.estimatedBy, parameters.distr, "_TAD_", DATE_STR, 0l)
    val sampledAbundanceFileName = selectedParams.dsName + "_Abundance_" + "Sample" + "_" + selectedParams.coverage * 100 + "_pc_" + selectedParams.distr + "_" + DATE_STR + ".csv"

    // params_sampleSize_Distribution_coverage_90_pc_nD_13945.5_syntheticSampleSizes_logNormal_16-08-31_1931-32.yaml
    val jsonCfgFileName = "config_" + parameters.dsName + "_Abundance_" + parameters.spec + "_" + selectedCoverage * 100 + "_pc_" + parameters.distr + "_" + DATE_STR + ".json"
    val jsonCfgFilePath = outputDir + File.separator + jsonCfgFileName
    val abundanceFilePath = outputDir + File.separator + abundanceFileName
    val sampledAbundanceFilePath = outputDir + File.separator + sampledAbundanceFileName

    // 44xSRS711891_TAD__98_pc_D_15185.1_lognormal_160630_1636-40_16-08-25_1604-21.csv
    val nD = selectedParams.coverage * selectedParams.S
    val tadSelectedFileName = selectedParams.dsName + s"_TAD_${(selectedParams.coverage * 100).toInt}_pc_D_${nD}_${selectedParams.distr}_${DATE_STR}.csv"
    val tadSelectedPath = outputDir + File.separator + tadSelectedFileName


    val abundanceFile = new File(abundanceFilePath)
    val abundanceWriter = CSVWriter.open(abundanceFile)
    //    val headerAbundanceFile = List("abundance")
    val headerAbundance = List("abundance")
    val headerTAD = List[String]("Abundance", "Num.Species")
    val abundance: List[Long] = getAbundaceForPoilog(parameters, abundanceWriter, headerAbundance).sorted
    val tadIn = utils.AbundanceConversion.convertAbundance2TADLong(abundance)
    val abundanceIn2: List[Long] = utils.AbundanceConversion.convertTAD2Abundance(tadIn).sorted
    println(s"\nAbu0 = $abundance")
    println(s"Abu2 = $abundanceIn2\n")
    val nL: Long = getNumOfIndividualsFromAbundance(abundance)
    val nL2: Long = getNumOfIndividualsFromAbundance(abundanceIn2)
    val abundanceIM = scala.collection.mutable.ListBuffer(abundance)
    val abundanceSampled: List[Long] = resampleWithoutReplacement(abundance, selectedCoverage)
    val tadSelected = utils.AbundanceConversion.convertAbundance2TADLong(abundanceSampled)
    //    val sampledTADHeader = List[String]("Abundance","Num.Species")
    writeListAsCSV[Long](abundanceSampled, headerAbundance, sampledAbundanceFilePath)
    writeHashMapAsCSV[Long, Int](tadSelected, tadSelectedPath, headerTAD)
    //********************************************************************

    val jsonStr = parameters.toJson.prettyPrint
    val p: PrintWriter = new PrintWriter(jsonCfgFilePath)
    p.write(jsonStr)
    p.close()
    println(jsonStr)
    println(s"JSON written into $jsonCfgFilePath")
    println(s"Output TAD into $tadSelectedPath")
    // tadSelectedPath
  }

  /**
    *
    * @param abundanceIM - abundance of species in sample
    * @param coverage    - expected fraction of species in the sample
    * @return abundance per species
    */
  def resampleWithoutReplacement(abundanceIM: List[Long], coverage: Double): List[Long] = {
    val nL = getNumOfIndividualsFromAbundance(abundanceIM)
    val nS: Int = abundanceIM.size
    val nD: Int = (coverage * nS).toInt
    println(s"Sampling $nD species")

    val abundance: ListBuffer[Long] = scala.collection.mutable.ListBuffer(abundanceIM: _*)
    //    val abundanceProb: ListBuffer[Double] = abundance.map(a => a / nL.asInstanceOf[Double])
    val selectedAbundance: scala.collection.mutable.HashMap[Long, Long] = HashMap[Long, Long]()

    val a = getSample(nD, abundance, nL, /*abundanceProb,*/ selectedAbundance)
    a.toList
  }


  /**
    *
    * @param nD
    * @param abundance
    * @param nL
    * @param selectedAbundance
    * @return
    */
  def getSample(nD: Int, abundance: ListBuffer[Long], nL: Long, /* abundanceProb: ListBuffer[Double],*/ selectedAbundance: HashMap[Long, Long]): Iterable[Long] = {
    var a = getSample0(nD, abundance, nL, /*abundanceProb,*/ selectedAbundance)
    a.values
  }

  def findBinIndex(abundance: ListBuffer[Long], u: Double, nL: Long): Int = {

    def findBinIdexRec(abundance: ListBuffer[Long], u: Double, index: Int, sumProb: Double): Int = {
      val newSumProb: Double = sumProb + abundance.lift(index).get.toDouble / nL.toDouble
      //      println(s"newSumProb $newSumProb")
      if (sumProb < u && u <= newSumProb) {
        index
      } else {
        findBinIdexRec(abundance, u, index + 1, newSumProb)
      }
    }

    findBinIdexRec(abundance, u, 0, 0d)
  }

  /**
    * Recursively samples individuals in (selectedAbundance) in the form of abundance per species from community until required fraction of species D/S is sampled.
    *
    * @param nD
    * @param abundance
    * @param nL
    * @param selectedAbundance
    * @return abundanbce per species
    */
  def getSample0(nD: Int, abundance: ListBuffer[Long], nL: Long, /*abundanceProb: ListBuffer[Double],*/ selectedAbundance: HashMap[Long, Long]): HashMap[Long, Long] = {
    if (selectedAbundance.size >= nD)
      selectedAbundance
    else {
      var u: Double = uniSampler.draw()
      var speciesId = findBinIndex(abundance, u, nL)
      if (selectedAbundance.contains(speciesId.toLong)) selectedAbundance.put(speciesId, selectedAbundance(speciesId) + 1) else selectedAbundance.put(speciesId, 1)
      //      println(s"species number $speciesId selected wih total count ${selectedAbundance(speciesId)}")
      abundance.update(speciesId, abundance(speciesId) - 1)
      //      println(s"${selectedAbundance.size} selected individuals")
      if (nL % 10 == 0) {
        println(s"${selectedAbundance.size} selected species and ${getNumOfIndividualsFromAbundance(selectedAbundance.values.toList)} individuals , target = $nD")
      }
      getSample0(nD, abundance, nL - 1, /*abundanceProb,*/ selectedAbundance)
    }
  }

  /**
    *
    * @param abundance
    * @return
    */
  def getNumOfIndividualsFromAbundance(abundance: List[Long]): Long = {
    abundance.foldLeft(0L)((r, c) => r + c.toLong)
  }

  /**
    * Sample abundance for every species
    *
    * @param parameters
    * @param fw
    * @param header
    * @return
    */
  def getAbundaceForPoilog(parameters: ParametersSamplingPoilog, fw: CSVWriter, header: List[String]): List[Long] = {
    fw.writeRow(header)
    val coverage = 1d
    val nS: Int = (parameters.S * coverage).round.toInt
    println(s"""nS = $nS""")
    val sigmaLog = Math.sqrt(parameters.varLog)

    def rPoilog = PoilogRScala.rPoilog(parameters.S, parameters.meanLog, sigmaLog)

    var iterPerPart: ListBuffer[Int] = new ListBuffer[Int]()
    var parts = 5
    var nIterPerPart: Int = (nS / parts)
    var ml: List[Int] = List.fill[Int](parts - 1)(nIterPerPart)
    iterPerPart ++= ml
    iterPerPart += nS - (parts - 1) * nIterPerPart
    var abundance = (1 to parts).toList.tap(x => println("Part " + x)).par.map(i => (1 to iterPerPart(i - 1)).map(subIdx => {
      val n = rPoilog;
      if (subIdx % 20 == 0) println(s"Part = $i - subIdx = $subIdx/${iterPerPart(i - 1)}, n = $n");
      fw.writeRow(List(n));
      n
    })).flatten.toList
    //    abundanceToOTUs
    fw.close()
    println("-------------------------")
    println("")
    abundance
  }

  def mySum(res: Long, x: Int): Long = res + x.toLong


}
