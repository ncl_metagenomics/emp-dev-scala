package utils

/**
  * Created by peter on 01/11/16.
  */
package object sampling {

  trait Parameters4Distribution {
    def meanOfLogValue: Double

    def varianceOfLogValue: Double
  }

  sealed abstract class Headers4AbundanceData(val name: String, val headers: List[String]) extends Headers4AbundanceData.Value

  object Headers4AbundanceData extends Enum4EMP[Headers4AbundanceData] {
    val HeaderAbundance = new Headers4AbundanceData("headerAbundance", List("abundance")) {}
    val HeaderTAD = new Headers4AbundanceData("headerTAD", List[String]("Abundance", "Num.Species")) {}
  }

  case class Parameters4FileName(dataSetName: String = "44xSRS711891", dataFormat: Option[String] = Some("Abundance"), scopeOfData: String = "Community", isGeneratedOrSynth: String = "Synthetic", coverage: Double = 1d, tadDistribution: String, dateTimeStamp: String, fileFormat: String = "csv", outDir: String = ".")

  case class Parameters4Sampling(paramsFN: Parameters4FileName, parameters4Distribution: Parameters4Distribution, numberOfSpeciest: Int)

  case class Parameters4ForLognormalDistribution(meanOfLogValue: Double, varianceOfLogValue: Double) extends Parameters4Distribution

  sealed abstract class TADDistributionInfo(val name: String) extends TADDistributionInfo.Value

  object TADDistributionInfo extends Enum4EMP[TADDistributionInfo] {
    val LOGNORMAL = new TADDistributionInfo("Lognormal") {}
    val INVERSE_GAUSSIAN = new TADDistributionInfo("InverseGaussian") {}
    val DEFAULT_VALUE = LOGNORMAL
  }

  sealed abstract class DataFormatInfo(val name: String) extends DataFormatInfo.Value

  object DataFormatInfo extends Enum4EMP[DataFormatInfo] {
    val ABUNDANCE = new DataFormatInfo("Abundance") {}
    val TAD = new DataFormatInfo("TAD") {}
    val DEFAULT_VALUE = TAD
  }

  sealed abstract class ScopeOfDataInfo(val name: String) extends ScopeOfDataInfo.Value

  object ScopeOfDataInfo extends Enum4EMP[ScopeOfDataInfo] {
    val COMMUNITY = new ScopeOfDataInfo("Community") {}
    val SAMPLE = new ScopeOfDataInfo("Sample") {}
    val DEFAULT_VALUE = COMMUNITY
  }

  sealed abstract class EmptyInfo(val name: String) extends EmptyInfo.Value

  object EmptyInfo extends Enum4EMP[EmptyInfo] {

  }

  sealed abstract class RealOrSyntheticInfo(val name: String) extends RealOrSyntheticInfo.Value

  object RealOrSyntheticInfo extends Enum4EMP[RealOrSyntheticInfo] {
    val REAL = new RealOrSyntheticInfo("Real") {}
    val SYNTHETIC = new RealOrSyntheticInfo("Synthetic") {}
    val DEFAULT_VALUE = SYNTHETIC
  }


  sealed abstract class FileFormatInfo(val name: String) extends FileFormatInfo.Value

  object FileFormatInfo extends Enum4EMP[FileFormatInfo] {
    val SAMPLE = new FileFormatInfo("sample") {}
    val CSV = new FileFormatInfo("csv") {}
    val DEFAULT_VALUE = CSV
  }


  val DEFAULT_COVERAGE_OF_SPECIES: Double = 0.9

}
