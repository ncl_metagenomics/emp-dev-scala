package utils.sampling

import org.apache.commons.math3.analysis.UnivariateFunction
import org.apache.commons.math3.analysis.function.Cos
import org.apache.commons.math3.analysis.integration.gauss.{GaussIntegrator, GaussIntegratorFactory}
import org.apache.commons.math3.special.Gamma
import basics._

import scala.annotation.tailrec

object Poilog {

  def main(args: Array[String]): Unit = {
    org.apache.commons.math3.special.Gamma.logGamma(5.0)
    val n = 1
    val mu = -1
    val sig = 3.4
    
    val r = dpoilog(n: Int, mu: Double, sig: Double)
//    println(s"P(n=$n,mu=$mu, sig=$sig) is $r")
  }

  /**
   *  @param 
   *  @param 
   */
  def dpoilog(n: Int, mu: Double, sig: Double): Double = {
    require(n >= 0, "n can't be negative.")
    // if (!all(is.finite(c(mu,sig)))) stop('all parameters should be finite')
    require(!mu.isInfinite() && !mu.isNaN(), "mu should be finite")
    require(!sig.isInfinite() && !sig.isNaN(), "sig should be finite")
    // (sig<=0) stop('sig is not larger than 0')
    require(sig > 0, "sig should be positive")
    //-----
    val v = sig * sig
    val m = maxF(n, mu, v)
    val a = lower(n, m, mu, v);
    val b = upper(n, m, mu, v);
    val fac = Gamma.logGamma(n + 1)
    
    val factory: GaussIntegratorFactory = new GaussIntegratorFactory();
    val cos: UnivariateFunction = new Cos()
    val myFun:UnivariateFunction =  new MyFunction(n, mu, v, fac)
    
    // create an Gauss integrator for the interval [0, PI/2]
    val integrator: GaussIntegrator = factory.legendre(7, a, b);
    val s: Double = integrator.integrate(myFun);
    val dPoilogVal = s*(1/Math.sqrt(2*Math.PI*v))
    
    val myIntegSimpson = IntegrationWithSimpson()
    val resSimpson = myIntegSimpson.calculate(myFun1(n,mu,v,fac), a, b, 10000)    // originaly was only 1000
    val dPoilogVal2  = resSimpson*(1/Math.sqrt(2*Math.PI*v))
    
    dPoilogVal2
  }
  
  def myFun1(n: Int, mu: Double, v: Double,fac:Double)(x: Double): Double = Poilog.my_f(x, n, mu, v, fac)
  
  def logMyFun1(n: Int, mu: Double, v: Double,fac:Double)(x: Double): Double = Poilog.logMy_f(x, n, mu, v, fac)


  def maxF(n: Int, mu: Double, v: Double): Double = {
    val z: Double = 0.0
    val d: Double = 100.0
    @tailrec
    def maxFIter(n: Int, mu: Double, v: Double, z: Double, d: Double): Double = {
      if (d <= 0.00001) {
        z
      } else {
        val nz = if ((n - 1 - Math.exp(z) - 1 / v * (z - mu)) > 0) {
          z + d
        } else {
          z - d
        }
        maxFIter(n, mu, v, nz, d / 2)
      }
    }

    maxFIter(n, mu, v, z, d)
  }

  /*
	 *   double upper(int x, double m, double my, double sig)
  {
     double d,z,mf;
     mf = (x-1)*m-exp(m)-0.5/sig*((m-my)*(m-my));
     z = m+20;
     d = 10;
     while (d>0.000001) {
        if ((x-1)*z-exp(z)-0.5/sig*((z-my)*(z-my))-mf+log(1000000)>0) z=z+d; else z=z-d;
        d=d/2;
     }
     return(z);
   }
	 */

  def upper(n: Int, m: Double, mu: Double, v: Double): Double = {
    val mf = (n - 1) * m - Math.exp(m) - 0.5 / (v * ((m - mu) * (m - mu)))
    val z = m + 20
    val d = 10
    @tailrec
    def upperIter(n: Int, m: Double, mu: Double, v: Double, mf: Double, z: Double, d: Double): Double = {
      if (d > 0.000001) {
        val nz = if ((n - 1) * z - Math.exp(z) - 0.5 / v * ((z - mu) * (z - mu)) - mf + Math.log(1000000) > 0)
          z + d
        else
          z - d
        val nd = d / 2
        upperIter(n, m, mu, v, mf, nz, nd)
      } else z
    }
    upperIter(n, m, mu, v, mf, z, d)
  }

  /*
	 *   double lower(int x, double m, double my, double sig)
  {
     double d,z,mf;
     mf = (x-1)*m-exp(m)-0.5/sig*((m-my)*(m-my));
     z = m-20;
     d = 10;
     while (d>0.000001) {
        if ((x-1)*z-exp(z)-0.5/sig*((z-my)*(z-my))-mf+log(1000000)>0) z=z-d; else z=z+d;
        d=d/2;
     }
     return(z);
   }
	 */
  def lower(n: Int, m: Double, mu: Double, v: Double): Double = {
    val mf = (n - 1) * m - Math.exp(m) - 0.5 / (v * ((m - mu) * (m - mu)))
    val z = m - 20
    val d = 10
    @tailrec
    def lowerIter(n: Int, m: Double, mu: Double, v: Double, mf: Double, z: Double, d: Double): Double = {
      if (d > 0.000001) {
        val nz = if ((n - 1) * z - Math.exp(z) - 0.5 / v * ((z - mu) * (z - mu)) - mf + Math.log(1000000) > 0) {
          z - d
        } else {
          z + d
        }
        val nd = d / 2
        lowerIter(n, m, mu, v, mf, nz, nd)
      } else z
    }
    lowerIter(n, m, mu, v, mf, z, d)
  }

  /*
   * void my_f_vec(double *z, int n, void *p)
    {
       int i;
       struct my_f_params * params = (struct my_f_params *)p;
       int x      = (params->x);
       double sig = (params->sig);
       double my  = (params->my);
       double fac = (params->fac);
       for (i=0;i<n;i++) z[i]=my_f(z[i],x,my,sig,fac);
       return;
  }
   * 
   */

  /*
   * double my_f(double z, int x, double my, double sig, double fac)
  {
     return exp(z*x-exp(z)-0.5/sig*((z-my)*(z-my))-fac);
  }
   * 
   */

  /**
   * @param z  is to
   * @ return Double value of function
   *
   */
  def my_f(z: Double, x: Int, my: Double, sig: Double, fac: Double) = {
    Math.exp(z * x - Math.exp(z) - 0.5 / sig * ((z - my) * (z - my)) - fac)
  }
  
  /**
   * @param z  is to
   * @ return Double log_e value of function
   */
  def logMy_f(z: Double, x: Int, my: Double, sig: Double, fac: Double) = {
    (z * x - Math.exp(z) - 0.5 / sig * ((z - my) * (z - my)) - fac)
  }
  

  /**
   * private static class MyFunction implements UnivariateFunction {
   * public double value(double x) {
   * double y = hugeFormula(x);
   * if (somethingBadHappens) {
   * throw new LocalException(x);
   * }
   * return y;
   * }
   * }
   */

  /**
   *
   */
  class MyFunction( n: Int, mu: Double, v: Double, fac: Double) extends UnivariateFunction {
    def value(x:Double):Double= Poilog.my_f(x, n, mu, v, fac)    
       
  }

  class LocalException(x: Double) extends RuntimeException {
    // The x value that caused the problem.

    def getX(): Double = {
      x
    }
  }

}