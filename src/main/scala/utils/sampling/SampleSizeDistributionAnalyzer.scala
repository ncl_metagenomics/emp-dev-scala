package utils.sampling

import java.io.{File, FileReader}

import breeze.stats.DescriptiveStats
import com.github.martincooper.datatable.DataColumn
import basics.ResultsCollector.StatisticsInformation
import basics.{DataTableReadWrite, MetropolisHMCMC, ResultsCollector}
import utils.{Enum4EMPColectResults, Enum4EMPSampleSize4Directory}

import scala.util.{Failure, Success, Try}

/**
  * Created by peter on 15/09/16.
  */
object SampleSizeDistributionAnalyzer {

  sealed abstract class FileDirRegExPattern(val tadFileNamePattern: String, val posteriorDatafilePatternStr: String, val posteriorDir: String, val sampleSizeFile: String) extends FileDirRegExPattern.Value

  object FileDirRegExPattern extends Enum4EMPColectResults[FileDirRegExPattern] {
    val OLD = new FileDirRegExPattern("", "", "","""^data_sampleSize_Distribution_coverage_90_pc_nD_.*sampleSizeDistribution\.csv$""") {}
    //
    val HUMAN_MICROBIOME = new FileDirRegExPattern("^44xSR(S711891|R15897XX)_(TAD|tad)(_.*){0,1}\\.sample$", "^posterior-.*\\.csv$", "s_\\d{1,8}K_\\d{6}-\\d{4}-\\d{2}", "") {}
    val REAL_DATA_S = new FileDirRegExPattern("^([a-zA-Z0-9]*)_(TAD|tad)\\.(sample)$", "^posterior-.*\\.csv$", "s_\\d{1,8}K_\\d{6}-\\d{4}-\\d{2}", """^sample_size_posterior-(\w)-s-(\w{1,3})-((\w{1,}_?(tad|TAD)?)\.(\w{3}|sample))_(\d{6}-\d{4}(_|-)?\w{2})_correct_Coverage_(\d{1,}).*\.csv$""") {}
    val REAL_DATA_S_OLD = new FileDirRegExPattern("^([a-zA-Z0-9]*)_(TAD|tad)\\.(sample)$", "^posterior-.*\\.csv$", "s_\\d{1,8}K_\\d{6}-\\d{4}-\\d{2}", """^sample_size_posterior-(\w)-s-(\w{1,3})-((\w{1,}_?(tad|TAD)?)\.(\w{3}|sample))_(\d{6}-\d{4}(_|-)?\w{2})_correct_Coverage_(\d{1,}).*\.csv$""") {}
    val REAL_DATA_C = new FileDirRegExPattern("^([a-zA-Z0-9]*)_(TAD|tad)\\.(sample)$", "^posterior-.*\\.csv$", "c_\\d{1,8}K_\\d{6}-\\d{4}-\\d{2}", """^sample_size_posterior-(\w)-c-(\w{1,3})-((\w{1,}_?(tad|TAD)?)\.(\w{3}|sample))_(\d{6}-\d{4}(_|-)?\w{2})_correct_Coverage_(\d{1,}).*\.csv$""") {}
    //  posteriorDatafilePatternStr = "^posterior-.*\\.csv$"

    //    val DEFAULT_VALUE = CSV
  }

  def main(args: Array[String]): Unit = {
    val mainDir1 = "/media/sf_data/nps409/Work2Sync/Projects/emp_dev_scala_runs/TAD_DATA_Synthetic_Out/SRS711891_Synthetic_Out/AutoEval_160629_1555-27_160816/test01"
    val mainDir = "/media/sf_data/nps409/Work2Sync/Projects/emp_dev_scala_runs/TAD_DATA_Synthetic_Out/SRS711891_Synthetic_Out/AutoEval_160629_1555-27_160816/test01"
    // data_sampleSize_Distribution_coverage_90_pc_nD_13945.5_syntheticSampleSizes_logNormal_16-08-24_1845-26.sampleSizeDistribution.csv

    val statInfo = getStatsFromSampleSizeDist(mainDir)

    println(statInfo.toString)
    println("")
  }

  /*utils.sampling.SampleSizeDistributionAnalyzer.FileDirRegExPattern.REAL_DATA_S */

  def getStatsFromSampleSizeDistBase(mainDir: String)(sampleSizeFNPattern: String): StatisticsInformation = {
    // val filePattern4SSizeDistStr: String = """^data_sampleSize_Distribution_coverage_90_pc_nD_.*sampleSizeDistribution\.csv$"""
    /*"""^data_sampleSize_Distribution_coverage_90_pc_nD_.*sampleSizeDistribution\.csv$"""*/
    val sSizeDistFile = ResultsCollector.getFileNameforPattern(new File(mainDir), sampleSizeFNPattern) match {
      case Some(f: File) => f
      case None => throw new Exception("")
    }

    val res = getStatistics(sSizeDistFile)
    res
  }

  def getStatsFromSampleSizeDist(mainDir: String): StatisticsInformation = {
    getStatsFromSampleSizeDistBase(mainDir)(utils.sampling.SampleSizeDistributionAnalyzer.FileDirRegExPattern.REAL_DATA_S.tadFileNamePattern)
  }

  /**
    * * Reading in file with abundance data in form (abundance, number of species in abundance) in form of dataframe.
    * use getStatisticsFromAsDataFrame
    *
    * @param sSizeDistFile
    * @return
    */

  def getStatisticsFromTADFile(sSizeDistFile: File) = {
    // val colTypes = Map("nSamples" -> Double)
    val colTypes = Map("nL_Recommended" -> Double)
    //val tadData = DataTableReadWrite.readCsv("SampleSizeDist", new FileReader(sSizeDistFile), colTypes)

    val tadData: Try[Array[(Int, Int)]] = if (sSizeDistFile.getAbsolutePath.matches(""".*\.csv$""")) Success(MetropolisHMCMC.readDataASCSVTAD(sSizeDistFile.getAbsolutePath))
    else if (sSizeDistFile.getAbsolutePath.matches(""".*\.sample""")) Success(MetropolisHMCMC.readData(sSizeDistFile.getAbsolutePath))
    else Failure(new Exception())

    val columnName = "nL_Recommended"

    // val nSTmp = tadData.columns.getAs[Double](columnName)
    val nSTmp0: (Vector[Double],Vector[Double]) = tadData.toOption match {
      case Some(x: Array[(Int, Int)]) => {
        val unx = x.unzip
        (unx._1.toVector.map(_.toDouble),unx._2.toVector.map(_.toDouble))
      }
      case None => throw new Exception("could't extract column")
    }
    //    val tmp50: Vector[Double] = nSTmp0.map(_.toDouble)
    val nSTmp1 = nSTmp0._2
    val nSMedian = DescriptiveStats.percentile(nSTmp1, 0.5)
    val nSPercentile2_5 = DescriptiveStats.percentile(nSTmp1, 0.025)
    val nSPercentile97_5 = DescriptiveStats.percentile(nSTmp1, 0.975)
    val (nSMean, _, _) = DescriptiveStats.meanAndCov(nSTmp1, nSTmp1)
    val nSVariance = DescriptiveStats.cov(nSTmp1, nSTmp1)
    //    (nSMedian: Double, nSPercentile2_5: Double, nSPercentile97_5: Double, nSMean: Double, nSVariance.asInstanceOf[Double]: Double)
    StatisticsInformation(nSMedian, nSPercentile2_5, nSPercentile97_5, nSMean, nSVariance.asInstanceOf[Double])
  }

  /**
    * Reading in file with abundance data in form (abundance, number of species in abundance) in form of dataframe
    * rename to getStatisticsFromAsDataFrame
    *
    * @param sSizeDistFile
    * @return
    */
  def getStatistics(sSizeDistFile: File) = {
    val colTypes = Map("nSamples" -> Double)
    val tadDataS = DataTableReadWrite.readCsv("SampleSizeDist", sSizeDistFile, colTypes)
    val tadData = tadDataS.get
    val columnName = "nSamples"
    val nSTmp = tadData.columns.getAs[Double](columnName)
    val nSTmp0: Vector[Double] = nSTmp.toOption match {
      case Some(x: DataColumn[Double]) => {
        println(s"column info = ${x.toString()} column name ${x.name}");
        x.data
      }
      case None => throw new Exception("could't extract column")
    }
    //    val tmp50: Vector[Double] = nSTmp0.map(_.toDouble)
    val nSTmp1 = nSTmp0
    val nSMedian = DescriptiveStats.percentile(nSTmp1, 0.5)
    val nSPercentile2_5 = DescriptiveStats.percentile(nSTmp1, 0.025)
    val nSPercentile97_5 = DescriptiveStats.percentile(nSTmp1, 0.975)
    val (nSMean, _, _) = DescriptiveStats.meanAndCov(nSTmp1, nSTmp1)
    val nSVariance = DescriptiveStats.cov(nSTmp1, nSTmp1)
    //    (nSMedian: Double, nSPercentile2_5: Double, nSPercentile97_5: Double, nSMean: Double, nSVariance.asInstanceOf[Double]: Double)
    StatisticsInformation(nSMedian, nSPercentile2_5, nSPercentile97_5, nSMean, nSVariance.asInstanceOf[Double])
  }
}
