val a = Array((5.1,4.3),(1.1,2.2))
val b  = a.unzip
// ******************************
b._1.toList
b._2.toList
// ******************************
b._1.toList.toVector
b._2.toList.toVector
// ******************************
b._1.toVector
b._2.toVector
// ******************************

import utils.sampling._
//SampleSizeDistributionAnalyzer.getStatisticsFromTADFile("""/media/sf_data/nps409/Work2Sync/Projects/emp_dev_scala_runs/TAD_DATA_Real_Out/AUTO_RUN_TEST/FS396a/FS396a_tad.sample""")