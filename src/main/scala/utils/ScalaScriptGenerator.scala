package utils

import java.io.{File, FileWriter, RandomAccessFile}
import java.util.Arrays.asList

import joptsimple.OptionParser
import utils.ReadingExperimentParameters.{InputParametersMCMCAndSamplingEffort, classFile2}

import scala.io.Source
import scala.tools.nsc.ScriptRunner

/**
  * Created by peter on 01/06/17.
  */
object ScalaScriptGenerator {

  case class InverseGaussianParams(nS: Double, mean: Double, param2: Double)

  case class Params(niters: Int, compiledCCodePath: String, mainScriptDir: String, spiecesCoverage: Double, varCovarScalingFactor: List[Double] = List(1, 1, 1), mainClass: String, iGParams: Option[InverseGaussianParams] = None, distribution: String, varCovarMode: String)

  val dateStr = basics.MCMC.DATE_STR2


  def generateShScript(datasetRelativePath: String, varCovarRelativePath: String, jarFileRelative: String, jarDependencyFileRelative: String, params: Params): String = {
    val igParams: Option[InverseGaussianParams] = params.iGParams
    val (nS, mean, beta) = igParams match {
      case Some(p) => (p.nS, p.mean, p.param2)
      case None => throw new Exception("Missing initial values!"); (9662, 0.05, 6.481367)
    }

    val s = new StringBuilder()
    s.append(
      s"""#!/bin/bash
         |
        |#----------------------------------------------------------------------------------#
         |# Grid engine job script for a java job with DMTCP checkpointing
         |# Notes:
         |#     - You should touch AUTO_RESTART_PLEASE in the working directory before
         |#       submitting this job if you wish to automatically re-submit.
         |#     - It is recommended to give named -o and -e output and error file names
         |#       so that output from different stages of the checkpointed job are
         |#       combined.
         |#     - Problems/comments/enquiries to Chris Graham: hope this works for you :-)
         |#----------------------------------------------------------------------------------#
         |
        |# Directives to grid engine
         |#$$ -cwd                         # Use current working directory.
         |#$$ -S /bin/bash                 # Interpreting script is bash.
         |#$$ -l h_vmem=18G
         |#$$ -pe threaded 1               # Slots on the same node should use pe threaded.
         |#$$ -o output.txt                # Recommended.
         |#   #-l s_rt=04:00:00
         |#$$ -e error.txt                 # Recommended.
         |# Do not use -N if you are doing automatic resubmit.
         |
         |echo  "=========================="
         |echo "Shell script running "
         |echo  "=========================="
         |
         |module load gsl/1.16
         |
         |extJAR="emp_dev_scala-assembly-1.0-deps.jar"
         |# MyCode JAR
         |mcJAR="emp_dev_scala-assembly-1.0-170530-2057_56.jar"
         |jarDir="../../../../jars"
         |rScalaJAR="/home/peter/R/x86_64-pc-linux-gnu-library/3.3/rscala/java/rscala_2.11-1.0.9.jar"
         |# Combining
         |extJAR="$${jarDir}/$${extJAR}"
         |mcJAR="$${jarDir}/$${mcJAR}"
         |
         |mcJAR="${jarFileRelative}"
         |echo "mcJAR $${mcJAR}"
         |
         |extJAR="${jarDependencyFileRelative}"
         |echo "extJAR $${extJAR}"
         |# **********************************************************************************
         |#
         |nIter=${params.niters}
         |initMu=${mean}
         |initVariance=${beta}
         |initialS=${nS}
         |varianceScalingFactor=${params.varCovarScalingFactor.mkString(",")}
         |tadFile="$datasetRelativePath"
         |varCovarFile="$varCovarRelativePath"
         |
        |# **********************************************************************************
         |java -Xmx1000M -XX:ParallelGCThreads=1 -XX:-UseParallelGC -cp  "$${extJAR}:$${mcJAR}" ${params.mainClass} --in "$${tadFile}" ---nIterations $${nIter} -c 0.9 --paramASigma 0.1 --paramBSigma 0.1  --SSigma 400 --initMu $${initMu} --initialS $${initialS} --initVariance $${initVariance} --varCovarFile "$${varCovarFile}" --varianceScalingFactor $${varianceScalingFactor}  --distribution "${params.distribution}" --varcovarmode ${params.varCovarMode} > >(tee stdout.log) 2> >(tee stderr.log >&2)
         |
        """.stripMargin)


    s.toString()
  }


  def getSubDirs(mainDir: String, patternStr: String): Seq[File] = {
    val d = new File(mainDir)
    val tmp = d.listFiles
    val res: Seq[File] = if (d.exists && d.isDirectory) {
      d.listFiles.filter(f => f.isDirectory && patternStr.r.findFirstIn(f.getName).isDefined).toList
    } else {
      List[File]()
    }
    res
  }

  def getSubFile(mainDir: String, patternStr: String): Seq[File] = {
    val d = new File(mainDir)
    val tmp = d.listFiles
    val res: Seq[File] = if (d.exists && d.isDirectory) {
      d.listFiles.filter(f => f.isFile && patternStr.r.findFirstIn(f.getName).isDefined).toList
    } else {
      List[File]()
    }
    res
  }

  def getCovareFileName(dsDir: String, pattern: String): String = {

    val files = getSubFile(dsDir, pattern)
    val fStr = if (files.size > 0)
      files(0).getAbsolutePath
    else
      ""
    fStr
  }


  def generateDMTCP(datasetRelativePath: String, varCovarRelativePath: String, jarFileRelative: String, jarDependencyFileRelative: String, params: Params): String = {

    val igParams: Option[InverseGaussianParams] = params.iGParams
    val (nS, mean, beta) = igParams match {
      case Some(p) => (p.nS, p.mean, p.param2)
      case None => throw new Exception("Initial parameters missing"); (9662, -4.640013, 6.481367)
    }

    val s = new StringBuilder()
    s.append(
      s"""#!/bin/bash
         |
         |#----------------------------------------------------------------------------------#
         |# Grid engine job script for a java job with DMTCP checkpointing
         |# Notes:
         |#     - You should touch AUTO_RESTART_PLEASE in the working directory before
         |#       submitting this job if you wish to automatically re-submit.
         |#     - It is recommended to give named -o and -e output and error file names
         |#       so that output from different stages of the checkpointed job are
         |#       combined.
         |#     - Problems/comments/enquiries to Chris Graham: hope this works for you :-)
         |#----------------------------------------------------------------------------------#
         |
         |# Directives to grid engine
         |#$$ -cwd                         # Use current working directory.
         |#$$ -S /bin/bash                 # Interpreting script is bash.
         |#$$ -l h_vmem=18G
         |#$$ -pe threaded 1               # Slots on the same node should use pe threaded.
         |#$$ -o output.txt                # Recommended.
         |#$$ -e error.txt                 # Recommended.
         |#$$ -l s_rt=47:03:00
         |
         |# Do not use -N if you are doing automatic resubmit.
         |
         |# Load required modules
         |module load dmtcp/000-2.5.0
         |module load gsl/1.16
         |#  module load dmtcp
         |
         |# The following line ensures that the DMTCP coordinator moves with the job when restarted
         |
         |export DMTCP_COORD_HOST=$$(hostname)
         |export DMTCP_COORD_PORT=0
         |
         |# Set the checkpoint interval in seconds - adjust accordingly
         |export DMTCP_CHECKPOINT_INTERVAL=120
         |
         |
         |# Launch executable with checkpointing
         |if [ -e ./dmtcp_restart_script.sh ]
         |  then
         |    # The file dmtcp_restart_script.sh is used to restart the job
         |    echo " $$(date +%c) ($$JOB_ID) : Restarting job from checkpoint file on $$HOSTNAME, job ID $$JOB_ID."
         |    ./dmtcp_restart_script.sh
         |  else
         |    # This is our first time running the job
         |    echo " $$(date +%c) ($$JOB_ID) : Running with checkpointing for first time on $$HOSTNAME, job ID $$JOB_ID."
         |
         |
         |      extJAR="emp_dev_scala-assembly-1.0-deps.jar"
         |      # MyCode JAR
         |      mcJAR="emp_dev_scala-assembly-1.0-170530-2057_56.jar"
         |      jarDir="../../../../jars"
         |      rScalaJAR="/home/peter/R/x86_64-pc-linux-gnu-library/3.3/rscala/java/rscala_2.11-1.0.9.jar"
         |      # Combining
         |      extJAR="$${jarDir}/$${extJAR}"
         |      mcJAR="$${jarDir}/$${mcJAR}"
         |      mcJAR="${jarFileRelative}"
         |      extJAR="${jarDependencyFileRelative}"
         |      # **********************************************************************************
         |      #
         |      nIter=${params.niters}
         |      initMu=${mean}
         |      initVariance=${beta}
         |      initialS=${nS}
         |      varianceScalingFactor=0.1,0.1,0.1
         |      tadFile="$datasetRelativePath"
         |      varCovarFile="$varCovarRelativePath"
         |
         |      # **********************************************************************************
         |      dmtcp_launch  java -Xmx1000M -XX:ParallelGCThreads=1 -XX:-UseParallelGC -cp  "$${extJAR}:$${mcJAR}" ${params.mainClass} --in "$${tadFile}" --nIterations $${nIter} --initMean $${initMu} --initShape $${initVariance} --initNSpecies $${initialS}  --varCovarFile "$${varCovarFile}" --varianceScalingFactor $${varianceScalingFactor} -d "IG" -t 10 --distribution "${params.distribution}"
         |
         |
         |
         |      # > >(tee stdout.log) 2> >(tee stderr.log >&2)
         |
         |      # ----------------------------------------------------------------------------------------
         |
         |
         |fi
         |
         |exit_status=$$?
         |
         |if [ $$exit_status -eq 0 ]
         |  then
         |    # Job has completed; clean up DMTCP files
         |    echo " $$(date +%c) ($$JOB_ID) : Job completed: cleaning up DMTCP checkpoint files"
         |    rm dmtcp_restart*sh
         |    rm -r ckpt*files
         |    rm ckpt*dmtcp
         |fi
        """.stripMargin)


    s.toString()

  }

  def generateDMTCP_OLD(datasetRelativePath: String, varCovarRelativePath: String, jarFileRelative: String, jarDependencyFileRelative: String, params: Params): String = {
    val s = new StringBuilder()
    s.append(
      s"""#!/bin/bash
         |
        |#----------------------------------------------------------------------------------#
         |# Grid engine job script for a java job with DMTCP checkpointing
         |# Notes:
         |#     - You should touch AUTO_RESTART_PLEASE in the working directory before
         |#       submitting this job if you wish to automatically re-submit.
         |#     - It is recommended to give named -o and -e output and error file names
         |#       so that output from different stages of the checkpointed job are
         |#       combined.
         |#     - Problems/comments/enquiries to Chris Graham: hope this works for you :-)
         |#----------------------------------------------------------------------------------#
         |
         |# Directives to grid engine
         |#$$ -cwd                         # Use current working directory.
         |#$$ -S /bin/bash                 # Interpreting script is bash.
         |#$$ -l h_vmem=18G
         |#$$ -pe threaded 1               # Slots on the same node should use pe threaded.
         |#$$ -o output.txt                # Recommended.
         |#   #-l s_rt=04:00:00
         |#$$ -e error.txt                 # Recommended.
         |# Do not use -N if you are doing automatic resubmit.
         |
         |
         |
         |echo  "=========================="
         |echo "Shell script running "
         |echo  "=========================="
         |
         |
         |# Load required modules
         |module load dmtcp
         |# module load dmtcp/000-2.5.0
         |
         |# The following line ensures that the DMTCP coordinator moves with the job when restarted
         |export DMTCP_HOST= $$(hostname)
         |export DMTCP_PORT=0
         |
         |# Set the checkpoint interval in seconds - adjust accordingly
         |export DMTCP_CHECKPOINT_INTERVAL=120
         |
         |
         |
         |# Launch executable with checkpointing
         |if [ -e ./dmtcp_restart_script.sh ]
         |  then
         |    # The file dmtcp_restart_script.sh is used to restart the job
         |    echo " $$(date +%c) ($$JOB_ID) : Restarting job from checkpoint file on $$HOSTNAME, job ID $$JOB_ID."
         |    ./dmtcp_restart_script.sh
         |  else
         |
         |      extJAR="emp_dev_scala-assembly-1.0-deps.jar"
         |      # MyCode JAR
         |      mcJAR="emp_dev_scala-assembly-1.0-170530-2057_56.jar"
         |      jarDir="../../../../jars"
         |      rScalaJAR="/home/peter/R/x86_64-pc-linux-gnu-library/3.3/rscala/java/rscala_2.11-1.0.9.jar"
         |      # Combining
         |      extJAR="$${jarDir}/$${extJAR}"
         |      mcJAR="$${jarDir}/$${mcJAR}"
         |      mcJAR="${jarFileRelative}"
         |      extJAR="${jarDependencyFileRelative}"
         |      # **********************************************************************************
         |      #
         |      nIter=${params.niters}
         |      initMu=-4.640013
         |      initVariance=6.481367
         |      initialS=9662
         |      varianceScalingFactor=0.1,0.1,0.1
         |      tadFile="$datasetRelativePath"
         |      varCovarFile="$varCovarRelativePath"
         |
         |      # **********************************************************************************
         |      dmtcp_launch  java -Xmx1000M -XX:ParallelGCThreads=1 -XX:-UseParallelGC -cp  "$${extJAR}:$${mcJAR}" ExperimentScalaRunner --in "$${tadFile}" -s $${nIter} -c ${params.spiecesCoverage} --paramASigma 0.1 --paramBSigma 0.1  --SSigma 400 --initMu $${initMu} --initialS $${initialS} --initVariance $${initVariance} --varCovarFile "$${varCovarFile}" --varianceScalingFactor $${varianceScalingFactor} --distribution ${params.distribution} --varcovarmode ${params.varCovarMode}  > >(tee stdout.log) 2> >(tee stderr.log >&2)
         |
         | # ----------------------------------------------------------------------------------------
         |
         |
         |fi
         |
         |exit_status=$$??
         |if [ $$exit_status -eq 0 ]
         |  then
         |    # Job has completed; clean up DMTCP files
         |    echo " $$(date +%c) ($$JOB_ID) : Job completed: cleaning up DMTCP checkpoint files"
         |    rm dmtcp_restart*sh
         |    rm -r ckpt*files
         |    rm ckpt*dmtcp
         |fi
         |
         |
         |
         |
        """.stripMargin)


    s.toString()

  }


  def processSingleDataDir(dsDir: String, scriptDir: String, jarFile: String, jarDependencyFile: String, paramsInit: Params): (Option[String], Option[String]) = {
    // AUTO_RESTART_PLEASE
    val dataFileFileStr: String = getCovareFileName(dsDir, """(?i)^(TAD|tad).*\.(csv|sample)$""")
    println(s"dataFileFileStr = $dataFileFileStr")
    val (varCovarFilePattern, oldPosteriroFilePattern): (String, String) = paramsInit.distribution.toLowerCase match {
      case "ig" => ("""(?i).*varCovar.*adjusted.*partial.*\.csv$""", "old_posterior-.*\\.csv")
      case "igln" => ("""(?i).*varCovarMatrix_Ln.*\.csv$""", "old_ln_posterior-.*\\.csv")
      // varCovarMatrix_Ln_adHoc_Illinois.csv
      case _ => throw new Exception(s"Incorrect varCovar mode")
    }

    val varCovarFileStr: String = getCovareFileName(dsDir, varCovarFilePattern)
    println(s"varCovarFileStr = $varCovarFileStr")
    // **** Old posterior files ***
    val igParams: Option[InverseGaussianParams] = getDistributionParamsFromPreviousMCMTraceIG(dsDir, oldPosteriroFilePattern)

    val newIGParams: Option[InverseGaussianParams] = igParams match {
      case Some(igParamsClean) => igParams
      case None => paramsInit.iGParams
    }

    val params = paramsInit.copy(iGParams = newIGParams)

    // *** Dataset name ***
    val dataFileNamePartialOnly = (new File(dsDir)).getName()
    // *** Scriptr File path
    val scriptPath4DS: File = (new File(scriptDir, s"${dataFileNamePartialOnly}_${dateStr}"))
    val scriptPath4DS2: File = (new File(scriptPath4DS, s"s-${dataFileNamePartialOnly}_${dateStr}"))
    // , "s-${dataFileNamePartialOnly}_${dateStr}"
    scriptPath4DS2.mkdirs()
    val scriptNameOutput: File = (new File(scriptPath4DS2, s"s-${dataFileNamePartialOnly}_${dateStr}.sh"))
    println(s"scriptNameOutput = $scriptNameOutput")
    // *************************************************************
    val dmtcpScriptNameOutput: File = (new File(scriptPath4DS2, s"s-${dataFileNamePartialOnly}_${dateStr}.qsub"))
    println(s"dmtcpScriptNameOutput = $dmtcpScriptNameOutput")
    //======== Create dummy file =========================================================
    (new File(scriptPath4DS2, "AUTO_RESTART_PLEASE")).createNewFile() //s"""AUTO_RESTART_PLEASE"""
    // ================================================
    // *** Relative Paths ****
    val relPathDataStr = MCMCIO.absoluteToRelativePath(scriptPath4DS2.getAbsolutePath, dataFileFileStr)
    println(s"relPathDataStr = ${relPathDataStr.getOrElse("??????")}")
    val relPathVarCovarStr = MCMCIO.absoluteToRelativePath(scriptPath4DS2.getAbsolutePath, varCovarFileStr)
    println(s"relPathVarCovarStr  = ${relPathVarCovarStr.getOrElse("????")} ")
    val jarFileRelative = MCMCIO.absoluteToRelativePath(scriptPath4DS2.getAbsolutePath, jarFile)
    val jarDependencyFileRelative = MCMCIO.absoluteToRelativePath(scriptPath4DS2.getAbsolutePath, jarDependencyFile)
    //  *** Script Content ****
    val scriptShellScript = generateShScript(relPathDataStr.getOrElse("???"), relPathVarCovarStr.getOrElse("???"), jarFileRelative.getOrElse("???"), jarDependencyFileRelative.getOrElse("????"), params)
    val dmtcpScript = generateDMTCP(relPathDataStr.getOrElse("???"), relPathVarCovarStr.getOrElse("???"), jarFileRelative.getOrElse("???"), jarDependencyFileRelative.getOrElse("????"), params)
    // *** RElative to script root directory
    val relativeShScript: Option[String] = MCMCIO.absoluteToRelativePath(scriptDir, scriptNameOutput.getAbsolutePath)
    val relativeDMTCPScript: Option[String] = MCMCIO.absoluteToRelativePath(scriptDir, dmtcpScriptNameOutput.getAbsolutePath)
    // ************************************************************
    val fwShellScriptOut = new FileWriter(scriptNameOutput)
    fwShellScriptOut.write(scriptShellScript)
    fwShellScriptOut.close()
    // ============================================// ************************************************************
    val fwDMTCPScriptOut = new FileWriter(dmtcpScriptNameOutput)
    fwDMTCPScriptOut.write(dmtcpScript)
    fwDMTCPScriptOut.close()
    // ============================================
    //    println(s"script = $script")outputDir0 <-
    println("--------------------------------------------------------------------------------------------------------------")
    (relativeShScript, relativeDMTCPScript)
  }

  /** For inverse Gaussian distribution
    *
    * @param dsDir
    * @return
    */
  private def getDistributionParamsFromPreviousMCMTraceIG(dsDir: String, filePatternStr: String = "old_posterior-.*\\.csv") = {

    val fs: List[File] = getSubFile(mainDir = dsDir, patternStr = filePatternStr).toList
    val f = if (fs.length > 0) {
      Some(fs(0))
    } else None

    val optIGParams = f match {
      case Some(f) =>
        val file: RandomAccessFile = new RandomAccessFile(f, "r")

        val firstLine = Option(file.readLine)
        if (file.length > 1024 + firstLine.fold(0)(_.length))
          file.skipBytes((file.length() - 1024).toInt)
        val lastLine = Iterator.continually(file.readLine)
          .takeWhile(_ != null)
          .foldLeft(Option.empty[String]) { case (_, line) => Option(line) }

        val header: Array[String] = firstLine.get.split(",")
        val pValues: Array[Double] = lastLine.get.split(",").map(_.toDouble)
        val paramMap: Map[String, Double] = (header zip pValues).toMap
        val nS = paramMap.get("S").get.toInt
        val param1 = paramMap.get("alpha").get
        val param2 = paramMap.get("beta").get
        val igParams = InverseGaussianParams(nS = nS, mean = param1, param2 = param2)
        Some(igParams)
      case None => None
    }
    optIGParams
  }

  def getInputParams4ScalaScriptGenerator(args: Array[String]) = {
    val parser: OptionParser = new OptionParser()
    parser.allowsUnrecognizedOptions()
    val dataGroupDir = parser.acceptsAll(asList("dataGroupDir", "i"), "dataGroupDi r- main data directory with subdirectories for individual datasets").withRequiredArg().ofType(classFile2)
    new {
      val dataGroupDir: File = dataGroupDir;
      val mainJarFile = dataGroupDir;
      val dependencyJarFile = dataGroupDir
    }
  }


  def main(args: Array[String]): Unit = {
    //*** Parameters ****************************************************************************************
    val varCovarModes = List("normal", "lnAdHoc")
    val distributions = List("ig", "igln")
    val varCovarMode: String = varCovarModes(1)
    val distribution = distributions(1)
    val a = getInputParams4ScalaScriptGenerator(args)
    val speciesCoverage = 0.3
    val igDefaultParams: Option[InverseGaussianParams] = distribution.toLowerCase match {
      case "ig" => println("")
        Some(InverseGaussianParams(20557.89, 0.3523375, 17.1555419444))
      case "igln" => println("")
        Some(InverseGaussianParams(9.1812896715, -2.4267057532, 1.8848463393))
      case _ => throw new Exception(s"Not matching expected combinations of (distribution,varCovarModes) = (${distribution},$varCovarMode)")
    }
    var params = Params(2400000, "", "", speciesCoverage, varCovarScalingFactor = List(1d, 1d, 1d), "ExperimentScalaRunner4IG", igDefaultParams, distribution, varCovarMode)
    // "igln"
    // ==========================================================================================
    val mainDataDirs = List(
      "/media/sf_data/nps409/Other/EBI Results From Topsy/emp_dev_scala_runs/TAD_DATA_Real_Out_4_var_distr/IG_Distr/synth/synth_sample_r_0.1_d_180124_1219-52_r2_0.3_d2_180226_1037-05-e01",
      "/media/sf_data/nps409/Other/EBI Results From Topsy/emp_dev_scala_runs/TAD_DATA_Real_Out_4_var_distr/IG_Distr/synth/synth_sample_r_0.1_d_180124_1219-52_r2_0.05_d2_180226_1032-17",
      "/media/sf_data/nps409/Other/EBI Results From Topsy/emp_dev_scala_runs/TAD_DATA_Real_Out_4_var_distr/IG_Distr/synth/synth_sample_r_0.1_d_180124_1219-52_r2_0.3_d2_180226_1037-05",
      "/mnt/topsy/share/nobackup/nps409/EBI/emp_dev_scala_runs/TAD_DATA_Real_Out_4_var_distr/IG_Distr/synth/synth_sample_r_0.1_d_180124_1219-52_r2_0.3_d2_180226_1037-05",
      "/mnt/topsy/share/nobackup/nps409/EBI/emp_dev_scala_runs/TAD_DATA_Real_Out_4_var_distr/IG_Distr/synth/synth_sample_r_0.1_d_180124_1219-52_r2_0.05_d2_180226_1032-17",
      "/mnt/topsy/share/nobackup/nps409/EBI/emp_dev_scala_runs/TAD_DATA_Real_Out_4_var_distr/IG_Distr/synth/synth_sample_r_0.1_d_180124_1219-52_r2_0.05_d2_180226_1032-17",
      "/mnt/topsy/share/nobackup/nps409/EBI/emp_dev_scala_runs/TAD_DATA_Real_Out_4_var_distr/IG_Distr/synth/synth_sample_r_0.1_d_180124_1219-52_r2_0.2_d2_180225_1431-36",
      "/mnt/topsy/share/nobackup/nps409/EBI/emp_dev_scala_runs/TAD_DATA_Real_Out_4_var_distr/IG_Distr/synth/synth_sample_r_0.1_d_180124_1219-52_r2_0.3_d2_180226_1037-05",
      "/mnt/topsy/share/nobackup/nps409/EBI/emp_dev_scala_runs/TAD_DATA_Real_Out_4_var_distr/IG_Distr/synth/synth_sample_r_0.1_d_180124_1219-52_r2_0.05_d2_180226_1032-17",
      "/mnt/topsy/share/nobackup/nps409/EBI/emp_dev_scala_runs/TAD_DATA_Real_Out_4_var_distr/IG_Distr/synth/synth_sample_r_0.1_d_180124_1219-52_r2_0.2_d2_180225_1431-36",
      "/mnt/topsy/share/nobackup/nps409/EBI/emp_dev_scala_runs/TAD_DATA_Real_Out_4_var_distr/IG_Distr/synth/synth_sample_r_0.1_d_180124_1219-52_r2_0.1_d2_180124_1228-40",
      "/mnt/topsy/share/nobackup/nps409/EBI/emp_dev_scala_runs/TAD_DATA_Real_Out_4_var_distr/IG_Distr/synth/synth_sample_r_0.1_d_171219_0417-51_r2_0.1_d2_180123_1355-27",
      "/mnt/topsy/share/nobackup/nps409/EBI/emp_dev_scala_runs/TAD_DATA_Real_Out_4_var_distr/IG_Distr",
      "/mnt/topsy/share/nobackup/nps409/EBI/emp_dev_scala_runs/TAD_DATA_Real_Out_4_var_distr/IG_Distr/synth/synth_community_r_0.05_171219_0318-42",
      "/mnt/topsy/share/nobackup/nps409/EBI/emp_dev_scala_runs/TAD_DATA_Real_Out_4_var_distr/IG_Distr"

    )
    val mainDataDir = mainDataDirs(0)
    val parentDirOnly = new File(mainDataDir).getName


    val p =
      """.*_(\d{1,3})_pc.*""".r
    val samplingRateInDataset = mainDataDir match {
      case p(c) => c.toInt
      case _ => 10
    }

    val pV = """.*_v(\d{1,3}).*""".r
    val dataVersion = mainDataDir match {
      case pV(c) => c
      case _ => "00"
    }
    val dirs: Seq[File] = getSubDirs(mainDataDir,""".*[a-zA-z].*""")
    println(s"DIRS = ${dirs.mkString(",")}")
    val jarFile = "/media/sf_data/nps409/Other/EBI Results From Topsy/jars/emp_dev_scala-assembly-1.0-180406-1928_02.jar"
//      "/media/sf_data/nps409/Other/EBI Results From Topsy/jars/emp_dev_scala-assembly-1.0-180406-1858_23.jar"
//      "/media/sf_data/nps409/Other/EBI Results From Topsy/jars/emp_dev_scala-assembly-1.0-180406-1837_06.jar"
    //"/media/sf_data/nps409/Other/EBI Results From Topsy/jars/emp_dev_scala-assembly-1.0-180406-1815_16.jar"
    //      "/media/sf_data/nps409/Other/EBI Results From Topsy/jars/emp_dev_scala-assembly-1.0-180406-1810_02.jar"
    //      "/media/sf_data/nps409/Other/EBI Results From Topsy/jars/emp_dev_scala-assembly-1.0-180406-0138_14.jar"
    //"/media/sf_data/nps409/Other/EBI Results From Topsy/jars/emp_dev_scala-assembly-1.0-180405-2310_25.jar"
    //"/media/sf_data/nps409/Other/EBI Results From Topsy/jars/emp_dev_scala-assembly-1.0-180405-2057_51.jar"
    //"/media/sf_data/nps409/Other/EBI Results From Topsy/jars/emp_dev_scala-assembly-1.0-180405-1200_13.jar"
    //"/media/sf_data/nps409/Other/EBI Results From Topsy/jars/emp_dev_scala-assembly-1.0-180404-0543_24.jar"
    //"/mnt/topsy/share/nobackup/nps409/EBI/jars/emp_dev_scala-assembly-1.0-180404-0543_24.jar"
    //"/mnt/topsy/share/nobackup/nps409/EBI/jars/emp_dev_scala-assembly-1.0-180302-0114_23.jar"
    val jarDependencyFile = "/media/sf_data/nps409/Other/EBI Results From Topsy/jars/emp_dev_scala-assembly-1.0-deps.jar"
    //"/mnt/topsy/share/nobackup/nps409/EBI/jars/emp_dev_scala-assembly-1.0-deps.jar"
    //"/mnt/topsy/share/nobackup/nps409/EBI/jars/emp_dev_scala-assembly-1.0-deps-171204-0549_00.jar"

    val parentDir: String = new File(mainDataDir).getParent

    val mainScriptDirOld = s"/mnt/topsy/share/nobackup/nps409/EBI/emp_dev_scala_runs/TAD_DATA_Synthetic_Out/CQ_Data_Generator_170627_2302/Script_Scala_${samplingRateInDataset}_pc_data_Script_${dateStr}_v${dataVersion}"

    val mainScriptDir = new File(parentDir,s"""Script_Scala_${samplingRateInDataset}_${parentDirOnly}_pc_v${dataVersion}_${dateStr}""").getCanonicalPath
    new File(mainScriptDir) mkdirs()
    val (los, losDMTCPR): (Seq[Option[String]], Seq[Option[String]]) = dirs.map(f => processSingleDataDir(f.getAbsolutePath, mainScriptDir,
      jarFile, jarDependencyFile, params)).unzip
    // ****************************************************************
    val lineSep: String = sys.props("line.separator")
    // ****************************************************************
    val s0 = los.collect { case Some(s) => val f = new File(s); val p = f.getParent; val n = f.getName; s"""(cd ${p}; bash $n)&""" }
    val fwShellScript = new FileWriter(new File(mainScriptDir,s"""shell_Scripts_Scala_${dateStr}.sh"""))
    //    val s0 = los.collect{case Some(s)=>s}
    fwShellScript.write(s0.mkString(sep = lineSep))
    fwShellScript.close()
    // ==================================================================
    // ****************************************************************// ****************************************************************
    val qsubShellScript = los.collect { case Some(s) => val f = new File(s); val p = f.getParent; val n = f.getName; s"""(cd ${p}; qsub $n)""" }
    val fwQsubShellScript = new FileWriter(new File(mainScriptDir,s"""qsub_shell_Scripts_Scala_${dateStr}.sh"""))
    //    val s0 = los.collect{case Some(s)=>s}
    fwQsubShellScript.write(qsubShellScript.mkString(sep = lineSep))
    fwQsubShellScript.close()
    // ==================================================================
    // ****************************************************************
    val fwDMTCPScript = new FileWriter(new File(mainScriptDir,s"""dmtcp_Scripts_Scala_${dateStr}.sh"""))
    val sDMTCP: Seq[String] = losDMTCPR.collect { case Some(s) => val f = new File(s); val p = f.getParent; val n = f.getName; s"""(cd ${p}; qsub $n)""" }
    //    val s0 = los.collect{case Some(s)=>s}
    fwDMTCPScript.write(sDMTCP.mkString(sep = lineSep))
    fwDMTCPScript.close()
    // ==================================================================
    println(s"Input written to ${mainScriptDir}")
  }

}
