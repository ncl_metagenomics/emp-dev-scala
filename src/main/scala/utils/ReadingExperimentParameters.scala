package utils

import java.io.File
import java.{lang, util}
import java.util.Arrays._

import joptsimple.{ArgumentAcceptingOptionSpec, OptionParser, OptionSet}
import utils.sampling._

import scala.util.{Failure, Success, Try}
import collection.JavaConverters._
import scala.collection.breakOut
import basics.MCMC
import play.api.libs.iteratee.Input

import scala.collection.JavaConversions._


/**
  * Created by peter on 11/10/16.
  */
object ReadingExperimentParameters {

  def getInput_IG_Distribution(args: Array[String]): utils.ReadingExperimentParameters.InputParams_IG_Distribution = {

    val parser: OptionParser = new OptionParser()
    parser.allowsUnrecognizedOptions()
    // ***********************
    val initMeanOpt = parser.acceptsAll(asList("initMean"), "Initial value for  alpha parameter of Inverse Gaussian ").withOptionalArg().ofType(classOf[java.lang.Double]).defaultsTo(0.1)
    val initScaleParamOpt = parser.acceptsAll(asList("initShape"), "Initial value for  shape parameter of Inverse Gaussian").withOptionalArg().ofType(classOf[java.lang.Double]).defaultsTo(2.2)

    //**********
    //***** Help and Missing options
    //**********
    val helpOpt = parser.acceptsAll(asList("h", "?"), "show help").forHelp()
    // "--version", "s", "--distribution", "lognormal"
    val options: OptionSet = parser.parse(args: _*)
    //    parser.printHelpOn(System.out)
    println(s"\nUnrecognized options ${options.nonOptionArguments()} \n")
    //****************
    //*** Extract Parameters
    //****************
    val initMean = initMeanOpt.value(options)
    val initBeta = initScaleParamOpt.value(options)
    // ***  RESULT
    InputParams_IG_Distribution(initMean, initBeta)
  }


  def getInput_4_2_ParamsDistribution(args: Array[String]): Input42ParametersDistribution = {
    val parser: OptionParser = new OptionParser()
    parser.allowsUnrecognizedOptions()

    //    val correlationFileOpt = parser.acceptsAll(asList("correleationFile"), "CSV file name with TAD data").withOptionalArg().ofType(classFile2)

    val varCovarianceFileOpt = parser.acceptsAll(asList("varCovarFile"), "CSV file name with TAD data").withOptionalArg().ofType(classFile2)
//
//    val initMeanOpt = parser.acceptsAll(asList(List("initMean"): _*), """ initial mean"","" """).withRequiredArg().ofType(java.lang.Double.TYPE).defaultsTo(1.0)
//
//    val initShapeOpt = parser.acceptsAll(asList(List("initShape"): _*), """ initial shape"","" """).withRequiredArg().ofType(java.lang.Double.TYPE).defaultsTo(1.0)

    /**
      * Scaling factor for variance of proposed values drawn from normal distribution distribution. Parameters:mean, variance, S.
      */
    val varianceScalingFactorOpt = parser.acceptsAll(asList("varianceScalingFactor"), "scaling multiplicative factor for variance covariance factor ").withOptionalArg().ofType(classOf[java.lang.Double]).withValuesSeparatedBy(",").defaultsTo(1.0, 1.0, 1.0)

    //**********
    //***** Help and Missing options
    //**********
    val helpOpt = parser.acceptsAll(asList("h", "?"), "show help").forHelp()
    // "--version", "s", "--distribution", "lognormal"
    val options: OptionSet = parser.parse(args: _*)
    //    parser.printHelpOn(System.out)
    println(s"\nUnrecognized options ${options.nonOptionArguments()} \n")

    // **********
    //***** Extracting  Values
    //**********
    //    val correlationFile: Option[File] = extractParameter(correlationFileOpt, options)
    val varCovarianceFile: Option[File] = extractParameter(varCovarianceFileOpt, options)
    val varianceScalingFactor0: util.List[lang.Double] = options.valuesOf(varianceScalingFactorOpt)
    val varianceScalingFactor: List[Double] = varianceScalingFactor0.toList.map(_.toDouble)
    //------

//    val initShape = Try(options.valueOf(initShapeOpt)).getOrElse(Double.NaN)
    //================================
    // *** Output ***
    Input42ParametersDistribution( varCovarianceFile, Option(varianceScalingFactor))

  }


  def getInputGeneralParams4MCMCA(args: Array[String]): _root_.utils.ReadingExperimentParameters.InputParametersMCMCGeneral = {

    val parser: OptionParser = new OptionParser()
    parser.allowsUnrecognizedOptions()

    val outDirOpt = parser.acceptsAll(asList("out", "o"), "directory - output directory").withRequiredArg().ofType(classFile2)

    val inputFileOpt = parser.acceptsAll(asList("in", "i"), "CSV file name with TAD data").withRequiredArg().ofType(classFile2)

    /**
      * "-n" - number of iterations (previously "-n")
      */
    val nMCMCiterationsOpt = parser.acceptsAll(asList("nIterations", "n"), "Number of MCMC iterations").withOptionalArg().ofType(classOf[java.lang.Integer]).defaultsTo(2400000)

    val thinningIntervalOpt = parser.acceptsAll(asList("thinning", "t"), "Thinning interval for MCMC").withOptionalArg().ofType(classOf[java.lang.Integer]).defaultsTo(10)

    val nSpeciesOpt = parser.acceptsAll(asList("initNSpecies","initialS", "s"), "Number of spiecies").withOptionalArg().ofType(classOf[java.lang.Double]).defaultsTo(-1d)

    //    val speciesCoverageOpt = parser.acceptsAll(asList("coverage", "c"), "Expected fraction of species we like to observe in sample").withOptionalArg().ofType(classOf[java.lang.Double]).defaultsTo(0.9)

    val distributionOpt = parser.acceptsAll(asList("distribution", "d"), "Form of TAD distribution: lognormal, IG").withOptionalArg().ofType(classOf[java.lang.String]).defaultsTo("IG")

    val versionOpt = parser.acceptsAll(asList("version"), "version of calculating distribtution value").withOptionalArg().ofType(classString2).defaultsTo("s")

    //    val correlationFileOpt = parser.acceptsAll(asList("correleationFile"), "CSV file name with TAD data").withOptionalArg().ofType(classFile2)
    //
    val varCovarianceFileOpt = parser.acceptsAll(asList("varCovarFile"), "CSV file name with TAD data").withOptionalArg().ofType(classFile2)
    //    val varianceScalingFactorOpt = parser.acceptsAll(asList("varianceScalingFactor"), "scaling multiplicative factor for variance covariance factor ").withOptionalArg().ofType(classOf[java.lang.Double]).withValuesSeparatedBy(",").defaultsTo(1.0, 1.0, 1.0)

    //**********
    //***** Help and Missing options
    //**********
    val helpOpt = parser.acceptsAll(asList("h", "?"), "show help").forHelp()
    // "--version", "s", "--distribution", "lognormal"
    val options: OptionSet = parser.parse(args: _*)
    //    parser.printHelpOn(System.out)
    println(s"\nUnrecognized options ${options.nonOptionArguments()} \n")

    // **********
    //***** OutputFileProcessing expect relative path to data from root program directory
    //**********
      val inputFile0 = inputFileOpt.value(options)
    println(s"Original value ${inputFile0.getAbsolutePath}")
    val workingDir: String = System.getProperty("user.dir")
    println(s"Working dir $workingDir")
    val inputFile = new File(inputFile0.getAbsolutePath)
    println(s"Absolute path to input file  $inputFile")

    val outDir: File = (if (options.has(outDirOpt)) {
      Success(outDirOpt.value(options))
    } else {
      if (inputFile != null) {
        Success(inputFile.getParentFile)
      }
      else {
        println("Help on options")
        parser.printHelpOn(System.out)
        Failure(new Exception("Input file  is null is not specified or does not exist!"))
      }
    }
      ).get

    // **********
    //***** Extracting Other Values
    //**********
    val nMCMCIterations: Int = nMCMCiterationsOpt.value(options)
    val v: lang.Double = nSpeciesOpt.value(options)
    val nSpecies: Try[Double] = nSpeciesOpt.value(options) match {
      case n:lang.Double => Success(n)
      case _ => Failure(new Exception("Integer value expected for initial number of species"))
    }
    val distribution: String = distributionOpt.value(options)
    val version: String = versionOpt.value(options)
    val thinningInterval: Int = thinningIntervalOpt.value(options).toInt
    //    val correlationFile: Option[File] = extractParameter(correlationFileOpt, options)
    val varCovarianceFile: Option[File] = extractParameter(varCovarianceFileOpt, options)
    //    val varianceScalingFactor0 = options.valuesOf(varianceScalingFactorOpt)
    //    val varianceScalingFactor: List[Double] = varianceScalingFactor0.toList.map(_.toDouble)
    //================================
    //todo: 171106 work here
    //    val nSpecies:Try[Int]= Failure(new Exception("nSInit not specified"))
    //*** Constructing Output *****
    // Nil.asInstanceOf
    InputParametersMCMCGeneral(inputFile, outDir, nMCMCIterations, distribution, nSpecies, version, thinningInterval, varCovarianceFile /*,correlationFile=correlationFile, varianceScalingFactor = Option(varianceScalingFactor) */)

  }

  //  val classString = Class.forName("java.lang.String")
  val classString2 = classOf[String]
  //  val classFile = Class.forName("java.io.File")
  val classFile2 = classOf[java.io.File]

  trait Enum4EMP2[A <: {def synonyms : List[String]; def acceptedValues : List[Any]; def acceptedEnum : List[Any]}] {

    trait Value {
      self: A =>
      _values :+= this
    }

    private var _values = List.empty[A]

    def values = _values
  }


  sealed abstract class ParametersNames(val synonyms: List[String], val acceptedValues: List[Any], val acceptedEnum: List[Any]) extends ParametersNames.Value

  object ParametersNames extends Enum4EMP2[ParametersNames] {
    val DATASET_NAME = new ParametersNames(List("dataSetName", "ds"), List(""), List()) {}
    val DATA_FORMAT = new ParametersNames(List("dataFormat", "df"), List("TAD", "Abundance"), List()) {
      override val acceptedEnum: List[TADDistributionInfo] = TADDistributionInfo.values
    }
    val SCOPE_OF_DATA = new ParametersNames(List("scopeOfData", "sc"), List("Community", "Sample"), List()) {}
    val REAL_OR_SYNTHETIC = new ParametersNames(List("realOrSynth", "ros"), List("Real", "Synthetic"), List()) {}
    val COVERAGE_OF_SPECIES = new ParametersNames(List("coverageOfSpecies", "cs"), List(11000), List()) {}
    val TAD_DISTRIBUTION = new ParametersNames(List("distribution", "dist"), List("Lognormal"), List()) {}
    val FILE_FORMAT = new ParametersNames(List("fileFormat", "ff"), List("csv", "sample"), List()) {}
    val OUTPUT_DIRECTORY = new ParametersNames(List("outDirectory", "od"), List("."), List()) {}
    val TAD_MEAN = new ParametersNames(List("mean", "m"), List(1.1), List()) {}
    val TAD_VARIANCE = new ParametersNames(List("variance", "v"), List(), List()) {}
    val NUMBER_OF_SPECIES = new ParametersNames(List("numberOfSpecies", "ns", "S"), List(11000), List()) {}
    //

  }

  //


  val classString = classOf[String]
  val classFile = classOf[java.io.File]

  def main(args: Array[String]): Unit = {
    val inputTADFile: File = ContentOfResourcesDirInSinlgeJar.getFullFileName("Data/44xSRS711891_TAD__50_pc_D_7747.5_lognormal_160630_1444-36_16-07-01_1801-30", "44xSRS711891_TAD__50_pc_D_7747.5_lognormal_160630_1444-36_16-07-01_1801-30.csv").get
    //*** Parameters 4 subsampling **********
    val args3: Array[String] = Array("--in", inputTADFile.getAbsolutePath, "-c", "0.9", "-c", "0.8")
    val res1: InputParameters4SubsamplingCommmunity = getInputParams4SubsamplingCommunity(args3)
    println(s"InputParameters4SubsamplingCommmunity:\n${res1.toString}")
    //    testGetInputParams4MCMCAndSamplingEffort(args)
    //*********************
    val args2 = Array("--in", inputTADFile.getAbsolutePath, "-s", "600000", "-c", "0.8", "--version", "s", "--distribution", "lognormal")
    testGetInputParams4MCMCAndSamplingEffort(args2)

  }


  val DEFAULT_COVERAGE_OF_SPECIES = 0.9
  val PN = ParametersNames


  def getInputParams4SamplingCommunity4Test(args: Array[String]) /*Parameters4Sampling*/ = {
    val parser: OptionParser = new OptionParser()
    parser.allowsUnrecognizedOptions()
    val inputFileOpt = parser.acceptsAll(asList(PN.DATASET_NAME.synonyms: _*), "dataset name").withRequiredArg().ofType(classString)

    val dataFormatOpt = parser.acceptsAll(asList(PN.DATA_FORMAT.synonyms: _*), s"""data format. Possible values:"Abundance","TAD".""").withRequiredArg().defaultsTo(DataFormatInfo.ABUNDANCE.name).ofType(classString)

    val scopeOfDataOpt = parser.acceptsAll(asList(PN.SCOPE_OF_DATA.synonyms: _*), s"""scope Of Data. Values: ${ScopeOfDataInfo.values.map(_.name).mkString(", ")}.""").withRequiredArg().defaultsTo(ScopeOfDataInfo.COMMUNITY.name).ofType(classString)

    val generatedOrRealOpt = parser.acceptsAll(asList(PN.REAL_OR_SYNTHETIC.synonyms: _*), s"""isGeneratedOrSynth. Values: ${RealOrSyntheticInfo.values.map(v => v.name).mkString(", ")} """).withRequiredArg().defaultsTo(RealOrSyntheticInfo.SYNTHETIC.name).ofType(classString)

    val coverageOfSpeciesOpt = parser.acceptsAll(asList(PN.COVERAGE_OF_SPECIES.synonyms: _*), """scope Of Data. Values: "","" """).withRequiredArg().ofType(java.lang.Double.TYPE).defaultsTo(DEFAULT_COVERAGE_OF_SPECIES)

    val tadDistributionOpt = parser.acceptsAll(asList(PN.TAD_DISTRIBUTION.synonyms: _*), s"""TAD distribution. Possible values:${TADDistributionInfo.values.map { v => v.name }.mkString(", ")}.""").withRequiredArg().defaultsTo(TADDistributionInfo.LOGNORMAL.name).ofType(classString)

    val fileFormatOpt = parser.acceptsAll(asList(PN.FILE_FORMAT.synonyms: _*), s"""file format. Values: ${FileFormatInfo.values.map { v => v.name }.mkString(", ")} """).withRequiredArg().ofType(classString).defaultsTo(FileFormatInfo.CSV.name)

    val outDirOpt = parser.acceptsAll(asList(PN.OUTPUT_DIRECTORY.synonyms: _*), "dataset name").withRequiredArg().ofType(classString)


    val meanOpt = parser.acceptsAll(asList(PN.TAD_MEAN.synonyms: _*), """mean of TAD.""").withRequiredArg().ofType(java.lang.Double.TYPE)

    val varianceOpt = parser.acceptsAll(asList(PN.TAD_VARIANCE.synonyms: _*), """variance of TAD.""").withRequiredArg().ofType(java.lang.Double.TYPE)

    val numerOfSp0eciesOpt = parser.acceptsAll(asList(PN.NUMBER_OF_SPECIES.synonyms: _*), """number of species""").withRequiredArg().ofType(java.lang.Integer.TYPE)

    /* help */
    parser.acceptsAll(asList("h", "?"), "show help").forHelp()
    parser.printHelpOn(System.out)

    /** * Parse ***/
    println(s"argument:s $args")
    val options: OptionSet = parser.parse(args: _*)
    /* *****     Extracted values *** */
    val dsName: String = inputFileOpt.value(options)
    val dataFormat: String = dataFormatOpt.value(options)
    val scopeOfData: String = scopeOfDataOpt.value(options)
    val generatedOrReal: String = generatedOrRealOpt.value(options)
    val coverageOfSpecies: Double = coverageOfSpeciesOpt.value(options)
    val tadDistribution: String = tadDistributionOpt.value(options)
    val dateTimeStamp = MCMC.DATE_STR
    val fileFormat = fileFormatOpt.value(options)
    val outputDir = outDirOpt.value(options)
    // *** number of species ****
    val numerOfSpecies: Int = numerOfSp0eciesOpt.value(options)
    // ***   Parameters for distribution  ****
    val mean = meanOpt.value(options)
    val variance: Double = varianceOpt.value(options)


    //    val parameters4Sampling: Parameters4Distribution = tadDistribution match {
    //      case TADDistributionInfo.LOGNORMAL.name => Parameters4ForLognormalDistribution(mean, variance)
    //    }

    println("")

    /**
      * Parameters4FileName(dataSetName: String = "44xSRS711891", dataFormat: Option[String] = Some("Abundance"), scopeOfData: String = "Community", isGeneratedOrSynth: String = "Synthetic", coverage: Double = 1d, dateTimeStamp: String, fileNameSuffix: String = "csv", outDir: String = ".")
      */

    (dsName, dataFormat, scopeOfData, generatedOrReal, coverageOfSpecies, tadDistribution, fileFormat, outputDir, mean, variance, numerOfSpecies)
  }

  def getInputParams4SamplingCommunity(args: Array[String]): Parameters4Sampling = {

    var (dsName, dataFormat, scopeOfData, generatedOrReal, coverageOfSpecies, tadDistribution, fileFormat, outputDir, meanOfLogValue, varianceOfLogValue, numerOfSpecies) = getInputParams4SamplingCommunity4Test(args)

    val parameters4Distribution: Parameters4Distribution = tadDistribution match {
      case TADDistributionInfo.LOGNORMAL.name => Parameters4ForLognormalDistribution(meanOfLogValue, varianceOfLogValue)
    }

    val parameters4FileName: Parameters4FileName = Parameters4FileName(dsName, Option(dataFormat), scopeOfData, generatedOrReal, coverageOfSpecies, tadDistribution, MCMC.DATE_STR, fileFormat, outputDir)

    val parameters4Sampling = Parameters4Sampling(parameters4FileName, parameters4Distribution, numerOfSpecies)
    println("")

    /**
      * Parameters4ForLognormalDistribution(meanOfLogValue:Double,varianceOfLogValue:Double)      *
      */


    /**
      * Parameters4FileName(dataSetName: String = "44xSRS711891", dataFormat: Option[String] = Some("Abundance"), scopeOfData: String = "Community", isGeneratedOrSynth: String = "Synthetic", coverage: Double = 1d, dateTimeStamp: String, fileNameSuffix: String = "csv", outDir: String = ".")
      */

    parameters4Sampling
  }

  def getInputParams4SubsamplingCommunity(args: Array[String]): InputParameters4SubsamplingCommmunity = {
    //*** Parser ****
    val parser: OptionParser = new OptionParser()
    parser.allowsUnrecognizedOptions()
    // *** Options ***
    val inputFileOpt: ArgumentAcceptingOptionSpec[File] = parser.acceptsAll(asList("in", "i"), "CSV file name with TAD data").withRequiredArg().ofType(classFile)
    val speciesCoverageOpt = parser.acceptsAll(asList("coverage", "c"), "Expected fraction of species we like to observe in sample").withOptionalArg().ofType(classOf[java.lang.Double])
    val options: OptionSet = parser.parse(args: _*)
    val outDirOpt = parser.acceptsAll(asList("out", "o"), "directory - output directory").withRequiredArg().ofType(classFile)
    // *** Values ***
    val inputFile: File = inputFileOpt.value(options)
    val xxx = options.valuesOf("c")
    val tmp = asList(5d, 6d, 7d)
    val speciesCoverage: java.util.List[java.lang.Double] = speciesCoverageOpt.values(options)
    println("")
    val speciesCoverage2: scala.List[Double] = speciesCoverage.asScala.map(_.doubleValue)(breakOut)

    val outDir: File = (if (options.has(outDirOpt)) {
      Success(outDirOpt.value(options))
    } else {
      if (inputFile != null) {
        Success(inputFile.getParentFile)
      }
      else {
        println("Help on options")
        parser.printHelpOn(System.out)
        Failure(new Exception("Input file  is null is not specified or does not exist!"))
      }
    }
      ).get

    parser.printHelpOn(System.out)
    InputParameters4SubsamplingCommmunity(inputFile, outDir, speciesCoverage2)
  }

  def testGetInputParams4MCMCAndSamplingEffort(args: Array[String]): Unit = {
    // /media/sf_data/nps409/Work2Sync/Projects/emp_dev_scala_runs/TAD_DATA_Synthetic_Out/SRS711891_Synthetic_Out/AutoEval160816_160630_1444-36_TEST/44xSRS711891_TAD__50_pc_D_7747.5_lognormal_160630_1444-36_16-07-01_1801-30-copy2/44xSRS711891_TAD__50_pc_D_7747.5_lognormal_160630_1444-36_16-07-01_1801-30.csv
    val inputTADFile: File = ContentOfResourcesDirInSinlgeJar.getFullFileName("Data/44xSRS711891_TAD__50_pc_D_7747.5_lognormal_160630_1444-36_16-07-01_1801-30", "44xSRS711891_TAD__50_pc_D_7747.5_lognormal_160630_1444-36_16-07-01_1801-30.csv").get

    val args2 = Array("--in", inputTADFile.getAbsolutePath, "-s", "600000", "-c", "0.8", "--version", "s", "--distribution", "lognormal")

    val myOpt: InputParametersMCMCAndSamplingEffort = getLNInputParams4MCMCAndSamplingEffort(args)
    val myOpt2: InputParametersMCMCAndSamplingEffort = getLNInputParams4MCMCAndSamplingEffort(args2)
    println(myOpt2.toString)
    println("")
  }

  /**
    * m
    *
    * @param args
    * @return
    */
  def getLNInputParams4MCMCAndSamplingEffort(args: Array[String]): InputParametersMCMCAndSamplingEffort = {
    val parser: OptionParser = new OptionParser()
    parser.allowsUnrecognizedOptions()
    val outDirOpt = parser.acceptsAll(asList("out", "o"), "directory - output directory").withRequiredArg().ofType(classFile2)
    val inputFileOpt = parser.acceptsAll(asList("in", "i"), "CSV file name with TAD data").withRequiredArg().ofType(classFile2)
    val nMCMCiterationsOpt = parser.acceptsAll(asList("nIterations", "s"), "Number of MCMC iterations").withOptionalArg().ofType(classOf[java.lang.Integer]).defaultsTo(400000)
    val thinningIntervalOpt = parser.acceptsAll(asList("thinning", "t"), "Thinning interval for MCMC").withOptionalArg().ofType(classOf[java.lang.Integer]).defaultsTo(10)
    val speciesCoverageOpt = parser.acceptsAll(asList("coverage", "c"), "Expected fraction of species we like to observe in sample").withOptionalArg().ofType(classOf[java.lang.Double]).defaultsTo(0.9)
    val distributionOpt = parser.acceptsAll(asList("distribution", "d"), "Form of TAD distribution").withOptionalArg().ofType(classOf[java.lang.String]).defaultsTo("lognormal")
    val versionOpt = parser.acceptsAll(asList("version"), "version of calculating distribtution value").withOptionalArg().ofType(classString2).defaultsTo("s")
    // ****   MCMC parameters -- variances of parametes  *****
    val paramASigmaOpt = parser.acceptsAll(asList("paramASigma"), "Standard deviation of first parameter").withOptionalArg().ofType(classOf[java.lang.Double]).defaultsTo(0.1)
    val paramBSigmaOpt = parser.acceptsAll(asList("paramBSigma"), "Standard deviation of second parameter").withOptionalArg().ofType(classOf[java.lang.Double]).defaultsTo(0.2)
    val paramSStDevOpt = parser.acceptsAll(asList("SSigma"), "Standard deviation of second parameter").withOptionalArg().ofType(classOf[java.lang.Integer]).defaultsTo(400)
    //*****************
    // ****   MCMC parameters -- initial values   *****
    val initMuOpt = parser.acceptsAll(asList("initMu"), "Initial value for  mean").withOptionalArg().ofType(classOf[java.lang.Double]).defaultsTo(0.1)
    val initVarianceOpt = parser.acceptsAll(asList("initVariance"), "Initial value for variance").withOptionalArg().ofType(classOf[java.lang.Double]).defaultsTo(6.0)
    val initSOpt = parser.acceptsAll(asList("initialS"), "Initial value for number of species").withOptionalArg().ofType(classOf[java.lang.Integer]).defaultsTo(400)
    //*****************
    val correlationFileOpt = parser.acceptsAll(asList("correleationFile"), "CSV file name with TAD data").withOptionalArg().ofType(classFile2)
    val varCovarianceFileOpt = parser.acceptsAll(asList("varCovarFile"), "CSV file name with TAD data").withOptionalArg().ofType(classFile2)
    //todo: 170309 Work here for varianceScalingFactor extraction
    val varianceScalingFactorOpt = parser.acceptsAll(asList("varianceScalingFactor"), "scaling multiplicative factor for variance covariance factor ").withOptionalArg().ofType(classOf[java.lang.Double]).withValuesSeparatedBy(",").defaultsTo(1.0, 1.0, 1.0)
    // varianceScalingFactor
    //-------------------
    // mcmcParamASigma
    // -------------------------------------
    val helpOpt = parser.acceptsAll(asList("h", "?"), "show help").forHelp()
    // "--version", "s", "--distribution", "lognormal"
    val options: OptionSet = parser.parse(args: _*)
    //    parser.printHelpOn(System.out)
    println(s"\nUnrecognized options ${options.nonOptionArguments()} \n")
    val inputFile0 = inputFileOpt.value(options)
    println(s"Original value ${inputFile0.getAbsolutePath}")
    val workingDir: String = System.getProperty("user.dir")
    println(s"Working dir $workingDir")
    val inputFile = new File(inputFile0.getAbsolutePath)
    println(s"Absolute path to input file  $inputFile")

    val outDir: File = (if (options.has(outDirOpt)) {
      Success(outDirOpt.value(options))
    } else {
      if (inputFile != null) {
        Success(inputFile.getParentFile)
      }
      else {
        println("Help on options")
        parser.printHelpOn(System.out)
        Failure(new Exception("Input file  is null is not specified or does not exist!"))
      }
    }
      ).get

    val nMCMCIterations: Int = nMCMCiterationsOpt.value(options)
    val speciesCoverage: Double = speciesCoverageOpt.value(options)
    val distribution: String = distributionOpt.value(options)
    val version: String = versionOpt.value(options)
    //----------------------------------
    val paramASigma: Double = paramASigmaOpt.value(options)
    val paramBSigma: Double = paramBSigmaOpt.value(options)
    val paramSStDev: Int = paramSStDevOpt.value(options)
    //-----------------------------------
    val initMu = initMuOpt.value(options)
    val initVariance = initVarianceOpt.value(options)
    val initS = initSOpt.value(options)
    //-----------------------------------
    val thinningInterval = thinningIntervalOpt.value(options)
    //------------------------------------------------------
    val correlationFile: Option[File] = extractParameter(correlationFileOpt, options)
    //******* var-covar ************
    val varCovarianceFile: Option[File] = extractParameter(varCovarianceFileOpt, options)
    //*******

    val varianceScalingFactor0 = options.valuesOf(varianceScalingFactorOpt)
    //varianceScalingFactorOpt.value(options)
    val varianceScalingFactor: List[Double] = varianceScalingFactor0.toList.map(_.toDouble)
    InputParametersMCMCAndSamplingEffort(inputFile, outDir, nMCMCIterations, speciesCoverage, distribution, version = version, thinningInterval = thinningInterval, paramASigma = paramASigma, paramBSigma = paramBSigma, paramSStDev = paramSStDev, correlationFile = correlationFile, varCovarFile = varCovarianceFile, varianceScalingFactor = Option(varianceScalingFactor), initMu = Option(initMu), initVariance = Option(initVariance), intS = Option(initS))
  }

  /**
    * m
    *
    * @param args
    * @return
    */
  def getInputParams4MCMCAndSamplingEffort(args: Array[String]): InputParametersMCMCAndSamplingEffort = {

    // ***************************************************************
    // ***
    // ***************************************************************
    val parser: OptionParser = new OptionParser()
    parser.allowsUnrecognizedOptions()
    val outDirOpt = parser.acceptsAll(asList("out", "o"), "directory - output directory").withRequiredArg().ofType(classFile2)
    val inputFileOpt = parser.acceptsAll(asList("in", "i"), "CSV file name with TAD data").withRequiredArg().ofType(classFile2)
    val nMCMCiterationsOpt = parser.acceptsAll(asList("nIterations", "s"), "Number of MCMC iterations").withOptionalArg().ofType(classOf[java.lang.Integer]).defaultsTo(400000)
    val thinningIntervalOpt = parser.acceptsAll(asList("thinning", "t"), "Thinning interval for MCMC").withOptionalArg().ofType(classOf[java.lang.Integer]).defaultsTo(10)
    val speciesCoverageOpt = parser.acceptsAll(asList("coverage", "c"), "Expected fraction of species we like to observe in sample").withOptionalArg().ofType(classOf[java.lang.Double]).defaultsTo(0.9)
    val distributionOpt = parser.acceptsAll(asList("distribution", "d"), "Form of TAD distribution").withOptionalArg().ofType(classOf[java.lang.String]).defaultsTo("lognormal")
    val versionOpt = parser.acceptsAll(asList("version"), "version of calculating distribtution value").withOptionalArg().ofType(classString2).defaultsTo("s")
    // ****   MCMC parameters -- variances of parametes  *****
    val paramASigmaOpt = parser.acceptsAll(asList("paramASigma"), "Standard deviation of first parameter").withOptionalArg().ofType(classOf[java.lang.Double]).defaultsTo(0.1)
    val paramBSigmaOpt = parser.acceptsAll(asList("paramBSigma"), "Standard deviation of second parameter").withOptionalArg().ofType(classOf[java.lang.Double]).defaultsTo(0.2)
    val paramSStDevOpt = parser.acceptsAll(asList("SSigma"), "Standard deviation of second parameter").withOptionalArg().ofType(classOf[java.lang.Integer]).defaultsTo(400)
    //*****************
    // ****   MCMC parameters -- initial values   *****
    val initMuOpt = parser.acceptsAll(asList("initMu"), "Initial value for  mean").withOptionalArg().ofType(classOf[java.lang.Double]).defaultsTo(0.1)
    val initVarianceOpt = parser.acceptsAll(asList("initVariance"), "Initial value for variance").withOptionalArg().ofType(classOf[java.lang.Double]).defaultsTo(6.0)
    val initSOpt = parser.acceptsAll(asList("initialS"), "Initial value for number of species").withOptionalArg().ofType(classOf[java.lang.Integer]).defaultsTo(400)
    //*****************
    val correlationFileOpt = parser.acceptsAll(asList("correleationFile"), "CSV file name with TAD data").withOptionalArg().ofType(classFile2)
    val varCovarianceFileOpt = parser.acceptsAll(asList("varCovarFile"), "CSV file name with TAD data").withOptionalArg().ofType(classFile2)
    //todo: 170309 Work here for varianceScalingFactor extraction
    val varianceScalingFactorOpt = parser.acceptsAll(asList("varianceScalingFactor"), "scaling multiplicative factor for variance covariance factor ").withOptionalArg().ofType(classOf[java.lang.Double]).withValuesSeparatedBy(",").defaultsTo(1.0, 1.0, 1.0)
    // varianceScalingFactor
    //-------------------
    // mcmcParamASigma
    // -------------------------------------
    val helpOpt = parser.acceptsAll(asList("h", "?"), "show help").forHelp()
    // "--version", "s", "--distribution", "lognormal"
    val options: OptionSet = parser.parse(args: _*)
    //    parser.printHelpOn(System.out)
    println(s"\nUnrecognized options ${options.nonOptionArguments()} \n")

    val inputFile0 = inputFileOpt.value(options)
    println(s"Original value ${inputFile0.getAbsolutePath}")
    val workingDir: String = System.getProperty("user.dir")
    println(s"Working dir $workingDir")
    val inputFile = new File(inputFile0.getAbsolutePath)
    println(s"Absolute path to input file  $inputFile")

    val outDir: File = (if (options.has(outDirOpt)) {
      Success(outDirOpt.value(options))
    } else {
      if (inputFile != null) {
        Success(inputFile.getParentFile)
      }
      else {
        println("Help on options")
        parser.printHelpOn(System.out)
        Failure(new Exception("Input file  is null is not specified or does not exist!"))
      }
    }
      ).get

    val nMCMCIterations: Int = nMCMCiterationsOpt.value(options)
    val speciesCoverage: Double = speciesCoverageOpt.value(options)
    val distribution: String = distributionOpt.value(options)
    val version: String = versionOpt.value(options)
    //----------------------------------
    val paramASigma: Double = paramASigmaOpt.value(options)
    val paramBSigma: Double = paramBSigmaOpt.value(options)
    val paramSStDev: Int = paramSStDevOpt.value(options)
    //-----------------------------------
    val initMu = initMuOpt.value(options)
    val initVariance = initVarianceOpt.value(options)
    val initS = initSOpt.value(options)
    //-----------------------------------
    val thinningInterval = thinningIntervalOpt.value(options)
    //------------------------------------------------------
    val correlationFile: Option[File] = extractParameter(correlationFileOpt, options)
    //******* var-covar ************
    val varCovarianceFile: Option[File] = extractParameter(varCovarianceFileOpt, options)
    //*******

    val varianceScalingFactor0 = options.valuesOf(varianceScalingFactorOpt)
    //varianceScalingFactorOpt.value(options)
    val varianceScalingFactor: List[Double] = varianceScalingFactor0.toList.map(_.toDouble)

    InputParametersMCMCAndSamplingEffort(inputFile, outDir, nMCMCIterations, speciesCoverage, distribution, version = version, thinningInterval = thinningInterval, paramASigma = paramASigma, paramBSigma = paramBSigma, paramSStDev = paramSStDev, correlationFile = correlationFile, varCovarFile = varCovarianceFile, varianceScalingFactor = Option(varianceScalingFactor), initMu = Option(initMu), initVariance = Option(initVariance), intS = Option(initS))
  }

  private def extractParameter(correlationFileOpt: ArgumentAcceptingOptionSpec[File], options: OptionSet) = {
    if (options.hasArgument(correlationFileOpt)) {
      Some(correlationFileOpt.value(options))
    } else None
  }


  /**
    *
    * Old and original
    *
    */
  case class InputParametersMCMCAndSamplingEffort(tadFileName: File, outDir: File, nMCMCIterations: Int = 800000, coverage: Double = 0.9, distribution: String = "lognormal", relTollerance: Double = 1.05, relTolleranceStrict: Double = 1.001, rootFindingMethod: String = "Pegasus", mu0: Double = 1d, variance0: Double = 1d, s0: Double = 10000, version: String = "s", mode: String = "all", thinningInterval: Int = 10, paramASigma: Double = 0.1, paramBSigma: Double = 0.2, paramSStDev: Int = 400, correlationFile: Option[File], varCovarFile: Option[File], varianceScalingFactor: Option[List[Double]], initMu: Option[Double], initVariance: Option[Double], intS: Option[Int])

  /**
    * Newer general
    *
    */
  case class InputParametersMCMCGeneral(tadFileName: File, outDir: File, nMCMCIterations: Int = 800000, /*coverage: Double = 0.9,*/ distribution: String = "lognormal", nSpeciesInit: Try[Double] = Failure(new Exception("nS0 not specified in input parameters")), version: String = "s", thinningInterval: Int = 10, varCovarFile: Option[File], estimateWhat: String = "all" /*, correlationFile: Option[File], varCovarFile: Option[File], varianceScalingFactor: Option[List[Double]]*/)

  case class Input42ParametersDistribution( varCovarFile: Option[File], varianceScalingFactor: Option[List[Double]])

  case class InputParams_IG_Distribution(alpha0: Double = 1d, beta0: Double = 1d)

  case class InputParameters4SubsamplingCommmunity(communityFileName: File, subSampleFileName: File, coverage: List[Double] = List(0.9))


  case class Params4SamplingFromCommunity(inputTADFilePath: Option[String], coverage: Option[Double])

  def getInputParams4SamplingFromCommunity(args: Array[String]) = {
    val parser: OptionParser = new OptionParser()
    parser.allowsUnrecognizedOptions()
    val inputFileOpt = parser.acceptsAll(asList(PN.DATASET_NAME.synonyms: _*), "dataset name").withRequiredArg().ofType(classString)
    val coverageOfSpeciesOpt = parser.acceptsAll(asList(PN.COVERAGE_OF_SPECIES.synonyms: _*), """scope Of Data. Values: "","" """).withRequiredArg().ofType(java.lang.Double.TYPE).defaultsTo(DEFAULT_COVERAGE_OF_SPECIES)
    parser.acceptsAll(asList("h", "?"), "show help").forHelp()
    parser.printHelpOn(System.out)

    /** * Parse ***/
    println(s"argument:s $args")
    val options: OptionSet = parser.parse(args: _*)
    /* *****     Extracted values *** */
    val inputFile: String = inputFileOpt.value(options)
    val coverageOfSpecies: Double = coverageOfSpeciesOpt.value(options)
    Params4SamplingFromCommunity(Option(inputFile), Option(coverageOfSpecies))
  }

  /**
    * Parsing input parameters for sampling data from  Poisson-Lognorma
    *
    * @param args
    */
  def getInputParams4SamplingFromPoilog(args: Array[String]) = {
    val parser: OptionParser = new OptionParser()
    parser.allowsUnrecognizedOptions()
    /*
    val parameters = ParametersSamplingPoilog(S = 100, coverage = 1.0, meanLog = -0.86, varLog = 8.91, dsName = "44xSRS711891", estimatedBy = "", distr = "lognormal", spec = "Community", date = "", nD = 0l)
     */

  }

  case class ParamsParams4SamplingFromPoilog()

}


