package utils.debug
import scala.language.implicitConversions


/**
  *
  */

object Tap {
  implicit def any2Tapper[A](toTap: A): Tapper[A] = new Tapper(toTap)
}