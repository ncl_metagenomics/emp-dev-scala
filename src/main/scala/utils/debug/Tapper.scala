package utils.debug

/**
  *
  * @param tapMe
  * @tparam A
  */


class Tapper[A](tapMe: A) {
  def tap(f: (A) => Unit): A = {
    f(tapMe)
    tapMe
  }
}
