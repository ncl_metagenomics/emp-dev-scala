package utils

import java.io.{File, FileReader}

import com.github.martincooper.datatable.{DataColumn, DataTable}
import com.github.tototoshi.csv.CSVReader
import basics._
import org.ddahl.rscala.RClient
import com.github.martincooper.datatable._

import scala.reflect.ClassTag
import scala.util.{Failure, Success, Try}


/**
  * Created by peter on 19/09/16.
  */
object EffectiveSampleSize {

  def getEffectiveSampleSize(nLEstimTmp0: Vector[Double], columnName: String): Double = {
    val R = RClient() // initialise an R interpreter
    R.eval("library(coda)")
    R.x = nLEstimTmp0.toArray[Double]
    val res = R.evalD0("2+3")
    val effectiveSize = R.evalI0("effectiveSize(x)")
    println(s"Results $res")
    println(s"Effective size =  $effectiveSize")
    println("")
    effectiveSize.asInstanceOf[Double]
  }


  def main(args: Array[String]): Unit = {
    val fileName = "/media/sf_data/nps409/Work2Sync/Projects/emp_dev_scala_runs/TAD_DATA_Synthetic_Out/SRS711891_Synthetic_Out/AutoEval_160629_1555-27_160816/test01/s-800K-160630-1457-27/sample_size_posterior-s-s-all-44xSRS711891_TAD__40_pc_D_6198_lognormal_160629_1555-27_16-06-29_1623-28.sample_160630-1457_27_correct_Coverage_900_%o_160912-0021_08.csv"
    //val csvReader = CSVReader.open(fileName)
    // mu,variance,nL_Recommended,nL_Observed,multipleOfActualSampleSize,coverage,mu_orig,nL_orig
    val colTypes = Map("mu" -> Double, "variance" -> Double, "nL_Recommended" -> Double, "nL_Observed" -> Double, "multipleOfActualSampleSize" -> Double, "coverage" -> Double, "mu_orig" -> Double, "nL_orig" -> Double) //
    val nLDataS: Try[DataTable] = DataTableReadWrite.readCsv("", new File(fileName), colTypes)
    val nLData = nLDataS.get

    val columnName = "nL_Recommended"
    val nLEstimTmp = nLData.columns.getAs[Double](columnName)
    val nLEstimTmp0: Vector[Double] = nLEstimTmp.toOption match {
      case Some(x: DataColumn[Double]) => {
        println(s"column info = ${x.toString()} column name ${x.name}");
        x.data
      }
      case None => throw new Exception("could't extract column")
    }

    val effectiveSize1 = getEffectiveSampleSize(nLEstimTmp0)
    val effectiveSize2: Double = getEffectiveSampleSize2(nLData, columnName, scala.Double)

    println(s"Effective sample size 1 = ${effectiveSize1}")
    println(s"Effective sample size 2 = ${effectiveSize2}")
    println(s"L estimated = ${nLEstimTmp0(0)}")
  }

  def getEffectiveSampleSize(nLEstimTmp0: Vector[Double]) = {
    val R = RClient() // initialise an R interpreter
    R.eval("library(coda)")
    R.x = nLEstimTmp0.toArray[Double]
    val res = R.evalD0("2+3")
    val effectiveSize = R.evalD0("effectiveSize(x)")
    println(s"Results $res")
    println(s"Effective size =  $effectiveSize")
    println("")
    effectiveSize
  }

  def getEffectiveSampleSize2(data: DataTable, columnName: String, colType: AnyRef): Int = {
    val vecData00 = colType match {
      case scala.Double => data.columns.getAs[Double](columnName).toOption match {
        case Some(x: DataColumn[Double]) => {
          println(s"column info = ${x.toString()} column name ${x.name}");
          x.data
        }
        case None => throw new Exception("could't extract column")
      }
      case scala.Int => data.columns.getAs[Int](columnName).toOption match {
        case Some(x: DataColumn[Int]) => {
          println(s"column info = ${x.toString()} column name ${x.name}");
          x.data.map(_.toDouble)
        }
        case None => throw new Exception("could't extract column")
      }
      case _ => throw new Exception("Wrong column type - it must be either integer or double")
    }

    val vecData = vecData00
    val R = RClient() // initialise an R interpreter
    R.eval("library(coda)")
    R.x = vecData.toArray[Double]
    //    val res = R.evalD0("2+3")
    val effectiveSize: Double = Try(R.evalD0("effectiveSize(x)")) match{
      case Success(size) => size
      case Failure(e) =>
        println(e.getMessage);
        0
    }
    //    println(s"Results $res")
    println(s"Effective size =  $effectiveSize")
    println("")
    effectiveSize.toInt
  }

  def getEffectiveSampleSize3(data: DataTable, columnName: String): Double = {
    val colsInfo = data.columns.toList.map(x => (x.name, x.columnType))
    val vecData03 = data.columns.getAs[Double](columnName).toOption
    val vecData04 = vecData03 match {
      case Some(x: DataColumn[Double]) => {
        println(s"column info = ${x.toString()} column name ${x.name}");
        x.data
      }
      case _ => throw new Exception("Wrong column type - it must be either integer or double")
    }
    //    val vecData00 = colType match {
    //      case scala.Double => data.columns.getAs[Double](columnName).toOption match {
    //        case Some(x: DataColumn[Double]) => {
    //          println(s"column info = ${x.toString()} column name ${x.name}");
    //          x.data
    //        }
    //        case None => throw new Exception("could't extract column")
    //      }
    //      case scala.Int => data.columns.getAs[Int](columnName).toOption match {
    //        case Some(x: DataColumn[Int]) => {
    //          println(s"column info = ${x.toString()} column name ${x.name}");
    //          x.data.map(_.toDouble)
    //        }
    //        case None => throw new Exception("could't extract column")
    //      }
    //      case _ => throw new Exception("Wrong column type - it must be either integer or double")
    //    }


    val vecData = vecData04
    //vecData00
    val R = RClient() // initialise an R interpreter
    R.eval("library(coda)")
    R.x = vecData.toArray[Double]
    val res = R.evalD0("2+3")
    val effectiveSize = R.evalD0("effectiveSize(x)")
    println(s"Results $res")
    println(s"Effective size =  $effectiveSize")
    println("")
    effectiveSize.asInstanceOf[Double]
  }

}
