import java.io.FileWriter

import com.github.tototoshi.csv.{CSVWriter => TCSVW}
import basics.MetropolisHMCMCObj.ParamsPS
import basics.ResultsCollector.StatisticsInformation
import basics._
import net.jcazevedo.moultingyaml._
import org.joda.time.DateTime
import utils.MyYamlParamsProtocolForRunner._

import scala.util.{Failure, Success, Try}

/**
  * Created by peter on 11/10/16.
  */
object ExperimentScalaRunner2 {
  def main(args: Array[String]): Unit = {
    val options = MCMC.getProgramOptions(args)
    //    val options2 = MCMC.getProgramOptions
    val version = options('version).toString()
    val estimateWhat = options('mod).toString()
    val fileNameTAD: String = options('in).toString()
    val outDirectoryName0: String = options('out).toString()
    val distribution: String = options('distribution).toString()
    val outFileNamePosteriorParams = MCMC.modifyFileName(fileNameTAD, outDirectoryName0, version, estimateWhat)
    val outputYamlFileN = MCMC.constructYamlFileName(fileNameTAD, outDirectoryName0, version, estimateWhat)
    val mu0: Double = options('mu0).toString().toDouble //1
    //var sigma0: Double = Math.sqrt(options('variance0).toString().toDouble) //1
    val variance0 = options('variance0).toString().toDouble
    //********************
    val mcmcMetro = new MetropolisHMCMC()

    val data = MetropolisHMCMC.readDataASCSVTAD(fileNameTAD)
    val nIter: Int = options('s).toString().toInt //cast2Int(options('s))
    //****************
    val nD = MetropolisHMCMC.getD(data.drop(1))
    val nL = data.map((x) => x._1 * x._2).par.sum
    val s01: Int = options('s0).toString().toInt //match { case Some(x: Int) => x } // nL * 2
    val s0 = estimateWhat match {
      case "all" => if (s01 < (nD * 1.5)) nD * 1.5 else s01
      case "S" => if (s01 < (nD * 1.5)) nD * 1.5 else s01
      case _ => s01
    }
    val tCoverage: Double = (options('coverage)) match {
      case s: String => s.toDouble
      case d: Double => d
      case i: Int => i.toDouble
      case _ => throw new Error("coverage must be Double!"); Double.NaN
    }

    println(s"Input Parameters: file = ${fileNameTAD}, mu0 = ${mu0},  variancer0 = ${variance0}, s0 = ${s0}, nIter = ${nIter}, distribution=${distribution}")
    // Running MCMC computation
    val ((res: ParamsPS, nLL: Double), time) = MCMC.profile(mcmcMetro.calculate(mu0, variance0, s0, data, _nL = Some(nD), nIter = nIter, estimateWhat, version, outFileNamePosteriorParams, distribution, None))
    println(s"Estimate: mu = ${res.mu}, v = ${res.variance}, S = ${res.S}, NLL = ${nLL}")
    println(s"File written into $outFileNamePosteriorParams")
    println(s"Time of calculation = ${time * 1E-9} sec")
    println(s"Time of calculation = ${time} ")
    val outParams = utils.Params(res.mu, res.variance, res.S, res.nAccepted, nLL, time * 1e-9, nIter,  PosteriorParamsStat(StatisticsInformation(0.0,0.0,0.0,0.0,0.0),StatisticsInformation(0.0,0.0,0.0,0.0,0.0),StatisticsInformation(0.0,0.0,0.0,0.0,0.0)))
    //constructYamlFileName
    val yamlP = outParams.toYaml
    val fw = new FileWriter(outputYamlFileN)
    fw.write(yamlP.prettyPrint)
    fw.close()
    print(s"Yaml written to $outputYamlFileN")

    val abundDataFileN = fileNameTAD
    val postParamFileN = outFileNamePosteriorParams
    //val outputFileSamplingEffort = SamplingEffort.constructOutputFileNameAllOutputs(postParamFileN, tCoverage, "Pegasus")
    val outputFileSamplingEffortCorrectOutputs:String = SamplingEffort.constructOutputFileName4Correct(postParamFileN, tCoverage, "Pegasus")
    val outputIncorrectFile: String = SamplingEffort.constructIncorrectOutputFileName(postParamFileN, tCoverage, "Pegasus")
    //    val abuData = SamplingEffort.readData(abundDataFileN)
    val abuData = data
    //    val nD = abuData.map(x => x._2).par.sum

    val dropNumerator = 2
    val dropDenominator = 5
    val numRows2Skip = 0
    val (dataPosterior, head) = SamplingEffort.readDataPosteriorEstimates(postParamFileN, numRows2Skip)
    println(s"${dataPosterior(1).mu}, ${dataPosterior(1).variance}, ${dataPosterior(1).nCommunityTaxa}")

    val tollerance = 1.1 //relative error for difference of algorihms
    val tighterTollerance = 1.05
    val mu = -1.6
    val variance = 6.0
    //targeted coverage
    //val tCoverage = 0.9 //targeted coverage
    val targetFractionUnseen = 1 - tCoverage
    val lnTargetP = Math.log(targetFractionUnseen)
    val lnPInit = SamplingEffort.logPLogNormal.calculateWithRLikeScala(mu, variance, 0)
    //val nL = 19018
    //val nD = 2424

    println(s"*** Estimate Sampling Effort *******")
    assert(!mu.isInfinity && !mu.isNaN, s"mu=$mu")
    val nSampleEstW = SamplingEffort.getSampleSizeValueWithBrentPegasus(mu, variance, nL, tCoverage, tollerance, tighterTollerance,DateTime.now())
    println(s"Initial: nL = $nL, lnPInit = $lnPInit, c = ${1 - Math.exp(lnPInit)}")
    println(s"Target: lnTargetP = $lnTargetP, c = ${1 - Math.exp(lnTargetP)}")

    nSampleEstW match{
      case Success(nSampleEst)=> {
        println(s"Solution: required sampl size = ${nSampleEst.nL}, muS=${nSampleEst.nL}, c = ${nSampleEst.nL}")
        println(s"Solution required sample size as multiple of actual sample size = ${nSampleEst.nL}")
      }
      case Failure(e)=> println(e.getMessage)
    }


    implicit val fwCSV = SamplingEffort.OutputCSVWriters(TCSVW.open(outputFileSamplingEffortCorrectOutputs), TCSVW.open(outputIncorrectFile))
    fwCSV.correctOutput.writeRow(List("mu", "variance", "nL_Recommended", "nL_Observed", "multipleOfActualSampleSize", "coverage", "mu_org", "nL_orgi"))
    fwCSV.incorrectOutput.writeRow(List("mu", "variance", "nL_Recommended", "nL_Observed", "multipleOfActualSampleSize", "coverage", "mu_org", "nL_orgi"))
    val (res2: Array[Try[SamplingEffort.SampleSizeEstimateDS]], time2: Long) = SamplingEffort.profile(SamplingEffort.getSampleSizeDistribution(dataPosterior, mu, variance, nL, tCoverage, tollerance, tighterTollerance))
    fwCSV.correctOutput.close()
    fwCSV.incorrectOutput.close()

    val outputFileUsefulInfo = SamplingEffort.constructOutputFileNameAllOutputs(postParamFileN, "info_usefull_scalar_runner", tCoverage)
    val usefulInfoWriter = TCSVW.open(outputFileUsefulInfo)
    // val outSampleFN: String = outPutFN(postParamFileN)
    // val (res2: (ParamsPS, Double), time2) = profile(mcmcMetro.calculate(mu0, variance0, s0, data, _nL = Some(nL), nIter = nIter, estimateWhat, version, outfileName))
    //    SamplingEffort.writeOutput(res2, outputFileSamplingEffort)

    val outParamsWithCoverage = utils.ParamsWithCoverage(outParams.muLastVal, outParams.varianceLastVal, outParams.nSLastVal, outParams.nAccepted, time2, outParams.nIter, tCoverage, s"sampling effort for ${tCoverage * 100}% of species")
    val yamlPWCoverage = outParamsWithCoverage.toYaml
    println(yamlPWCoverage.prettyPrint)
    val outputYamlWCovrgFileN = MCMC.constructAnyFileName(fileNameTAD, outDirectoryName0, s"sample_size-s_", version, estimateWhat, "yaml")
    val fwYamlWCoverage = new FileWriter(outputYamlWCovrgFileN)
    fwYamlWCoverage.write(yamlPWCoverage.prettyPrint)
    fwYamlWCoverage.close()

    usefulInfoWriter.writeRow(List("time (posterior params) [s])", time))
    usefulInfoWriter.writeRow(List("time (sample size) [s])", time2))
    println(s"Output written to ${outputFileSamplingEffortCorrectOutputs}")
    println(s"Time of calculation = ${time2 * 1E-9} seconds")
    println("Finished estimating of sampling effort")
    usefulInfoWriter.close()
  }

}
