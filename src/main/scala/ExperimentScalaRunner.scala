import java.io.{BufferedWriter, File, FileReader, FileWriter}
import java.nio.file.{Path, Paths => NIOPaths}

import com.github.tototoshi.csv.{CSVWriter => TCSVW}
import basics.MetropolisHMCMCObj.ParamsPS
import basics.ResultsCollector.{StatisticsInformation, extractStats4Variable}
import basics._
import breeze.linalg.{DenseMatrix, DenseVector}
import com.github.martincooper.datatable.{DataRow, DataRowCollection, DataTable}
import net.jcazevedo.moultingyaml._
import utils.MyYamlParamsProtocolForRunner._
import utils.{DateTimeUtils, FileUtils, ReadingExperimentParameters, Statistics4MCMC}
import utils.ReadingExperimentParameters.InputParametersMCMCAndSamplingEffort
import utils.Statistics4MCMC._

import scala.util.{Failure, Success, Try}
import net.jcazevedo.moultingyaml.DefaultYamlProtocol._
import net.jcazevedo.moultingyaml.YamlWriter
import org.joda.time.{DateTime, Duration}
import utils.CovarianceOfPosteriorParams.calcAndSaveCovarianceWLognormal
import utils.DMTCPScriptGenerator.{DMTCPParameters, getDMTCPSCRIPT}

import scala.collection.mutable
import scala.concurrent.duration.TimeUnit
//import scala.reflect.io.File


object ExperimentScalaRunner {
  def constructOutputDirName(clOptionsGeneral: ReadingExperimentParameters.InputParametersMCMCGeneral, outDirectoryName00: String) = {
    outDirectoryName00 + sep + clOptionsGeneral.version + "-" + clOptionsGeneral.nMCMCIterations / 1000 + "K-" + clOptionsGeneral.distribution + "-" + MCMC.DATE_STR

  }

  def constructOutputDirName(clOptionsGeneral: ReadingExperimentParameters.InputParametersMCMCGeneral, clOptions2ParamsDistribution: ReadingExperimentParameters.Input42ParametersDistribution, clOptionsIGDistribution: ReadingExperimentParameters.InputParams_IG_Distribution, outDirectoryName00: String) = {

    outDirectoryName00 + sep + clOptionsGeneral.version + "-" + clOptionsGeneral.nMCMCIterations / 1000 + "K-" + clOptionsGeneral.distribution + "-" + MCMC.DATE_STR
  }


  def constructOutputDirName4IG(clOptionsGeneral: ReadingExperimentParameters.InputParametersMCMCGeneral, clOptions2ParamsDistribution: ReadingExperimentParameters.Input42ParametersDistribution, outDirectoryName00: String) = {

    outDirectoryName00 + sep + clOptionsGeneral.version + "-" + clOptionsGeneral.nMCMCIterations / 1000 + "K-" + clOptionsGeneral.distribution + "-" + MCMC.DATE_STR

  }


  val sep: String = File.separator

  override def finalize(): Unit = super.finalize()


  def writeMCMCInputParams2YAML(ymlFNMCMCInputParams: String, options: InputParametersMCMCAndSamplingEffort) = {
    import utils.MyYamlParamsProtocolForRunner._
    //    val outParamsWithCoverage = utils.ParamsWithCoverage(outParams.mu, outParams.variance, outParams.S, outParams.nAccepted, time2, outParams.nIter, tCoverage, s"sampling effort for ${tCoverage
    //    val yamlPWCoverage = outParamsWithCoverage.toYaml
    // Option(options.varCovarFile.get.getAbsolutePath))

    val varFilePath: Option[String] = options.varCovarFile match {
      case Some(f: java.io.File) => Option(f.getAbsolutePath)
      case _ => None
    }
    val input2Yaml = utils.ParamsMCMCInput(Option(options.mu0), Option(options.variance0), Option(options.s0.toInt), Option(options.paramASigma), Option(options.paramBSigma), Option(options.paramSStDev), Option(options.nMCMCIterations), Option(options.thinningInterval), options.varianceScalingFactor, varFilePath)

    val inputInYaml = input2Yaml.toYaml
    println(s"MCMC input parameters ")
    println(inputInYaml.prettyPrint)
    //    val outputYamlWCovrgFileN = MCMC.constructAnyFileName(fileNameTAD, outDirectoryName0, s"sample_size-s_", version, estimateWhat, "yaml")
    val fwYamlWCoverage = new FileWriter(ymlFNMCMCInputParams)
    fwYamlWCoverage.write(inputInYaml.prettyPrint)
    fwYamlWCoverage.close()
  }

  /**
    *
    * @param inputDirectoryName
    * @param outDirectoryName0
    * @param strings
    * @return
    */
  def copyScripts(inputDirectoryName: String, outDirectoryName0: String, strings: List[String] = List(""".*\.sh$ """, """.*\.dmtcp$""")): Seq[Try[Long]] = {
    //    FileUtils.getDateAsString()
    //    FileUtils.copyFileToDir()
    val filesList: List[File] = basics.ResultsCollector.getSubFiles(inputDirectoryName, strings)
    filesList.map(f => utils.FileUtils.copyFileToDir(f.getCanonicalPath, outDirectoryName0))
  }

  /**
    *
    * @param newLinkString
    * @param targetString
    * @return
    */
  def createSoftLink(newLinkString: String, targetString: String) = {
    import java.io.IOException
    import java.nio.file.Files
    import java.nio.file.Path

    val newLink: Path = new File(newLinkString).toPath
    val target: Path = new File(targetString).toPath

    try
      Files.createSymbolicLink(newLink, target)
    catch {
      case x: IOException =>
        System.err.println(x.getMessage)
        println(x.getMessage)
      case x: UnsupportedOperationException =>
        // Some file systems do not support symbolic links.
        System.err.println(x.getMessage)
        println(x.getMessage)
    }
  }


  def createSoftLinkToDir(newLinkDir: String, target: String) = {
    //    val targetName = File.get
    val targetFile: File = new File(target)
    val targetFileName = targetFile.getName
    val newLingPath = NIOPaths.get(newLinkDir, targetFileName)
    val newLingPathStr = newLingPath.toString
    println("a")
    createSoftLink(newLingPathStr, target)
    println("a")

  }

  /**
    * Get median, mean and CI for '''posterior''' parameters
    *
    * @param outFileNamePosteriorParams
    * @return
    */
  def getPosteriorParamsStat(outFileNamePosteriorParams: String): PosteriorParamsStat = {
    val posteriorData: Try[DataTable] = basics.ResultsCollector.readPosteriorData(Option(new File(outFileNamePosteriorParams)))
    val nSkipRows = 10000

    val n: Int = posteriorData match {
      case Success(pD) => pD.size
      case Failure(e) =>
        println(e.getMessage())
        0
    }

    val nSkipRowsUsed = if (n - nSkipRows < 1000) {
      val nskip0 = n * 2 / 5
      val r0 = n - nskip0
      nskip0
    } else {
      nSkipRows
    }

    val nSEstO: Try[ResultsCollector.StatisticsInformation] = basics.ResultsCollector.extractStats4Variable(posteriorData.get, nSkipRowsUsed, "S")
    val nSEst: ResultsCollector.StatisticsInformation = nSEstO match {
      case Success(nEst: StatisticsInformation) => nEst
      case Failure(e) => throw new Exception(e.getMessage + "\n" + s" for file with posterior $outFileNamePosteriorParams ")
    }

    val muEstO = basics.ResultsCollector.extractStats4Variable(posteriorData.get, nSkipRowsUsed, "mu")
    val muEst: ResultsCollector.StatisticsInformation = muEstO match {
      case Success(est: StatisticsInformation) => est
      case Failure(e) => throw new Exception(e.getMessage + "\n" + s" for file with posterior $outFileNamePosteriorParams ")
    }
    //sigma
    val varianceEst0 = basics.ResultsCollector.extractStats4Variable(posteriorData.get, nSkipRowsUsed, "variance")
    val varianceEst: ResultsCollector.StatisticsInformation = varianceEst0 match {
      case Success(est: StatisticsInformation) => est
      case Failure(e) => throw new Exception(e.getMessage + "\n" + s" for file with posterior $outFileNamePosteriorParams ")
    }
    val paramsMedian = PosteriorParamsStat(muEst, varianceEst, nSEst)
    paramsMedian
  }

  def writeTextToFile(fileName: String, text: String): Unit = {
    val file = new File(fileName)
    val bw = new BufferedWriter(new FileWriter(file))
    bw.write(text)
    bw.close()
  }

  def getDMTCPScriptFN(covarianceFileN: String, dbName: String) = {
    val f = new File(covarianceFileN)
    val parentDir = f.getParent
    val fileName = f.getName
    val newName = s"${dbName}_${MCMC.DATE_STR2}.dmtcp"
    // Br_90_0313_covar_300K_170313_2327_scaleFactor.dmtcp
    new File(parentDir, newName).getAbsolutePath
  }

  /**
    *
    * @param inputDirectoryName
    * @param covarianceFile
    * @param posteriorParamsStat
    * @param varianceScalingFactor
    * @param fileNameTAD
    * @param dbName
    * @param res
    */
  def createWriteDMTCScript(inputDirectoryName: String, covarianceFile: Option[String], posteriorParamsStat: PosteriorParamsStat, varianceScalingFactor: Option[List[Double]], fileNameTAD: String, dbName: String, res: basics.MetropolisHMCMCObj.ParamsPS) = {
    val relCovarPath: Option[String] = utils.MCMCIO.absoluteToRelativePath(inputDirectoryName, covarianceFile.get)
    val dmtcpParams = DMTCPParameters(res.mu, res.variance, res.S, relCovarPath.get, varianceScalingFactor.get, fileNameTAD)
    val dmtcpScript = getDMTCPSCRIPT(dmtcpParams)
    val dmTcpScriptFileName: String = getDMTCPScriptFN(covarianceFile.get, dbName)
    writeTextToFile(dmTcpScriptFileName, dmtcpScript)
  }

  /**
    * Extract  database name from database directory path
    *
    * @param outDirectoryName
    * @return databse neame as string
    */
  def getDBName(outDirectoryName: String) = {
    val dbName = new File(outDirectoryName).getName
    dbName
  }


  def executeExperimentScalaRunner(args: Array[String]): Unit = {

    val clOptions: InputParametersMCMCAndSamplingEffort = ReadingExperimentParameters.getLNInputParams4MCMCAndSamplingEffort(args)
    // *** *** *** *** ***
    val version = clOptions.version
    //options('version).toString()
    val estimateWhat = clOptions.mode
    //options('mod).toString()
    val fileNameTAD: String = clOptions.tadFileName.getCanonicalPath
    //getAbsolutePath
    //options('in).toString()
    val outDirectoryName00: String = {
      clOptions.outDir.getAbsolutePath
    }
    //options('out).toString()
    val outDirectoryName0 = constructOutputDirName(clOptions, outDirectoryName00)
    val distribution: String = clOptions.distribution
    val dbNme = getDBName(outDirectoryName00)

    val inputDirectoryName = new File(".").getCanonicalFile().getAbsolutePath
    println(s"Current directorry= ${inputDirectoryName}")

    val inputDirectoryName02 = getClass().getProtectionDomain().getCodeSource().getLocation()
    println(s"Jar directory = ${inputDirectoryName}")
    //************************************************************
    Try(copyScripts(inputDirectoryName, outDirectoryName0, List(""".*\.sh$ """, """.*\.dmtcp$""")))
    Try(copyScripts(inputDirectoryName, outDirectoryName0, List("""output\.txt$""", """error\.txt""")))
    Try(copyScripts(inputDirectoryName, outDirectoryName0, List(""".*\.log$""")))
    Try(createSoftLinkToDir(outDirectoryName0, inputDirectoryName))
    //----------------------------------------------------
    //options('distribution).toString()
    val outFileNamePosteriorParams = MCMC.modifyFileName(fileNameTAD, outDirectoryName0, version, estimateWhat)
    // MCMC in Parameters
    //TODO: 170301 output MCMC input parameters to YAML file
    /**
      * output MCMC input parameters to YAML file
      */
    val ymlFNMCMCInputParams = basics.MCMC.modifyFileNameInGeneral("mcmc-input-parameters", outDirectoryName0, version, estimateWhat, "yaml")
    writeMCMCInputParams2YAML(ymlFNMCMCInputParams, clOptions)
    //---
    val outputYamlFileN = MCMC.constructYamlFileName(fileNameTAD, outDirectoryName0, version, estimateWhat)
    val mu0: Double = clOptions.mu0
    //options('mu0).toString().toDouble //1
    //var sigma0: Double = Math.sqrt(options('variance0).toString().toDouble) //1
    val variance0 = clOptions.variance0
    //options('variance0).toString().toDouble
    //********************
    val mcmcMetro = new MetropolisHMCMC(clOptions.thinningInterval)
    // *********************************************************
    val fileNameOnlyTAD = clOptions.tadFileName.getName
    val data = (if (fileNameOnlyTAD.matches(""".*\.csv$""")) Success(MetropolisHMCMC.readDataASCSVTAD(fileNameTAD))
    else if (fileNameOnlyTAD.matches(""".*\.sample""")) Success(MetropolisHMCMC.readData(fileNameTAD))
    else Failure(new Exception("Input file must be *.csv or *.sample file"))).get
    val nIter: Int = clOptions.nMCMCIterations
    //options('s).toString().toInt //cast2Int(options('s))
    //****************
    val nD = MetropolisHMCMC.getD(data)
    val nL = data.map((x) => x._1 * x._2).par.sum
    val s0 = clOptions.s0
    val tCoverage: Double = clOptions.coverage

    // *** Correlation matrix
    //    val choleskyLowerTriangleOfCorrelationMatrix: Option[DenseMatrix[Double]] =
    //      choleskyLowerTriangleOfMatrix(clOptions.correlationFile)
    //------------------------
    // *** Variance-covariance Matrix
    val choleskyLowerTriangleOfAdjustedVarCovarMatrx: Option[DenseMatrix[Double]] =
    choleskyLowerTriangleOfCovarianceMatrix(clOptions.varCovarFile, clOptions.distribution, clOptions.varianceScalingFactor.get)
    //------------------------
    println(s"Input Parameters:")
    println(s"    input file = ${} ")
    println(s"    number of iterations = ${clOptions.nMCMCIterations}, thinning interval = ${clOptions.thinningInterval}, distribution = ${distribution}")
    println(s"    required expected fraction of species  = ${100 * clOptions.coverage}%")
    // Running MCMC computation
    val ((res: ParamsPS, nLL: Double), time) = MCMC.profile(mcmcMetro.calculate(mu0, variance0, s0, data, estimateWhat = estimateWhat, version = version, outfileName = outFileNamePosteriorParams, distribution = distribution, choleskyLowerTriangleOfVarCovarMatrx = choleskyLowerTriangleOfAdjustedVarCovarMatrx, nIter = clOptions.nMCMCIterations))

    val covarianceFile: Try[String] = calcAndSaveCovarianceWLognormal(outFileNamePosteriorParams)
    //*************************************************
    val posteriorParamsStat: PosteriorParamsStat = getPosteriorParamsStat(outFileNamePosteriorParams)
    Try(createWriteDMTCScript(inputDirectoryName, covarianceFile.toOption, posteriorParamsStat, clOptions.varianceScalingFactor, fileNameTAD, dbNme, res))
    // *************************************************
    //*************************************************
    println(s"Parameter estimation completed in ${DateTimeUtils.durationToString((time * 1e-6).toLong)}\n") // (time * 1E-9} sec
    println(s"Distribution of posterior parameters in $outFileNamePosteriorParams")
    val outParams = utils.Params(res.mu, res.variance, res.S, res.nAccepted, nLL, time * 1e-9, nIter, posteriorParamsStat)

    val abundDataFileN = fileNameTAD
    val postParamFileN = outFileNamePosteriorParams
    val outputFileSamplingEffort = SamplingEffort.constructOutputFileNameAllOutputs(postParamFileN, tCoverage, "Pegasus", "s", outParams)
    val outputFileSamplingEffort2 = SamplingEffort.constructOutputFileName4Correct(postParamFileN, tCoverage, "Pegasus")
    val outputIncorrectFile: String = SamplingEffort.constructIncorrectOutputFileName(postParamFileN, tCoverage, "Pegasus")
    val abuData = data

    val dropNumerator = 2
    val dropDenominator = 5
    val numRows2Skip = 0
    val (dataPosterior, head) = SamplingEffort.readDataPosteriorEstimates(postParamFileN, numRows2Skip)
    // println(s"${dataPosterior(1).mu}, ${dataPosterior(1).variance}, ${dataPosterior(1).nCommunityTaxa}")

    val tollerance = 1.1
    //relative error for difference of algorihms
    val tighterTollerance = 1.05
    val mu = -1.6
    val variance = 6.0
    //targeted coverage
    //val tCoverage = 0.9 //targeted coverage
    val targetFractionUnseen = 1 - tCoverage
    val lnTargetP = Math.log(targetFractionUnseen)
    val lnPInit = SamplingEffort.logPLogNormal.calculateWithRLikeScala(mu, variance, 0)
    //val nL = 19018
    //val nD = 2424

    println(s"\n*** Estimate Sampling Effort *******")
    assert(!mu.isInfinity && !mu.isNaN, s"mu=$mu")
    val nSampleEstW = SamplingEffort.getSampleSizeValueWithBrentPegasus(mu, variance, nL, tCoverage, tollerance, tighterTollerance, DateTime.now())

    nSampleEstW match {
      case Success(nSampleEst) => {
      }
      case Failure(e) => println(e.getMessage)
    }

    // *********************************
    implicit val fwCSV = SamplingEffort.OutputCSVWriters(TCSVW.open(outputFileSamplingEffort2), TCSVW.open(outputIncorrectFile))
    fwCSV.correctOutput.writeRow(List("mu", "variance", "nL_Recommended", "nL_Observed", "multipleOfActualSampleSize", "coverage", "mu_org", "nL_orgi"))
    fwCSV.incorrectOutput.writeRow(List("mu", "variance", "nL_Recommended", "nL_Observed", "multipleOfActualSampleSize", "coverage", "mu_org", "nL_orgi"))
    val (res2: Array[Try[SamplingEffort.SampleSizeEstimateDS]], time2: Long) = SamplingEffort.profile(SamplingEffort.getSampleSizeDistribution(dataPosterior, mu, variance, nL, tCoverage, tollerance, tighterTollerance))
    fwCSV.correctOutput.close()
    fwCSV.incorrectOutput.close()

    val outputFileUsefulInfo = SamplingEffort.constructOutputFileNameAllOutputs(postParamFileN, "timing_info_scala_runner_", tCoverage)
    val usefulInfoWriter = TCSVW.open(outputFileUsefulInfo)
    // val outSampleFN: String = outPutFN(postParamFileN)
    // val (res2: (ParamsPS, Double), time2) = profile(mcmcMetro.calculate(mu0, variance0, s0, data, _nL = Some(nL), nIter = nIter, estimateWhat, version, outfileName))
    //SamplingEffort.writeOutput(res2, outputFileSamplingEffort)
    //TODO: 170301 correctly name output parameters - estimated values are last values in the chain and not statistics or aggreagated values
    val outParamsWithCoverage = utils.ParamsWithCoverage(outParams.muLastVal, outParams.varianceLastVal, outParams.nSLastVal, outParams.nAccepted, time2, outParams.nIter, tCoverage, s"sampling effort for ${tCoverage * 100}% of species")
    val yamlPWCoverage = outParamsWithCoverage.toYaml
    println(yamlPWCoverage.prettyPrint)
    val outputYamlWCovrgFileN = MCMC.constructAnyFileName(fileNameTAD, outDirectoryName0, s"sample_size-s_", version, estimateWhat, "yaml")
    val fwYamlWCoverage = new FileWriter(outputYamlWCovrgFileN)
    fwYamlWCoverage.write(yamlPWCoverage.prettyPrint)
    fwYamlWCoverage.close()

    usefulInfoWriter.writeRow(List("time (posterior params) [s])", DateTimeUtils.durationToString((time * 1e-6).toLong)))
    usefulInfoWriter.writeRow(List("time (sample size) [s])", DateTimeUtils.durationToString((time2 * 1e-6).toLong)))
    println(s"\nSample size estimation completed in  ${DateTimeUtils.durationToString((time2 * 1e-6).toLong)} ")
    println(s"Output written to $outputFileSamplingEffort\n")
    //    println("Finished estimating of sampling effort")
    println("************************************************")
    println(s"Parameter estimation completed in ${DateTimeUtils.durationToString((time * 1e-6).toLong)}  \n")
    println(s"\n Total running time ${DateTimeUtils.durationToString(((time + time2) * 1e-6).toLong)}  \n")
    Try(copyScripts(inputDirectoryName, outDirectoryName0, List("""output\.txt$ """, """error\.txt""")))
    usefulInfoWriter.close()

  }

  /**
    *
    * @param args
    */
  def main(args: Array[String]): Unit = {
    executeExperimentScalaRunner(args)

  }

  private def choleskyLowerTriangleOfMatrix(correlationFile: Option[File]) = {
    // clOptions.correlationFile
    correlationFile match {
      case Some(fileName: File) => Option(getCholeskyLowerTriangleOfCorrelationMatrix(fileName))
      case None => None
      case _ => throw new Error(s"Correlation matrix in file ${correlationFile}")
    }
  }

  /**
    * Cholesky lower Triangle of already adjusted var-covar variance
    *
    * @param correlationFile
    * @param distribution
    * @return
    */
  private def choleskyLowerTriangleOfCovarianceMatrix(correlationFile: Option[File], distribution: String, scalingFactor: Double) = {
    //todo:170310 call appropriate conversion for distribution
    val colTypes = Map("mu" -> Double, "variance" -> Double, "S" -> Double)
    // distribution: Lognormal
    correlationFile match {
      case Some(fileName: File) => Option({
        //todo: 170312 work here
        val covarianceMatrix: DenseMatrix[Double] = utils.Statistics4MCMC.readMatrix3By3(fileName, colTypes)
        val scaledCovariance = getRescaledCovariance(covarianceMatrix, scalingFactor)
        getCholeskyLowerTriangular(scaledCovariance)
      })
      case None => None
      case _ => throw new Error(s"Correlation matrix in file ${correlationFile}")
    }
  }

  /**
    * Cholesky lower Triangle of already adjusted var-covar variance
    *
    * @param correlationFile
    * @param distribution
    * @return
    */
   private def choleskyLowerTriangleOfCovarianceMatrix(correlationFile: Option[File], distribution: String, scalingFactor: List[Double]): Option[DenseMatrix[Double]] = {
    //todo:170310 call appropriate conversion for distribution
    val colTypes = Map("mu" -> Double, "variance" -> Double, "S" -> Double)
    // distribution: Lognormal
    correlationFile match {
      case Some(fileName: File) => Option({
        //todo: 170312 work here
        val covarianceMatrix: DenseMatrix[Double] = utils.Statistics4MCMC.readMatrix3By3(fileName, colTypes)
        val scaledCovariance = getRescaledCovariance(covarianceMatrix, scalingFactor)
        getCholeskyLowerTriangular(scaledCovariance)
      })
      case None => None
      case _ => throw new Error(s"Correlation matrix in file ${correlationFile}")
    }
  }

  /**
    * Cholesky lower Triangle of already adjusted var-covar variance
    *
    * @param correlationFile
    * @param distribution
    * @return
    */
   def choleskyLowerTriangleOfCovarianceMatrix(correlationFile: Option[File], distribution: String, scalingFactor: List[Double], colTypes:Map[String, Double.type]  ): Option[DenseMatrix[Double]] = {
    //todo:170310 call appropriate conversion for distribution
//    val colTypes: Map[String, Double.type] = Map("mu" -> Double, "variance" -> Double, "S" -> Double)
    // distribution: Lognormal
    correlationFile match {
      case Some(fileName: File) => Option({
        //todo: 170312 work here
        val covarianceMatrix: DenseMatrix[Double] = utils.Statistics4MCMC.readMatrix3By3(fileName, colTypes)
        val scaledCovariance: DenseMatrix[Double] = getRescaledCovariance(covarianceMatrix, scalingFactor)
        getCholeskyLowerTriangular(scaledCovariance)
      })
      case None => None
      case _ => throw new Error(s"Correlation matrix in file ${correlationFile}")
    }
  }

  private def choleskyLowerTriangleOfCovarianceMatrix(covarianceMatrix: DenseMatrix[Double], scalingFactor: List[Double]) = {
    val scaledCovariance = getRescaledCovariance(covarianceMatrix, scalingFactor)
    getCholeskyLowerTriangular(scaledCovariance)
  }

  private def choleskyLowerTriangleOfCovarianceMatrixWithAdjustment(correlationFile: Option[File], distribution: String, covarianceScaler: Double) = {
    //todo:170310 call appropriate conversion for distribution
    val colTypes = Map("mu" -> Double, "variance" -> Double, "S" -> Double)
    // distribution: Lognormal
    correlationFile match {
      case Some(fileName: File) => Option({
        //todo: 170312 work here
        val covarianceMatrix: DenseMatrix[Double] = utils.Statistics4MCMC.readMatrix3By3(fileName, colTypes)
        val (adjustedVariance, adjustedCovariance) = utils.Statistics4MCMC.getAdustedAndScaledCovarianceForLognormal(covarianceMatrix, 1d)
        getCholeskyLowerTriangular(adjustedCovariance)
      })
      case None => None
      case _ => throw new Error(s"Correlation matrix in file ${correlationFile}")
    }
  }


  def constructOutputDirName(clOptions: InputParametersMCMCAndSamplingEffort, outDirectoryName00: String): String = {
    outDirectoryName00 + sep + clOptions.version + "-" + clOptions.nMCMCIterations / 1000 + "K-" + clOptions.distribution + "-" + clOptions.rootFindingMethod + "-" + clOptions.relTollerance + "-" + clOptions.relTolleranceStrict + "-" + MCMC.DATE_STR
  }
}


