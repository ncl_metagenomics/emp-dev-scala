import java.io.{File, PrintWriter}

import com.github.tototoshi.csv.CSVWriter
import utils.AbundanceIO
import utils.sampling._
import utils.sampling.SamplingPoilog._
import utils.sampling._
import spray.json.DefaultJsonProtocol._
import spray.json.{RootJsonFormat, _}


/**
  * Created by peter on 26/10/16.
  */
object SamplingCommunityFromPoissonLognormal {
  def main(args: Array[String]): Unit = {
    runner01(args)
  }


  def runner01(args: Array[String]): Unit = {
    val allParams = utils.ReadingExperimentParameters.getInputParams4SamplingCommunity(args)
    val fileNameAbundanceOut = AbundanceIO.outputFileName4CommunityData(allParams.paramsFN.dataSetName, /*allParams.paramsFN.dataFormat.get*/ "Abundance", allParams.paramsFN.scopeOfData, allParams.paramsFN.isGeneratedOrSynth, allParams.paramsFN.coverage, allParams.paramsFN.tadDistribution, allParams.paramsFN.dateTimeStamp, allParams.paramsFN.fileFormat)
    val fileNameTADOut = AbundanceIO.outputFileName4CommunityData(allParams.paramsFN.dataSetName, /*allParams.paramsFN.dataFormat.get*/ "TAD", allParams.paramsFN.scopeOfData, allParams.paramsFN.isGeneratedOrSynth, allParams.paramsFN.coverage, allParams.paramsFN.tadDistribution, allParams.paramsFN.dateTimeStamp, allParams.paramsFN.fileFormat)
    val jsonCfgFileName = AbundanceIO.outputFileName4CommunityData(allParams.paramsFN.dataSetName, /*allParams.paramsFN.dataFormat.get*/ "Config", allParams.paramsFN.scopeOfData, allParams.paramsFN.isGeneratedOrSynth, allParams.paramsFN.coverage, allParams.paramsFN.tadDistribution, allParams.paramsFN.dateTimeStamp, "json")





    val subdirName = AbundanceIO.outputDirName(allParams.paramsFN.dataSetName, allParams.paramsFN.dataFormat.get, allParams.paramsFN.scopeOfData, allParams.paramsFN.isGeneratedOrSynth, allParams.paramsFN.coverage, allParams.paramsFN.tadDistribution, allParams.paramsFN.dateTimeStamp)
    val fullSubdir = allParams.paramsFN.outDir + File.separator + subdirName

    (new File(fullSubdir)).mkdirs()
    val file4AbundancePath = fullSubdir + File.separator + fileNameAbundanceOut
    val file4TADPath = fullSubdir + File.separator + fileNameTADOut
    val jsonCfgFilePath = fullSubdir + File.separator + jsonCfgFileName
    println(s"Output Abundance : $file4AbundancePath")
    println(s"Output TAD : $file4TADPath")

    val parameters = allParams.paramsFN.tadDistribution match {
      case TADDistributionInfo.LOGNORMAL.name => ParametersSamplingPoilog(S = allParams.numberOfSpeciest, coverage = allParams.paramsFN.coverage, meanLog = allParams.parameters4Distribution.asInstanceOf[Parameters4Distribution].meanOfLogValue, varLog = allParams.parameters4Distribution.asInstanceOf[Parameters4Distribution].varianceOfLogValue, dsName = allParams.paramsFN.dataSetName, estimatedBy = allParams.paramsFN.tadDistribution, distr = allParams.paramsFN.tadDistribution, spec = allParams.paramsFN.scopeOfData, date = allParams.paramsFN.dateTimeStamp, nD = allParams.numberOfSpeciest)
      case _ => throw new Exception(s"parameter value does not match any of allowed values ${TADDistributionInfo.values.mkString(", ")}")
    }
    println()

    val headerAbundance = List("abundance")
    val headerTAD = List[String]("Abundance", "Num.Species")
    val abundanceWriter = CSVWriter.open(new File(file4AbundancePath))
    /**
      * Sampling from Community
      */
    val abundance: List[Long] = getAbundaceForPoilog(parameters, abundanceWriter, headerAbundance).sorted
    val nL = getNumOfIndividualsFromAbundance(abundance)
    val parameters2 = new ParametersSamplingPoilog11(parameters,Option(nL))
    // ---- END ----
    // ***  TAD conversion ****
    val tadData = utils.AbundanceConversion.convertAbundance2TADLong(abundance)
    //--- END ----
    writeHashMapAsCSV[Long, Int](tadData, file4TADPath, headerTAD)
    // *** write Parameters as JSON file ****
    val jsonStr = parameters2.toJson.prettyPrint
    val p: PrintWriter = new PrintWriter(jsonCfgFilePath)
    p.write(jsonStr)
    p.close()
    println(jsonStr)

    // ------------

    println(s"Output Abundance : $file4AbundancePath")
    println(s"Output TAD : $file4TADPath")
    println("""*** FINISHED ***""")
  }

  def getSubDirName(p: utils.sampling.Parameters4Sampling): String = {

    ""
  }

}
