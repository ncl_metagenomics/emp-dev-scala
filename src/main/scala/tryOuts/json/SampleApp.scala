package tryOuts.json

/**
  * Created by peter on 24/10/16.
  */

import com.fasterxml.jackson.databind.{JsonNode, ObjectMapper}
import play.libs.Json
import spray.json._

//class SampleApp {
//  import MyJsonProtocol._
//  val inputJson = """{"children":["a", "b", "c"]}"""
//  println(s"Deserialise: ${inputJson.parseJson.convertTo[MyCaseClass]}")
//}

object SampleApp {

  def main(args: Array[String]): Unit = {
    import MyJsonProtocol._
    val inputJson:String = """{"children":["a", "b", "c"]}"""
    val myObject = inputJson.parseJson.convertTo[MyCaseClass]
    println(s"Deserialise: ${myObject}")
    val jsonStrt = myObject.toJson.prettyPrint
    println(s"JSonPretty print:\n ${jsonStrt}")
    println("")


    val jsonStrt02: JsonNode = Json.parse(jsonStrt)
    val mapper: ObjectMapper = new ObjectMapper();
    val a = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(jsonStrt02)
    println("Reconstructed:\n"+a)
    println("")

    //    val jsonStrt = lParamsTest.toJson.prettyPrint
    //    println(jsonStrt)
    //    val jsonStrt02:JsonNode = Json.parse(jsonStrt)
    //    println("")
    //    println(s"parsed back")
    //
    //
    //    val mapper:ObjectMapper  = new ObjectMapper();
    //    val a = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(jsonStrt02)
    //    println(a)


    //    val jsonStr = .toJson.prettyPrint
    //    val p: PrintWriter = new PrintWriter(jsonCfgFilePath)
    //    p.write(jsonStr)
    //    p.close()


  }

  case class MyCaseClass(children: List[String])

  object MyJsonProtocol extends DefaultJsonProtocol {
    //Explicit type added here now
    implicit val myCaseClassSchemaFormat: JsonFormat[MyCaseClass] = jsonFormat1(MyCaseClass)
  }

}