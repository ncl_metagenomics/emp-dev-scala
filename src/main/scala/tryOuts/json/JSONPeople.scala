package tryOuts.json

import scala.util.parsing.json.JSONArray
import spray.json.DefaultJsonProtocol
import spray.json._
/**
  * Created by peter on 21/10/16.
  *
  * Example how to parse list of comlex objects from JSON using spray-json
  */
object JSONPeople {
  case class Person(name: String, age: String)
  case class FriendList(items: List[Person])

  object FriendsProtocol extends DefaultJsonProtocol {
    implicit val personFormat = jsonFormat2(Person)
    implicit object friendListJsonFormat extends RootJsonFormat[FriendList] {
      def read(value: JsValue) = FriendList(value.convertTo[List[Person]])
      override def write(f: FriendList) = {
        JsArray(f.items.map(_.toJson).toVector)//serializationError("not supported") //f.items.toJson
      }
    }
  }


//  def write(pers: Person) = JsObject(Map(
//    "name" -> JsString(pers.name),
//    "adresses" -> JsArray(pers.adresses.map(_.toJson).toList)
//  ))

  //******************************************************
//  def write(h: Handshake): JsValue =
//    JsObject(
//      "channel"  -> JsString(h.channel),
//      "id" -> JsNumber(h.id),
//      "supportedConnectionTypes" -> JsArray(h.supportedConnectionTypes.map(value => JsString(value))),
//      "version" -> JsString(h.version),
//      "minimumVersion" -> JsString(h.minimumVersion)
//    )


  def main(args: Array[String]): Unit = {
    import FriendsProtocol._

//    val mainJSON: String =
//      """
//        |   {"source":[
//        |           {"name":"john","age":20},
//        |           {"name":"michael","age":25},
//        |           {"name":"sara", "age":23}
//        |         ]
//        |}
//        |
//      """.stripMargin

    val mainJSON: String =
      """
        [{
            "name": "John",
            "age": "30"
        },
        {
            "name": "Tom",
            "age": "25"
        },
 |      {
 |            "name": "Jacque",
 |            "age": "21"
 |      }]
      """.stripMargin

    val input = mainJSON.mkString.parseJson

    val friendList = input.convertTo[FriendList]

    println("as FrientList object toString:" + friendList)

    println("as json:" + friendList.toJson.prettyPrint)


    //val jsonMainArr: JSONArray = JSONArray(mainJSON.g) //getJSONArray("source"));

    //    for (int i = 0;
    //    i < jsonMainArr.length();
    //    i ++)
    //    {
    //      // **line 2**
    //      JSONObject childJSONObject = jsonMainArr.getJSONObject(i);
    //      String name = childJSONObject.getString("name");
    //      int age = childJSONObject.getInt("age");
    //    }
  }
}
