package tryOuts.json

import com.fasterxml.jackson.databind.{JsonNode, ObjectMapper}
import play.libs.Json
import utils.sampling.SamplingPoilog._
import spray.json.{JsArray, _}

import scala.util.parsing.json.JSONArray
import spray.json.DefaultJsonProtocol
import spray.json.DefaultJsonProtocol._
import spray.json._


/**
  * Created by peter on 30/09/16.  *
  *
  * Reading and writing list of Objects to json
  *
  */

object JsonExample {
  case class ParametersList(items: List[ParametersSamplingPoilog])

  object ParametersListProtocol extends DefaultJsonProtocol {
    implicit val parametersJson:RootJsonFormat[ParametersSamplingPoilog] = jsonFormat10(ParametersSamplingPoilog.apply)
    implicit object friendListJsonFormat extends RootJsonFormat[ParametersList] {
      def read(value: JsValue) = ParametersList(value.convertTo[List[ParametersSamplingPoilog]])
      override def write(f: ParametersList) = {
        JsArray(f.items.map(_.toJson).toVector)//serializationError("not supported") //f.items.toJson
      }
    }
  }

  def main(args: Array[String]): Unit = {
    import ParametersListProtocol._
    val parameters = ParametersSamplingPoilog(S=100)

    val lParamsTest = List(parameters,parameters)
    val jsonStrt = lParamsTest.toJson.prettyPrint
    println(jsonStrt)
    val jsonStrt02:JsonNode = Json.parse(jsonStrt)
    println("")
    println(s"parsed back")



    val mapper:ObjectMapper  = new ObjectMapper();
    val a:String = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(jsonStrt02)

    val input = a.mkString.parseJson
    val paramListNew = input.convertTo[ParametersList]



    println("Original:\n"+a)

    println("Converted and parsed:\n"+paramListNew.toJson.prettyPrint)

    println("\nConverted and parsed compact:\n"+paramListNew.toJson.compactPrint)

    println("-------------------")
  }

}
