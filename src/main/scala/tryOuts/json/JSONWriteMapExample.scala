package tryOuts.json

import play.api.libs.json._

import scala.util.parsing.json.JSONArray
import spray.json.DefaultJsonProtocol
import spray.json._

/**
  * Created by peter on 24/10/16.
  */
object JSONWriteMapExample {

  case class Person(name: String, age: String)

  object PersonProtocol extends DefaultJsonProtocol {
    implicit val personFormat = jsonFormat2(Person)
  }

  def main(args: Array[String]): Unit = {
    val mainJSON: String =
      """
        [{
            "name": "John",
            "age": "30"
        },
        {
            "name": "Tom",
            "age": "25"
        }]
      """

    val res = Json.parse(mainJSON)
    //    val jsonarray = new JSONArray(res.asInstanceOf[List[Person]])
    println("dfd")
  }

}
