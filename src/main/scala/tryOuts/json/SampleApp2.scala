package tryOuts.json

import com.fasterxml.jackson.databind.{JsonNode, ObjectMapper}
import play.libs.Json
import spray.json._
import tryOuts.json.SampleApp2.MyName


/**
  * Created by peter on 24/10/16.
  */
object SampleApp2 {

  def main(args: Array[String]): Unit = {
    import MyJsonProtocol._
    val inputJson = """{"children":[{"name":"a"}, {"name":"b"}, {"name":"c"}]}"""
    val inputJson2 = """[{"name":"a"}, {"name":"b"}, {"name":"c"}]"""


    val myObject = inputJson.parseJson.convertTo[MyCaseClass]
    //    val myObject2 = inputJson2.parseJson.convertTo[List[MyName]]
    println(s"Deserialise: ${myObject}")
    val jsonStrt = myObject.toJson.prettyPrint
    println(s"JSonPretty print:\n ${jsonStrt}")
    println("")


    val jsonStrt02: JsonNode = Json.parse(jsonStrt)
    val mapper: ObjectMapper = new ObjectMapper();
    val a = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(jsonStrt02)
    println("Reconstructed:\n" + a)
    println("")

    //    val jsonStrt = lParamsTest.toJson.prettyPrint
    //    println(jsonStrt)
    //    val jsonStrt02:JsonNode = Json.parse(jsonStrt)
    //    println("")
    //    println(s"parsed back")
    //
    //
    //    val mapper:ObjectMapper  = new ObjectMapper();
    //    val a = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(jsonStrt02)
    //    println(a)


    //    val jsonStr = .toJson.prettyPrint
    //    val p: PrintWriter = new PrintWriter(jsonCfgFilePath)
    //    p.write(jsonStr)
    //    p.close()


  }

  case class MyName(name: String)

  case class MyCaseClass(children: List[MyName])

  object MyJsonProtocol extends DefaultJsonProtocol {
    //Explicit type added here now
    implicit val MyNameSchemaFormat: JsonFormat[MyName] = jsonFormat1(MyName)
    implicit val myCaseClassSchemaFormat: JsonFormat[MyCaseClass] = jsonFormat1(MyCaseClass)
    //    implicit val listOfChildrenSchemaFormat: JsonFormat[List[MyName]] = jsonFormat1(List[MyName])


  }

  //
  //  object ListOfChildren{
  //    implicit val listOfChildrenSchemaFormat: JsonFormat[List[MyName]] = jsonFormat1(List[MyName])
  //  }

}

