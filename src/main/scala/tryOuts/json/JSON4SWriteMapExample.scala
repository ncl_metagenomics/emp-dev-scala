package tryOuts.json

/**
  * Created by peter on 06/03/17.
  */
object JSON4SWriteMapExample {

  import org.json4s._
  import org.json4s.native.Serialization._
  import org.json4s.native.Serialization

  implicit val formats = Serialization.formats(NoTypeHints)

  val m = Map(
    "name" -> "john doe",
    "age" -> 18,
    "hasChild" -> true,
    "childs" -> List(
      Map("name" -> "dorothy", "age" -> 5, "hasChild" -> false),
      Map("name" -> "bill", "age" -> 8, "hasChild" -> false)))


  def main(args: Array[String]): Unit = {
    val a = writePretty(m)
    println(a)

  }

}
