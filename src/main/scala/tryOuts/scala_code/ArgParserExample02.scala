package tryOuts.scala_code

object ArgParserExample02 {
  def parseOptions(args: List[String], required: List[Symbol], optional: Map[String, Symbol], options: Map[Symbol, String]): Map[Symbol, String] = {
    args match {
      // Empty list
      case Nil => options

      // Keyword arguments
      case key :: value :: tail if optional.get(key) != None =>
        parseOptions(tail, required, optional, options ++ Map(optional(key) -> value))

      // Positional arguments
      case value :: tail if required != Nil =>
        parseOptions(tail, required.tail, optional, options ++ Map(required.head -> value))

      // Exit if an unknown argument is received
      case _ =>
        printf("unknown argument(s): %s\n", args.mkString(", "))
        sys.exit(1)
    }
  }

  def main(sysargs: Array[String]) {
    // Required positional arguments by key in options
    val required = List('arg1, 'arg2)

    // Optional arguments by flag which map to a key in options
    val optional = Map("--flag1" -> 'flag1, "--flag2" -> 'flag2)

    // Default options that are passed in
    var defaultOptions:Map[Symbol, String] = Map('flag2->"1000")

    // Parse options based on the command line args
    val options = parseOptions(sysargs.toList, required, optional, defaultOptions)
    println("'flag1 = "+ options('flag1) )
    println("'flag2 = "+ options('flag2) )
    println("'arg1 = "+ options('arg1) )
    println("'arg2 = "+ options('arg2) )
    println("*********************************************************" )
//    println("s = "+ options(s )
//    println("showme=" + showme)
//    println("filename=" + filename)
//    println("remainingopts=" + remainingopts)
  }
  
}

//"MetroLogNormal - fits a compound Poisson Log-Normal distn to a sample\n",
//                        "Required parameters:\n",
//			"   -out  filestub      output file stub\n",
//			"   -in   filename      parameter file  \n",
//                        "Optional:\n",
//			"   -s    integer       generate integer MCMC samples\n",
//			"   -seed   long        seed random number generator\n",
//			"   -sigmaX float       std. dev. of X prop. distn\n",
//			"   -sigmaY float       ...          Y             \n",
//			"   -sigmaS float       ...          S             \n",
//			"   -v                  verbose\n"