package tryOuts.scala_code.integration

import org.apache.commons.math3.analysis.integration._
import org.apache.commons.math3.analysis._

object ApacheIntegrationExample extends App { 
  var n=2
  var relativeAccuracy= 0.1
  var absoluteAccuracy=0.1
  var minimalIterationCount = 100
  var maximalIterationCount = 500
  var lgeCalc = new IterativeLegendreGaussIntegrator(n,relativeAccuracy, absoluteAccuracy, minimalIterationCount, maximalIterationCount)
  var  maxEval = 30000
  var  lower = 0
  var upper = 10
  
  class MyFnc extends UnivariateFunction{
      def  value (x:Double):Double={
           x
      }     
  }
  
  var mf = new MyFnc
  
  
  var resLGE = lgeCalc.integrate(maxEval, mf, lower, upper)
  println("resLGE "+ resLGE)
}