package tryOuts.scala_code.cmdlinepars

object CMDLineParsSimple {
  def main(args: Array[String]): Unit = {
    println("Nothing yet!!!")
       
    
    def parseArgs(args: Array[String]):Map[Symbol, Any] = {
      val usage = """
						Usage: parser [-v] [-f file] [-s sopt] ...
						Where: 
              Required parameters:\n"
                      -in "data/Brazil.sample" 
                      -out "BrazilCodeLiteBrazil.sample.out"
              Optional: 
                      -s integer generate integer MCMC samples
						"""
      if (args.length == 0) println(usage)
      val arglist = args.toList
      type OptionMap = Map[Symbol, Any]

      def nextOption(map: OptionMap, list: List[String]): OptionMap = {
        def isSwitch(s: String) = (s(0) == '-')
        list match {
          case Nil => map
          case "-in" :: value :: tail =>
            nextOption(map ++ Map('in -> value), tail)
          case "-out" :: value :: tail =>
            nextOption(map ++ Map('out -> value), tail)
          case "-s" :: value :: tail =>
            nextOption(map ++ Map('s -> value.toInt), tail)
          case string :: Nil => nextOption(map ++ Map('infile -> string), list.tail)
          case option :: tail =>
            println("Unknown option " + option)
            sys.exit(1)
        }
      }
      val options = nextOption(Map(), args.toList)
      // println(options)   
      options
    }
    
    var res = parseArgs(args)
    println(res)    
  }
}