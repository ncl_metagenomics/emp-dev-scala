package tryOuts.scala_code.cmdlinepars

object CMDLineParsSimple01 {

  def main(sysargs: Array[String]): Unit = {
    /**
     * Called itertively
     */
    def parseOptions(args: List[String], required: List[Symbol], optional: Map[String, Symbol], defaultOptions: Map[Symbol, Any], iterations: Int): Map[Symbol, Any] = {
      val usage = """
						Usage: parser [-v] [-f file] [-s sopt] ...
						Where: 
						Required parameters:\n"
						-in input file name 
						-out output file name
						Optional: 
						-s integer generate  MCMC samples
						"""
      if (args.length == 0 && iterations < 1) println(usage)
      args match {
        // Empty list
        case Nil => defaultOptions

        // Keyword arguments
        case "-s" :: value :: tail =>
          parseOptions(tail, required, optional, defaultOptions ++ Map(optional("-s") -> value.toInt), iterations + 1)
        case key :: value :: tail if optional.get(key) != None =>
          parseOptions(tail, required, optional, defaultOptions ++ Map(optional(key) -> value), iterations + 1)

        // Positional arguments
        case value :: tail if required != Nil =>
          parseOptions(tail, required.tail, optional, defaultOptions ++ Map(required.head -> value), iterations + 1)

        // Exit if an unknown argument is received
        case _ =>
          printf("unknown argument(s): %s\n", args.mkString(", "))
          sys.exit(1)
      }
    }

    // Required positional arguments by key in options
    val required = List('arg1, 'arg2)

    // Optional arguments by flag which map to a key in options
    val optional = Map("-in" -> 'in, "-out" -> 'out, "-s" -> 's)

    // Default options that are passed in
    val defaultOptions: Map[Symbol, Any] = Map('s02 -> 100)

    // Parse options based on the command line args
    val options = parseOptions(sysargs.toList, required, optional, defaultOptions, 0)
    //val options = parseOptions(Nil, required, optional, defaultOptions,0)  

    println(options)
  }

}