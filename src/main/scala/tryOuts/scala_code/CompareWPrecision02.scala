package tryOuts.scala_code

import scala.language.implicitConversions

object CompareWPrecision02 extends App {
  case class Precision(val p: Double)
  class withAlmostEquals(d: Double) {
    def ~=(d2: Double)(implicit p: Precision) = (d - d2).abs <= p.p
  }
  
  implicit def add_~=(d:Double) = new withAlmostEquals(d)
  
  // Necessary !!!
  implicit val precision = Precision(0.001)
  val res = 0.0~=0.0
  println("0.0 ~= 0.00001 = "+ res)
}