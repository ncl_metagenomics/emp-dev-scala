package tryOuts.scala_code

import java.text.SimpleDateFormat
import java.util.Calendar

object DateToStringEx extends App{
    val today = Calendar.getInstance().getTime()
    // (2) create a date "formatter" (the date format we want)
    val formatter = new SimpleDateFormat("yyyy-MM-dd-hh.mm.ss")
 
    // (3) create a new String using the date format we want
    val  folderName = formatter.format(today)
    println(folderName)
    
}