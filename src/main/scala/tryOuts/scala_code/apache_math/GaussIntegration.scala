package tryOuts.scala_code.apache_math

import org.apache.commons.math3.analysis.UnivariateFunction;
import org.apache.commons.math3.analysis.function.Cos;
import org.apache.commons.math3.analysis.integration.gauss.GaussIntegratorFactory;
import org.apache.commons.math3.analysis.integration.gauss.GaussIntegrator;

object GaussIntegration {

  def main(args: Array[String]):Unit = {
    val factory: GaussIntegratorFactory = new GaussIntegratorFactory();
    val cos: UnivariateFunction = new Cos();
    // create an Gauss integrator for the interval [0, PI/2]
    val integrator: GaussIntegrator = factory.legendre(7, 0, 0.5 * Math.PI);
    val s: Double = integrator.integrate(cos);
    print(s"Integral of cos from  0 to 0.5*Pi is $s")
  }

}