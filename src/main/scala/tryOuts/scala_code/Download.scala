package tryOuts.scala_code

object Download{
  object Type extends Enumeration {
    type Type = Value
    val Image = "1"
    val Video = "2"
    val Pdf = "3"
    val File = "4"
  }
}

object Strategies {
  type Strategy[T, O] = T => O

def imageDownload[T](): Strategy[T, java.io.File] =
    (t: T) => {
      //Receive download strategy information
      val dw = t.asInstanceOf[DownloadStrategy]
      //juicy code goes here
      java.io.File.createTempFile("", "")
    }

  def videoDownload[T](): Strategy[T, java.io.File] =
    (t: T) =>
      java.io.File.createTempFile("", "")

  def rawFileDownload[T](): Strategy[T, java.io.File] =
    (t: T) =>
      java.io.File.createTempFile("", "")

  //this is the fallback default
  def download[T](): Strategy[T, java.io.File] =
    (t: T) => {
      java.io.File.createTempFile("", "")
    }

  //a method to select the strategy to use
  def selectStrategy[T](selector: String): Strategy[T, java.io.File] =
    selector match {
      case Download.Type.Image => {
        imageDownload()
      }
      case Download.Type.Video => {
        videoDownload()
      }
      case Download.Type.Pdf => {
        rawFileDownload()
      }
      case Download.Type.File => {
        rawFileDownload()
      }
      case _ => download()
    }
  
  
//  def download(path: String) = Action {
//    implicit request =>
//      val file: Option[File] = FileStore.byPath(path, true)
//      val ds = DownloadStrategy(request, path, file)
//      //request.getQueryString("t") - Download type
//      val str = Strategies.selectStrategy[DownloadStrategy](request.getQueryString("t").getOrElse(""))
//      val x = str(ds)
////      Ok.sendFile(
////        content = x
////      )
//  }
  
}

class Action
class Request[T]
class AnyContent
class File

case class DownloadStrategy(request: Request[AnyContent], path: String, file: Option[File]) {

}

//Controller code
