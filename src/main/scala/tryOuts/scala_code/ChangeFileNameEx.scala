package tryOuts.scala_code

import java.text.SimpleDateFormat
import java.util.Calendar
import java.io.{File=>JavaFile}

object ChangeFileNameEx extends App {
  val ds = getDateAsString("yyMMdd-hhmmss")
  val fileName = "content/topic/subtopic/post.md"
  println(ds)
  val fN:String = appendString2FileName("content/topic/subtopic/post.md", ds)
  println(s"New file name is $fN")

  def getDateAsString(formatStr: String): String = {
    var today = Calendar.getInstance().getTime();
    var formatter = new SimpleDateFormat(formatStr);
    formatter.format(today);
  }

  def appendString2FileName(filenName: String, str2Append: String): String = {
    val f = new java.io.File(filenName)
    val parentDir = f.getParent
    println(s"*** Parts of parent file name: $parentDir")
    
    val newshortName:String = f.getName+"_"+str2Append+".out"
    new JavaFile(parentDir,newshortName).getPath
  }
}