package tryOuts.scala_code

object TupleTest {
  def main(args:Array[String])= {
    var (a, b, c, d):(Integer,Integer,Double,String) = (1, 2, 3.14, "Peter")
    println(s"a = ${a}, d=$d")
  }
}