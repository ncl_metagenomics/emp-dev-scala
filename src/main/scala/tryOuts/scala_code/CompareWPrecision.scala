package tryOuts.scala_code

object CompareWPrecision extends App {
  case class Precision(val p: Double)

  implicit class DoubleWithAlmostEquals(val d: Double) extends AnyVal {
    def ~=(d2: Double)(implicit p: Precision) = (d - d2).abs < p.p
  }
  
  implicit val precision = Precision(0.001)
  val res = 0.0 ~= 0.00001
  println("0.0 ~= 0.00001 = "+ res)
}