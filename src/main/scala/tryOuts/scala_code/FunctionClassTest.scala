package tryOuts.scala_code

/**
 * @author peter
 */
object FunctionClassTest extends App{
  val isPythTripple2: (Int, Int, Int) => Boolean = new Function3[Int, Int, Int, Boolean] {
    def apply(a: Int, b: Int, c: Int): Boolean = {
      val aSquare = a * a
      val bSquare = b * b
      val cSquare = c * c

      aSquare + bSquare == cSquare
    }
  }
  println("Result ",isPythTripple2(1,2,3))
}

