package tryOuts.scala_code

object SimpleMovingDifferenceEx {
  def main(args:Array[String])= {
    val period = 2
    var numbers = List(2.0, 4.0, 7.0, 6.0, 3.0, 8.0, 12.0, 9.0, 4.0, 1.0)
    val mdiff = movingDiff(numbers, period)
    println("mdiff = " + mdiff.mkString("<", ",", ">"))
  }
  def movingDiff(values: List[Double], period: Int): List[Double] = {
    period match {
      case 0 => throw new IllegalArgumentException
      case 1 => values
      case 2 => values.sliding(2).toList.map(_.sum)
      case _ => sys.error("period has to by <3 now")
    }
  }
}