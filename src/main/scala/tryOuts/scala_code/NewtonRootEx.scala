package tryOuts.scala_code
import scala.math.Pi
/**
 * @author peter
 */
object NewtonRootEx extends App{
  // Returns an approximation to the fixed-point of the given function f
  // (using the given first_guess to get us started.)
  def fixedPoint(f: Double => Double, firstGuess: Double): Double = {
    val tolerance = 0.0001
    val maxIterations = 100
    var guess = firstGuess
    for (i <- 1 to maxIterations) {
      if (Math.abs(guess - f(guess)) < tolerance) {
        return guess
      }

      guess = f(guess)
      //println(guess)
    }

    /* We return NaN if we didn't converge in time. */
    return Double.NaN
  }
  
  var res = fixedPoint(Math.cos, Pi/3)
  println("Cos(30) Result ", res)
  
  res = fixedPoint(Math.sin, Pi/6)
  println("Sin(60) Result ", res)
}
