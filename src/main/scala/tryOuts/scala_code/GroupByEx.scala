package tryOuts.scala_code

/**
 *
 */
object GroupByEx extends App {

  val array = Array.fill(2, 2)(0)
  println(array.deep.mkString("\n"))
  //  println(stringOf(array))
  //stringOf(array)
  // a.zip(0 until a.size), but zipWithIndex is easier. 

  //Scala groupBy: Want array indices satisfying predicate, not array values
  val m = Array(10, 20, 30, 30, 50, 60, 70, 80).zipWithIndex.groupBy(s =>
    s._1 % 30 == 0).map(e => e._1 -> (e._2.unzip._2))

  println(s"Indices of values divisible by 30 ${m.mkString(" ")}")  
  m.foreach({case(tV,num)=>println(tV + " " + num.mkString(", ") ) })
  
  m.foreach((num)=>println(num._1 + " " + num._2.mkString(", ") ) )
  // only want the true values
  val m02 = Array(10, 20, 30, 30, 50, 60, 70, 80).zipWithIndex.partition(s =>
    s._1 % 30 == 0)._1.unzip._2
  println(s"Values divisible by 30 ${m02.mkString(" ")}")
  // println(a.mkString(" ")) 
  
  println("m02 = " + m02.mkString(" and "))
  println("m02 = " + m02.mkString("<", ",", ">"))
  //mkString("<", ",", ">")
  val m03 = Array(10,20,30,30,50,60,70,80).zipWithIndex.filter{ _._1 % 30 == 0 }.map{ _._2 }
  println("m03 = "+m03.mkString("<", ",", ">"))
  
  val m04 = Array(10, 20, 30, 30, 50, 60, 70, 80).zipWithIndex.collect {case (element, index) if element % 30 == 0 => index }
  println("m04 = "+m04.mkString("<", ",", ">"))
  
  val m05 = Array(10,20,30,30,50,60,70,80).filter{ _ % 30 == 0 }
  println("m05 = "+m05.mkString("<", ",", ">"))
  
  val m06 = Array(10, 20, 30, 30, 50, 60, 70, 80).zipWithIndex.collect {case (element, index) if element % 30 == 0 => element }
  println("m06 = "+m06.mkString("<", ",", ">"))
  
  val m07 = Array(10, 20, 30, 30, 50, 60, 70, 80).collect {case element if element % 30 == 0 => element }
  println("m07 = "+m07.mkString("<", ",", ">"))
  
  val m08 = Array(10, 20, 30, 30, 50, 60, 70, 80).filter {element => element % 30 == 0}
  println("m08 = "+m08.mkString("<", ",", ">"))
  
  val m09 = Array(10, 20, 30, 30, 50, 60, 70, 80).filter {element => true}
  println("m09 = "+m09.mkString("<", ",", ">"))
  
  val m10 = Array(10, 20, 30, 30, 50, 60, 70, 80).collect {case element if element % 30 == 0 => element }
  println("m10 = " + m10.mkString("<", ",", ">"))
  
  val m11 = Array(10, 20, 30, 30, 50, 60, 70, 80).filter {_ % 30 == 0}
  println("m11 = " + m11.mkString("<", ",", ">"))
  
}