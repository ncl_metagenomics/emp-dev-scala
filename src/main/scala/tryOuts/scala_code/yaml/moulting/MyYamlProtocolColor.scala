package tryOuts.scala_code.yaml.moulting


import net.jcazevedo.moultingyaml._
import scala.reflect.runtime.universe

case class Color(name: String, red: Int, green: Int, blue: Int)

object MyYamlProtocolForColor extends DefaultYamlProtocol {
  implicit val colorFormat = yamlFormat4(Color)
  
}
