package tryOuts.scala_code.yaml.moulting

import net.jcazevedo.moultingyaml._
import MyYamlProtocolForColor._
//import examples.scala_code.yaml.moulting.Color

import java.io.FileWriter

import scala.reflect.io.Path
import java.io.BufferedWriter

object MoultingYAML01 extends App {
  val fileOut = "src/main/scala/examples/scala_code/yaml/moulting/outputMoultingYAML01_A.yaml"
  val fileOut2 = "src/main/scala/examples/scala_code/yaml/moulting/outputMoultingYAML01_B.yaml"
  //    val output:Output = Resource.fromFile(fileOut)
  //    val fOps : FileOps = Path (fileOut2)
  val fw = new FileWriter(fileOut)
  val bw = new BufferedWriter(new FileWriter(fileOut2))
  val yaml = Color("CadetBlue", 95, 158, 160).toYaml
  val color = yaml.convertTo[Color]
  val c = yaml.asInstanceOf[Color]
  val yaml2  = c.toYaml
  
  
  
  println(yaml.prettyPrint)
  fw.write(yaml.prettyPrint)
  bw.write(yaml.prettyPrint)
  fw.close()
  bw.close()
}

