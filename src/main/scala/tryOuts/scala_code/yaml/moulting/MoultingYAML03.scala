package tryOuts.scala_code.yaml.moulting

import net.jcazevedo.moultingyaml._
import net.jcazevedo.moultingyaml.DefaultYamlProtocol._
import scala.io.Source

object MoultingYAML03 extends App {
  
  case class Service(val name: String,
                     val image: Option[Double] = None,
                     val containerName: Option[Double] = None,
                     val command: Option[Double] = None,
                     val environment: Option[Double] = None,
                     val volumes: Option[String] = None,
                     val ports: Option[String] = None)

  val file = "/media/sf_Dropbox/Newcastle-Project/src/Projects/Quince-C/Data-synthetic/sads4sampleSize/" +
    "params_sampleSize_Distribution_coverage_90_pc_nD_2430_syntheticSampleSizes_logNormal_2016-04-04_01-06-29_b.yaml"
  val yamls0 = Source.fromFile(file).mkString.parseYaml
  yamls0.prettyPrint
  println(yamls0)
  println(yamls0.prettyPrint)
  //val myMap = yamls0.convertTo[Params]

  // val a = yamls0.convertTo[Service]
  val yamlAst = List(1, 2, 3).toYaml
  val myList = yamlAst.convertTo[List[Int]]
  
   println(yamls0)
  // val yamls = ParseY
  //  forall(yamls) {
  //    innerYaml =>
  //      innerYaml.prettyPrint.parseYaml mustEqual innerYaml
  //  }
}
