package tryOuts.scala_code.yaml.moulting
import net.jcazevedo.moultingyaml._
import scala.reflect.runtime.universe

case class Params( meanLog: Option[Double],
                   sdLog: Option[Double],
                   coverage: Option[Double],
                   varLog: Option[Double],
                   S: Option[Double])

object MyYamlParamsProtocol extends DefaultYamlProtocol {
  implicit val paramsFormat = yamlFormat5(Params)
}