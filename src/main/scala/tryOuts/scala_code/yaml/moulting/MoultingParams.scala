package tryOuts.scala_code.yaml.moulting

import java.io._
import scala.io.Source
import net.jcazevedo.moultingyaml._
import MyYamlProtocolForColor._

object MoultingParams extends App{
  val file = "/media/sf_Dropbox/Newcastle-Project/src/Projects/Quince-C/Data-synthetic/sads4sampleSize/" +
    "params_sampleSize_Distribution_coverage_90_pc_nD_2430_syntheticSampleSizes_logNormal_2016-04-04_01-06-29_b.yaml"
  val yamls0 = Source.fromFile(file).mkString.parseYaml
  println(yamls0.prettyPrint)
  
  
  
  val fileOut = "src/main/scala/examples/scala_code/yaml/moulting/outputMoultingYAML01_A.yaml"
  val fileOut2 = "src/main/scala/examples/scala_code/yaml/moulting/outputMoultingYAML01_B.yaml"
  //    val output:Output = Resource.fromFile(fileOut)
  //    val fOps : FileOps = Path (fileOut2)
  
  val bw = new BufferedWriter(new FileWriter(fileOut2))
 
  val yaml = Color("CadetBlue", 95, 158, 160).toYaml  
  println(yaml.prettyPrint)
  bw.write(yaml.prettyPrint)
  val color = yaml.convertTo[Color]  
  val c = yaml.asInstanceOf[Color]
  
  bw.close()
}