package tryOuts.scala_code.yaml.jackson

import java.io.FileReader
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory
import com.fasterxml.jackson.databind.ObjectMapper
import java.io.FileOutputStream

object ApplySample extends App {
  val reader = new FileReader("src/main/scala/examples/scala_code/yaml/jackson/sample.yaml")
  ///media/sf_data/nps409/Work2Sync/Projects/emp_dev_scala/src/main/scala/examples/scala_code/yaml/jackson/sample.yaml
  val mapper = new ObjectMapper(new YAMLFactory())
  val config: Sample = mapper.readValue(reader, classOf[Sample])
  val yamlFileOut = "src/main/scala/examples/scala_code/yaml/jackson/sample_out.yaml"
  val fos:FileOutputStream  = new FileOutputStream(yamlFileOut);
 
  
  val yamlFactory = new YAMLFactory 
  val gen = yamlFactory.createGenerator(fos)
  gen.setCodec(new ObjectMapper) 
  gen.writeObject(config)
  
  println("Koniec")
}