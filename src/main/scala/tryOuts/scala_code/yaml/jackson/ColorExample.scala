package tryOuts.scala_code.yaml.jackson
import java.util.{ List => JList, Map => JMap }
import collection.JavaConversions._
import com.fasterxml.jackson._
import com.fasterxml.jackson.annotation._
import com.fasterxml.jackson.dataformat._
import com.fasterxml.jackson.databind._
import com.google.common.base.Preconditions
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory
import com.sun.org.apache.xalan.internal.xsltc.trax.OutputSettings
import java.io.FileOutputStream

object ColorExample extends App {
  case class Color(
      @JsonProperty("name") _name: String,
      @JsonProperty("red") _red: Int,
      @JsonProperty("green") _greeen: Int,
      @JsonProperty("blue") _blue: Int
  )
      // name: String, red: Int, green: Int, blue: Int)
  val color2 = Color("green",10, 12,22)
  val yamlFileOut = "src/main/scala/examples/scala_code/yaml/jackson/jackson_color_example_out.yaml"
  // /src/main/scala/examples/scala_code/yaml/jackson/ColorExample.scala
  val fos:FileOutputStream  = new FileOutputStream(yamlFileOut);
  val os =  new OutputSettings()
  val yamlFactory = new YAMLFactory 
  val gen = yamlFactory.createGenerator(fos);
  gen.setCodec(new ObjectMapper) 
  gen.writeObject(color2)
  
}