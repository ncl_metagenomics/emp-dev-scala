package tryOuts.scala_code.yaml.jackson

import java.util.{ List => JList, Map => JMap }
import collection.JavaConversions._
import com.fasterxml.jackson._
import com.fasterxml.jackson.annotation._
import com.fasterxml.jackson.dataformat._
import com.fasterxml.jackson.databind._
import com.google.common.base.Preconditions
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory
import com.sun.org.apache.xalan.internal.xsltc.trax.OutputSettings
import java.io.FileOutputStream
import com.fasterxml.jackson.databind.node.ObjectNode
import com.fasterxml.jackson.core.JsonParser

object Gabor extends App{
  case class Color(
      @JsonProperty("name") _name: String,
      @JsonProperty("red") _red: Int,
      @JsonProperty("green") _greeen: Int,
      @JsonProperty("blue") _blue: Int
  )
      // name: String, red: Int, green: Int, blue: Int)
  val color2 = Color("green",10, 12,22)
  
  val yamlFileIn = "/media/sf_Dropbox/Newcastle-Project/src/Projects/Quince-C/Data-synthetic/sads4sampleSize/params_sampleSize_Distribution_coverage_90_pc_nD_2430_syntheticSampleSizes_logNormal_2016-04-04_01-06-29_b.yaml"
  val yf: YAMLFactory = new YAMLFactory()
  val mapper: ObjectMapper = new ObjectMapper(yf)

  
  val yamlFactory = new YAMLFactory 
 // val gen = yamlFactory.createGenerator(fos)
  
  // val root = mapper.readTree(yamlFactory.createParser(yamlFileIn))
  // modify root here     
  val yamlFileOut = "params_sampleSize_Distribution_coverage_90_pc_nD_2430_syntheticSampleSizes_logNormal_2016-04-04_01-06-29_b.yaml"
  val fos: FileOutputStream = new FileOutputStream(yamlFileOut);
  yf.createGenerator(fos).writeObject(color2); // works. yay.
}