package tryOuts.scala_code.yaml.jackson

import java.util.{ List => JList, Map => JMap }
import collection.JavaConversions._
import com.fasterxml.jackson._
import com.fasterxml.jackson.annotation._
import com.fasterxml.jackson.dataformat._
import com.fasterxml.jackson.databind._
import com.google.common.base.Preconditions


class Sample(@JsonProperty("name") _name: String,
    @JsonProperty("parameters") _parameters: JMap[String, String],
    @JsonProperty("things") _things: JList[Thing]) {

  val name:String = Preconditions.checkNotNull(_name, "name cannot be null","nameVal")
  val parameters: Map[String, String] = Preconditions.checkNotNull(_parameters, "parameters cannot be null",Map("paramVAl"->"sdda")).toMap
  val things: List[Thing] = Preconditions.checkNotNull(_things, "things cannot be null",List("thingVal")).toList
}

class Thing(@JsonProperty("colour") _colour: String,
    @JsonProperty("priority") _priority: Int) {
  val colour = Preconditions.checkNotNull(_colour, "colour cannot be null","blue")
  val priority = Preconditions.checkNotNull(_priority, "priority cannot be null", "0")
}