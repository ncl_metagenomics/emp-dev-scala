package tryOuts.scala_code.yaml.snakeyaml

import org.yaml.snakeyaml.Yaml
import scala.beans.BeanProperty
//import scala.collection.mutable.ListBuffer
//import java.lang.String

object YamlBeanTest3 {

  def main(args: Array[String]) {
    val data = "--- !!examples.scala_code.yaml.snakeyaml.Person2 [ Andrey, Somov, 99 ]"
    val yaml = new Yaml
    val obj = yaml.load(data)
    val person = obj.asInstanceOf[Person2]
    println(person)
  }

}

/**
 * With the approach shown in the main method (load() plus
 * asInstanceOf), this class must declare its properties in the
 * constructor.
 */
// snakeyaml requires properties to be specified in the constructor
// this works also
case class Person2(var firstName: String, var lastName: String, var age: Int) {
  def setFirstName(s: String) {firstName = s} 
  def setLastName(s: String) {lastName = s}
  def setAge(i: Int) {age = i}
}

