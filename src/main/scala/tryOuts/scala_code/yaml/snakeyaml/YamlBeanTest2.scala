package tryOuts.scala_code.yaml.snakeyaml

import java.lang.String
import org.yaml.snakeyaml.Yaml
import scala.beans.BeanProperty
//import scala.collection.mutable.ListBuffer

object YamlBeanTest2 {

  def main(args: Array[String]) {
    val data = "--- !!examples.scala_code.yaml.snakeyaml.Person [ Andrey, Somov, 99 ]"
    val yaml = new Yaml
    val obj = yaml.load(data)
    val person = obj.asInstanceOf[Person]
    println(person)
    println(yaml.toString())
  }

}

/**
 * With the approach shown in the main method (load() plus
 * asInstanceOf), this class must declare its properties in the
 * constructor.
 */
// snakeyaml requires properties to be specified in the constructor
class Person(@BeanProperty var firstName: String = null,
    @BeanProperty var lastName: String = null,
    @BeanProperty var age: Int ) {
  override def toString(): String = {
    return String.format("%s, %s, %d", firstName, lastName, age.asInstanceOf[AnyRef])
  }
}