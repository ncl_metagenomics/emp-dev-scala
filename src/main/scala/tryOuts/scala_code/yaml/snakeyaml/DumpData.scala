package tryOuts.scala_code.yaml.snakeyaml

import scala.collection.mutable.HashMap
import org.yaml.snakeyaml.Yaml
import java.io.FileWriter

object DumpData extends App{
  val data = scala.collection.mutable.Map[String,Any](
      "name" -> "Silenthand Olleander"
      )
   data +=("race" -> "Human")
   val a = Array("","")
   // Array("","ONE_EYE" )
   //data += ("traits" -> a)

   val yaml:Yaml  = new Yaml();
   val writer  = new FileWriter("src/main/scala/examples/scala_code/yaml/snakeyaml/out_p_infos.yaml");
   yaml.dump(data, writer);
   println(yaml.toString())
   println("Koniec")
}