package tryOuts.scala_code

import java.io.File.{separator=>jseparator}

object FilePathParserEx extends App{
  val f = new java.io.File("content/topic/subtopic/post.md")
  println("*** Parts of parent file name: ")
  val a = f.getParent split java.io.File.separator
  a.map { x => println(x) }
  println("----------------------------------------------")
  println("*** Parts of path of file name: ")
  val b = f.getPath split java.io.File.separator
  b.map { x => println(x) }
  println("----------------------------------------------")
  println("*** Parts of name file name: ")
  val c = f.getName split java.io.File.separator
  c.map { x => println(x) }
  println("*** Parts of file name: ")
  val z = c.toList.last split "\\."
  println(z.mkString(","))
  println("-----------")
  z.toList.map { println }
  println("-----------")
  val q = a.mkString(java.io.File.separator)+java.io.File.separator+z.mkString(".")
  println(q)
}