package tryOuts.scala_code
import com.github.tototoshi.csv._
import java.nio.file.{Paths, Files}


object Writing2CSVFile extends App{
  val f = new File()
  val writer = CSVWriter.open("out.csv")
  writer.writeAll(List(List("a", "b", "c"), List("d", "e", "f")))
  writer.writeRow(List("g", "h", "j"))
  writer.close()
  println("File exists " +Files.exists(Paths.get("out.csv")))
}