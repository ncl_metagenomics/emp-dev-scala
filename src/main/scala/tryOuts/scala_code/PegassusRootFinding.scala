package tryOuts.scala_code
import org.apache.commons.math3.analysis.UnivariateFunction
import org.apache.commons.math3.analysis.solvers.PegasusSolver
/**
 * @author peter
 */
object PegassusRootFinding extends App{
  val tolRel = 1e-3
  val tolAbs = 1e-2
  val f: UnivariateFunction = new UnivariateFunction() {
    def value(t: Double): Double = {
      t*t-2*t + 1
    }
  }
  val  rs:PegasusSolver = new PegasusSolver(tolRel,tolAbs)
  var res = rs.solve(100, f, 0.0,10.0,0.6)
  println("root of \"t*t-2t + 1\"= ", res)
}