import breeze.linalg._
import breeze.plot._
import breeze.plot.HistogramBins.fromNumber


object PlotScalaEx {
  def main(args: Array[String]) {
    val l1 = List(1,2,3)
    val l2 = 4 :: l1
    val l3 = l2 map { x => x*x }
    val l4 = l2.map(x => x*x)
    //*****************************************
    def quadratic(a: Double, b: Double, c: Double, x: Double): Double =
      a * x * x + b * x + c         
      
    //------------------------------------------------------
    //    plotFun(x => x * x)
    val f = Figure()
    val p = f.subplot(0)
    val x = linspace(0.0, 1.0)
    p += plot(x, x :^ 2.0)
    p += plot(x, x :^ 3.0, '.')
    p.xlabel = "x axis"
    p.ylabel = "y axis"
    f.saveas("lines.png") // s

    //******************************************
    val p2 = f.subplot(2, 1, 1)
    val n = 1
    val mu = 2.0
    val varianceLnX = 1.0
    val g = breeze.stats.distributions.Gaussian(0, 1)
    p2 += hist(g.sample(100000), 100)
    p2.title = "A normal distribution"
    f.saveas("subplots.png")

  }
}
