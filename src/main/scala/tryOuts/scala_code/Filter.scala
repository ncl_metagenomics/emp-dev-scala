package tryOuts.scala_code

object Filter extends App {
  var xs = Array(10, 20, 30, 30, 50, 60, 70, 80)
  val m03 = Array(10, 20, 30, 30, 50, 60, 70, 80).zipWithIndex.filter {
    _._1 % 30 == 0
  }.map {
    _._2
  }
  println("m03 = " + m03.mkString("<", ",", ">"))

  val m04 = Array(10, 20, 30, 30, 50, 60, 70, 80).zipWithIndex.collect { case (element, index) if element % 30 == 0 => index }
  println("m04 = " + m04.mkString("<", ",", ">"))

  val m05 = Array(10, 20, 30, 30, 50, 60, 70, 80).filter {
    _ % 30 == 0
  }
  println("m05 = " + m05.mkString("<", ",", ">"))

  val m06 = Array(10, 20, 30, 30, 50, 60, 70, 80).zipWithIndex.collect { case (element, index) if element % 30 == 0 => element }
  println("m06 = " + m06.mkString("<", ",", ">"))

  val m10 = Array(10, 20, 30, 30, 50, 60, 70, 80).collect { case element if element % 30 == 0 => element }
  println("m10 = " + m10.mkString("<", ",", ">"))

  val m11 = Array(10, 20, 30, 30, 50, 60, 70, 80).filter {
    _ % 30 == 0
  }
  var f01 = xs flatMap {
    case s: Int => if (s % 30 == 0) Some((s, "test")) else None
    //    case _ => None
  }

  println("f01 = " + f01.mkString("<", ",", ">"))
  var f = xs flatMap {
    case s: Int => if (s % 30 == 0) Some(s) else None
    //    case _ => None
  }
  println("m11 = " + m11.mkString("<", ",", ">"))
  val f02 = xs flatMap {
    case s: Int => {
      if (s % 30 == 0) Some(Map("test" -> s)) else None
    }
    //    case _ => None
  }
  println("f02 = " + f02.mkString("<", ",", ">"))
  val f03 = xs flatMap {
    case s: Int => {
      var c = s * s;
      if (s % 30 == 0) Some(Map(s -> c)) else None
    }
    //    case _ => None
  }
  println("f = " + f.mkString("<", ",", ">"))

  //f01
  //f
  println("f03 = " + f03.mkString("<", ",", ">"))

}