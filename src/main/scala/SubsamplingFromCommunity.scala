import java.io.File

import basics._
import utils.AbundanceConversion._
import utils.AbundanceIO._
import utils.ReadingExperimentParameters
import utils.ReadingExperimentParameters._
import utils.sampling.SamplingPoilog._
import utils.sampling._

import scala.collection.immutable.{Map =>IMap}
import scala.collection.mutable.{Map => MMap}

//import scala.List
//import scala.Predef.Map

import scala.language.postfixOps

/**
  * Created by peter on 11/11/16.
  */
object SubsamplingFromCommunity {


  def processParameters(args: Array[String]): InputParameters4SubsamplingCommmunity = {
    val p = ReadingExperimentParameters.getInputParams4SubsamplingCommunity(args)
    p
  }

  def main(args: Array[String]): Unit = {
    subSample(args)
  }

  /**
    * Subsample with single value of coverage
    *
    * @param args
    */
  def subSample(args: Array[String]): Unit = {
    val params: InputParameters4SubsamplingCommmunity = processParameters(args)
    val fileNameTAD = params.communityFileName //.inputTADFilePath.get
    val tadData0: Option[Array[(Int, Int)]] = readTADDAta(fileNameTAD.getAbsolutePath)
    val tadData: Map[Long, Int] = tadData0 match {
      case Some(tad: Array[(Int, Int)]) => {
        val a0: List[(Int, Int)] = utils.ArrayUtil.toList1[(Int, Int)](tad)
        val a1 = a0.map(e=>(e._1.toLong,e._2))
        val a: IMap[Long, Int] = IMap(a1.toSeq: _*)
        a//.map { case (k: Int, v: Int) => (k.toLong, v) }.toMap
      }

      case None => throw new Exception("")
    }

    val coverages: List[Double] = params.coverage
    //coverages.par.foreach(coverage=>{})
    //val coverage = coverages(0) /* match {
    //      case Some(c) => c
    //      case None => throw new Exception(s"Coverage option not found!")
    //    }*/
    coverages.par.foreach(coverage => {
      val abundance: List[Long] = convertTAD2Abundance(tadData)
      val abundanceSampled: List[Long] = resampleWithoutReplacement(abundance, coverage)
      val tadSampled: Map[Long, Int] = utils.AbundanceConversion.convertAbundance2TADLong(abundanceSampled)
      val fileInfo = (fileNameTAD.getParent, fileNameTAD.getName)

      val outputfileNameOnly = utils.AbundanceIO.outputFileName4SubCommunityData(fileInfo._2, fileInfo._1, coverage, MCMC.DATE_STR)
      val baseNameAndSuffix = splitFileNameToBaseNameAndSuffix(outputfileNameOnly)
      val outputDir = fileNameTAD.getParent + File.separator+baseNameAndSuffix.baseName.get
      (new File(outputDir).mkdirs())

      val outputfilePath = outputDir + File.separator + outputfileNameOnly
      SamplingPoilog.writeHashMapAsCSV(tadSampled, outputfilePath, utils.sampling.Headers4AbundanceData.HeaderTAD.headers)
      scala.Predef.println("Input parameters")
      println(s"input parameters =  ${params.toString}")
      println(s"TAD output file path = $outputfilePath")
      println(s"TAD output dir = ${fileInfo._1}")
    })
  }
}
