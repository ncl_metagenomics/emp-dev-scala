import scopt.OptionParser
import java.io.{File, FilenameFilter}

import ch.qos.logback.core.joran.conditional.ThenOrElseActionBase

import scala.util.matching.Regex
import scopt._

import scala.util.{Failure, Success, Try}

object VarCovarCalculatorRunner {

  case class VarCovarConfig(inDir: String = "", distribution: String = "IG")

  def getOptions(args: Array[String]): Try[VarCovarConfig] = {
    val parser = new scopt.OptionParser[VarCovarConfig]("scopt") {
      head("scopt", "3.x")

      opt[String]('i', "inDir").action((x, c) =>
        c.copy(inDir = x)).text("input Directory")

      opt[String]('d', "distr").action((x, c) =>
        c.copy(distribution = x)).text("distribution used")

      help("help").text("prints this usage text")

    }
    parser.parse(args, VarCovarConfig()) match {
      case Some(config) =>
        print(s"inDir =  ${config.inDir}")
        Success(config)
      // do stuff

      case None => Failure(new Exception())
      // arguments are bad, error m
    }
  }


  def recursiveListFiles(f: File, r: Regex): Array[File] = {
    val these: Array[File] = f.listFiles
    val good = these.filter(f => r.findFirstIn(f.getName).isDefined)
    val result = good ++ these.filter(_.isDirectory).flatMap(recursiveListFiles(_, r))
    result
  }



  def main(args: Array[String]): Unit = {
    val distributions = List("igln" , "ig")
//    val  = distributions(0) //"igln"
    val myOptions = getOptions(args)
    val distribution: String = myOptions.get.distribution
    val r: Regex =
      """^posterior-.*\.csv$""".r
    val inDir = new File(myOptions.get.inDir)
    if (inDir.exists()) {
      println(s"""\nListing all files""")
      val fileList: Array[File] = recursiveListFiles(inDir, r)
      if (fileList.isEmpty) throw new Exception(s"No ${r.toString()} file found in ${myOptions.get.inDir}")
      println(fileList.mkString("sep"))
      println("=============")
      // val res = utils.CovarianceOfPosteriorParams.calcAndSaveCovariance(selectedFile, "IG")
      println(s"Processing ${fileList.length} files")
      val res = fileList.map(f => utils.CovarianceOfPosteriorParams.calcAndSaveCovariance(f.getAbsolutePath, distribution))
      println(s"var-covar_File_name = $res")
      println("============================")
    }
    else throw new Exception(s"Directory ${inDir.getAbsolutePath} does not exist")
  }


}
