import scala.sys.process._

object RunExternalCommands {
  println("Welcome to the Scala worksheet")       //> Welcome to the Scala worksheet
  //val executable = "which ls".lines_!.headOption
  val dirContents = "ls".!!                       //> dirContents  : String = "artifacts.xml
                                                  //| configuration
                                                  //| eclipse
                                                  //| eclipse.ini
                                                  //| epl-v10.html
                                                  //| features
                                                  //| icon.xpm
                                                  //| META-INF
                                                  //| notice.html
                                                  //| p2
                                                  //| plugins
                                                  //| readme
                                                  //| "
                                                  
	Seq("sh", "-c", "tail -f /var/log/syslog > /dev/null &") !
                                                  //> res0: Int = 0
}