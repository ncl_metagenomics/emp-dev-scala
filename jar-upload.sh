#!/bin/sh
# jar-upload.sh
# Make the fat jar and upload to the public web

# Get the latest upload with:
# wget http://www.mas.ncl.ac.uk/~ndjw1/files/emp_dev_scala-assembly-1.0-fat.jar

./sbt fat:assembly

scp target/scala-2.11/emp_dev_scala-assembly-1.0-fat.jar ndjw1@linux.mas.ncl.ac.uk:public_html/files/


# eof


