# README #

# EBI-MP #

Utilities for estimating distribution of taxon abundance distribution and sampling effort based on the framework described in paper:

Quince, Curtis, Sloan (2008) The rational exploration of microbial diversity, ISME, 2, 997--1006.



## Generating jar file ##

 To get jar file "emp_dev_scala_fat-assembly-${version}.jar", e.g. "emp_dev_scala_fat-assembly-1.0.jar", with all libraries

  * __sbt fat:assembly__

 To get jar file "emp_dev_scala-assembly-1.0.jar" that does not contain other libraries


### Typical configuration to  run  "fat" jar  file "emp_dev_scala-assembly-1.0-fat.jar" where TAD input file is "data/DemoData/Brazil.sample", number of iterations is 100,000, and coverage of species is 80%. 

```java  -cp   emp_dev_scala-assembly-1.0-fat.jar     ExperimentScalaRunner  --in "data/DemoData/Brazil.sample"   -s 100000 -c 0.8 ```

* -s 100000 --- number of MCMC
* -c 0.8 --- expected coverage of species is 80% for sampling effort estimation

### Command line options
|Option|Required  |Default             |Description                                         |Other values            |
|:--------|:--------:|--------------------|:---------------------------------------------------|------------------------|
| -\-in    |    Y     |                    |input file name                                     |                        |
| -\-out   |          |input file directory|output directory path                               |                        |
|-\-thinning  -t     | |   10                |thinning interval                                   
|  -c or -\-coverage    |          |   0.9              |is decimal number, required coverage of species = D/S, D - number of species in the sample, S - total number of species|   |
|  -s     |          | 800,000            |integer generate  MCMC samples|                     |                        |
|  -d     |          |  lognormal         |type of taxon abundance distribution (TAD)  used to fit data to|             |



### Input file format ###

+ Taxon abundance distribution data - format recognized based on filename suffix "csv" or "sample"
    + CSV file (*.csv) containing taxon abundance data with header on the first line, where abundance is first column and number of taxa is second column OR

    + "sample" file of abundance without header, where on the first line is number of data points/rows followd by TAD data, where abundance is first column and number of taxa is second column


### Posterior parameters output

* located in input directory by default or specified output directory
* file name: posterior-s-s-all-${input _file_name name}_${time_stamp}.csv|
* e.g. for Brazil.sample TAD data file "posterior-s-s-all-Brazil.sample_161018-2009_40.csv"
#### Columns:
|Column #  |Header	            		     |Description 																					|
|:-------------:|:--------------------------------------|:----------------------------------------------------------------------------------------------------------------------------------------------------|
|1	  	   |nIter					     | Interation number												|
|2		   |mu					    |	Estimated mean of log value ln(X) of lognormally distributed variable X								|
|3		   |variance					|Estimated variance of log value ln(X) of lognormally distributed variable X					|
|4		   |	S						|Estimated number of speciesn in population 									|
|5		   |nLL					|	Negative log-likelihood									|
|6	            |acceptance			|Debugging  info									|
|7	            |n_accepted			|Debugging  info									|





### Sampling effort output 

* located in input directory  by default or specified output directory
* file name: sample_size_posterior-s-s-all-${input _file_name name}_${time_stamp}.csv|
* e.g. for Brazil.sample file and 90% coverage : sample_size_posterior-s-s-all-Brazil.sample_161018-2009_40_correct_Coverage_900_%o_Pegasus_161018-2009_40.csv
#### Columns:
|Column #  |Header	            	|Description 																					|
|:-------------:|:--------------------------------------|:--------------------------------------------------------------------------------------------------------------------------------------------------|
|1                | mu    		                             | Estimated mean of log value ln(X) of lognormally distributed variable X  			              				|
|2                | variance		                     | Estimated variance of log value ln(X) of lognormally distributed variable X							|
|3		   | nL_Recommended                  | Estimated sampling effort																	|
|4		   | nL_Observed			     | Sample size of dataset																		|
|5		   | multipleOfActualSampleSize |	Estimated sampling effort as multiple of current sample											|
|6     	   | coverage    		            | Exact expected fraction of species in sample with estimated sample size nL_Recommended				|
|7	           | mu_org				    | Debugging value     																			|
|8	           | mu_org				    | Debugging value       																		|
|9		   | nL_orgi 				    | Debugging value       																		|





#####  Estimated parameters with no solution for sampling effort
*  located in input directory  by default or specified output directory
* file name: "No_solution_for_sample_size_posterior-s-s-all-Brazil.sample_161018-2009_40_incorrect_coverage_900_%o_Pegasus_161018-2009_40.csv"

[Read me for other usage](ReadMe02.md)