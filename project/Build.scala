import java.text.SimpleDateFormat
import java.util.Calendar

import sbt._
import Keys._


object HelloBuild extends Build {
  final val fString = "yyMMdd-HHmm_ss"

  def getDateAsString(formatStr: String): String = {
    var today = Calendar.getInstance().getTime();
    var formatter = new SimpleDateFormat(formatStr);
    formatter.format(today);
  }

  val dateStr = settingKey[String](getDateAsString(fString))
  val sampleKeyA = settingKey[String]("demo key A")
  val sampleKeyB = settingKey[String]("demo key B")
  val sampleKeyC = settingKey[String]("demo key C")
  val sampleKeyD = settingKey[String]("demo key D")

  override lazy val settings = super.settings ++
    Seq(
      sampleKeyA := "A: in Build.settings in Build.scala",
      dateStr := getDateAsString(fString),
      resolvers := Seq()
    )

  lazy val root = Project(id = "emp_dev_scala",
    base = file("."),
    settings = Seq(
      sampleKeyB := "B: in the root project settings in Build.scala"
    ))
}