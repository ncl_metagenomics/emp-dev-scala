import sbt.Keys._


lazy val hello = taskKey[Unit]("An example task")

lazy val commonSettings = Seq(
  // version := "0.2-SNAPSHOT",
  // organization := "com.example",
  //*****************************


  name := "emp_dev_scala",
  version := "1.0",
  scalaVersion := "2.11.11",
  //scalaVersion := "2.12.3",


  crossScalaVersions := Seq("2.11.8", "2.12.1"),



  scalacOptions ++= Seq("-unchecked", "-deprecation", "-feature"),

  libraryDependencies ++= Seq(
    // other dependencies here
    "org.scalanlp" %% "breeze" % "0.13.2" withSources() withJavadoc(),
    // native libraries are not included
    // native libraries greatly improve performance, but increase jar sizes.
    // It also packages various blas implementations, which have licenses that may or may not
    // be compatible with the Apache License. No GPL code, as best I know.
    "org.scalanlp" %% "breeze-natives" % "0.13.2" withSources() withJavadoc(),
    // the visualization library is distributed separately as well.
    // It depends on LGPL code.
    "org.scalanlp" %% "breeze-viz" % "0.13.2" withSources() withJavadoc(),
    // ***
    "org.scalactic" %% "scalactic" % "3.0.1" withSources() withJavadoc(),
    "org.scalatest" %% "scalatest" % "3.0.1" % "test" withSources() withJavadoc(),
    "org.scalamock" %% "scalamock-scalatest-support" % "3.2.2" % "test" withSources() withJavadoc(),
    "org.scalacheck" %% "scalacheck" % "1.13.4" % "test" withSources() withJavadoc(),
    "org.apache.commons" % "commons-math3" % "3.6.1" withSources() withJavadoc(),
    "com.github.tototoshi" %% "scala-csv" % "1.3.3" withSources() withJavadoc(),
    "com.typesafe.scala-logging" %% "scala-logging" % "3.5.0" withSources() withJavadoc(),
    "ch.qos.logback" % "logback-classic" % "1.1.7" withSources() withJavadoc(),
    "org.codehaus.groovy" % "groovy-all" % "1.8.2" withSources() withJavadoc(),
    "net.jcazevedo" %% "moultingyaml" % "0.3.0" withSources() withJavadoc(),
    "com.fasterxml.jackson.core" % "jackson-core" % "2.7.3" withSources() withJavadoc(),
    "com.google.guava" % "guava" % "19.0" withSources() withJavadoc(),
    "com.fasterxml.jackson.core" % "jackson-annotations" % "2.5.4" withSources() withJavadoc(),
    "com.fasterxml.jackson.core" % "jackson-databind" % "2.7.3" withSources() withJavadoc(),
    "com.fasterxml.jackson.dataformat" % "jackson-dataformat-yaml" % "2.7.3" withSources() withJavadoc(),
    "org.json4s" %% "json4s-native" % "3.5.0" withSources() withJavadoc(),
    // "org.json4s" %% "json4s-jackson" % "3.5.0" withSources() withJavadoc(),
    "com.github.nscala-time" %% "nscala-time" % "2.14.0" withSources() withJavadoc(),
    "com.github.martincooper" %% "scala-datatable" % "0.7.0" withSources() withJavadoc(),
    "com.chuusai" %% "shapeless" % "2.3.2" withSources() withJavadoc(),
    "org.ddahl"  %% "rscala" % "1.0.14"  withSources() withJavadoc(),
    "com.typesafe.play" % "play-json_2.11" % "2.4.2" withSources() withJavadoc(),
    "org.yaml" % "snakeyaml" % "1.9" withSources() withJavadoc(),
    //"org.scalaj" %% "scalaj-time" % "0.7" withSources() withJavadoc(),
    "com.github.fommil" %% "spray-json-shapeless" % "1.3.0" withSources() withJavadoc() ,
    "com.github.scopt" %% "scopt" % "3.6.0" withSources() withJavadoc()
  ),

  resolvers ++= Seq(
    "Sonatype Snapshots" at "https://oss.sonatype.org/content/repositories/snapshots/",
    "Sonatype Releases" at "https://oss.sonatype.org/content/repositories/releases/",
    Resolver.sonatypeRepo("releases"),
    Resolver.sonatypeRepo("snapshots"),
    "Typesafe Repo" at "http://repo.typesafe.com/typesafe/releases/"
  ),

  libraryDependencies += "net.sf.jopt-simple" % "jopt-simple" % "5.0.2" withSources() withJavadoc(),

  // libraryDependencies += "net.java.dev.jna" % "jna" % "4.0.0",
  libraryDependencies += "net.java.dev.jna" % "jna" % "4.5.0",
  libraryDependencies += "com.nativelibs4java" % "jnaerator-runtime" % "0.12",
  libraryDependencies += "com.nativelibs4java" % "jnaerator" % "0.12",

  logLevel in assembly := sbt.Level.Error,
  test in assembly := {}
)

val slim = config("slim") extend (Compile)
val fat = config("fat") extend (Compile)


lazy val emp = (project in file(".")).
  settings(commonSettings: _*).configs(slim, fat).settings(
  // id="fat",
  name := "emp_dev_scala",

  // mainClass in assembly := Some("com.example.Main"),
  assemblyJarName in assembly := name.value + "-assembly-" + version.value+"-deps-"+ dateStr.value   + ".jar",
  // emp_dev_scala-assembly-1.0-deps.jar
  assembleArtifact in assemblyPackageScala := true,
  //logLevel in sbtassembly.Plugin.AssemblyKeys.assembly := Level.Warn,
  test in assembly := {}
).
  settings(inConfig(slim)(Classpaths.configSettings ++ Defaults.configTasks ++ baseAssemblySettings ++ Seq(
    hello := { println("Hello!") },
    assemblyJarName in assembly := name.value + "-assembly-" + version.value+"-"+dateStr.value + ".jar",
    assemblyOption in assembly := (assemblyOption in assembly).value.copy(includeScala = false, includeDependency = false),
    logLevel in assembly := sbt.Level.Error,
    logLevel in sbtassembly.AssemblyKeys.assembly := Level.Error,
    test := {},
    test in assembly := test.value
  )): _*).
  settings(inConfig(fat)(Classpaths.configSettings ++ Defaults.configTasks ++ baseAssemblySettings ++ Seq(
    assemblyJarName in assembly := name.value + "-assembly-" + version.value +"-"+dateStr.value+ "-fat.jar",
    assemblyOption in assembly := (assemblyOption in assembly).value.copy(includeScala = true, includeDependency = true),
    logLevel in assembly := sbt.Level.Info,
    logLevel in sbtassembly.AssemblyKeys.assembly := Level.Info,
    test := {},
    test in assembly := test.value
  )): _*)


assemblyMergeStrategy in assembly := {
  case PathList("META-INF", xs @ _*) => MergeStrategy.discard
  case x => MergeStrategy.first
}
