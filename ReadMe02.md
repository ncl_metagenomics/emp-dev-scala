# Other programs 

[TOC]

#  Syntetics sample preparation

## Sampling community data from poilog TAD (Poisson-lognormal distribution)
- output data file
	- abundance distribution file  *__```44xSRS711891_Abundance_Community_100.0_pc_lognormal_161024-2234_41.csv```__*
		- name construction: 
			- ```${datasetName}_${fileFormat}_${typeOfData}_${Coverage of speciest}_pc_${distribution}_${DateTime}.csv```
		- parts:
			- ```${datasetName}``` -- possible value: 44xSRS711891
			- ```${fileFormat}``` -- Abundance or TAD.
			- ```${typeOfData}``` -- Community or Sample
			- ```${Coverage of speciest}```-- 
			- ```${distribution}``` -- 
			- ```${DateTime}``` -- 

## Sampling from community data

* SamplingPoilog
	* `resampleWithoutReplacement(abundanceIM: List[Long], coverage: Double): List[Long] `
	
	## Reading command line parameters
	
* utils.**ReadingExperimentParameters**

	* ***Work in progress***  ``` getInputParams4SubsamplingCommunity(args: Array[String]): InputParameters4SubsamplingCommmunity```
	
       - ```getInputParams4MCMCAndSamplingEffort```

```java
import collection.JavaConverters._
import scala.collection.breakOut
val l: List[Double] = j.asScala.map(_.doubleValue)(breakOut)
```




